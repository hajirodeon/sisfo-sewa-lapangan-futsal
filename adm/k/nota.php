<?php
session_start();

require("../../inc/config.php");
require("../../inc/fungsi.php");
require("../../inc/koneksi.php");
$tpl = LoadTpl("../../template/kasir.html");


nocache;




//nilai
$judulku = "KASIR : Item Produk";
$judul = $judulku;
$filenya = "nota.php";
$ikd = nosql($_REQUEST['ikd']);
$ikod = nosql($_REQUEST['ikod']);
$inm = balikin($_REQUEST['inm']);
$istn = nosql($_REQUEST['istn']);
$ihrg = nosql($_REQUEST['ihrg']);
$ijml = nosql($_REQUEST['ijml']);
$bookkd = cegah($_REQUEST['bookkd']);
$bookkode = cegah($_REQUEST['bookkode']);
$notakd = $bookkd;
$s = nosql($_REQUEST['s']);
$set = nosql($_REQUEST['set']);
$ke = "$filenya?bookkd=$bookkd&bookkode=$bookkode&notakd=$notakd";


//default jumlah
if ($ikod != "")
	{
	$ijml = "1";
	}


//today
$xtgl1 = nosql($tanggal);
$xbln1 = nosql($bulan);
$xthn1 = nosql($tahun);



//atrribut
if (empty($notakd))
	{
	$attribut = "disabled";
	}


//keydown.
//tombol "CTRL"=17, utk. nota baru
//tombol "HOME"=36, utk. pilih nota
//tombol "END"=35, utk. save & print
//tombol "ESC"=27, utk. keluar
$dikeydown = "var keyCode = event.keyCode;
				if (keyCode == 35)
					{
					if (document.formx.notakdx.value == '')
						{
						alert('Gagal Melakukan Printing. Nota Masih Kosong, atau Nota Belum Dipilih. ');
						}
					else
						{
						location.href='nota_bayar.php?bookkd=$bookkd&bookkode=$bookkode&s=$s&notakd=$notakd';
						}
					}

				if (keyCode == 36)
					{
					open_pilih();
					return false
					}

				if (keyCode == 27)
					{
					parent.ks_window.hide();
					}";













//proses input baru /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//kode
if ($_POST['kode0'])
	{
	//nilai
	$kodeux = nosql($_POST['kodex']);
	$kodeu = substr($kodeux,0,12);
	
	
	$notakd = nosql($_POST['notakdx']);
	$jmlx = nosql($_POST['jmlx']);
	$hrgx = nosql($_POST['hrgx']);
	$stotx = $jmlx * $hrgx;
	$ke = "$filenya?bookkd=$bookkd&bookkode=$bookkode&notakd=$notakd";
	//pekem($kodeu,$ke);




	//cek, barkode-kah...? lebih dari 10 angka, BARCODE ////////////////////////////////////////////////////////////////////////////////
	if ((strlen($kodeu) > 10) AND (is_numeric($kodeu)))
		{
		//cek input
		$qcr = mysqli_query($koneksi, "SELECT * FROM m_brg ".
								"WHERE barkode = '$kodeu'");
		$rcr = mysqli_fetch_assoc($qcr);
		$tcr = mysqli_num_rows($qcr);
		$kodex = nosql($rcr['kode']);
		$brgkd = nosql($rcr['kd']);
		}
	else
		{
		//cek input
		$qcr = mysqli_query($koneksi, "SELECT * FROM m_brg ".
								"WHERE kode = '$kodeu'");
		$rcr = mysqli_fetch_assoc($qcr);
		$tcr = mysqli_num_rows($qcr);
		$kodex = nosql($rcr['kode']);
		$brgkd = nosql($rcr['kd']);
		}


	//nek kode barang tidak ada. atau salah
	if ($tcr == 0)
		{
		//null-kan
		xclose($koneksi);

		//re-direct
		$pesan = "Tidak ada Barang dengan Kode/Barcode : $kodex. Harap Diulangi...!!";
		pekem($pesan,$ke);
		exit();
		}
	else
		{
		//deteksi, jika sudah ada
		$qcc = mysqli_query($koneksi, "SELECT * FROM nota_detail ".
											"WHERE nota_kd = '$notakd' ".
											"AND brg_kd = '$brgkd'");
		$rcc = mysqli_fetch_assoc($qcc);
		$tcc = mysqli_num_rows($qcc);
		$cc_qty = nosql($rcc['qty']);
		$qty_all = $cc_qty + 1;

		if ($tcc != 0) //jika iya
			{
			//jika sudah ada, update jumlah
			//deteksi, (jmlx + qty) > dari stock
			$qcc1 = mysqli_query($koneksi, "SELECT * FROM m_brg ".
												"WHERE kd = '$brgkd'");
			$rcc1 = mysqli_fetch_assoc($qcc1);
			$jml_cc1 = nosql($rcc1['jml_stock']);
			$jml_min = nosql($rcc1['jml_min']);

			//nek (jmlx + qty) lebih...
			if ($qty_all >= $jml_cc1)
				{
				//null-kan
				xclose($koneksi);

				//re-direct
				$pesan = "Jumlah Item Melebihi Jumlah Stock Yang Ada. Harap Dipehatikan...!!";
				pekem($pesan,$ke);
				exit();
				}
			else
				{
				//kurangi stock toko
				//deteksi
				$qdtx = mysqli_query($koneksi, "SELECT * FROM m_brg ".
													"WHERE kd = '$brgkd'");
				$rdtx = mysqli_fetch_assoc($qdtx);
				$dtx_toko = nosql($rdtx['jml_stock']);
				$dtx_hrg = nosql($rdtx['hrg_jual']);
				$dtx_stotx = $dtx_hrg * $qty_all;

				//nek mencukupi
				if ($dtx_toko > $qty_all)
					{
					$s_toko = $qty_all;

					mysqli_query($koneksi, "UPDATE m_brg ".
												"SET jml_stock = jml_stock - '$s_toko' ".
												"WHERE kd = '$brgkd'");
					}
				else if ($dtx_toko < $qty_all)
					{
					$s_toko =  $qty_all; //sisa utk toko

					//update toko
					mysqli_query($koneksi, "UPDATE m_brg ".
												"SET jml_stock = jml_stock - '$s_toko' ".
												"WHERE kd = '$brgkd'");

					}

				//ke detail....
				//update
				mysqli_query($koneksi, "UPDATE nota_detail SET qty = '$qty_all', ".
										"subtotal = '$dtx_stotx', ".
										"postdate = '$today' ".
										"WHERE nota_kd = '$notakd' ".
										"AND brg_kd = '$brgkd'");

				//null-kan
				xclose($koneksi);

				//re-direct
				xloc($ke);
				exit();
				}
			}
		else //jika tidak
			{
			//jika jumlah di-input
			if (empty($jmlx))
				{
				//nama item
				$qnm = mysqli_query($koneksi, "SELECT * FROM m_brg ".
												"WHERE kd = '$brgkd'");
				$rnm = mysqli_fetch_assoc($qnm);
				$inm = urlencode(cegah($rnm['nama']));
				$istn = nosql($rnm['satuan']);
				$ihrg = nosql($rnm['hrg_jual']);

				//null-kan
				xclose($koneksi);

				//re-direct
				$ke = "$filenya?bookkd=$bookkd&bookkode=$bookkode&notakd=$notakd&ikod=$kodex&inm=$inm&istn=$istn&ihrg=$ihrg";
				xloc($ke);
				exit();
				}
			else
				{
				//deteksi, jmlx > dari stock
				$qcc1 = mysqli_query($koneksi, "SELECT * FROM m_brg ".
												"WHERE kd = '$brgkd'");
				$rcc1 = mysqli_fetch_assoc($qcc1);
				$brg_nama = cegah($rcc1['nama']);
				$brg_kode = cegah($rcc1['kode']);
				$brg_barkode = cegah($rcc1['barkode']);
				$brg_kategori = cegah($rcc1['kategori']);
				$brg_satuan = cegah($rcc1['satuan']);
				$brg_harga = cegah($rcc1['hrg_jual']);
				$jml_cc1 = nosql($rcc1['jml_stock']);

				//nek jmlx lebih...
				if ($jmlx >= $jml_cc1)
					{
					//null-kan
					xclose($koneksi);

					//re-direct
					$pesan = "Jumlah Item Melebihi Jumlah Stock Yang Ada. Harap Dipehatikan...!!";
					pekem($pesan,$ke);
					exit();
					}
				else
					{
					//kurangi stock toko
					//deteksi
					$qdtx = mysqli_query($koneksi, "SELECT * FROM m_brg ".
														"WHERE kd = '$brgkd'");
					$rdtx = mysqli_fetch_assoc($qdtx);
					$dtx_toko = nosql($rdtx['jml_stock']);

					//nek mencukupi
					if ($dtx_toko > $jmlx)
						{
						$s_toko =  $jmlx;

						mysqli_query($koneksi, "UPDATE m_brg ".
												"SET jml_stock = jml_stock - '$s_toko' ".
												"WHERE kd = '$brgkd'");
						}
					else if ($dtx_toko < $jmlx)
						{
						$s_toko =  $jmlx; //sisa utk toko

						//update toko
						mysqli_query($koneksi, "UPDATE m_brg ".
												"SET jml_stock = jml_stock - '$s_toko' ".
												"WHERE kd = '$brgkd'");
						}






					//ke detail....
					//insert
					mysqli_query($koneksi, "INSERT INTO nota_detail(kd, nota_kd, brg_kd, ".
												"brg_kode, brg_barkode, brg_nama, ".
												"brg_satuan, brg_kategori, ".
												"brg_harga, qty, subtotal, ".
												"user_kd, user_kode, user_nama, postdate) VALUES ".
												"('$x', '$notakd', '$brgkd', ".
												"'$brg_kode', '$brg_barkode', '$brg_nama', ".
												"'$brg_satuan', '$brg_kategori', ".
												"'$brg_harga', '$jmlx', '$stotx', ".
												"'$kuz_kd', '$kuz_kode', '$kuz_nama', '$today')");

					//null-kan
					xclose($koneksi);

					//re-direct
					xloc($ke);
					exit();
					}
				}
			}
		}
	}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



//proses hapus //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
if (($_POST['s'] == "hapus")
	AND ($_POST['kdx']))
	{
	//nilai
	$kdx = nosql($_POST['kdx']);
	$notakd = nosql($_POST['notakdx']);
	$ke = "$filenya?bookkd=$bookkd&bookkode=$bookkode&notakd=$notakd";



	//deteksi
	$qcc = mysqli_query($koneksi, "SELECT * FROM nota_detail ".
										"WHERE nota_kd = '$notakd' ".
										"AND kd = '$kdx'");
	$rcc = mysqli_fetch_assoc($qcc);
	$brg_kd = nosql($rcc['brg_kd']);
	$qty_toko = nosql($rcc['qty']);


	//update stock kembali...
	mysqli_query($koneksi, "UPDATE m_brg ".
							"SET jml_stock = jml_stock + '$qty_toko' ".
							"WHERE kd = '$brg_kd'");


	//update
	mysqli_query($koneksi, "DELETE FROM nota_detail ".
								"WHERE nota_kd = '$notakd' ".
								"AND kd = '$kdx'");

	//null-kan
	xclose($koneksi);

	//re-direct
	xloc($ke);
	exit();
	}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



//proses edit ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
if (($_POST['s'] == "edit")
	AND ($_POST['kdx'])
	AND ($_POST['kodex'])
	AND ($_POST['jmlx']))
	{
	//nilai
	$kdx = nosql($_POST['kdx']);
	$notakd = nosql($_POST['notakdx']);
	$kodeu = nosql($_POST['kodex']);
	$jmlx = nosql($_POST['jmlx']);
	$hrgx = nosql($_POST['hrgx']);
	$stotx = nosql($_POST['stotx']);
	$ke = "$filenya?bookkd=$bookkd&bookkode=$bookkode&s=$s&notakd=$notakd";


	//cek, barkode-kah...? lebih dari 12 angka, BARCODE ////////////////////////////////////////////////////////////////////////////////
	if ((strlen($kodeu) > 12) AND (is_numeric($kodeu)))
		{
		//cek input
		$qcr = mysqli_query($koneksi, "SELECT * FROM m_brg ".
								"WHERE barkode = '$kodeu'");
		$rcr = mysqli_fetch_assoc($qcr);
		$tcr = mysqli_num_rows($qcr);
		$kodex = nosql($rcr['kode']);
		$brgkd = nosql($rcr['kd']);
		}
	else
		{
		//cek input
		$qcr = mysqli_query($koneksi, "SELECT * FROM m_brg ".
								"WHERE kode = '$kodeu'");
		$rcr = mysqli_fetch_assoc($qcr);
		$tcr = mysqli_num_rows($qcr);
		$kodex = nosql($rcr['kode']);
		$brgkd = nosql($rcr['kd']);
		}



	//nek kode barang tidak ada. atau salah
	if ($tcr == 0)
		{
		//null-kan
		xclose($koneksi);

		//re-direct
		$pesan = "Tidak ada Barang dengan Kode/Barcode : $kodex. Harap Diulangi...!!";
		pekem($pesan,$ke);
		exit();
		}
	else
		{
		//netralkan dahulu .......................................................................
		//deteksi
		$qcc = mysqli_query($koneksi, "SELECT * FROM nota_detail ".
								"WHERE nota_kd = '$notakd' ".
								"AND kd = '$kdx'");
		$rcc = mysqli_fetch_assoc($qcc);
		$qty_toko = nosql($rcc['qty']);

		//update stock kembali...
		mysqli_query($koneksi, "UPDATE m_brg ".
						"SET jml_stock = jml_stock + '$qty_toko' ".
						"WHERE kd = '$brgkd'");


		//lakukan sekarang .......................................................................
		//deteksi, jmlx > dari stock
		$qcc1 = mysqli_query($koneksi, "SELECT * FROM m_brg ".
								"WHERE kd = '$brgkd'");
		$rcc1 = mysqli_fetch_assoc($qcc1);
		$jml_cc1 = nosql($rcc1['jml_stock']);
		$jml_min = nosql($rcc1['jml_min']);

		//nek jmlx lebih...
		if ($jmlx >= $jml_cc1)
			{
			//null-kan
			xclose($koneksi);

			//re-direct
			$pesan = "Jumlah Item Melebihi Jumlah Stock Yang Ada. Harap Dipehatikan...!!";
			pekem($pesan,$ke);
			exit();
			}
		else
			{
			//kurangi stock toko
			//deteksi
			$qdtx = mysqli_query($koneksi, "SELECT * FROM m_brg ".
									"WHERE kd = '$brgkd'");
			$rdtx = mysqli_fetch_assoc($qdtx);
			$dtx_toko = nosql($rdtx['jml_stock']);

			//nek mencukupi
			if ($dtx_toko > $jmlx)
				{
				$s_toko =  $jmlx;

				mysqli_query($koneksi, "UPDATE m_brg ".
								"SET jml_stock = jml_stock - '$s_toko' ".
								"WHERE kd = '$brgkd'");
				}
			else if ($dtx_toko < $jmlx)
				{
				$s_toko =  $jmlx; //sisa utk toko

				//update toko
				mysqli_query($koneksi, "UPDATE m_brg ".
								"SET jml_stock = jml_stock - '$s_toko' ".
								"WHERE kd = '$brgkd'");
				}

			//update detail
			mysqli_query($koneksi, "UPDATE nota_detail SET brg_kd = '$brgkd', ".
										"qty = '$jmlx', ".
										"subtotal = '$stotx' ".
										"WHERE nota_kd = '$notakd' ".
										"AND kd = '$kdx'");

			//null-kan
			xclose($koneksi);

			//re-direct
			xloc($ke);
			exit();
			}
		}
	}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////





//focus /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//jika blm ada nota, bikin aja...
if (empty($notakd))
	{
	$diload = "$dikeydown;isodatetime();";
	}
else
	{
	//subtotal-nya...
	$qstu = mysqli_query($koneksi, "SELECT SUM(subtotal) AS subtotal ".
							"FROM nota_detail ".
							"WHERE nota_kd = '$notakd'");
	$rstu = mysqli_fetch_assoc($qstu);
	$stu_subtotal = nosql($rstu['subtotal']);


	//nek tanpa ikod
	if (empty($ikod))
		{
		$diload = "isodatetime();document.formx.kode0.focus();document.formx.stotx.value='$stu_subtotal';";
		}
	else
		{
		$diload = "isodatetime();document.formx.jml0.focus();document.formx.stotx.value='$stu_subtotal';";
		}
	}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


//isi *START
ob_start();


echo '<script type="text/javascript" src="'.$sumber.'/inc/js/dhtmlwindow_admks.js"></script>
<script type="text/javascript" src="'.$sumber.'/inc/js/modal.js"></script>
<script type="text/javascript">

function open_brg()
	{
	brg_window=dhtmlmodal.open(\'Daftar Stock\',
	\'iframe\',
	\'nota_brg.php\',
	\'Daftar Stock\',
	\'width=750px,height=325px,center=1,resize=0,scrolling=0\')

	brg_window.onclose=function()
		{
		var kodex=this.contentDoc.getElementById("kodex");

		document.formx.kode0.value=kodex.value;
		document.formx.kode0.focus();
		return true
		}
	}


</script>';

//js
require("../../inc/js/jam.js");
require("../../inc/js/number.js");
?>

<style>
	.layar {
	BACKGROUND-COLOR: BLACK;
	COLOR: #00FF00;
	font-family: Verdana, Arial, Helvetica, sans-serif;
	FONT-SIZE: 60px;
	FONT-WEIGHT: normal;
	border: 1px solid #996600;
}

</style>



<?php
//echo '<form name="formx" method="post">';
echo '<form name="formx" action="'.$ke.'" method="post">';


//nota-nya
$qntt = mysqli_query($koneksi, "SELECT * FROM nota ".
						"WHERE kd = '$notakd'");
$rntt = mysqli_fetch_assoc($qntt);
$ntt_nota = nosql($rntt['no_nota']);



//wong e
$qntt2 = mysqli_query($koneksi, "SELECT * FROM orderan ".
									"WHERE kd = '$bookkd'");
$rntt2 = mysqli_fetch_assoc($qntt2);
$ntt_pel2 = cegah($rntt2['o_nama']);
$ntt_pel = balikin($rntt2['o_nama']);
$ntt_pel_alamat = balikin($rntt2['o_alamat']);
$ntt_pel_telp = balikin($rntt2['o_telp']);
$ntt_pel_ktp = balikin($rntt2['o_no_ktp']);
$ntt_tempat_biaya = balikin($rntt2['tempat_biaya']);
$ntt_sewa_durasi = balikin($rntt2['sewa_durasi']);
$ntt_sewa_biaya = balikin($rntt2['sewa_biaya']);
$ntt_per_menit = round($ntt_tempat_biaya / 60);
$ntt_mulai_postdate = balikin($rntt2['mulai_postdate']);




//jika null, kasi baru
if (empty($ntt_nota))
	{
	//insert-kan...
	mysqli_query($koneksi, "INSERT INTO nota(kd, book_kd, book_kode, ".
					"pelanggan, tgl, no_nota, postdate) VALUES ".
					"('$notakd', '$bookkd', '$bookkode', ".
					"'$ntt_pel2', '$today', '$bookkode', '$today')");
	}






//entri kan
$bookkode2 = "MAINFUTSAL";
$brgnama = cegah("Bermain Mulai $ntt_mulai_postdate");





//hapus dulu
mysqli_query($koneksi, "DELETE FROM nota_detail ".
							"WHERE nota_kd = '$notakd' ".
							"AND brg_kode = '$bookkode2'");




//insert
mysqli_query($koneksi, "INSERT INTO nota_detail(kd, nota_kd, brg_kd, ".
							"brg_kode, brg_barkode, brg_nama, ".
							"brg_satuan, brg_kategori, ".
							"brg_harga, qty, subtotal, ".
							"user_kd, user_kode, user_nama, postdate) VALUES ".
							"('$x', '$notakd', '$bookkd', ".
							"'$bookkode2', '$bookkode', '$brgnama', ".
							"'Menit', 'Bermain', ".
							"'$ntt_per_menit', '$ntt_sewa_durasi', '$ntt_sewa_biaya', ".
							"'$kuz_kd', '$kuz_kode', '$kuz_nama', '$today')");












//total-nya
$qtuh = mysqli_query($koneksi, "SELECT SUM(subtotal) AS total ".
						"FROM nota_detail ".
						"WHERE nota_kd = '$notakd'");
$rtuh = mysqli_fetch_assoc($qtuh);
$tuh_total = nosql($rtuh['total']);

//nek null
if (empty($tuh_total))
	{
	$tuh_total = "0";
	}
	
	
	
							
							
							
							
//total-nya
$qtuh2 = mysqli_query($koneksi, "SELECT SUM(qty) AS total ".
						"FROM nota_detail ".
						"WHERE nota_kd = '$notakd'");
$rtuh2 = mysqli_fetch_assoc($qtuh2);
$tuh2_total = nosql($rtuh2['total']);

//nek null
if (empty($tuh2_total))
	{
	$tuh2_total = "0";
	}
	





//total-nya
$qtuh3 = mysqli_query($koneksi, "SELECT SUM(subtotal) AS total ".
									"FROM nota_detail ".
									"WHERE nota_kd = '$notakd' ".
									"AND brg_kode <> 'MAINFUTSAL'");
$rtuh3 = mysqli_fetch_assoc($qtuh3);
$tuh3_total = nosql($rtuh3['total']);

//nek null
if (empty($tuh3_total))
	{
	$tuh3_total = "0";
	}

	
	
	
		
	
	
//update kan
mysqli_query($koneksi, "UPDATE orderan SET item_jml = '$tuh2_total', ".
							"item_biaya = '$tuh3_total', ".
							"total_biaya = '$tuh_total' ".
							"WHERE kd = '$notakd'");
							
							
							
							


echo '<a href="bayar.php" class="btn btn-danger"><< DAFTAR BOOKING</a>
<hr>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr>
<td valign="top">

<table width="100%" border="0" cellspacing="0" cellpadding="3">
<tr>
<td class="kasir1">Kode Booking </td>
<td class="kasir1">: <b>'.$bookkode.'</b>
</td>
</tr>

<tr>
<td class="kasir1">Pelanggan </td>
<td class="kasir1">: KTP.<b>'.$ntt_pel_ktp.'</b></td>
</tr>

<tr>
<td class="kasir1">&nbsp;</td>
<td class="kasir1">&nbsp;&nbsp;NAMA.<b>'.$ntt_pel.'</b></td>
</tr>

<tr>
<td class="kasir1">&nbsp;</td>
<td class="kasir1">&nbsp;&nbsp;ALAMAT.<b>'.$ntt_pel_alamat.'</b></td>
</tr>

<tr>
<td class="kasir1">&nbsp;</td>
<td class="kasir1">&nbsp;&nbsp;TELEPON.<b>'.$ntt_pel_telp.'</b></td>
</tr>
</table>


</td>
<td valign="top" align="right">
<input name="layar" type="text" size="20" value="'.$tuh_total.'" class="layar" style="text-align:right;width:200;height:70" readonly>
<br>
</td>
</tr>
</table>

<br>
<table width="100%" border="0" cellspacing="0" cellpadding="3">
<tr>
<td>
[<strong>Tombol INSERT</strong> : CARI ITEM PRODUK].
[<strong>Tombol DEL</strong> : HAPUS ITEM PRODUK].
[<strong>Tombol END</strong> : SELESAI & BAYAR].
</td>
<td align="right">
<input type="text" name="display_jam" size="10" style="background-color:yellow;border:0;font-size:27;text-align:right">
</td>
</tr>
</table>

<table width="100%" border="1" cellpadding="3" cellspacing="0">
<tr>
<td width="100" align="center"><strong>KODE</strong></td>
<td align="center"><strong>NAMA BARANG</strong></td>
<td width="200" align="center"><strong>HARGA</strong></td>
<td width="100" align="center"><strong>QTY</strong></td>
<td width="100" align="center"><strong>SATUAN</strong></td>
<td width="100" align="center"><strong>SUBTOTAL</strong></td>
</tr>
<tr>
<td>
<input name="kode0" type="text" size="7" value="'.$ikod.'" maxlength="15" class="btn btn-block btn-warning"
onKeyDown="var keyCode = event.keyCode;
if (keyCode == 13)
	{
	document.formx.kodex.value = document.formx.kode0.value;
	document.formx.submit();
	}

if (keyCode == 38)
	{
	document.formx.kode'.$tcob.'.focus();
	}

if (keyCode == 40)
	{
	document.formx.kode1.focus();
	}

if (keyCode == 45)
	{
	open_brg();
	return false
	}


if (keyCode == 35)
	{
	location.href=\'nota_bayar.php?bookkd='.$bookkd.'&bookkode='.$bookkode.'&s='.$s.'&notakd='.$notakd.'\';
	}
	
	
	
if (keyCode == 46)
	{
	alert(\'Pilih Dahulu Item Yang Akan Dihapus...!!\');
	location.href=\''.$ke.'\';
	}
" '.$attribut.'>
</td>
<td>
<input name="nm0" type="text" value="'.$inm.'" class="btn btn-block btn-default" readonly>
</td>
<td>
<input name="hrg0" type="text" size="10" value="'.$ihrg.'" class="btn btn-block btn-default" style="text-align:right" readonly>
</td>
<td>
<input name="jml0" type="text" size="5" value="'.$ijml.'" class="btn btn-block btn-warning" style="text-align:right"
onKeyPress="return numbersonly(this, event)"
onKeyUp="document.formx.subtotal.value=Math.round(document.formx.jml0.value * document.formx.hrgx.value);
document.formx.stotx.value=document.formx.subtotal.value;
document.formx.layar.value=document.formx.subtotal.value;"
onKeyDown="var keyCode = event.keyCode;
if (keyCode == 13)
	{
	document.formx.jmlx.value = document.formx.jml0.value;
	document.formx.submit();
	}

if (keyCode == 46)
	{
	location.href=\''.$ke.'\';
	}
" '.$attribut.'>
</td>
<td>
<input name="stn0" type="text" size="5" value="'.$istn.'" class="btn btn-block btn-default" readonly>
</td>
<td>
<input name="subtotal" type="text" size="10" value="" class="btn btn-block btn-default" style="text-align:right" readonly>
</td>
</tr>';

//data ne
$qcob = mysqli_query($koneksi, "SELECT * FROM nota_detail ".
									"WHERE nota_kd = '$notakd' ".
									"ORDER BY postdate ASC");
$rcob = mysqli_fetch_assoc($qcob);
$tcob = mysqli_num_rows($qcob);

//nek gak null
if ($tcob != 0)
	{
	do
		{
		$nomerx = $nomerx + 1;

		if ($warna_set ==0)
			{
			$warna = $warna01;
			$warna_set = 1;
			}
		else
			{
			$warna = $warna02;
			$warna_set = 0;
			}

		//pageup ////////////////////////
		$nil = $nomerx - 1;

		if ($nil < 1)
			{
			$nil = 0;
			}

		if ($nil > $tcob)
			{
			$nil = $tcob;
			}


		//pagedown ////////////////////////
		$nild = $nomerx + 1;

		if ($nild < 1)
			{
			$nild = $nild + 1;
			}

		if ($nild > $tcob)
			{
			$nild = 0;
			}

		$cob_kd = nosql($rcob['kd']);
		$cob_kode = nosql($rcob['brg_kode']);
		$cob_jml = nosql($rcob['qty']);
		$cob_nm = balikin($rcob['brg_nama']);
		$cob_satuan = nosql($rcob['brg_satuan']);
		$cob_hrg = nosql($rcob['brg_harga']);


		echo "<tr valign=\"top\" bgcolor=\"$warna\"
		onkeyup=\"this.bgColor='$warnaover';\"
		onkeydown=\"this.bgColor='$warna';\"
		onmouseover=\"this.bgColor='$warnaover';\"
		onmouseout=\"this.bgColor='$warna';\">";
		echo '<td>
		<input name="kd'.$nomerx.'" type="hidden" value="'.$cob_kd.'">
		<input name="kode'.$nomerx.'" type="text" value="'.$cob_kode.'" size="7" maxlength="15" class="btn btn-block btn-warning"
		onKeyDown="var keyCode = event.keyCode;
		if (keyCode == 13)
			{
			document.formx.jml'.$nomerx.'.focus();
			}

		if (keyCode == 38)
			{
			document.formx.kode'.$nil.'.focus();
			}

		if (keyCode == 39)
			{
			document.formx.jml'.$nomerx.'.focus();
			}

		if (keyCode == 40)
			{
			document.formx.kode'.$nild.'.focus();
			}

		
		if (keyCode == 35)
			{
			location.href=\'nota_bayar.php?bookkd='.$bookkd.'&bookkode='.$bookkode.'&s='.$s.'&notakd='.$notakd.'\';
			}



		if (keyCode == 46)
			{
			document.formx.s.value = \'hapus\';
			document.formx.kdx.value = document.formx.kd'.$nomerx.'.value;
			document.formx.submit();
			}">
		</td>
		<td>
		<input name="nm'.$nomerx.'" type="text" value="'.$cob_nm.'" class="btn btn-block btn-default" readonly>
		</td>
		<td>
		<input name="hrg'.$nomerx.'" type="text" value="'.$cob_hrg.'" size="10" class="btn btn-block btn-default" style="text-align:right" readonly>
		</td>
		<td>
		<input name="jml'.$nomerx.'" type="text" value="'.$cob_jml.'" size="5" class="btn btn-block btn-warning" style="text-align:right"
		onKeyPress="return numbersonly(this, event)"
		onKeyUp="document.formx.stot'.$nomerx.'.value=Math.round(document.formx.hrg'.$nomerx.'.value * document.formx.jml'.$nomerx.'.value);
		document.formx.layar.value=document.formx.stot'.$nomerx.'.value;"
		onKeyDown="var keyCode = event.keyCode;
		if (keyCode == 13)
			{
			document.formx.s.value = \'edit\';
			document.formx.kdx.value = document.formx.kd'.$nomerx.'.value;
			document.formx.kodex.value = document.formx.kode'.$nomerx.'.value;
			document.formx.jmlx.value = document.formx.jml'.$nomerx.'.value;
			document.formx.stotx.value = document.formx.stot'.$nomerx.'.value;
			document.formx.submit();
			}

		if (keyCode == 37)
			{
			document.formx.kode'.$nomerx.'.focus();
			}

		if (keyCode == 38)
			{
			document.formx.jml'.$nomerx.'.focus();
			}

		if (keyCode == 40)
			{
			document.formx.jml'.$nomerx.'.focus();
			}


		if (keyCode == 46)
			{
			document.formx.s.value = \'hapus\';
			document.formx.kdx.value = document.formx.kd'.$nomerx.'.value;
			document.formx.submit();
			}">
		</td>
		<td>
		<input name="stn'.$nomerx.'" type="text" value="'.$cob_satuan.'" size="5" class="btn btn-block btn-default" readonly>
		</td>
		<td>
		<input name="stot'.$nomerx.'" type="text" value="'.round($cob_hrg * $cob_jml).'" size="12" class="btn btn-block btn-default" style="text-align:right" readonly>
		</td>
		</tr>';
		}
	while ($rcob = mysqli_fetch_assoc($qcob));
	}

echo '</table>
<input name="s" type="hidden" value="'.$s.'">
<input name="kdx" type="hidden" value="'.$ikd.'">
<input name="kodex" type="hidden" value="'.$ikod.'">
<input name="hrgx" type="hidden" value="'.$ihrg.'">
<input name="jmlx" type="hidden" value="'.$ijml.'">
<input name="notakdx" type="hidden" value="'.$notakd.'">
<input name="stotx" type="hidden" value="'.$stu_subtotal.'">
</form>';

//isi
$isi = ob_get_contents();
ob_end_clean();

require("../../inc/niltpl.php");

//null-kan
xclose($koneksi);
exit();
?>