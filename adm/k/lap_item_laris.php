<?php
session_start();

require("../../inc/config.php");
require("../../inc/fungsi.php");
require("../../inc/koneksi.php");
require("../../inc/cek/adm.php");
require("../../inc/class/paging.php");
$tpl = LoadTpl("../../template/admin.html");

nocache;

//nilai
$filenya = "lap_item_laris.php";
$judul = "[LAP.KASIR]. Laporan Item Terlaris";
$judulku = "[LAP.KASIR]. Laporan Item Terlaris";
$judulx = $judul;
$limit = 1000; //jumlah data item terlaris
$xbln1 = nosql($_REQUEST['xbln1']);
$xthn1 = nosql($_REQUEST['xthn1']);
$s = nosql($_REQUEST['s']);
$page = nosql($_REQUEST['page']);
if ((empty($page)) OR ($page == "0"))
	{
	$page = "1";
	}


//focus
if (empty($xbln1))
	{
	$diload = "document.formx.xbln1.focus();";
	}
else if (empty($xthn1))
	{
	$diload = "document.formx.xthn1.focus();";
	}
else
	{
	//netralkan dahulu...!!
	mysqli_query($koneksi, "DELETE FROM item_laris ".
					"WHERE round(bln) = '$xbln1' ".
					"AND round(thn) = '$xthn1'");


	//ambil data dari nota //////////////////////////////////////////////////////////////////////////////////////////////////////////////
	$qnot = mysqli_query($koneksi, "SELECT DISTINCT(brg_kd) AS brgkd ".
										"FROM nota_detail ".
										"WHERE round(DATE_FORMAT(postdate, '%m')) = '$xbln1' ".
										"AND round(DATE_FORMAT(postdate, '%Y')) = '$xthn1'");
	$rnot = mysqli_fetch_assoc($qnot);
	$tnot = mysqli_num_rows($qnot);

	//nek ada
	if ($tnot != 0)
		{
		do
			{
			//nilai
			$nom = $nom + 1;
			$xx = md5("today3$nom");
			$n_brgkd = nosql($rnot['brgkd']);
			
			//detail
			$qnotx1 = mysqli_query($koneksi, "SELECT * FROM m_brg ".
												"WHERE kd = '$n_brgkd'");
			$rnotx1 = mysqli_fetch_assoc($qnotx1);
			$n_brg_kode = cegah($rnotx1['kode']);
			$n_brg_nama = cegah($rnotx1['nama']);
			$n_brg_satuan = cegah($rnotx1['satuan']);
			$n_brg_harga = cegah($rnotx1['hrg_jual']);
						

			//qty-ne...
			$qnotx = mysqli_query($koneksi, "SELECT SUM(qty) AS qty ".
									"FROM nota_detail ".
									"WHERE brg_kd = '$n_brgkd' ".
									"AND round(DATE_FORMAT(postdate, '%m')) = '$xbln1' ".
									"AND round(DATE_FORMAT(postdate, '%Y')) = '$xthn1'");
			$rnotx = mysqli_fetch_assoc($qnotx);
			$tnotx = mysqli_num_rows($qnotx);

			//nilai qty
			$n_qty = nosql($rnotx['qty']);
			
			
			$n_subtotal = $n_qty * $n_brg_harga;
			
			

			//masukkan
			mysqli_query($koneksi, "INSERT INTO item_laris(kd, bln, thn, ".
										"brg_kd, brg_kode, brg_nama, ".
										"brg_satuan, brg_harga, ".
										"jml, subtotal, postdate) VALUES ".
										"('$xx', '$xbln1', '$xthn1', ".
										"'$n_brgkd', '$n_brg_kode', '$n_brg_nama', ".
										"'$n_brg_satuan', '$n_brg_harga', ".
										"'$n_qty', '$n_subtotal', '$today')");
			}
		while ($rnot = mysqli_fetch_assoc($qnot));
		}
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	}







require_once("../../inc/class/dompdf/autoload.inc.php");

use Dompdf\Dompdf;
$dompdf = new Dompdf();








//nek excel
if ($_POST['btnEX'])
	{
	//nilai
	$xbln1 = balikin($_POST['xbln1']);
	$xthn1 = balikin($_POST['xthn1']);



	
	//isi *START
	ob_start();
	

		echo '<table class="table" border="0" cellpadding="3" cellspacing="0" width="100%">
		<thead>
		<tr>
		<td width="100"><img src="../../img/logo2.png" alt="Logo" height="100"></td>
		<td><b>'.$sek_nama.'</b>
	    <br>'.$sek_alamat.'
	    <br>WA.: '.$sek_telp.'
		
		</td>
		</tr>
		</thead>
		</table>';

			
				
	    echo '<hr>';



		
		
		echo '<h3>LAPORAN ITEM TERLARIS, '.$xbln1.'-'.$xthn1.'</h3>
		<hr>';
		
		
		
		

	
		//query
		$p = new Pager();
		$start = $p->findStart($limit);
	
		$sqlcount = "SELECT * FROM item_laris ".
						"WHERE bln = '$xbln1' ".
						"AND thn = '$xthn1' ".
						"ORDER BY round(jml) DESC";
		$sqlresult = $sqlcount;
	
		$count = mysqli_num_rows(mysqli_query($koneksi, $sqlcount));
		$pages = $p->findPages($count, $limit);
		$result = mysqli_query($koneksi, "$sqlresult LIMIT ".$start.", ".$limit);
		$pagelist = $p->pageList($_GET['page'], $pages, $target);
		$data = mysqli_fetch_array($result);
	
		//nek ada
		if ($count != 0)
			{
			//data - datanya
			echo '<div class="table-responsive">
			<table class="table" border="1" cellpadding="3" cellspacing="0" width="100%">
			<thead>
		
			<tr bgcolor="'.$warnaheader.'">
			<td width="5"><strong><font color="'.$warnatext.'">No.</font></strong></td>
			<td><strong><font color="'.$warnatext.'">NAMA</font></strong></td>
			<td width="50" align="center"><strong><font color="'.$warnatext.'">HARGA</font></strong></td>
			<td width="5" align="center"><strong><font color="'.$warnatext.'">SATUAN</font></strong></td>
			<td width="5" align="center"><strong><font color="'.$warnatext.'">TERJUAL</font></strong></td>
			<td width="50" align="center"><strong><font color="'.$warnatext.'">SUBTOTAL</font></strong></td>
			</tr>
			</thead>
			<tbody>';
	
			do
				{
				if ($warna_set ==0)
					{
					$warna = $warna01;
					$warna_set = 1;
					}
				else
					{
					$warna = $warna02;
					$warna_set = 0;
					}
	
				$nomer = $nomer + 1;
				$x_kode = nosql($data['brg_kode']);
				$x_nama = balikin($data['brg_nama']);
				$x_harga = balikin($data['brg_harga']);
				$x_satuan = balikin($data['brg_satuan']);
				$x_jml = nosql($data['jml']);
				$x_subtotal = nosql($data['subtotal']);
	
	
				echo "<tr bgcolor=\"$warna\" onmouseover=\"this.bgColor='$warnaover';\" onmouseout=\"this.bgColor='$warna';\">";
				echo '<td align="center">'.$nomer.'.</td>
				<td>
				'.$x_nama.'
				<br>
				Kode:'.$x_kode.'
				</td>
				<td align="right">'.xduit3($x_harga).'</td>
				<td>'.$x_satuan.'</td>
				<td align="right">'.$x_jml.'</td>
				<td align="right">'.$x_subtotal.'</td>
		        </tr>';
				}
			while ($data = mysqli_fetch_assoc($result));
	
			echo '</tbody>
			</table>
			</div>
			
			<table width="100%" border="0" cellspacing="0" cellpadding="3">
			<tr>
			<td>
			<input name="xbln1" type="hidden" value="'.$xbln1.'">
			<input name="xthn1" type="hidden" value="'.$xthn1.'">
			<input name="page" type="hidden" value="'.$page.'">
			</td>
			</tr>
			</table>';
			}
		else
			{
			//tidak ada data
			echo '<h3>
			<font color="red"><strong>TIDAK ADA DATA</strong></font>
			</h3>';
			}
	
	
		echo '</div>';
	
	
	
		
		//isi
		$isiku = ob_get_contents();
		ob_end_clean();
	
	
	

	
	$dompdf->loadHtml($isiku);
	
	// Setting ukuran dan orientasi kertas
	$dompdf->setPaper('A4', 'potrait');
	// Rendering dari HTML Ke PDF
	$dompdf->render();
	
	
	$pdf = $dompdf->output();
	
	ob_end_clean();
	
	// Melakukan output file Pdf
	$dompdf->stream('lap-item-terlaris-'.$xbln1.'-'.$xthn1.'.pdf');
	
	




	exit();
	}	



















//isi *START
ob_start();


//require
require("../../inc/js/jumpmenu.js");
require("../../inc/js/swap.js");


?>



  
  <script>
  	$(document).ready(function() {
    $('#table-responsive').dataTable( {
        "scrollX": true
    } );
} );
  </script>
  
<?php


//view //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
echo '<form method="post" name="formx">

<table width="100%" border="0" cellspacing="0" cellpadding="3">
<tr bgcolor="'.$warna02.'">
<td>';

echo "<select name=\"xbln1\" onChange=\"MM_jumpMenu('self',this,0)\" class=\"btn btn-warning\">";
echo '<option value="'.$xbln1.'" selected>'.$arrbln[$xbln1].'</option>';

for ($j=1;$j<=12;$j++)
	{
	echo '<option value="'.$filenya.'?xbln1='.$j.'">'.$arrbln[$j].'</option>';
	}

echo '</select>';

echo "<select name=\"xthn1\" onChange=\"MM_jumpMenu('self',this,0)\" class=\"btn btn-warning\">";
echo '<option value="'.$xthn1.'" selected>'.$xthn1.'</option>';

for ($k=$tahun;$k<=$tahun+1;$k++)
	{
	$x_thn = $k;
	echo '<option value="'.$filenya.'?xbln1='.$xbln1.'&xthn1='.$x_thn.'">'.$x_thn.'</option>';
	}

echo '</select>
</td>
</tr>
</table>
<br>';


//nek masih do null
if (empty($xbln1))
	{
	echo "<font color='red'>
	<h3>
	<strong>Bulan Belum Dipilih...!!</strong>
	</h3>
	</font>";
	}
else if (empty($xthn1))
	{
	echo "<font color='red'>
	<h3>
	<strong>Tahun Belum Dipilih...!!</strong>
	</h3>
	</font>";
	}
else
	{
	//query
	$p = new Pager();
	$start = $p->findStart($limit);

	$sqlcount = "SELECT * FROM item_laris ".
					"WHERE bln = '$xbln1' ".
					"AND thn = '$xthn1' ".
					"ORDER BY round(jml) DESC";
	$sqlresult = $sqlcount;

	$count = mysqli_num_rows(mysqli_query($koneksi, $sqlcount));
	$pages = $p->findPages($count, $limit);
	$result = mysqli_query($koneksi, "$sqlresult LIMIT ".$start.", ".$limit);
	$pagelist = $p->pageList($_GET['page'], $pages, $target);
	$data = mysqli_fetch_array($result);

	//nek ada
	if ($count != 0)
		{
		//data - datanya
		echo '<input name="btnEX" type="submit" value="EXPORT PDF >>" class="btn btn-danger">
		
		<div class="table-responsive">
		<table class="table" border="1">
		<thead>
			
		<tr bgcolor="'.$warnaheader.'">
		<td width="5"><strong><font color="'.$warnatext.'">NoUrut.</font></strong></td>
		<td><strong><font color="'.$warnatext.'">NAMA</font></strong></td>
		<td width="150" align="center"><strong><font color="'.$warnatext.'">HARGA</font></strong></td>
		<td width="50" align="center"><strong><font color="'.$warnatext.'">SATUAN</font></strong></td>
		<td width="50" align="center"><strong><font color="'.$warnatext.'">TERJUAL</font></strong></td>
		<td width="150" align="center"><strong><font color="'.$warnatext.'">SUBTOTAL</font></strong></td>
		</tr>
		</thead>
		<tbody>';

		do
			{
			if ($warna_set ==0)
				{
				$warna = $warna01;
				$warna_set = 1;
				}
			else
				{
				$warna = $warna02;
				$warna_set = 0;
				}

			$nomer = $nomer + 1;
			$x_kode = nosql($data['brg_kode']);
			$x_nama = balikin($data['brg_nama']);
			$x_harga = balikin($data['brg_harga']);
			$x_satuan = balikin($data['brg_satuan']);
			$x_jml = nosql($data['jml']);
			$x_subtotal = nosql($data['subtotal']);


			echo "<tr bgcolor=\"$warna\" onmouseover=\"this.bgColor='$warnaover';\" onmouseout=\"this.bgColor='$warna';\">";
			echo '<td align="center">'.$nomer.'.</td>
			<td>
			'.$x_nama.'
			<br>
			Kode:'.$x_kode.'
			</td>
			<td align="right">'.xduit3($x_harga).'</td>
			<td>'.$x_satuan.'</td>
			<td align="right">'.$x_jml.'</td>
			<td align="right">'.xduit3($x_subtotal).'</td>
	        </tr>';
			}
		while ($data = mysqli_fetch_assoc($result));

		echo '</tbody>
		</table>
		</div>
		
		<table width="100%" border="0" cellspacing="0" cellpadding="3">
		<tr>
		<td>
		<input name="xbln1" type="hidden" value="'.$xbln1.'">
		<input name="xthn1" type="hidden" value="'.$xthn1.'">
		<input name="page" type="hidden" value="'.$page.'">
		</td>
		</tr>
		</table>';
		}
	else
		{
		//tidak ada data
		echo '<br>
		<p>
		<font color="red"><strong>BELUM ADA DATA.</strong></font>
		</p>';
		}
	}

echo '</form>';

//isi
$isi = ob_get_contents();
ob_end_clean();

require("../../inc/niltpl.php");


//null-kan
xclose($koneksi);
exit();
?>