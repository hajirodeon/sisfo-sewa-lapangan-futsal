<?php
session_start();

require("../../inc/config.php");
require("../../inc/fungsi.php");
require("../../inc/koneksi.php");
require("../../inc/cek/adm.php");
require("../../inc/class/paging.php");
$tpl = LoadTpl("../../template/admin.html");

nocache;

//nilai
$filenya = "lap_brg_keluar.php";
$judul = "[LAP.KASIR]. Barang Keluar";
$judulku = "[LAP.KASIR]. Barang Keluar";
$judulx = $judul;

$s = nosql($_REQUEST['s']);
$page = nosql($_REQUEST['page']);
if ((empty($page)) OR ($page == "0"))
	{
	$page = "1";
	}





$e_tgl1 = balikin($_REQUEST['e_tgl1']);
$e_tgl2 = balikin($_REQUEST['e_tgl2']);






//PROSES ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//tampilkan
if ($_POST['btnTPL'])
	{
	//ambil nilai
	$e_tgl1 = cegah($_POST['e_tgl1']);
	$e_tgl2 = cegah($_POST['e_tgl2']);
	
	
	//re-direct
	$ke = "$filenya?e_tgl1=$e_tgl1&e_tgl2=$e_tgl2";
	xloc($ke);
	exit();
	}

//PROSES ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////











///////////////////////////////////////////////////////////////////////////////////////////////////////

require_once("../../inc/class/dompdf/autoload.inc.php");

use Dompdf\Dompdf;
$dompdf = new Dompdf();











if ($_POST['btnEX'])
	{
	//nilai
	$e_tgl1 = balikin($_POST['e_tgl1']);
	$e_tgl2 = balikin($_POST['e_tgl2']);
	
	
	
	//isi *START
	ob_start();
	
	
			
		
		//detail
		$qku = mysqli_query($koneksi, "SELECT * FROM cp_profil");
		$rku = mysqli_fetch_assoc($qku);
		$ku_judul = balikin($rku['judul']);
		$ku_isi = balikin($rku['isi']);
		$ku_web = balikin($rku['web']);
		$ku_email = balikin($rku['email']);
		$ku_alamat = balikin($rku['alamat']);
		$ku_alamat2 = balikin($rku['alamat_googlemap']);
		$ku_telp = balikin($rku['telp']);
		$ku_fax = balikin($rku['fax']);
		$ku_fb = balikin($rku['fb']);
		$ku_twitter = balikin($rku['twitter']);
		$ku_youtube = balikin($rku['youtube']);
		$ku_wa = balikin($rku['wa']);
		$ku_instagram = balikin($rku['instagram']);
	
	
	


		echo '<table class="table" border="0" cellpadding="3" cellspacing="0" width="100%">
		<thead>
		<tr>
		<td width="100"><img src="../../img/logo2.png" alt="Logo" height="100"></td>
		<td><b>GALAXY FUTSAL</b>
	    <br>'.$ku_alamat.'
	    <br>WA.: '.$ku_wa.'
		
		</td>
		</tr>
		</thead>
		</table>';

			
				
	    echo '<hr>';
		
		
		echo '<h3>LAPORAN BARANG KELUAR : '.$e_tgl1.' sampai '.$e_tgl2.'</h3>
		<hr>';
		
		
		
				
				
		$e_tgl1x = $e_tgl1;
		$e_tgl2x = $e_tgl2;
		
		

		
		
		//pecah
		$pecahku = explode("-", $e_tgl1x);
		$pecahku1 = $pecahku[2];
		$pecahku2 = $pecahku[1];
		$pecahku3 = $pecahku[0];
		$begin1 = "$pecahku3-$pecahku2-$pecahku1 00:00:00";
		$begin = new DateTime("$pecahku3-$pecahku2-$pecahku1");
		
		
		
		//pecah
		$pecahkux = explode("-", $e_tgl2x);
		$pecahkux1 = $pecahkux[2];
		$pecahkux2 = $pecahkux[1];
		$pecahkux3 = $pecahkux[0];
		$end1 = "$pecahkux3-$pecahkux2-$pecahkux1 00:00:00";
		$end = new DateTime("$pecahkux3-$pecahkux2-$pecahkux1");
		
		
		$daterange = new DatePeriod($begin, new DateInterval('P1D'), $end);
		
		
		
		
			
		//query
		$p = new Pager();
		$limit = 1000;
		$start = $p->findStart($limit);
		
		$sqlcount = "SELECT DISTINCT(brg_kd) AS brgkdku ".
						"FROM nota_detail ".
						"WHERE postdate >= '$begin1' ".
						"AND postdate <= '$end1' ".
						"ORDER BY brg_nama ASC";
		
		
		$sqlresult = $sqlcount;
		
		$count = mysqli_num_rows(mysqli_query($koneksi, $sqlcount));
		$pages = $p->findPages($count, $limit);
		$result = mysqli_query($koneksi, "$sqlresult LIMIT ".$start.", ".$limit);
		$target = "$filenya?xtgl1=$xtgl1&xbln1=$xbln1&xthn1=$xthn1";
		$pagelist = $p->pageList($_GET['page'], $pages, $target);
		$data = mysqli_fetch_array($result);
		
		
		
		if ($count != 0)
			{
			//jumlahnya
			$qjml = mysqli_query($koneksi, "SELECT SUM(qty) AS jml ".
									"FROM nota_detail ".
									"WHERE postdate >= '$begin1' ".
									"AND postdate <= '$end1'");
			$rjml = mysqli_fetch_assoc($qjml);
			$jml_qty = nosql($rjml['jml']);
	
			
			
					
			//subtotal
			$qjml = mysqli_query($koneksi, "SELECT SUM(subtotal) AS jml ".
												"FROM nota_detail ".
												"WHERE postdate >= '$begin1' ".
												"AND postdate <= '$end1'");
			$rjml = mysqli_fetch_assoc($qjml);
			$jml_subtotal = nosql($rjml['jml']);
			
			//data - datanya
			echo '[Jml. Item Produk : <b>'.$count.'</b>]. 
			
			[Qty.Item Produk : <b>'.$jml_qty.'</b>]. 
			
			[Subtotal : <b>'.xduit3($jml_subtotal).'</b>].
			<div class="table-responsive">
			<table class="table" border="1" cellpadding="3" cellspacing="0" width="100%">
			<thead>
			<tr bgcolor="'.$warnaheader.'">
			<td width="5" align="center"><strong><font color="'.$warnatext.'">No.</font></strong></td>
			<td><strong><font color="'.$warnatext.'">Nama Barang</font></strong></td>
			<td width="5" align="center"><strong><font color="'.$warnatext.'">Jumlah</font></strong></td>
			<td width="50" align="center"><strong><font color="'.$warnatext.'">Harga</font></strong></td>
			<td width="50" align="center"><strong><font color="'.$warnatext.'">Subtotal</font></strong></td>
			<td width="5" align="center"><strong><font color="'.$warnatext.'">STOCK AKHIR</font></strong></td>
			</tr>
			</thead>
			<tbody>';
	
			do
				{
				if ($warna_set ==0)
					{
					$warna = $warna01;
					$warna_set = 1;
					}
				else
					{
					$warna = $warna02;
					$warna_set = 0;
					}
	
				$nomer = $nomer + 1;
				$brgkd = nosql($data['brgkdku']);
				
				
				//artinya....
				$qbrg = mysqli_query($koneksi, "SELECT * FROM nota_detail ".
													"WHERE brg_kd = '$brgkd' ".
													"ORDER BY brg_nama ASC");
				$rbrg = mysqli_fetch_assoc($qbrg);
				$tbrg = mysqli_num_rows($qbrg);
				$brg_kode = balikin($rbrg['brg_kode']);
				$brg_nama = balikin($rbrg['brg_nama']);
				$brg_satuan = balikin($rbrg['brg_satuan']);
				$brg_harga = balikin($rbrg['brg_harga']);
				$brg_subtotal = balikin($rbrg['subtotal']);
				
	
	
				//jumlahnya
				$qjml = mysqli_query($koneksi, "SELECT SUM(qty) AS jml ".
										"FROM nota_detail ".
										"WHERE postdate >= '$begin1' ".
										"AND postdate <= '$end1' ".
										"AND brg_kd = '$brgkd'");
				$rjml = mysqli_fetch_assoc($qjml);
				$jml_qty = nosql($rjml['jml']);
	
	
				//subtotalnya
				$qjml2 = mysqli_query($koneksi, "SELECT SUM(subtotal) AS jml ".
										"FROM nota_detail ".
										"WHERE postdate >= '$begin1' ".
										"AND postdate <= '$end1' ".
										"AND brg_kd = '$brgkd'");
				$rjml2 = mysqli_fetch_assoc($qjml2);
				$jml2_subtotal = nosql($rjml2['jml']);
	
	
	
				//stock akhir
				$qyuk = mysqli_query($koneksi, "SELECT * FROM m_brg ".
													"WHERE kd = '$brgkd'");
				$ryuk = mysqli_fetch_assoc($qyuk);
				$yuk_stock = balikin($ryuk['jml_stock']);
	
	
				echo "<tr bgcolor=\"$warna\" onmouseover=\"this.bgColor='$warnaover';\" onmouseout=\"this.bgColor='$warna';\">";
				echo '<td align="center">'.$nomer.'.</td>
				<td>
				'.$brg_nama.'
				<br>
				Kode:'.$brg_kode.'
				</td>
				<td align="right">
				'.$jml_qty.' '.$brg_satuan.'
				</td>
				<td align="right">'.xduit3($brg_harga).'</td>
				<td align="right">'.xduit3($jml2_subtotal).'</td>
				<td align="right">'.$yuk_stock.'</td>
		        </tr>';
				}
			while ($data = mysqli_fetch_assoc($result));
	
	
			//jumlahnya
			$qjml = mysqli_query($koneksi, "SELECT SUM(qty) AS jml ".
									"FROM nota_detail ".
									"WHERE postdate >= '$begin1' ".
									"AND postdate <= '$end1'");
			$rjml = mysqli_fetch_assoc($qjml);
			$jml_qty = nosql($rjml['jml']);
	
			
			
					
			//subtotal
			$qjml = mysqli_query($koneksi, "SELECT SUM(subtotal) AS jml ".
												"FROM nota_detail ".
												"WHERE postdate >= '$begin1' ".
												"AND postdate <= '$end1'");
			$rjml = mysqli_fetch_assoc($qjml);
			$jml_subtotal = nosql($rjml['jml']);
	
	
			echo '<tr bgcolor="'.$warnaheader.'">
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td align="right"><strong><font color="'.$warnatext.'">'.$jml_qty.'</font></strong></td>
			<td align="center">&nbsp;</td>
			<td align="right"><strong><font color="'.$warnatext.'">'.xduit3($jml_subtotal).'</font></strong></td>
			<td>&nbsp;</td>
			</tr>
			</tbody>
			</table>
			</div>';
			}
		else
			{
			echo '<font color="red"><strong>TIDAK ADA DATA BARANG KELUAR.</strong></font>';
			}
	
	
	//isi
	$isi = ob_get_contents();
	ob_end_clean();
	
	

	
	$dompdf->loadHtml($isi);
	
	// Setting ukuran dan orientasi kertas
	$dompdf->setPaper('A4', 'potrait');
	// Rendering dari HTML Ke PDF
	$dompdf->render();
	
	
	$pdf = $dompdf->output();
	
	ob_end_clean();
	
	// Melakukan output file Pdf
	$dompdf->stream('lap-barang-keluar-'.$e_tgl1.'-'.$e_tgl2.'.pdf');
	
	
	





	exit();
	}	
///////////////////////////////////////////////////////////////////////////////////////////////////////




















//isi *START
ob_start();



//require
require("../../inc/js/jumpmenu.js");
require("../../inc/js/swap.js");
require("../../inc/js/number.js");

?>



  
  <script>
  	$(document).ready(function() {
    $('#table-responsive').dataTable( {
        "scrollX": true
    } );
} );
  </script>
  
<?php
$e_tgl1x = $e_tgl1;
$e_tgl2x = $e_tgl2;


//jika null
if (empty($e_tgl1))
	{
	$e_tgl1x = "$tahun-$bulan-$tanggal";
	}

//jika null
if (empty($e_tgl2))
	{
	$e_tgl2x = "$tahun-$bulan-$tanggal";
	}




//view //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
echo '<form method="post" name="formx">

<div class="row">
	<div class="col-md-12">
		
		<input name="e_tgl1" id="e_tgl1" type="date" size="10" value="'.$e_tgl1x.'" class="btn btn-warning">
		
		Sampai 
		
		<input name="e_tgl2" id="e_tgl2" type="date" size="10" value="'.$e_tgl2x.'" class="btn btn-warning">
		
		<input name="btnTPL" type="submit" value="TAMPILKAN >>" class="btn btn-danger">
		
	</div>
</div>';



//pecah
$pecahku = explode("-", $e_tgl1x);
$pecahku1 = $pecahku[2];
$pecahku2 = $pecahku[1];
$pecahku3 = $pecahku[0];
$begin1 = "$pecahku3-$pecahku2-$pecahku1 00:00:00";
$begin = new DateTime("$pecahku3-$pecahku2-$pecahku1");



//pecah
$pecahkux = explode("-", $e_tgl2x);
$pecahkux1 = $pecahkux[2];
$pecahkux2 = $pecahkux[1];
$pecahkux3 = $pecahkux[0];
$end1 = "$pecahkux3-$pecahkux2-$pecahkux1 00:00:00";
$end = new DateTime("$pecahkux3-$pecahkux2-$pecahkux1");


$daterange = new DatePeriod($begin, new DateInterval('P1D'), $end);





//jika ada value
if (!empty($e_tgl1x))
	{
	//query
	$p = new Pager();
	$start = $p->findStart($limit);
	
	$sqlcount = "SELECT DISTINCT(brg_kd) AS brgkdku ".
					"FROM nota_detail ".
					"WHERE postdate >= '$begin1' ".
					"AND postdate <= '$end1' ".
					"ORDER BY brg_nama ASC";
	
	
	$sqlresult = $sqlcount;
	
	$count = mysqli_num_rows(mysqli_query($koneksi, $sqlcount));
	$pages = $p->findPages($count, $limit);
	$result = mysqli_query($koneksi, "$sqlresult LIMIT ".$start.", ".$limit);
	$target = "$filenya?xtgl1=$xtgl1&xbln1=$xbln1&xthn1=$xthn1";
	$pagelist = $p->pageList($_GET['page'], $pages, $target);
	$data = mysqli_fetch_array($result);
	
	
	
	if ($count != 0)
		{
		//jumlahnya
		$qjml = mysqli_query($koneksi, "SELECT SUM(qty) AS jml ".
								"FROM nota_detail ".
								"WHERE postdate >= '$begin1' ".
								"AND postdate <= '$end1'");
		$rjml = mysqli_fetch_assoc($qjml);
		$jml_qty = nosql($rjml['jml']);

		
		
				
		//subtotal
		$qjml = mysqli_query($koneksi, "SELECT SUM(subtotal) AS jml ".
											"FROM nota_detail ".
											"WHERE postdate >= '$begin1' ".
											"AND postdate <= '$end1'");
		$rjml = mysqli_fetch_assoc($qjml);
		$jml_subtotal = nosql($rjml['jml']);
		
		//data - datanya
		echo '<br><input name="btnEX" type="submit" value="EXPORT PDF >>" class="btn btn-danger">
		<br>
		[Jml. Item Produk : <b>'.$count.'</b>]. 
		
		[Qty.Item Produk : <b>'.$jml_qty.'</b>]. 
		
		[Subtotal : <b>'.xduit3($jml_subtotal).'</b>].
		<div class="table-responsive">
		<table class="table" border="1">
		<thead>
		<tr bgcolor="'.$warnaheader.'">
		<td width="50"><strong><font color="'.$warnatext.'">Kode</font></strong></td>
		<td><strong><font color="'.$warnatext.'">Nama Barang</font></strong></td>
		<td width="50" align="center"><strong><font color="'.$warnatext.'">Jumlah</font></strong></td>
		<td width="150" align="center"><strong><font color="'.$warnatext.'">Harga</font></strong></td>
		<td width="150" align="center"><strong><font color="'.$warnatext.'">Subtotal</font></strong></td>
		<td width="50" align="center"><strong><font color="'.$warnatext.'">STOCK AKHIR</font></strong></td>
		</tr>
		</thead>
		<tbody>';

		do
			{
			if ($warna_set ==0)
				{
				$warna = $warna01;
				$warna_set = 1;
				}
			else
				{
				$warna = $warna02;
				$warna_set = 0;
				}

			$nomer = $nomer + 1;
			$brgkd = nosql($data['brgkdku']);
			
			
			//artinya....
			$qbrg = mysqli_query($koneksi, "SELECT * FROM nota_detail ".
												"WHERE brg_kd = '$brgkd' ".
												"ORDER BY brg_nama ASC");
			$rbrg = mysqli_fetch_assoc($qbrg);
			$tbrg = mysqli_num_rows($qbrg);
			$brg_kode = balikin($rbrg['brg_kode']);
			$brg_nama = balikin($rbrg['brg_nama']);
			$brg_satuan = balikin($rbrg['brg_satuan']);
			$brg_harga = balikin($rbrg['brg_harga']);
			$brg_subtotal = balikin($rbrg['subtotal']);
			


			//jumlahnya
			$qjml = mysqli_query($koneksi, "SELECT SUM(qty) AS jml ".
									"FROM nota_detail ".
									"WHERE postdate >= '$begin1' ".
									"AND postdate <= '$end1' ".
									"AND brg_kd = '$brgkd'");
			$rjml = mysqli_fetch_assoc($qjml);
			$jml_qty = nosql($rjml['jml']);


			//subtotalnya
			$qjml2 = mysqli_query($koneksi, "SELECT SUM(subtotal) AS jml ".
									"FROM nota_detail ".
									"WHERE postdate >= '$begin1' ".
									"AND postdate <= '$end1' ".
									"AND brg_kd = '$brgkd'");
			$rjml2 = mysqli_fetch_assoc($qjml2);
			$jml2_subtotal = nosql($rjml2['jml']);



			//stock akhir
			$qyuk = mysqli_query($koneksi, "SELECT * FROM m_brg ".
												"WHERE kd = '$brgkd'");
			$ryuk = mysqli_fetch_assoc($qyuk);
			$yuk_stock = balikin($ryuk['jml_stock']);


			echo "<tr bgcolor=\"$warna\" onmouseover=\"this.bgColor='$warnaover';\" onmouseout=\"this.bgColor='$warna';\">";
			echo '<td>'.$brg_kode.'</td>
			<td>'.$brg_nama.'</td>
			<td align="right">
			'.$jml_qty.' '.$brg_satuan.'
			</td>
			<td align="right">'.xduit3($brg_harga).'</td>
			<td align="right">'.xduit3($jml2_subtotal).'</td>
			<td align="right">'.$yuk_stock.'</td>
	        </tr>';
			}
		while ($data = mysqli_fetch_assoc($result));


		//jumlahnya
		$qjml = mysqli_query($koneksi, "SELECT SUM(qty) AS jml ".
								"FROM nota_detail ".
								"WHERE postdate >= '$begin1' ".
								"AND postdate <= '$end1'");
		$rjml = mysqli_fetch_assoc($qjml);
		$jml_qty = nosql($rjml['jml']);

		
		
				
		//subtotal
		$qjml = mysqli_query($koneksi, "SELECT SUM(subtotal) AS jml ".
											"FROM nota_detail ".
											"WHERE postdate >= '$begin1' ".
											"AND postdate <= '$end1'");
		$rjml = mysqli_fetch_assoc($qjml);
		$jml_subtotal = nosql($rjml['jml']);


		echo '<tr bgcolor="'.$warnaheader.'">
		<td>&nbsp;</td>
		<td>&nbsp;</td>
		<td align="right"><strong><font color="'.$warnatext.'">'.$jml_qty.'</font></strong></td>
		<td align="center">&nbsp;</td>
		<td align="right"><strong><font color="'.$warnatext.'">'.xduit3($jml_subtotal).'</font></strong></td>
		<td>&nbsp;</td>
		</tr>
		</tbody>
		</table>
		</div>';
		}
	else
		{
		echo '<br>
		<p>
		<font color="red"><strong>BELUM ADA DATA.</strong></font>
		</p>';
		}
	}	



echo '</form>
<br><br><br>';

//isi
$isi = ob_get_contents();
ob_end_clean();

require("../../inc/niltpl.php");

//null-kan
xfree($result);
xclose($koneksi);
exit();
?>