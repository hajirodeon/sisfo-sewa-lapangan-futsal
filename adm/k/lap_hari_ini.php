<?php
session_start();

require("../../inc/config.php");
require("../../inc/fungsi.php");
require("../../inc/koneksi.php");
require("../../inc/cek/adm.php");
require("../../inc/class/paging.php");
$tpl = LoadTpl("../../template/admin.html");

nocache;

//nilai
$filenya = "lap_hari_ini.php";
$judul = "[LAP.KASIR]. Hari Ini";
$judulku = "[LAP.KASIR]. Hari Ini";
$judulx = $judul;

$s = nosql($_REQUEST['s']);
$page = nosql($_REQUEST['page']);
if ((empty($page)) OR ($page == "0"))
	{
	$page = "1";
	}



$limit = 1000;










///////////////////////////////////////////////////////////////////////////////////////////////////////

require_once("../../inc/class/dompdf/autoload.inc.php");

use Dompdf\Dompdf;
$dompdf = new Dompdf();











if ($_POST['btnEX'])
	{
	//isi *START
	ob_start();
	
	
			
		

	
		
		//ketahui harinya...
		$tglmasuk2 = "$tahun-$bulan-$tanggal";
		$datenya = strtotime($tglmasuk2);
		$harinya = date('w', $datenya);
		
		$harinya2 = $arrhari[$harinya];
		

		echo '<table class="table" border="0" cellpadding="3" cellspacing="0" width="100%">
		<thead>
		<tr>
		<td width="100"><img src="../../img/logo2.png" alt="Logo" height="100"></td>
		<td><b>'.$sek_nama.'</b>
	    <br>'.$sek_alamat.'
	    <br>WA.: '.$sek_telp.'
		
		</td>
		</tr>
		</thead>
		</table>';

			
				
	    echo '<hr>';
		
		
		echo '<h3>LAPORAN PENJUALAN HARI INI : '.$harinya2.', '.$tanggal.'-'.$bulan.'-'.$tahun.'</h3>
		<hr>';
		
		
		
		
		
		$begin1 = "$tahun-$bulan-$tanggal";
		
		
		
		//query
		$p = new Pager();
		$start = $p->findStart($limit);
		
		$sqlcount = "SELECT DISTINCT(brg_kd) AS brgkdku ".
						"FROM nota_detail ".
						"WHERE postdate LIKE '$begin1%' ".
						"ORDER BY brg_nama ASC";
		
		
		$sqlresult = $sqlcount;
		
		$count = mysqli_num_rows(mysqli_query($koneksi, $sqlcount));
		$pages = $p->findPages($count, $limit);
		$result = mysqli_query($koneksi, "$sqlresult LIMIT ".$start.", ".$limit);
		$pagelist = $p->pageList($_GET['page'], $pages, $target);
		$data = mysqli_fetch_array($result);
		
		
		
		if ($count != 0)
			{
			//jumlahnya
			$qjml = mysqli_query($koneksi, "SELECT SUM(qty) AS jml ".
									"FROM nota_detail ".
									"WHERE postdate LIKE '$begin1%'");
			$rjml = mysqli_fetch_assoc($qjml);
			$jml_qty = nosql($rjml['jml']);
		
			
			
					
			//subtotal
			$qjml = mysqli_query($koneksi, "SELECT SUM(subtotal) AS jml ".
												"FROM nota_detail ".
												"WHERE postdate LIKE '$begin1%'");
			$rjml = mysqli_fetch_assoc($qjml);
			$jml_subtotal = nosql($rjml['jml']);
			
			//data - datanya
			echo '[Jml. Item Produk : <b>'.$count.'</b>]. 
			
			[Qty.Item Produk : <b>'.$jml_qty.'</b>]. 
			
			[TOTAL : <b>'.xduit3($jml_subtotal).'</b>].
			
			<div class="table-responsive">
			<table class="table" border="1" cellpadding="3" cellspacing="0">
			<thead>
			<tr bgcolor="'.$warnaheader.'">
			<td width="5"><strong><font color="'.$warnatext.'">No.</font></strong></td>
			<td><strong><font color="'.$warnatext.'">Nama Barang</font></strong></td>
			<td width="50" align="center"><strong><font color="'.$warnatext.'">Jumlah</font></strong></td>
			<td width="50" align="center"><strong><font color="'.$warnatext.'">Harga</font></strong></td>
			<td width="50" align="center"><strong><font color="'.$warnatext.'">Subtotal</font></strong></td>
			<td width="50" align="center"><strong><font color="'.$warnatext.'">STOCK AKHIR</font></strong></td>
			</tr>
			</thead>
			<tbody>';
		
			do
				{
				if ($warna_set ==0)
					{
					$warna = $warna01;
					$warna_set = 1;
					}
				else
					{
					$warna = $warna02;
					$warna_set = 0;
					}
		
				$nomer = $nomer + 1;
				$brgkd = nosql($data['brgkdku']);
				
				
				//artinya....
				$qbrg = mysqli_query($koneksi, "SELECT * FROM nota_detail ".
													"WHERE brg_kd = '$brgkd' ".
													"ORDER BY brg_nama ASC");
				$rbrg = mysqli_fetch_assoc($qbrg);
				$tbrg = mysqli_num_rows($qbrg);
				$brg_kode = balikin($rbrg['brg_kode']);
				$brg_nama = balikin($rbrg['brg_nama']);
				$brg_satuan = balikin($rbrg['brg_satuan']);
				$brg_harga = balikin($rbrg['brg_harga']);
				$brg_subtotal = balikin($rbrg['subtotal']);
				
		
		
				//jumlahnya
				$qjml = mysqli_query($koneksi, "SELECT SUM(qty) AS jml ".
										"FROM nota_detail ".
										"WHERE postdate LIKE '$begin1%' ".
										"AND brg_kd = '$brgkd'");
				$rjml = mysqli_fetch_assoc($qjml);
				$jml_qty = nosql($rjml['jml']);
		
		
				//subtotalnya
				$qjml2 = mysqli_query($koneksi, "SELECT SUM(subtotal) AS jml ".
										"FROM nota_detail ".
										"WHERE postdate LIKE '$begin1%' ".
										"AND brg_kd = '$brgkd'");
				$rjml2 = mysqli_fetch_assoc($qjml2);
				$jml2_subtotal = nosql($rjml2['jml']);
		
		
		
				//stock akhir
				$qyuk = mysqli_query($koneksi, "SELECT * FROM m_brg ".
													"WHERE kd = '$brgkd'");
				$ryuk = mysqli_fetch_assoc($qyuk);
				$yuk_stock = balikin($ryuk['jml_stock']);
		
		
				echo "<tr bgcolor=\"$warna\" onmouseover=\"this.bgColor='$warnaover';\" onmouseout=\"this.bgColor='$warna';\">";
				echo '<td>'.$nomer.'.</td>
				<td>
				'.$brg_nama.'
				<br>
				Kode : '.$brg_kode.'
				</td>
				<td align="right">
				'.$jml_qty.' '.$brg_satuan.'
				</td>
				<td align="right">'.xduit3($brg_harga).'</td>
				<td align="right">'.xduit3($jml2_subtotal).'</td>
				<td align="right">'.$yuk_stock.'</td>
		        </tr>';
				}
			while ($data = mysqli_fetch_assoc($result));
		
		
			//jumlahnya
			$qjml = mysqli_query($koneksi, "SELECT SUM(qty) AS jml ".
									"FROM nota_detail ".
									"WHERE postdate LIKE '$begin1%'");
			$rjml = mysqli_fetch_assoc($qjml);
			$jml_qty = nosql($rjml['jml']);
		
			
			
					
			//subtotal
			$qjml = mysqli_query($koneksi, "SELECT SUM(subtotal) AS jml ".
												"FROM nota_detail ".
												"WHERE postdate LIKE '$begin1%'");
			$rjml = mysqli_fetch_assoc($qjml);
			$jml_subtotal = nosql($rjml['jml']);
		
		
			echo '<tr bgcolor="'.$warnaheader.'">
			<td>&nbsp;</td>
			<td>&nbsp;</td>
			<td align="right"><strong><font color="'.$warnatext.'">'.$jml_qty.'</font></strong></td>
			<td align="center">&nbsp;</td>
			<td align="right"><strong><font color="'.$warnatext.'">'.xduit3($jml_subtotal).'</font></strong></td>
			<td>&nbsp;</td>
			</tr>
			</tbody>
			</table>
			</div>';
			}
		else
			{
			echo '<h3>
			<font color="red">BELUM ADA BARANG KELUAR.</font>
			</h3>';
			}

	
	//isi
	$isi = ob_get_contents();
	ob_end_clean();
	
	

	
	$dompdf->loadHtml($isi);
	
	// Setting ukuran dan orientasi kertas
	$dompdf->setPaper('A4', 'potrait');
	// Rendering dari HTML Ke PDF
	$dompdf->render();
	
	
	$pdf = $dompdf->output();
	
	ob_end_clean();
	
	// Melakukan output file Pdf
	$dompdf->stream('lap-hari-ini-'.$harinya2.'-'.$tanggal.'-'.$bulan.'-'.$tahun.'.pdf');
	
	
	





	exit();
	}	
///////////////////////////////////////////////////////////////////////////////////////////////////////















//isi *START
ob_start();



//require
require("../../inc/js/jumpmenu.js");
require("../../inc/js/swap.js");
require("../../inc/js/number.js");

?>



  
  <script>
  	$(document).ready(function() {
    $('#table-responsive').dataTable( {
        "scrollX": true
    } );
} );
  </script>
  
<?php
//view //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
echo '<form method="post" name="formx">';





//ketahui harinya...
$tglmasuk2 = "$tahun-$bulan-$tanggal";
$datenya = strtotime($tglmasuk2);
$harinya = date('w', $datenya);

$harinya2 = $arrhari[$harinya];



echo '<h3>'.$harinya2.', '.$tanggal.'-'.$bulan.'-'.$tahun.'</h3>
<hr>';





$begin1 = "$tahun-$bulan-$tanggal";



//query
$p = new Pager();
$start = $p->findStart($limit);

$sqlcount = "SELECT DISTINCT(brg_kd) AS brgkdku ".
				"FROM nota_detail ".
				"WHERE postdate LIKE '$begin1%' ".
				"ORDER BY brg_nama ASC";


$sqlresult = $sqlcount;

$count = mysqli_num_rows(mysqli_query($koneksi, $sqlcount));
$pages = $p->findPages($count, $limit);
$result = mysqli_query($koneksi, "$sqlresult LIMIT ".$start.", ".$limit);
$pagelist = $p->pageList($_GET['page'], $pages, $target);
$data = mysqli_fetch_array($result);



if ($count != 0)
	{
	//jumlahnya
	$qjml = mysqli_query($koneksi, "SELECT SUM(qty) AS jml ".
							"FROM nota_detail ".
							"WHERE postdate LIKE '$begin1%'");
	$rjml = mysqli_fetch_assoc($qjml);
	$jml_qty = nosql($rjml['jml']);

	
	
			
	//subtotal
	$qjml = mysqli_query($koneksi, "SELECT SUM(subtotal) AS jml ".
										"FROM nota_detail ".
										"WHERE postdate LIKE '$begin1%'");
	$rjml = mysqli_fetch_assoc($qjml);
	$jml_subtotal = nosql($rjml['jml']);
	
	//data - datanya
	echo '<input name="btnEX" type="submit" value="EXPORT PDF >>" class="btn btn-danger">
	<br>
	[Jml. Item Produk : <b>'.$count.'</b>]. 
	
	[Qty.Item Produk : <b>'.$jml_qty.'</b>]. 
	
	[TOTAL : <b>'.xduit3($jml_subtotal).'</b>].
	
	<div class="table-responsive">
	<table class="table" border="1">
	<thead>
	<tr bgcolor="'.$warnaheader.'">
	<td width="50"><strong><font color="'.$warnatext.'">No.</font></strong></td>
	<td><strong><font color="'.$warnatext.'">Nama Barang</font></strong></td>
	<td width="50" align="center"><strong><font color="'.$warnatext.'">Jumlah</font></strong></td>
	<td width="150" align="center"><strong><font color="'.$warnatext.'">Harga</font></strong></td>
	<td width="150" align="center"><strong><font color="'.$warnatext.'">Subtotal</font></strong></td>
	<td width="50" align="center"><strong><font color="'.$warnatext.'">STOCK AKHIR</font></strong></td>
	</tr>
	</thead>
	<tbody>';

	do
		{
		if ($warna_set ==0)
			{
			$warna = $warna01;
			$warna_set = 1;
			}
		else
			{
			$warna = $warna02;
			$warna_set = 0;
			}

		$nomer = $nomer + 1;
		$brgkd = nosql($data['brgkdku']);
		
		
		//artinya....
		$qbrg = mysqli_query($koneksi, "SELECT * FROM nota_detail ".
											"WHERE brg_kd = '$brgkd' ".
											"ORDER BY brg_nama ASC");
		$rbrg = mysqli_fetch_assoc($qbrg);
		$tbrg = mysqli_num_rows($qbrg);
		$brg_kode = balikin($rbrg['brg_kode']);
		$brg_nama = balikin($rbrg['brg_nama']);
		$brg_satuan = balikin($rbrg['brg_satuan']);
		$brg_harga = balikin($rbrg['brg_harga']);
		$brg_subtotal = balikin($rbrg['subtotal']);
		


		//jumlahnya
		$qjml = mysqli_query($koneksi, "SELECT SUM(qty) AS jml ".
								"FROM nota_detail ".
								"WHERE postdate LIKE '$begin1%' ".
								"AND brg_kd = '$brgkd'");
		$rjml = mysqli_fetch_assoc($qjml);
		$jml_qty = nosql($rjml['jml']);


		//subtotalnya
		$qjml2 = mysqli_query($koneksi, "SELECT SUM(subtotal) AS jml ".
								"FROM nota_detail ".
								"WHERE postdate LIKE '$begin1%' ".
								"AND brg_kd = '$brgkd'");
		$rjml2 = mysqli_fetch_assoc($qjml2);
		$jml2_subtotal = nosql($rjml2['jml']);



		//stock akhir
		$qyuk = mysqli_query($koneksi, "SELECT * FROM m_brg ".
											"WHERE kd = '$brgkd'");
		$ryuk = mysqli_fetch_assoc($qyuk);
		$yuk_stock = balikin($ryuk['jml_stock']);


		echo "<tr bgcolor=\"$warna\" onmouseover=\"this.bgColor='$warnaover';\" onmouseout=\"this.bgColor='$warna';\">";
		echo '<td>'.$nomer.'.</td>
		<td>
		'.$brg_nama.'
		<br>
		Kode : '.$brg_kode.'
		</td>
		<td align="right">
		'.$jml_qty.' '.$brg_satuan.'
		</td>
		<td align="right">'.xduit3($brg_harga).'</td>
		<td align="right">'.xduit3($jml2_subtotal).'</td>
		<td align="right">'.$yuk_stock.'</td>
        </tr>';
		}
	while ($data = mysqli_fetch_assoc($result));


	//jumlahnya
	$qjml = mysqli_query($koneksi, "SELECT SUM(qty) AS jml ".
							"FROM nota_detail ".
							"WHERE postdate LIKE '$begin1%'");
	$rjml = mysqli_fetch_assoc($qjml);
	$jml_qty = nosql($rjml['jml']);

	
	
			
	//subtotal
	$qjml = mysqli_query($koneksi, "SELECT SUM(subtotal) AS jml ".
										"FROM nota_detail ".
										"WHERE postdate LIKE '$begin1%'");
	$rjml = mysqli_fetch_assoc($qjml);
	$jml_subtotal = nosql($rjml['jml']);


	echo '<tr bgcolor="'.$warnaheader.'">
	<td>&nbsp;</td>
	<td>&nbsp;</td>
	<td align="right"><strong><font color="'.$warnatext.'">'.$jml_qty.'</font></strong></td>
	<td align="center">&nbsp;</td>
	<td align="right"><strong><font color="'.$warnatext.'">'.xduit3($jml_subtotal).'</font></strong></td>
	<td>&nbsp;</td>
	</tr>
	</tbody>
	</table>
	</div>';
	}
else
	{
	echo '<br>
	<p>
	<font color="red"><strong>BELUM ADA DATA.</strong></font>
	</p>';
	}




echo '</form>
<br><br><br>';

//isi
$isi = ob_get_contents();
ob_end_clean();

require("../../inc/niltpl.php");

//null-kan
xfree($result);
xclose($koneksi);
exit();
?>