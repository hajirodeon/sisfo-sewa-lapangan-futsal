<?php
require("../../inc/config.php");
require("../../inc/fungsi.php");
require("../../inc/koneksi.php");
require("../../inc/class/paging.php");
$tpl = LoadTpl("../../template/window.html");

nocache;

//nilai
$filenya = "nota_brg.php";
$judul = "Daftar Stock";
$judulku = $judul;
$kunci = cegah($_REQUEST['kunci']);
$ke = "$filenya?kunci=$kunci";
$page = nosql($_REQUEST['page']);
if ((empty($page)) OR ($page == "0"))
	{
	$page = "1";
	}

//keydown.
$x_enter2 = 'onKeyDown="return handleEnter(this, event)"';

//tombol "ESC"=27, utk. keluar
$dikeydown = "var keyCode = event.keyCode;
				if (keyCode == 27)
					{
					parent.brg_window.hide();
					}";

//focus
$diload = "document.formx.kunci.focus();";



//PROSES ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//nek reset
if ($_POST['btnRST'])
	{
	//re-direct
	xloc($filenya);
	exit();
	}
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




//isi *START
ob_start();

//query
$p = new Pager();
$start = $p->findStart($limit);

//jika cari /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
if ($_POST['btnCRI'])
	{
	$kunci = cegah($_POST['kunci']);

	//nek null
	if (empty($kunci))
		{
		//re-direct
		xloc($ke);
		exit();
		}
	else
		{
		$sqlcount = "SELECT * FROM m_brg ".
						"WHERE kode LIKE '%$kunci%' ".
						"OR nama LIKE '%$kunci%' ".
						"OR kategori LIKE '%$kunci%' ".
						"ORDER BY nama ASC";

		$sqlresult = $sqlcount;

		$count = mysqli_num_rows(mysqli_query($koneksi, $sqlcount));
		$pages = $p->findPages($count, $limit);
		$result = mysqli_query($koneksi, "$sqlresult LIMIT ".$start.", ".$limit);
		$target = "$filenya?kunci=$kunci";
		$pagelist = $p->pageList($_GET['page'], $pages, $target);
		$data = mysqli_fetch_array($result);
		}
	} ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
else
	{
	$sqlcount = "SELECT * FROM m_brg ".
					"ORDER BY nama ASC";

	$sqlresult = $sqlcount;

	$count = mysqli_num_rows(mysqli_query($koneksi, $sqlcount));
	$pages = $p->findPages($count, $limit);
	$result = mysqli_query($koneksi, "$sqlresult LIMIT ".$start.", ".$limit);
	$target = "$filenya?kunci=$kunci";
	$pagelist = $p->pageList($_GET['page'], $pages, $target);
	$data = mysqli_fetch_array($result);
	}




//require
require("../../inc/js/down_enter.js");
require("../../inc/js/swap.js");
xheadline($judul);

//view //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
echo '<form action="'.$filenya.'" method="post" name="formx">
<table width="100%" border="0" cellspacing="0" cellpadding="0">
<tr valign="top">
<td>
<input name="kunci" type="text" size="10" class="btn-info">
<input name="btnCRI" type="submit" value="CARI" class="btn-danger">
<input name="btnRST" type="submit" value="RESET" class="btn-warning">
</td>
</tr>
</table>';



echo '<table width="100%" border="1" cellspacing="0" cellpadding="3">
<tr valign="top" bgcolor="'.$warnaheader.'">
<td><strong><font color="'.$warnatext.'">Nama Barang</font></strong></td>
<td width="100"><strong><font color="'.$warnatext.'">Kategori</font></strong></td>
<td width="100"><strong><font color="'.$warnatext.'">Stock</font></strong></td>
<td width="100"><strong><font color="'.$warnatext.'">@ Harga</font></strong></td>
</tr>';

if ($count != 0)
	{
	do
		{
		if ($warna_set ==0)
			{
			$warna = $warna01;
			$warna_set = 1;
			}
		else
			{
			$warna = $warna02;
			$warna_set = 0;
			}

		$nomer = $nomer + 1;
		$kd = nosql($data['kd']);
		$kategori = balikin($data['kategori']);
		$satuan = balikin($data['satuan']);
		$kode = nosql($data['kode']);
		$nama = balikin($data['nama']);
		$hrg_jual = xduit2($data['hrg_jual']);
		$brg_jml = nosql($data['jml_stock']);

		//nek null
		if (empty($hrg_jual))
			{
			$hrg_jual = '-';
			}

		if (empty($brg_jml))
			{
			$brg_jml = '-';
			}


		echo "<tr valign=\"top\" bgcolor=\"$warna\" onmouseover=\"this.bgColor='$warnaover';\" onmouseout=\"this.bgColor='$warna';\"
		onClick=\"document.formx.kodex.value='$kode';
		parent.brg_window.hide();
		\">";
		echo '<td>
		'.$nama.'
		<br>
		Kode : '.$kode.'
		</td>
		<td>'.$kategori.'</td>
		<td>'.$brg_jml.'  '.$satuan.'</td>
		<td align="right">'.$hrg_jual.'</td>
        </tr>';
		}
	while ($data = mysqli_fetch_assoc($result));
	}


echo '</table>
<table width="100%" border="0" cellspacing="0" cellpadding="3">
<tr>
<td>
<input id="kodex" name="kodex" type="hidden" value="" size="10">
</td>
<td align="right">
<strong><font color="#FF0000">'.$count.'</font></strong> Data. '.$pagelist.'
</td>
</tr>
</table>';


echo '</form>';

//isi
$isi = ob_get_contents();
ob_end_clean();

require("../../inc/niltpl.php");

//null-kan
xclose($koneksi);
exit();
?>