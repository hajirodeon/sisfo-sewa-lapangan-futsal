<?php
session_start();

require("../../inc/config.php");
require("../../inc/fungsi.php");
require("../../inc/koneksi.php");
require("../../inc/cek/adm.php");
$tpl = LoadTpl("../../template/admin.html");

nocache;

//nilai
$filenya = "tgl.php";
$judul = "Per Tanggal";
$judul = "[KASIR]. Per Tanggal";
$judulku = "$judul";
$judulx = $judul;
$ubln = nosql($_REQUEST['ubln']);
$uthn = nosql($_REQUEST['uthn']);

$s = nosql($_REQUEST['s']);
$kunci = cegah($_REQUEST['kunci']);
$kunci2 = balikin($_REQUEST['kunci']);
$page = nosql($_REQUEST['page']);
if ((empty($page)) OR ($page == "0"))
	{
	$page = "1";
	}





///////////////////////////////////////////////////////////////////////////////////////////////////////

require_once("../../inc/class/dompdf/autoload.inc.php");

use Dompdf\Dompdf;
$dompdf = new Dompdf();











if ($_POST['btnEX'])
	{
	//isi *START
	ob_start();
	
	



		echo '<table class="table" border="0" cellpadding="3" cellspacing="0" width="100%">
		<thead>
		<tr>
		<td width="100"><img src="../../img/logo2.png" alt="Logo" height="100"></td>
		<td><b>'.$sek_nama.'</b>
	    <br>'.$sek_alamat.'
	    <br>WA.: '.$sek_telp.'
		
		</td>
		</tr>
		</thead>
		</table>';

			
				
	    echo '<hr>';


		$month = round($ubln);
		$year = round($uthn);
		
		//tanggal terakhir  
		$days_in_month = date('t',mktime(0,0,0,$month,1,$year));
			
		
		
		echo '<h3>LAPORAN PER TANGGAL, '.$ubln.'-'.$uthn.'</h3>
		<hr>';
		
		
		
		
		echo '<div class="table-responsive">
		<table class="table" border="1" cellpadding="3" cellspacing="0" width="100%">
		<thead>
		
		<tr valign="top" bgcolor="'.$warnaheader.'">
		<td><strong><font color="'.$warnatext.'">TANGGAL</font></strong></td>
		<td width="50"><strong><font color="'.$warnatext.'">BOOKING</font></strong></td>
		<td width="50"><strong><font color="'.$warnatext.'">ITEM TERJUAL</font></strong></td>
		<td width="50"><strong><font color="'.$warnatext.'">SUBTOTAL</font></strong></td>
		<td width="50"><strong><font color="'.$warnatext.'">PENGELUARAN</font></strong></td>
		<td width="50"><strong><font color="'.$warnatext.'">SALDO</font></strong></td>
		</tr>
		</thead>
		<tbody>';
		
		for ($k=1;$k<=$days_in_month;$k++) 
			{
			if ($warna_set ==0)
				{
				$warna = $warna01;
				$warna_set = 1;
				}
			else
				{
				$warna = $warna02;
				$warna_set = 0;
				}
	
	
			//jumlah booking
			$qku3 = mysqli_query($koneksi, "SELECT * FROM orderan ".
											"WHERE round(DATE_FORMAT(tgl_pakai, '%d')) = '$k' ".
											"AND round(DATE_FORMAT(tgl_pakai, '%m')) = '$ubln' ".
											"AND round(DATE_FORMAT(tgl_pakai, '%Y')) = '$uthn'");
			$rku3 = mysqli_fetch_assoc($qku3);
			$tku3 = mysqli_num_rows($qku3);
			
			
	
	
			//ketahui harinya...
			$tglmasuk2 = "$uthn-$ubln-$k";
			$datenya = strtotime($tglmasuk2);
			$harinya = date('w', $datenya);
			
			$harinya2 = $arrhari[$harinya];
			
	
			//jumlah item
			$qku31 = mysqli_query($koneksi, "SELECT SUM(qty) AS totalnya ".
											"FROM nota_detail ".
											"WHERE round(DATE_FORMAT(postdate, '%d')) = '$k' ".
											"AND round(DATE_FORMAT(postdate, '%m')) = '$ubln' ".
											"AND round(DATE_FORMAT(postdate, '%Y')) = '$uthn'");
			$rku31 = mysqli_fetch_assoc($qku31);
			$tku31 = mysqli_num_rows($qku31);
			$ku31_jml = balikin($rku31['totalnya']);
	
			//jika null
			if (empty($ku31_jml))
				{
				$ku31_jml = 0;
				}
	
	
	
	
			//jumlah nominal
			$qku31 = mysqli_query($koneksi, "SELECT SUM(subtotal) AS totalnya ".
											"FROM nota_detail ".
											"WHERE round(DATE_FORMAT(postdate, '%d')) = '$k' ".
											"AND round(DATE_FORMAT(postdate, '%m')) = '$ubln' ".
											"AND round(DATE_FORMAT(postdate, '%Y')) = '$uthn'");
			$rku31 = mysqli_fetch_assoc($qku31);
			$tku31 = mysqli_num_rows($qku31);
			$ku31_total = balikin($rku31['totalnya']);
	
			//jika null
			if (empty($ku31_total))
				{
				$ku31_total = 0;
				}
	
	
	

			//jumlah nominal pengeluaran
			$qku32 = mysqli_query($koneksi, "SELECT SUM(nilai) AS totalnya ".
											"FROM uang_keluar ".
											"WHERE round(DATE_FORMAT(per_tgl, '%d')) = '$k' ".
											"AND round(DATE_FORMAT(per_tgl, '%m')) = '$ubln' ".
											"AND round(DATE_FORMAT(per_tgl, '%Y')) = '$uthn'");
			$rku32 = mysqli_fetch_assoc($qku32);
			$tku32 = mysqli_num_rows($qku32);
			$ku32_total = balikin($rku32['totalnya']);
	
			//jika null
			if (empty($ku32_total))
				{
				$ku32_total = 0;
				}
	
			
			
				
			//sisa
			$jml_sisax = $ku31_total - $ku32_total;
		
	
	
	
			
			
			echo "<tr valign=\"top\" bgcolor=\"$warna\" onmouseover=\"this.bgColor='$warnaover';\" onmouseout=\"this.bgColor='$warna';\">";
			echo '<td>'.$harinya2.', '.$k.' '.$arrbln[$ubln].' '.$uthn.'</td>
			<td align="right"><font color="red">'.$tku3.'</font></td>
			<td align="right"><font color="red">'.$ku31_jml.'</font></td>
			<td align="right"><font color="red">'.xduit3($ku31_total).'</font></td>
			<td align="right"><font color="red">'.xduit3($ku32_total).'</font></td>
			<td align="right"><font color="red">'.xduit3($jml_sisax).'</font></td>
			</tr>';
			}
		
	
	
	
	
		//jumlah booking
		$qku3x = mysqli_query($koneksi, "SELECT * FROM orderan ".
										"WHERE round(DATE_FORMAT(tgl_pakai, '%m')) = '$ubln' ".
										"AND round(DATE_FORMAT(tgl_pakai, '%Y')) = '$uthn'");
		$rku3x = mysqli_fetch_assoc($qku3x);
		$tku3x = mysqli_num_rows($qku3x);
	
	
	
	
	
	
		//jumlah item
		$qku31 = mysqli_query($koneksi, "SELECT SUM(qty) AS totalnya ".
										"FROM nota_detail ".
										"WHERE round(DATE_FORMAT(postdate, '%m')) = '$ubln' ".
										"AND round(DATE_FORMAT(postdate, '%Y')) = '$uthn'");
		$rku31 = mysqli_fetch_assoc($qku31);
		$tku31 = mysqli_num_rows($qku31);
		$ku31_jml = balikin($rku31['totalnya']);
	
		//jika null
		if (empty($ku31_jml))
			{
			$ku31_jml = 0;
			}
	
	
	
	
		//jumlah nominal
		$qku31 = mysqli_query($koneksi, "SELECT SUM(subtotal) AS totalnya ".
										"FROM nota_detail ".
										"WHERE round(DATE_FORMAT(postdate, '%m')) = '$ubln' ".
										"AND round(DATE_FORMAT(postdate, '%Y')) = '$uthn'");
		$rku31 = mysqli_fetch_assoc($qku31);
		$tku31 = mysqli_num_rows($qku31);
		$ku31_total = balikin($rku31['totalnya']);
	
		//jika null
		if (empty($ku31_total))
			{
			$ku31_total = 0;
			}
	

	
	
	
		//jumlah nominal pengeluaran
		$qku32 = mysqli_query($koneksi, "SELECT SUM(nilai) AS totalnya ".
										"FROM uang_keluar ".
										"WHERE round(DATE_FORMAT(per_tgl, '%m')) = '$ubln' ".
										"AND round(DATE_FORMAT(per_tgl, '%Y')) = '$uthn'");
		$rku32 = mysqli_fetch_assoc($qku32);
		$tku32 = mysqli_num_rows($qku32);
		$ku32_total = balikin($rku32['totalnya']);
	
		//jika null
		if (empty($ku32_total))
			{
			$ku32_total = 0;
			}
	
		
		
			
		//sisa
		$jml_sisax = $ku31_total - $ku32_total;

		
	
	
		echo '<tr valign="top" bgcolor="'.$warnaheader.'">
		<td>&nbsp;</td>
		<td align="right"><strong><font color="'.$warnatext.'">'.$tku3x.'</font></strong></td>
		<td align="right"><strong><font color="'.$warnatext.'">'.$ku31_jml.'</font></strong></td>
		<td align="right"><strong><font color="'.$warnatext.'">'.xduit3($ku31_total).'</font></strong></td>
		<td align="right"><strong><font color="'.$warnatext.'">'.xduit3($ku32_total).'</font></strong></td>
		<td align="right"><strong><font color="'.$warnatext.'">'.xduit3($jml_sisax).'</font></strong></td>
		</tr>
		
		
		</tbody>
		  </table>
		  </div>';
	



	
	//isi
	$isi = ob_get_contents();
	ob_end_clean();
	
	

	
	$dompdf->loadHtml($isi);
	
	// Setting ukuran dan orientasi kertas
	$dompdf->setPaper('A4', 'potrait');
	// Rendering dari HTML Ke PDF
	$dompdf->render();
	
	
	$pdf = $dompdf->output();
	
	ob_end_clean();
	
	// Melakukan output file Pdf
	$dompdf->stream('lap-tanggal-'.$ubln.'-'.$uthn.'.pdf');
	
	
	





	exit();
	}	
///////////////////////////////////////////////////////////////////////////////////////////////////////






	
	











//nek batal
if ($_POST['btnBTL'])
	{
	//re-direct
	xloc($filenya);
	exit();
	}





/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////












//isi *START
ob_start();


//require
require("../../template/js/jumpmenu.js");
require("../../template/js/checkall.js");
require("../../template/js/swap.js");
?>
	
	
	  
  <script>
  	$(document).ready(function() {
    $('#table-responsive').dataTable( {
        "scrollX": true
    } );
} );
  </script>
  
<?php
//view //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


echo '<form action="'.$filenya.'" method="post" name="formx">
<div class="row">
	<div class="col-md-12">';
	

	echo "<p>
	Bulan : 
	<br>
	<select name=\"ublnx\" onChange=\"MM_jumpMenu('self',this,0)\" class=\"btn btn-warning\">";
	echo '<option value="'.$ubln.'" selected>'.$arrbln[$ubln].'</option>';
	
	for ($i=1;$i<=12;$i++)
		{
		echo '<option value="'.$filenya.'?ubln='.$i.'">'.$arrbln[$i].'</option>';
		}
				
	echo '</select>';
	


	echo "<select name=\"ublnx\" onChange=\"MM_jumpMenu('self',this,0)\" class=\"btn btn-warning\">";
	echo '<option value="'.$uthn.'" selected>'.$uthn.'</option>';
	
	for ($i=$tahun;$i<=$tahun+1;$i++)
		{
		echo '<option value="'.$filenya.'?ubln='.$ubln.'&uthn='.$i.'">'.$i.'</option>';
		}
				
	echo '</select>
	</p>		
	
	
	
	</div>
</div>

<hr>';


if (empty($ubln))
	{
	echo '<font color="red">
	<h3>BULAN Belum Dipilih...!!</h3>
	</font>';
	}


else if (empty($uthn))
	{
	echo '<font color="red">
	<h3>TAHUN Belum Dipilih...!!</h3>
	</font>';
	}
	
else
	{
	$month = round($ubln);
	$year = round($uthn);
	
	//tanggal terakhir  
	$days_in_month = date('t',mktime(0,0,0,$month,1,$year));
		
	
	echo '<input name="ubln" type="hidden" value="'.$ubln.'">
	<input name="uthn" type="hidden" value="'.$uthn.'">
	<input name="btnEX" type="submit" value="EXPORT PDF >>" class="btn btn-danger">
	
	<div class="table-responsive">          
	<table class="table" border="1">
	<thead>
	
	<tr valign="top" bgcolor="'.$warnaheader.'">
	<td><strong><font color="'.$warnatext.'">TANGGAL</font></strong></td>
	<td width="100"><strong><font color="'.$warnatext.'">BOOKING</font></strong></td>
	<td width="100"><strong><font color="'.$warnatext.'">ITEM TERJUAL</font></strong></td>
	<td width="150"><strong><font color="'.$warnatext.'">SUBTOTAL</font></strong></td>
	<td width="150"><strong><font color="'.$warnatext.'">PENGELUARAN</font></strong></td>
	<td width="150"><strong><font color="'.$warnatext.'">SALDO</font></strong></td>
	</tr>
	</thead>
	<tbody>';
	
	for ($k=1;$k<=$days_in_month;$k++) 
		{
		if ($warna_set ==0)
			{
			$warna = $warna01;
			$warna_set = 1;
			}
		else
			{
			$warna = $warna02;
			$warna_set = 0;
			}


		//jumlah booking
		$qku3 = mysqli_query($koneksi, "SELECT * FROM orderan ".
										"WHERE round(DATE_FORMAT(tgl_pakai, '%d')) = '$k' ".
										"AND round(DATE_FORMAT(tgl_pakai, '%m')) = '$ubln' ".
										"AND round(DATE_FORMAT(tgl_pakai, '%Y')) = '$uthn'");
		$rku3 = mysqli_fetch_assoc($qku3);
		$tku3 = mysqli_num_rows($qku3);
		
		


		//ketahui harinya...
		$tglmasuk2 = "$uthn-$ubln-$k";
		$datenya = strtotime($tglmasuk2);
		$harinya = date('w', $datenya);
		
		$harinya2 = $arrhari[$harinya];
		

		//jumlah item
		$qku31 = mysqli_query($koneksi, "SELECT SUM(qty) AS totalnya ".
										"FROM nota_detail ".
										"WHERE round(DATE_FORMAT(postdate, '%d')) = '$k' ".
										"AND round(DATE_FORMAT(postdate, '%m')) = '$ubln' ".
										"AND round(DATE_FORMAT(postdate, '%Y')) = '$uthn'");
		$rku31 = mysqli_fetch_assoc($qku31);
		$tku31 = mysqli_num_rows($qku31);
		$ku31_jml = balikin($rku31['totalnya']);

		//jika null
		if (empty($ku31_jml))
			{
			$ku31_jml = 0;
			}




		//jumlah nominal
		$qku31 = mysqli_query($koneksi, "SELECT SUM(subtotal) AS totalnya ".
										"FROM nota_detail ".
										"WHERE round(DATE_FORMAT(postdate, '%d')) = '$k' ".
										"AND round(DATE_FORMAT(postdate, '%m')) = '$ubln' ".
										"AND round(DATE_FORMAT(postdate, '%Y')) = '$uthn'");
		$rku31 = mysqli_fetch_assoc($qku31);
		$tku31 = mysqli_num_rows($qku31);
		$ku31_total = balikin($rku31['totalnya']);

		//jika null
		if (empty($ku31_total))
			{
			$ku31_total = 0;
			}




		//jumlah nominal pengeluaran
		$qku32 = mysqli_query($koneksi, "SELECT SUM(nilai) AS totalnya ".
										"FROM uang_keluar ".
										"WHERE round(DATE_FORMAT(per_tgl, '%d')) = '$k' ".
										"AND round(DATE_FORMAT(per_tgl, '%m')) = '$ubln' ".
										"AND round(DATE_FORMAT(per_tgl, '%Y')) = '$uthn'");
		$rku32 = mysqli_fetch_assoc($qku32);
		$tku32 = mysqli_num_rows($qku32);
		$ku32_total = balikin($rku32['totalnya']);

		//jika null
		if (empty($ku32_total))
			{
			$ku32_total = 0;
			}

		
		
			
		//sisa
		$jml_sisax = $ku31_total - $ku32_total;
	
	
		$xyz = md5("$tglmasuk2");
	
	
	
		//kosongkan saldo dulu, trus entri ulang
		mysqli_query($koneksi, "DELETE FROM saldo ".
									"WHERE user_kd = '$kuz_kd' ".
									"AND per_tgl LIKE '$tglmasuk2%'");
									
									
		//insert
		mysqli_query($koneksi, "INSERT INTO saldo(kd, user_kd, user_kode, ".
									"user_nama, perolehan, pengeluaran, ".
									"saldo, per_tgl, postdate) VALUES ".
									"('$xyz', '$kuz_kd', '$kuz_kode', ".
									"'$kuz_nama', '$ku31_total', '$ku32_total', ".
									"'$jml_sisax', '$tglmasuk2', '$today')");



		
		
		echo "<tr valign=\"top\" bgcolor=\"$warna\" onmouseover=\"this.bgColor='$warnaover';\" onmouseout=\"this.bgColor='$warna';\">";
		echo '<td>'.$harinya2.', '.$k.' '.$arrbln[$ubln].' '.$uthn.'</td>
		<td align="right"><font color="red">'.$tku3.'</font></td>
		<td align="right"><font color="red">'.$ku31_jml.'</font></td>
		<td align="right"><font color="red">'.xduit3($ku31_total).'</font></td>
		<td align="right"><font color="red">'.xduit3($ku32_total).'</font></td>
		<td align="right"><font color="red">'.xduit3($jml_sisax).'</font></td>
		</tr>';
		}
	




	//jumlah booking
	$qku3x = mysqli_query($koneksi, "SELECT * FROM orderan ".
									"WHERE round(DATE_FORMAT(tgl_pakai, '%m')) = '$ubln' ".
									"AND round(DATE_FORMAT(tgl_pakai, '%Y')) = '$uthn'");
	$rku3x = mysqli_fetch_assoc($qku3x);
	$tku3x = mysqli_num_rows($qku3x);






	//jumlah item
	$qku31 = mysqli_query($koneksi, "SELECT SUM(qty) AS totalnya ".
									"FROM nota_detail ".
									"WHERE round(DATE_FORMAT(postdate, '%m')) = '$ubln' ".
									"AND round(DATE_FORMAT(postdate, '%Y')) = '$uthn'");
	$rku31 = mysqli_fetch_assoc($qku31);
	$tku31 = mysqli_num_rows($qku31);
	$ku31_jml = balikin($rku31['totalnya']);

	//jika null
	if (empty($ku31_jml))
		{
		$ku31_jml = 0;
		}




	//jumlah nominal
	$qku31 = mysqli_query($koneksi, "SELECT SUM(subtotal) AS totalnya ".
									"FROM nota_detail ".
									"WHERE round(DATE_FORMAT(postdate, '%m')) = '$ubln' ".
									"AND round(DATE_FORMAT(postdate, '%Y')) = '$uthn'");
	$rku31 = mysqli_fetch_assoc($qku31);
	$tku31 = mysqli_num_rows($qku31);
	$ku31_total = balikin($rku31['totalnya']);

	//jika null
	if (empty($ku31_total))
		{
		$ku31_total = 0;
		}





	//jumlah nominal pengeluaran
	$qku32 = mysqli_query($koneksi, "SELECT SUM(nilai) AS totalnya ".
									"FROM uang_keluar ".
									"WHERE round(DATE_FORMAT(per_tgl, '%m')) = '$ubln' ".
									"AND round(DATE_FORMAT(per_tgl, '%Y')) = '$uthn'");
	$rku32 = mysqli_fetch_assoc($qku32);
	$tku32 = mysqli_num_rows($qku32);
	$ku32_total = balikin($rku32['totalnya']);

	//jika null
	if (empty($ku32_total))
		{
		$ku32_total = 0;
		}

	
	
		
	//sisa
	$jml_sisax = $ku31_total - $ku32_total;



	echo '<tr valign="top" bgcolor="'.$warnaheader.'">
	<td>&nbsp;</td>
	<td align="right"><strong><font color="'.$warnatext.'">'.$tku3x.'</font></strong></td>
	<td align="right"><strong><font color="'.$warnatext.'">'.$ku31_jml.'</font></strong></td>
	<td align="right"><strong><font color="'.$warnatext.'">'.xduit3($ku31_total).'</font></strong></td>
	<td align="right"><strong><font color="'.$warnatext.'">'.xduit3($ku32_total).'</font></strong></td>
	<td align="right"><strong><font color="'.$warnatext.'">'.xduit3($jml_sisax).'</font></strong></td>
	</tr>
	
	
	</tbody>
	  </table>
	  </div>';

	}


echo '</form>';



//isi
$isi = ob_get_contents();
ob_end_clean();

require("../../inc/niltpl.php");


//null-kan
xclose($koneksi);
exit();
?>