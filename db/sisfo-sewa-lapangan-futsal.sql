-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 06, 2023 at 02:12 AM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sisfo-sewa-lapangan-futsal`
--

-- --------------------------------------------------------

--
-- Table structure for table `adminx`
--

CREATE TABLE `adminx` (
  `kd` varchar(50) NOT NULL,
  `usernamex` varchar(100) DEFAULT NULL,
  `passwordx` varchar(100) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `adminx`
--

INSERT INTO `adminx` (`kd`, `usernamex`, `passwordx`, `postdate`) VALUES
('21232f297a57a5a743894a0e4a801fc3', 'admin', '21232f297a57a5a743894a0e4a801fc3', '2023-02-06 08:11:00');

-- --------------------------------------------------------

--
-- Table structure for table `item_laris`
--

CREATE TABLE `item_laris` (
  `kd` varchar(50) NOT NULL DEFAULT '',
  `bln` char(2) DEFAULT NULL,
  `thn` varchar(4) DEFAULT NULL,
  `brg_kd` varchar(50) DEFAULT NULL,
  `brg_kode` varchar(100) DEFAULT NULL,
  `brg_nama` varchar(100) DEFAULT NULL,
  `brg_satuan` varchar(100) DEFAULT NULL,
  `brg_harga` varchar(100) DEFAULT NULL,
  `jml` varchar(10) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL,
  `subtotal` varchar(15) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item_laris`
--

INSERT INTO `item_laris` (`kd`, `bln`, `thn`, `brg_kd`, `brg_kode`, `brg_nama`, `brg_satuan`, `brg_harga`, `jml`, `postdate`, `subtotal`) VALUES
('8845c3919e4047713a253446540a65e1', '11', '2022', '99797ea5ce8cc9b4ed11fdaa69476dfa41', '40', 'TEH JAVANA 350 ML', 'PCS', '5000', '2', '2022-11-09 12:26:37', '10000'),
('f43757908c58c984d5fb449505700e52', '10', '2022', '99797ea5ce8cc9b4ed11fdaa69476dfa27', '26', 'AIR MINERAL PRIMA 600 ML', 'PCS', '3000', '3', '2022-11-07 22:11:43', NULL),
('23e1578b52fd29fcc1b7b6e575f554a5', '10', '2022', '99797ea5ce8cc9b4ed11fdaa69476dfa20', '19', 'SEWA LAPANGAN MERAH MALAM WEEK END', 'JAM', '140000', '2', '2022-11-07 22:11:43', NULL),
('003744bf622fedc8c679896d558c29ca', '11', '2022', '99797ea5ce8cc9b4ed11fdaa69476dfa27', '26', 'AIR MINERAL PRIMA 600 ML', 'PCS', '3000', '17', '2022-11-09 12:26:37', '51000'),
('71d7f97d5e40d3c1c6bb4b851607086f', '11', '2022', '99797ea5ce8cc9b4ed11fdaa69476dfa14', '13', 'HYDRO COCO 330 ML', 'PCS', '10000', '23', '2022-11-09 12:26:37', '230000'),
('9966845e359e2686f1f3e48d736c5cdb', '11', '2022', '99797ea5ce8cc9b4ed11fdaa69476dfa29', '28', 'ROKOK ECERAN', 'PCS', '2500', '2', '2022-11-09 12:26:37', '5000'),
('9df4dcce0e5c145bf18e7dfc196cca83', '11', '2022', '99797ea5ce8cc9b4ed11fdaa69476dfa36', '35', 'SUSU ULTRA MILK', 'PCS', '7000', '1', '2022-11-09 12:26:37', '7000'),
('acd019c31253a3226ef9c18ebd02b698', '11', '2022', '99797ea5ce8cc9b4ed11fdaa69476dfa42', '41', 'TEH PUCUK HARUM', 'PCS', '5000', '5', '2022-11-09 12:26:37', '25000'),
('86e459e9e03412fc62c5e8ce4295a07c', '11', '2022', '99797ea5ce8cc9b4ed11fdaa69476dfa38', '37', 'TEBS BOTOL PLASTIK 500 ML', 'PCS', '7000', '4', '2022-11-09 12:26:37', '28000'),
('4bf85b9a14e829388a28cbdf88ad1d0a', '11', '2022', '99797ea5ce8cc9b4ed11fdaa69476dfa33', '32', 'SARI KACANG HIJAU', 'PCS', '5000', '1', '2022-11-09 12:26:37', '5000'),
('28e92c4aa9214d2e5ae4476cd0a29feb', '11', '2022', '99797ea5ce8cc9b4ed11fdaa69476dfa13', '12', 'HYDRO COCO 250 ML', 'PCS', '7500', '3', '2022-11-09 12:26:37', '22500'),
('859454f4c5bf6d9147cb43d2ef368085', '11', '2022', '99797ea5ce8cc9b4ed11fdaa69476dfa23', '22', 'MIZONE', 'PCS', '6000', '1', '2022-11-09 12:26:37', '6000'),
('63aec812f23bb4d81c1f715f8ec86f61', '11', '2022', '99797ea5ce8cc9b4ed11fdaa69476dfa25', '24', 'POCARI SWEAT', 'PCS', '8000', '3', '2022-11-09 12:26:37', '24000'),
('6c71cd20e8d8d8759c5fa4f1bc88373b', '11', '2022', '99797ea5ce8cc9b4ed11fdaa69476dfa28', '27', 'PULPY ORANGE', 'PCS', '5000', '2', '2022-11-09 12:26:37', '10000'),
('088be5c432d6dade1c35af6e82741901', '11', '2022', '99797ea5ce8cc9b4ed11fdaa69476dfa24', '23', 'ORANGE WATER', 'PCS', '10000', '3', '2022-11-09 12:26:37', '30000'),
('edf968fc50c3c82069c38eaa5e0e5b9c', '11', '2022', '99797ea5ce8cc9b4ed11fdaa69476dfa9', '8', 'SEWA LAPANGAN HIJAU MALAM WEEK DAY', 'JAM', '120000', '7', '2022-11-09 12:26:37', '840000'),
('32f951f1f78f9134c03ab6ab628d71fc', '11', '2022', '99797ea5ce8cc9b4ed11fdaa69476dfa6', '5', 'FLORIDINA', 'PCS', '5000', '3', '2022-11-09 12:26:37', '15000'),
('78bf7e01324dc712769ec9f1d239a119', '11', '2022', '99797ea5ce8cc9b4ed11fdaa69476dfa31', '30', 'ROKOK GUDANG GARAM SURYA 12', 'PCS', '22000', '1', '2022-11-09 12:26:37', '22000'),
('b329d146e0ddcb6fa1c40f32367e6082', '11', '2022', '99797ea5ce8cc9b4ed11fdaa69476dfa30', '29', 'ROKOK GUDANG GARAM SIGNATURE', 'PCS', '20000', '1', '2022-11-09 12:26:37', '20000'),
('b561536e0c1290cb43eb2d7194ed2e19', '11', '2022', '99797ea5ce8cc9b4ed11fdaa69476dfa19', '18', 'SEWA LAPANGAN MERAH MALAM WEEK DAY', 'JAM', '120000', '5', '2022-11-09 12:26:37', '600000'),
('1bc83111008697f361d1e3036ce04b86', '12', '2022', '99797ea5ce8cc9b4ed11fdaa69476dfa31', '30', 'ROKOK GUDANG GARAM SURYA 12', 'PCS', '23000', '1', '2022-12-13 09:52:04', '23000'),
('1e867af8962babe7251ff6de2e64c03e', '12', '2022', '99797ea5ce8cc9b4ed11fdaa69476dfa35', '34', 'SUSU MILKU', 'PCS', '5000', '2', '2022-12-13 09:52:04', '10000'),
('b3c8489d50ad0ee26209f4cc0d8ead28', '12', '2022', '99797ea5ce8cc9b4ed11fdaa69476dfa12', '11', 'SEWA LAPANGAN HIJAU PAGI WEEK END', 'JAM', '100000', '6', '2022-12-13 09:52:04', '600000'),
('6481d56eb3e165732cc1f0a5c571a190', '12', '2022', '99797ea5ce8cc9b4ed11fdaa69476dfa39', '38', 'TEH BOTOL SOSRO KACA', 'PCS', '3000', '11', '2022-12-13 09:52:04', '33000'),
('8c195dc8ea923e22be11d22a6a500500', '12', '2022', '99797ea5ce8cc9b4ed11fdaa69476dfa30', '29', 'ROKOK GUDANG GARAM SIGNATURE', 'PCS', '21000', '1', '2022-12-13 09:52:04', '21000'),
('5f000d2e0211645ae64e458fcdc98605', '12', '2022', '99797ea5ce8cc9b4ed11fdaa69476dfa7', '6', 'FRUIT TEA SOSRO KACA 235 ML', 'PCS', '3000', '9', '2022-12-13 09:52:04', '27000'),
('1485c557abe3d7de55fc7d9378653f5d', '12', '2022', '99797ea5ce8cc9b4ed11fdaa69476dfa24', '23', 'ORANGE WATER', 'PCS', '10000', '1', '2022-12-13 09:52:04', '10000'),
('9310543ecb3e4e2313ccdb71deeaf100', '12', '2022', '99797ea5ce8cc9b4ed11fdaa69476dfa6', '5', 'FLORIDINA', 'PCS', '5000', '3', '2022-12-13 09:52:04', '15000'),
('cd02371c6b261df7129ae62c018579ec', '12', '2022', '5a999455e84eca294d9dd7b616f04b1b', '8997212800035', 'YUZU ISOTONIC 350ML', 'PCS', '5000', '1', '2022-12-13 09:52:04', '5000'),
('88a1c6877973c37d7f6ae9e431e8bff3', '12', '2022', '99797ea5ce8cc9b4ed11fdaa69476dfa34', '33', 'SNACK', 'PCS', '10000', '2', '2022-12-13 09:52:04', '20000');

-- --------------------------------------------------------

--
-- Table structure for table `m_brg`
--

CREATE TABLE `m_brg` (
  `kd` varchar(50) NOT NULL DEFAULT '',
  `kategori` varchar(100) DEFAULT NULL,
  `satuan` varchar(100) DEFAULT NULL,
  `kode` varchar(100) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL,
  `hrg_jual` varchar(15) DEFAULT NULL,
  `jml_stock` varchar(15) DEFAULT NULL,
  `barkode` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_brg`
--

INSERT INTO `m_brg` (`kd`, `kategori`, `satuan`, `kode`, `nama`, `postdate`, `hrg_jual`, `jml_stock`, `barkode`) VALUES
('99797ea5ce8cc9b4ed11fdaa69476dfa3', 'MNM', 'PCS', '2', 'AMUNIZER C1000 BOTOL 140 ML', '2023-01-14 12:17:37', '9000', '9', '2'),
('99797ea5ce8cc9b4ed11fdaa69476dfa4', 'MNM', 'PCS', '3', 'BIR BINTANG 0xpersenx', '2023-01-14 12:17:45', '15000', '4', '3'),
('99797ea5ce8cc9b4ed11fdaa69476dfa5', 'SEWA', 'JAM', '4', 'DP LAPANGAN', '2022-10-31 14:21:59', '0', '0', '4'),
('99797ea5ce8cc9b4ed11fdaa69476dfa6', 'MNM', 'PCS', '5', 'FLORIDINA', '2023-01-14 12:18:38', '5000', '55', '5'),
('99797ea5ce8cc9b4ed11fdaa69476dfa7', 'MNM', 'PCS', '6', 'FRUIT TEA SOSRO KACA 235 ML', '2023-01-14 12:18:53', '3000', '33', '6'),
('99797ea5ce8cc9b4ed11fdaa69476dfa8', 'MNM', 'PCS', '7', 'FRUIT TEA SOSRO PLATIK 350 ML', '2023-01-14 12:19:22', '5000', '188', '7'),
('79137a3bd7bb34d9c8cb5003f94cb41a', 'Lapangan', 'JAM', '111111', 'SEWA LAPANGAN MERAH MALAM xgmringx NON FREE MINUM', '2023-01-14 12:16:51', '140000', '-2', NULL),
('2d3b97e3595a05cea007f54469961af7', 'Lapangan', 'JAM', '11111', 'SEWA LAPANGAN HIJAU MALAM xgmringx NON FREE MINUM', '2023-01-14 12:10:47', '140000', '-3', NULL),
('99797ea5ce8cc9b4ed11fdaa69476dfa13', 'MNM', 'PCS', '12', 'HYDRO COCO 250 ML', '2023-01-14 12:20:27', '7500', '329', '12'),
('99797ea5ce8cc9b4ed11fdaa69476dfa14', 'MNM', 'PCS', '13', 'HYDRO COCO 330 ML', '2023-01-14 12:20:40', '10000', '268', '13'),
('99797ea5ce8cc9b4ed11fdaa69476dfa15', 'MNM', 'PCS', '14', 'AMUNIZER C 1000 xstrix 140 ML', '2023-01-14 12:21:07', '9000', '9', '14'),
('99797ea5ce8cc9b4ed11fdaa69476dfa16', 'MNM', 'PCS', '15', 'ISO PLUS', '2023-01-14 12:21:37', '5000', '28', '15'),
('99797ea5ce8cc9b4ed11fdaa69476dfa17', 'MNM', 'PCS', '16', 'KOPI GOLDA', '2023-01-14 12:21:51', '5000', '43', '16'),
('99797ea5ce8cc9b4ed11fdaa69476dfa18', 'MNM', 'PCS', '17', 'KOPI KOPIKO', '2023-01-14 12:22:19', '7000', '10', '17'),
('cdef4e9ddf36c2f4663d786d9e6f0d14', 'Lapangan', 'JAM', '111', 'SEWA LAPANGAN HIJAU MALAM xgmringx FREE MINUM', '2023-01-14 12:14:39', '160000', '0', '111'),
('5d15012627bcc870c1c2d120d23d84eb', 'Lapangan', 'JAM', '11', 'SEWA LAPANGAN MERAH PAGI', '2023-01-14 12:07:17', '110000', '-2', NULL),
('2047a24fc4df502f655903e12a6d1254', 'Lapangan', 'JAM', '1', 'SEWA LAPANGAN HIJAU PAGI', '2023-01-14 12:03:47', '110000', '-6', NULL),
('99797ea5ce8cc9b4ed11fdaa69476dfa23', 'MNM', 'PCS', '22', 'MIZONE', '2023-01-14 12:22:38', '6000', '56', '22'),
('99797ea5ce8cc9b4ed11fdaa69476dfa24', 'MNM', 'PCS', '23', 'ORANGE WATER', '2023-01-14 12:22:53', '10000', '25', '23'),
('99797ea5ce8cc9b4ed11fdaa69476dfa25', 'MNM', 'PCS', '24', 'POCARI SWEAT', '2023-01-14 12:23:07', '8000', '109', '24'),
('99797ea5ce8cc9b4ed11fdaa69476dfa26', 'MNM', 'PCS', '25', 'AIR MINERAL PRIMA 1500 ML', '2023-01-14 12:17:17', '5000', '1259', '25'),
('99797ea5ce8cc9b4ed11fdaa69476dfa27', 'MNM', 'PCS', '26', 'AIR MINERAL PRIMA 600 ML', '2023-01-14 12:17:31', '3000', '2024', '26'),
('99797ea5ce8cc9b4ed11fdaa69476dfa28', 'MNM', 'PCS', '27', 'PULPY ORANGE', '2023-01-14 12:23:20', '5000', '26', '27'),
('99797ea5ce8cc9b4ed11fdaa69476dfa29', 'ROKOK', 'PCS', '28', 'ROKOK ECERAN', '2023-01-14 12:23:35', '2500', '36', '28'),
('99797ea5ce8cc9b4ed11fdaa69476dfa30', 'MNM', 'PCS', '29', 'ROKOK GUDANG GARAM SIGNATURE', '2023-01-14 12:23:48', '23000', '11', '29'),
('99797ea5ce8cc9b4ed11fdaa69476dfa31', 'Rokok', 'PCS', '30', 'ROKOK GUDANG GARAM SURYA 12', '2023-01-14 12:24:07', '25000', '22', '30'),
('99797ea5ce8cc9b4ed11fdaa69476dfa32', 'Rokok', 'PCS', '31', 'ROKOK GUDANG GARAM SURYA 16', '2023-01-14 12:24:37', '31000', '7', '31'),
('99797ea5ce8cc9b4ed11fdaa69476dfa33', 'MNM', 'PCS', '32', 'SARI KACANG HIJAU', '2023-01-14 12:24:53', '5000', '40', '32'),
('99797ea5ce8cc9b4ed11fdaa69476dfa34', 'MNM', 'PCS', '33', 'SNACK', '2023-01-14 12:25:08', '10000', '12', '33'),
('99797ea5ce8cc9b4ed11fdaa69476dfa35', 'MNM', 'PCS', '34', 'SUSU MILKU', '2023-01-14 12:25:27', '5000', '35', '34'),
('99797ea5ce8cc9b4ed11fdaa69476dfa36', 'MNM', 'PCS', '35', 'SUSU ULTRA MILK', '2023-01-14 12:26:10', '7000', '37', '35'),
('99797ea5ce8cc9b4ed11fdaa69476dfa37', 'MNM', 'PCS', '36', 'TEBS BOTOL KACA', '2023-01-14 12:26:30', '4000', '50', '36'),
('99797ea5ce8cc9b4ed11fdaa69476dfa38', 'MNM', 'PCS', '37', 'TEBS BOTOL PLASTIK 500 ML', '2023-01-14 12:25:51', '7000', '33', '37'),
('99797ea5ce8cc9b4ed11fdaa69476dfa39', 'MNM', 'PCS', '38', 'TEH BOTOL SOSRO KACA', '2023-01-14 12:27:24', '3000', '0', '38'),
('99797ea5ce8cc9b4ed11fdaa69476dfa40', 'MNM', 'PCS', '39', 'TEH BOTOL SOSRO PLASTIK 350 ML', '2023-01-14 12:27:39', '5000', '201', '39'),
('99797ea5ce8cc9b4ed11fdaa69476dfa41', 'MNM', 'PCS', '40', 'TEH JAVANA 350 ML', '2023-01-14 12:27:51', '5000', '38', '40'),
('99797ea5ce8cc9b4ed11fdaa69476dfa42', 'MNM', 'PCS', '41', 'TEH PUCUK HARUM', '2023-01-14 12:29:12', '5000', '13', '41'),
('99797ea5ce8cc9b4ed11fdaa69476dfa43', 'MNM', 'PCS', '42', 'YOU C 1000', '2023-01-14 12:29:24', '9000', '0', '42'),
('a16e94b812c206f5d2f6d97d2bca0581', 'Snack', 'PCS', '43', 'BISKUIT GABIN', '2023-01-14 12:19:54', '5000', '6', '43'),
('5a999455e84eca294d9dd7b616f04b1b', 'Minuman', 'PCS', '8997212800035', 'YUZU ISOTONIC 350ML', '2023-01-14 12:29:36', '5000', '64', '8997212800035'),
('b9d0c34886fe203cc9496ef4928bb95e', 'Lapangan', 'JAM', '1111', 'SEWA LAPANGAN MERAH MALAM xgmringx FREE MINUM', '2023-01-14 12:15:17', '160000', '0', '1111'),
('c7a28783c9e8b19a4e48347be7c473ef', 'Minuman', 'PCS', '8996006858245', 'TEBS BOTOL PLASTIK 300ML', '2023-01-14 12:27:01', '5000', '188', '8996006858245'),
('403b451ba8bc581c0935dc7c0d92516e', 'Snack', 'PCS', '23464839018716', 'Biskuit', '2023-01-14 12:18:07', '5000', '1', '23464839018716');

-- --------------------------------------------------------

--
-- Table structure for table `m_brg_masuk`
--

CREATE TABLE `m_brg_masuk` (
  `kd` varchar(50) NOT NULL DEFAULT '',
  `tgl_masuk` date DEFAULT NULL,
  `brg_kd` varchar(50) DEFAULT NULL,
  `kategori` varchar(100) DEFAULT NULL,
  `satuan` varchar(100) DEFAULT NULL,
  `kode` varchar(100) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL,
  `hrg_jual` varchar(15) DEFAULT NULL,
  `jml_stock` varchar(15) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_brg_masuk`
--

INSERT INTO `m_brg_masuk` (`kd`, `tgl_masuk`, `brg_kd`, `kategori`, `satuan`, `kode`, `nama`, `postdate`, `hrg_jual`, `jml_stock`) VALUES
('7a4393641ab4ee7ac7a7f540617b278a', '2022-11-08', '99797ea5ce8cc9b4ed11fdaa69476dfa30', 'MNM', 'PCS', '29', 'ROKOK GUDANG GARAM SIGNATURE', '2022-11-08 14:11:24', '20000', '10'),
('6f9f9c8bc8e5e9738db8f874e2bb2b62', '2022-11-08', '99797ea5ce8cc9b4ed11fdaa69476dfa31', 'MNM', 'PCS', '30', 'ROKOK GUDANG GARAM SURYA 12', '2022-11-08 14:11:44', '22000', '40'),
('9025ad9ec3dc74d093f61fdc6a35ed7f', '2022-11-08', '99797ea5ce8cc9b4ed11fdaa69476dfa32', 'MNM', 'PCS', '31', 'ROKOK GUDANG GARAM SURYA 16', '2022-11-08 14:12:00', '28000', '10');

-- --------------------------------------------------------

--
-- Table structure for table `m_kategori`
--

CREATE TABLE `m_kategori` (
  `kd` varchar(50) NOT NULL DEFAULT '',
  `nama` varchar(100) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_kategori`
--

INSERT INTO `m_kategori` (`kd`, `nama`, `postdate`) VALUES
('d09f48a068497251026fe69139ff6f6a', 'Makanan', '2022-09-29 10:09:14'),
('5e87ded0581d6cde432ebc9cf16c923b', 'Minuman', '2022-10-11 14:14:31'),
('f23cca20bf7d24d672b4e8c6aefbc47e', 'Snack', '2022-10-11 14:16:32'),
('a512f0ef8208d08047e1a0886ce46b8a', 'Rokok', '2022-10-11 14:16:22'),
('280e52c4cb556f794f032f6d99ed12b0', 'Hot Drink', '2022-10-11 14:19:18'),
('e3042d1161c663e59bf9985b0e8895f3', 'Bola', '2022-11-18 13:49:15'),
('e3db12f946ef62855e13ead4d25dd024', 'Lapangan', '2023-01-14 11:18:53');

-- --------------------------------------------------------

--
-- Table structure for table `m_pelanggan`
--

CREATE TABLE `m_pelanggan` (
  `kd` varchar(50) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `alamat` varchar(100) DEFAULT NULL,
  `nama_team` varchar(100) DEFAULT NULL,
  `telp` varchar(100) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL,
  `jml_booking` varchar(10) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_pelanggan`
--

INSERT INTO `m_pelanggan` (`kd`, `nama`, `alamat`, `nama_team`, `telp`, `postdate`, `jml_booking`) VALUES
('d3d239696051d3d6b09bd4b0ab5048df', 'Jodevan', 'Semarang', 'Jodevan', '089530933761', '2022-10-31 17:10:46', '1'),
('168e3899d5c1f5bdd74c19abf3698f05', 'Silvi', 'Semarang', 'Wkwk Fc', '085785122887', '2022-10-31 18:27:37', '1'),
('387d6df87d7ac1afac347264bd7dd4d8', 'Benjot', 'Semarang', 'Palebon Fc', '085785122887', '2022-10-31 18:28:52', '1'),
('3b83c7d45717ff3edc9fe62a64e4568f', 'Nanang imam Fadjri', 'Semarang', 'Jfc', '082138912563', '2022-11-01 13:55:53', '1'),
('5cccf932ca402acb0a2e940a7fb7f04b', 'Pjkr', 'Semarang', 'Pjkr', '082138912563', '2022-11-01 21:29:04', '2'),
('b37a9f8f6f7e7688acff13b4373f7e43', 'Join fc', 'Semarang', 'Join fv', '088', '2022-11-02 17:01:23', '1'),
('b79020be40bd22e05c12e90a93fae9b6', 'Naufal', 'Semarang', 'Naufal', '088', '2022-11-02 20:51:15', '1'),
('3bd788f4e13c4fdb483228ee0565d244', 'Ran', 'Semarang', 'Ran', '088', '2022-11-03 17:24:49', '1'),
('fc48cf7d7025d2a0050617ebcae05a3d', 'Smansa', 'Semarang', 'Smansa', '082138912563', '2022-11-04 14:33:37', '1'),
('a87c7c19072731be84e0dd4381d9f0c4', 'Ibnu', 'Semarang', 'Ibnu', '089531086820', '2022-11-04 14:37:02', '1'),
('4b55072ae0eae841d59b5a1b17808675', 'Fik unisulla', 'Semarang', 'Fik unisulla', '081953772771', '2022-11-04 14:40:01', '1'),
('4724068562f9b5668b77b86fe76e982f', 'Telkom', 'Semarang', 'Telkom', '082138912563', '2022-11-04 14:43:37', '2'),
('033c462340c93e7cc10236c915179279', 'Armatim', 'Semarang', 'Armatim', '082138912563', '2022-11-04 14:45:29', '1'),
('a3499c98f670010f2f2398c564318698', 'Moreno', 'Semarang', 'Moreno', '082138912563', '2022-11-04 14:47:36', '2'),
('4b77c909de45af3ad02655685127ba95', 'Fauzan', 'Semarang', 'Fauzan', '082138912563', '2022-11-04 14:53:10', '1'),
('8492dc67e010b9a75dfa3b39a4a47590', '6', '6', '6', '6', '2022-11-05 17:10:55', '1'),
('3c0386e48976af9507a8bfc23f31e57d', 'Angin laut', 'Semarang', 'Angin laut', '082138912563', '2022-11-05 17:40:27', '2'),
('685d9015979c3a1bdba0820b903208d5', 'Siliwangi', 'Semarang', 'Siliwangi', '082138912563', '2022-11-05 17:43:26', '1'),
('5bde16a25d4f6da4b3bb08fc5ae7af4d', 'Pirel fc', 'Semarang', 'Pirel fc', '082138912563', '2022-11-05 17:57:34', '2'),
('77473e5602d541763ae947f68026d6f5', 'Atlantik', 'Semarang', 'Atlantik', '082138912563', '2022-11-05 22:02:18', '1'),
('7c726696c617fc27d10baeb170a06607', 'Devandra', 'Semarang', 'Devandra', '082138912563', '2022-11-07 17:07:17', '1'),
('49468ad1e459b024ec01242a6b54d2d7', 'USM', 'Semarang', 'Usm', '0811111', '2022-11-07 17:17:40', '2'),
('242f888da1b55a67fb956afc1b877874', 'Indonesia power', 'Semarang', 'Indonesia power', '082138912563', '2022-11-07 17:23:38', '1'),
('8550b226f1671fa0b918176d9b5d083a', 'Pm2', 'Semarang', 'Pm2', '082138912563', '2022-11-07 17:25:40', '1'),
('67de860d5ba97dde460cb158c683e8c5', 'Aziz', 'Semarang', 'Aziz', '082138912563', '2022-11-07 17:26:47', '2'),
('766cf33e003884d4d23bfbf64887d33f', 'Pambudi', 'Semarang', 'Pambudi', '082138912563', '2022-11-07 17:30:25', '1'),
('20ab28bd41a71d1c6887f960aa0f6ecd', 'Wawan', 'Semarang', 'Wawan', '082138912563', '2022-11-07 17:33:17', '1'),
('baf0266e12caabceacceb15439f3ae7c', 'Amni', 'Semarang', 'Amni', '082138912563', '2022-11-07 17:38:29', '1'),
('f756894c228fe0c1e4986404da9d5bf9', 'Lumpia mbak lin', 'Semarang', 'Lumpia mbak lin', '082138912563', '2022-11-07 17:39:14', '1'),
('7b9f042ad9f7ceb9bf7ea922c596d83f', 'Arjuna', 'Semarang', 'Arjuna', '082138912563', '2022-11-07 17:40:27', '1'),
('1d8dac03f5bdc416eaff09d16127f364', 'Tehnik sipil', 'Semarang', 'Tehnik sipil', '082138912563', '2022-11-07 17:44:59', '2'),
('d223fa2001dffde19212c33008422ce8', 'Ikrom', 'Semarang', 'Ikrom', '082138912563', '2022-11-07 17:47:38', '1'),
('fcf29da862812e941d6d572d36bedba8', 'Haryono bj', 'Semarang', 'Bank Jateng', '082138912563', '2022-11-07 17:49:20', '1'),
('7235ae1f86e4ab06bba0cad87d78d4f2', 'Ready fc', 'Semarang', 'Ready fc', '082138912563', '2022-11-07 17:51:39', '1'),
('57e0546f9cb369c85de374e2e4ca37ae', 'Kofu', 'Semarang', 'Kofu', '082138912563', '2022-11-07 17:53:55', '1'),
('6241fc7ec4009507880f47c1ca0f2e7c', 'Gyr', 'Semarang', 'Gyr', '082138912563', '2022-11-07 17:55:17', '1'),
('d877c316c97932287d18c2a3f536bf99', 'Gondrong', 'Semarang', 'Gondrong', '082138912563', '2022-11-07 17:58:20', '1'),
('ebf5b076dd826e895d919d44205bd3c9', 'Upgris', 'Semarang', 'Upgris', '082138912563', '2022-11-07 18:00:05', '1'),
('7724c68e69142aad1b1751802dc731cb', 'Multindo', 'Semarang', 'Multindo', '082138912563', '2022-11-07 18:15:41', '1'),
('bb77bfbceb4428762cae3d159dfd975a', 'Barito fc', 'Semarang', 'Barito fc', '082138912563', '2022-11-07 18:18:13', '2'),
('b5098b12959e731109214f6ac3f63788', 'Wahyu', 'Semarang', 'Wahyu', '082138912563', '2022-11-07 18:22:55', '1'),
('51ad36f0edad3d29d503673ef3a27eb5', 'Hjma', 'Semarang', 'Hjma', '082138912563', '2022-11-07 18:25:42', '1'),
('6580d47b9b8d9fc17dbb8d4009d54e94', 'Dwi Andi', 'Semarang', 'Dwi andi', '082138912563', '2022-11-07 18:28:59', '2'),
('22d6a0eee7ff3c1a0cc65fd292f32149', 'Iqbal', 'Semarang', 'Iqbaal', '082138912563', '2022-11-07 18:37:26', '1'),
('2a51dd70c8458cf7b665c5e7f8a32359', 'Amri', 'Semarang', 'Amri', '082138912563', '2022-11-07 21:08:28', '2'),
('0ba6a70d20f250b4bcb2220c1ed3af17', 'Teknik Sipil', 'Semarang', 'Teknik Sipil', '081325608361', '2022-11-08 09:32:19', '1'),
('c5b69e058845b942bf7675e52d88898d', 'Yansyah', 'Semarang', 'Yansyah', '082138912563', '2022-11-08 16:46:36', '1'),
('45f6b1fecfb782e2518b3d970003178a', 'Febbi', 'Semarang', 'Febbi', '082138912563', '2022-11-08 16:48:11', '1'),
('ff116ada7979d6612621375af7a2bb27', 'Adella', 'Semarang', 'Adella', '082138912563', '2022-11-08 17:02:09', '1'),
('7c292ac6704844737b4639b95291bc47', 'Brian', 'Semarang', 'Brian', '082138912563', '2022-11-08 17:37:55', '1'),
('4dad06c1e523fc387129514cea1ac2e9', 'Pasukan rob', 'Semarang', 'Pasukan rob', '082138912563', '2022-11-08 17:39:36', '1'),
('bc19ec917c4a3feae2d358f0eeda0dfe', 'Gavin', 'Semarang', 'Gavin', '082138912563', '2022-11-09 16:30:05', '1'),
('4ba36e534fc713b1347c2a621c4f6764', 'Irfandi', 'Semarang', 'Irfandi', '082138912563', '2022-11-09 16:32:54', '1'),
('1e2b13db100eae0a036c9198656ff3c9', 'Abdul', 'Semarang', 'Abdul', '082138912563', '2022-11-10 17:30:37', '1'),
('944b5d113cac9b564c4fa900b68131b6', 'Ilham agum', 'Pt pel', 'Pt pel', '+62 895xstrix3604xstrix81709', '2023-01-07 19:38:35', '2'),
('ed43d75dced775f0370a00934f1b077c', 'Aurelio', 'Semarang', 'Aurelio', '089676760663', '2023-01-12 15:28:25', '3');

-- --------------------------------------------------------

--
-- Table structure for table `m_satuan`
--

CREATE TABLE `m_satuan` (
  `kd` varchar(50) NOT NULL DEFAULT '',
  `nama` varchar(100) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `m_satuan`
--

INSERT INTO `m_satuan` (`kd`, `nama`, `postdate`) VALUES
('e033c11c4ca62908fd15df3eb523c7d8', 'PCS', '2022-08-22 23:30:28'),
('938048d950293d883a02a80dedf4b3ed', 'Karton', '2022-10-11 14:41:55'),
('cd27fd5c288666f36d7dc25060ea86b4', 'JAM', '2023-01-14 11:21:02');

-- --------------------------------------------------------

--
-- Table structure for table `m_tempat`
--

CREATE TABLE `m_tempat` (
  `kd` varchar(50) NOT NULL,
  `kode` varchar(100) DEFAULT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `harga_weekdays_pagi` varchar(15) DEFAULT NULL,
  `harga_weekdays_sore` varchar(15) DEFAULT NULL,
  `harga_weekend_pagi` varchar(15) DEFAULT NULL,
  `harga_weekend_sore` varchar(15) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL,
  `filex` longtext DEFAULT NULL,
  `ket` longtext DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_tempat`
--

INSERT INTO `m_tempat` (`kd`, `kode`, `nama`, `harga_weekdays_pagi`, `harga_weekdays_sore`, `harga_weekend_pagi`, `harga_weekend_sore`, `postdate`, `filex`, `ket`) VALUES
('24aed6f88e985ec20a7637213b508e3b', 'LAP. MERAH', 'LAPANGAN MERAH', '110000', '160000', '110000', '160000', '2023-01-14 11:14:52', '24aed6f88e985ec20a7637213b508e3b-1.png', 'Lapangan Interlock'),
('648817e492a11eb4a83217851002aaed', 'LAP. HIJAU', 'LAPANGAN HIJAU', '110000', '160000', '110000', '160000', '2023-01-14 11:14:26', '648817e492a11eb4a83217851002aaed-1.png', 'Lapangan Interlock');

-- --------------------------------------------------------

--
-- Table structure for table `nomerku`
--

CREATE TABLE `nomerku` (
  `noid` int(50) NOT NULL,
  `nota_kd` varchar(50) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `team` varchar(100) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nomerku`
--

INSERT INTO `nomerku` (`noid`, `nota_kd`, `nama`, `team`, `postdate`) VALUES
(4302, '493e111078bd4d29bd9b92157fb31cfc', '9', '8', '2022-10-19 22:23:49'),
(4309, '70aa117ed42c0389827b6a3eca75ed72', 'ok', 'ok', '2022-10-20 10:52:55'),
(4310, '89c1ac04c29f97d2bfc91f41693c4278', 'uu', 'uu', '2022-10-20 10:53:13'),
(4311, '7758bca6de889ee3848cc870a864a660', 'agus muhajirr', 'agus123', '2022-10-20 14:01:40'),
(4312, 'c14af74d006ae4c173d9595b054c7e48', 'siti', 'sitigirl', '2022-10-20 14:02:34'),
(4313, '543c066d84199dd5c9e9379a12e800d1', 'lukman', 'kali771', '2022-10-20 14:07:39'),
(4314, '61ffa6e617518bdc4d40c8b819d6c223', 'Nanang', 'Glory', '2022-10-20 14:37:23'),
(4315, 'bda65e6b4c3b99c324bd6df62127cf41', 'Silvi', 'Palebon', '2022-10-20 14:53:05'),
(4316, '01b3a7a2eb110abfbb9f70fd34baa799', 'Tanto', 'Sumber Sejahtera', '2022-10-20 14:59:29'),
(4317, '00a8cc60f81553a6e0c351888150134b', 'Kedik fc', 'Kedik fc', '2022-10-20 20:14:19'),
(4318, 'b24da44433fdf94f70c7c981f6691885', 'Sendang guwo fc', 'Sendang guwo fc', '2022-10-20 20:30:56'),
(4319, 'd1666346281c52de59b830dd0ef279af', 'Sendang guwo fc', 'Sendang guwo fc', '2022-10-20 20:32:14'),
(4320, 'b64758363b4ca4194d42da05ba10af6d', 'Rehan', 'Rehan', '2022-10-20 20:44:02'),
(4321, '486ccafacb87d3d8242de04ff7c361e4', 'Rehan', 'Rehan', '2022-10-20 20:44:26'),
(4322, '3f4dc5ee8139d7e2e55ec96f86b2720e', 'tanto', 'tanto fc', '2022-10-21 12:30:43');

-- --------------------------------------------------------

--
-- Table structure for table `nomerku2210`
--

CREATE TABLE `nomerku2210` (
  `noid` int(50) NOT NULL,
  `nota_kd` varchar(50) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `team` varchar(100) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nomerku2210`
--

INSERT INTO `nomerku2210` (`noid`, `nota_kd`, `nama`, `team`, `postdate`) VALUES
(1, '767ab5cb3f6fc3d480ef13989e0be275', '77', '77', '2022-10-21 22:20:18'),
(2, '54296f4cac378c31801a719fee19a54d', '89', '89', '2022-10-21 22:20:35'),
(3, 'a788da95beb174345eb8756639c07f73', 'kk', 'kk', '2022-10-21 22:21:30'),
(4, '7df7c95c29168ed5a626b60533022b83', '9', '9', '2022-10-23 22:46:10'),
(5, '9a57d0a5b45ffd32b9ef4e3a0e6f9b1a', '6', '6', '2022-10-23 22:46:46'),
(6, 'e017362e8a2872a1e9e6295ba8c1f695', 'uu', 'uu', '2022-10-24 11:04:42'),
(7, '1ec21788f6502986dcd2d5b18f1b02e8', 'y', 'y', '2022-10-24 11:05:19'),
(8, 'e74118f6539600e591d9908a17264e27', 'Galaxy', 'Galaxy', '2022-10-24 18:22:18'),
(9, '4d08109215db03d71fbfca49f7f5413d', 'Nanang', 'Palebon', '2022-10-24 18:33:36'),
(10, 'eb1c8c079269ebd70a209047d1203f39', 'Sendang guwo fc', 'Glory', '2022-10-24 18:49:20'),
(11, '3f51c4670854ac0adaf90f9c54d1e1b4', 'tanto', 'tanto', '2022-10-24 22:12:49'),
(12, '333aa2940360e51e12ada987f71ab110', 'tanto', 'tanto', '2022-10-24 22:22:12'),
(13, 'f62bd84ec35131dbdad8f85b288649ef', 'adi', 'adi', '2022-10-24 22:24:23'),
(14, 'd78b7ea909f3e09029b538e7bd985c85', 'adil', 'adil', '2022-10-24 22:27:32'),
(15, '248628918f8d459750df4dbedda5c305', 'ADI', 'ADI', '2022-10-25 14:03:21'),
(16, '1cb331d4d613ee169cbb75b2bddfb0a2', 'Silvi', 'Silvi', '2022-10-26 10:42:19'),
(17, 'ccebd2431a170fad773d595209020b81', '5', '5', '2022-10-27 00:03:51'),
(18, '746edfb571c399001fe4e6d46814a641', '8', '8', '2022-10-27 00:07:02'),
(19, '051202efe023a217e6655740881a55fb', 'ali', 'ali', '2022-10-27 12:10:31'),
(20, '6eba34f048f6aa31d8689d955eb75f7c', 'a', 'a', '2022-10-27 12:50:31'),
(21, 'e89ebff433c153485791242226259e34', 'adi', 'adi', '2022-10-27 12:50:37'),
(22, '1384a9260c9d8bd10c904de725321ac3', 'Nanang', 'Rehan', '2022-10-27 14:58:14'),
(23, '5deae8d0c380ac2754386c61aeac3d89', 'Nanang imam Fadjri', 'Jfc', '2022-10-27 15:12:05'),
(24, 'edbe16e3c3dbdac01c8ff92081824884', 'Nanang imam Fadjri', 'Jfc', '2022-10-27 15:13:33'),
(25, 'b1b12ece3678698be39346bbef7c6409', '8', '8', '2022-10-29 23:22:21'),
(26, 'a1e1e64b889d24ca25a9f66a9f7d79cc', '9', '9', '2022-10-29 23:30:50'),
(27, 'af3e510e4fcca1f406949185d7980a79', '2', '2', '2022-10-29 23:33:53'),
(28, 'c4a1de8f0a2c8e87d1272e60e1a0d556', '5', '5', '2022-10-29 23:36:19'),
(29, '8fc235aedcee010c0cd84290431a6431', '6', '6', '2022-10-29 23:36:50'),
(30, 'e4b1a8d4f9e38e39a89eb7eee475db13', '7', '7', '2022-10-29 23:38:16'),
(31, '159e66bdcaea5958bdb6c034e9fb1802', '8', '8', '2022-10-29 23:38:31'),
(32, '93ea0d06d54223136df99bca5ccacf86', '8', '8', '2022-10-29 23:38:55'),
(33, '4a9c8656deea1b253ffaf2e74dc66dbc', '8', '8', '2022-10-29 23:39:24'),
(34, 'e21c5c0c6b428e2880faed8edebc94d6', '8', '8', '2022-10-29 23:40:28'),
(35, '15ca4a94be7dd83bf2b68a1cf1660845', '7', '7', '2022-10-29 23:41:44'),
(36, '57a3cb66137809b150c79b33e8907071', '3', '3', '2022-10-29 23:43:21'),
(37, '903056f0bc0b0682091962bb91ad306b', '2', '2', '2022-10-29 23:44:36'),
(38, '5d80544e1a1b9b5b6ff8ef8a239f408e', '3', '3', '2022-10-29 23:47:24'),
(39, 'e250e40066f014ebe45366515bd8fceb', 'j', 'j', '2022-10-30 09:38:05'),
(40, 'd3d239696051d3d6b09bd4b0ab5048df', 'Jodevan', 'Jodevan', '2022-10-31 17:10:46'),
(41, '168e3899d5c1f5bdd74c19abf3698f05', 'Silvi', 'Wkwk Fc', '2022-10-31 18:27:37'),
(42, '387d6df87d7ac1afac347264bd7dd4d8', 'Benjot', 'Palebon Fc', '2022-10-31 18:28:52');

-- --------------------------------------------------------

--
-- Table structure for table `nomerku2211`
--

CREATE TABLE `nomerku2211` (
  `noid` int(50) NOT NULL,
  `nota_kd` varchar(50) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `team` varchar(100) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nomerku2211`
--

INSERT INTO `nomerku2211` (`noid`, `nota_kd`, `nama`, `team`, `postdate`) VALUES
(1, '3b83c7d45717ff3edc9fe62a64e4568f', 'Nanang imam Fadjri', 'Jfc', '2022-11-01 13:55:53'),
(2, '5cccf932ca402acb0a2e940a7fb7f04b', 'Pjkr', 'Pjkr', '2022-11-01 21:29:04'),
(3, 'b37a9f8f6f7e7688acff13b4373f7e43', 'Join fc', 'Join fv', '2022-11-02 17:01:23'),
(4, 'b79020be40bd22e05c12e90a93fae9b6', 'Naufal', 'Naufal', '2022-11-02 20:51:15'),
(5, '3bd788f4e13c4fdb483228ee0565d244', 'Ran', 'Ran', '2022-11-03 17:24:49'),
(6, 'fc48cf7d7025d2a0050617ebcae05a3d', 'Smansa', 'Smansa', '2022-11-04 14:33:37'),
(7, 'a87c7c19072731be84e0dd4381d9f0c4', 'Ibnu', 'Ibnu', '2022-11-04 14:37:02'),
(8, '4b55072ae0eae841d59b5a1b17808675', 'Fik unisulla', 'Fik unisulla', '2022-11-04 14:40:01'),
(9, '4724068562f9b5668b77b86fe76e982f', 'Telkom', 'Telkom', '2022-11-04 14:43:37'),
(10, '033c462340c93e7cc10236c915179279', 'Armatim', 'Armatim', '2022-11-04 14:45:29'),
(11, 'a3499c98f670010f2f2398c564318698', 'Moreno', 'Moreno', '2022-11-04 14:47:36'),
(12, '24691fba2026e256ed83b6112cbf24a2', 'Moreno', 'Moreno', '2022-11-04 14:51:29'),
(13, '4b77c909de45af3ad02655685127ba95', 'Fauzan', 'Fauzan', '2022-11-04 14:53:10'),
(14, '8492dc67e010b9a75dfa3b39a4a47590', '6', '6', '2022-11-05 17:10:55'),
(15, '3c0386e48976af9507a8bfc23f31e57d', 'Angin laut', 'Angin laut', '2022-11-05 17:40:27'),
(16, '5e9f94271f7c46c4b998f818a3823bab', 'Angin laut', 'Angin laut', '2022-11-05 17:41:41'),
(17, '685d9015979c3a1bdba0820b903208d5', 'Siliwangi', 'Siliwangi', '2022-11-05 17:43:26'),
(18, '5bde16a25d4f6da4b3bb08fc5ae7af4d', 'Pirel fc', 'Pirel fc', '2022-11-05 17:57:34'),
(19, '77473e5602d541763ae947f68026d6f5', 'Atlantik', 'Atlantik', '2022-11-05 22:02:18'),
(20, '7c726696c617fc27d10baeb170a06607', 'Devandra', 'Devandra', '2022-11-07 17:07:17'),
(21, '49468ad1e459b024ec01242a6b54d2d7', 'Usm', 'Usm', '2022-11-07 17:17:40'),
(22, 'c274d618652c862a21c188f653cbe78d', 'Usm', 'Usm', '2022-11-07 17:18:51'),
(23, '20dad599d84f285f3927bec10279d8d2', 'Devandra', 'Devandra', '2022-11-07 17:19:49'),
(24, 'bc4f560fdea10481d5418ff3b17a73df', 'Devandra', 'Devandra', '2022-11-07 17:20:53'),
(25, '242f888da1b55a67fb956afc1b877874', 'Indonesia power', 'Indonesia power', '2022-11-07 17:23:38'),
(26, '8550b226f1671fa0b918176d9b5d083a', 'Pm2', 'Pm2', '2022-11-07 17:25:40'),
(27, '67de860d5ba97dde460cb158c683e8c5', 'Aziz', 'Aziz', '2022-11-07 17:26:47'),
(28, '85272cde9ff1f33c4700e5cbabae8be6', 'Aziz', 'Aziz', '2022-11-07 17:29:18'),
(29, '766cf33e003884d4d23bfbf64887d33f', 'Pambudi', 'Pambudi', '2022-11-07 17:30:25'),
(30, '20ab28bd41a71d1c6887f960aa0f6ecd', 'Wawan', 'Wawan', '2022-11-07 17:33:17'),
(31, 'baf0266e12caabceacceb15439f3ae7c', 'Amni', 'Amni', '2022-11-07 17:38:29'),
(32, 'f756894c228fe0c1e4986404da9d5bf9', 'Lumpia mbak lin', 'Lumpia mbak lin', '2022-11-07 17:39:14'),
(33, '7b9f042ad9f7ceb9bf7ea922c596d83f', 'Arjuna', 'Arjuna', '2022-11-07 17:40:27'),
(34, '1d8dac03f5bdc416eaff09d16127f364', 'Tehnik sipil', 'Tehnik sipil', '2022-11-07 17:44:59'),
(35, '66410548ea22255c62fd54293bba578d', 'Tehnik sipil', 'Tehnik sipil', '2022-11-07 17:45:24'),
(36, 'd223fa2001dffde19212c33008422ce8', 'Ikrom', 'Ikrom', '2022-11-07 17:47:38'),
(37, 'fcf29da862812e941d6d572d36bedba8', 'Haryono bj', 'Bank Jateng', '2022-11-07 17:49:20'),
(38, '7235ae1f86e4ab06bba0cad87d78d4f2', 'Ready fc', 'Ready fc', '2022-11-07 17:51:39'),
(39, '57e0546f9cb369c85de374e2e4ca37ae', 'Kofu', 'Kofu', '2022-11-07 17:53:55'),
(40, '6241fc7ec4009507880f47c1ca0f2e7c', 'Gyr', 'Gyr', '2022-11-07 17:55:17'),
(41, 'd877c316c97932287d18c2a3f536bf99', 'Gondrong', 'Gondrong', '2022-11-07 17:58:20'),
(42, 'ebf5b076dd826e895d919d44205bd3c9', 'Upgris', 'Upgris', '2022-11-07 18:00:05'),
(43, '7724c68e69142aad1b1751802dc731cb', 'Multindo', 'Multindo', '2022-11-07 18:15:41'),
(44, 'bb77bfbceb4428762cae3d159dfd975a', 'Barito fc', 'Barito fc', '2022-11-07 18:18:13'),
(45, 'a915c17627587b15c3e6d8f2ca9fafb9', 'Barito fc', 'Barito fc', '2022-11-07 18:20:24'),
(46, 'b5098b12959e731109214f6ac3f63788', 'Wahyu', 'Wahyu', '2022-11-07 18:22:55'),
(47, '51ad36f0edad3d29d503673ef3a27eb5', 'Hjma', 'Hjma', '2022-11-07 18:25:42'),
(48, '6580d47b9b8d9fc17dbb8d4009d54e94', 'Dwi Andi', 'Dwi andi', '2022-11-07 18:28:59'),
(49, 'ddd6fbe450211e2343e620649e5fbda7', 'Dwi Andi', 'Dwi andi', '2022-11-07 18:30:42'),
(50, '2ee8f98c876e4286785766e2579e2223', 'Pirel fc', 'Pirel fc', '2022-11-07 18:35:08'),
(51, '22d6a0eee7ff3c1a0cc65fd292f32149', 'Iqbal', 'Iqbaal', '2022-11-07 18:37:26'),
(52, '2a51dd70c8458cf7b665c5e7f8a32359', 'Amri', 'Amri', '2022-11-07 21:08:28'),
(53, 'aa3ad3733f306a56f63e0e2e7d199ce3', 'Amri', 'Amri', '2022-11-07 21:09:22'),
(54, '0ba6a70d20f250b4bcb2220c1ed3af17', 'Teknik Sipil', 'Teknik Sipil', '2022-11-08 09:32:19'),
(55, 'c5b69e058845b942bf7675e52d88898d', 'Yansyah', 'Yansyah', '2022-11-08 16:46:36'),
(56, '45f6b1fecfb782e2518b3d970003178a', 'Febbi', 'Febbi', '2022-11-08 16:48:11'),
(57, 'ff116ada7979d6612621375af7a2bb27', 'Adella', 'Adella', '2022-11-08 17:02:09'),
(58, '7c292ac6704844737b4639b95291bc47', 'Brian', 'Brian', '2022-11-08 17:37:55'),
(59, '4dad06c1e523fc387129514cea1ac2e9', 'Pasukan rob', 'Pasukan rob', '2022-11-08 17:39:36'),
(60, '80a566071e57a5848035caa8f7c6b7a7', 'Telkom', 'Telkom', '2022-11-08 17:40:23'),
(61, '0ff0cc592e1be3d3c7791504633842a1', 'Adella', 'Adella', '2022-11-08 17:41:33'),
(62, '5ecffe0132a16f5e24061379327f8707', 'Brian', 'Brian', '2022-11-08 17:42:10'),
(63, '518b4257b0f283b5a755d59cb150a3bf', 'USM', 'USM', '2022-11-09 16:02:49'),
(64, '43d0fd8e5b83cebad3ca32ffce3ca2af', 'Pjkr', 'Pjkr', '2022-11-09 16:28:03'),
(65, 'bc19ec917c4a3feae2d358f0eeda0dfe', 'Gavin', 'Gavin', '2022-11-09 16:30:05'),
(66, '4ba36e534fc713b1347c2a621c4f6764', 'Irfandi', 'Irfandi', '2022-11-09 16:32:54'),
(67, 'f3199f1bf8ad9ec44d45ee8ed78d018d', 'Barito fc', 'Barito fc', '2022-11-09 16:38:20'),
(68, '4675e0751f6506e87370c944ca126a08', 'Dwi Andi', 'Dwi andi', '2022-11-09 16:40:25'),
(69, '1e2b13db100eae0a036c9198656ff3c9', 'Abdul', 'Abdul', '2022-11-10 17:30:37');

-- --------------------------------------------------------

--
-- Table structure for table `nomerku2212`
--

CREATE TABLE `nomerku2212` (
  `noid` int(50) NOT NULL,
  `nota_kd` varchar(50) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `team` varchar(100) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `nomerku2301`
--

CREATE TABLE `nomerku2301` (
  `noid` int(50) NOT NULL,
  `nota_kd` varchar(50) NOT NULL,
  `nama` varchar(100) DEFAULT NULL,
  `team` varchar(100) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nomerku2301`
--

INSERT INTO `nomerku2301` (`noid`, `nota_kd`, `nama`, `team`, `postdate`) VALUES
(1, '944b5d113cac9b564c4fa900b68131b6', 'Ilham agum', 'Pt pel', '2023-01-07 19:38:35'),
(2, 'd07e7f373db01fb6eb875da031b80eca', 'Ilham agum', 'Pt pel', '2023-01-07 19:38:35'),
(3, 'ed43d75dced775f0370a00934f1b077c', 'Aurelio', 'Aurelio', '2023-01-12 15:28:25'),
(4, '211c09ae09cbe9926a7bfd929cb18915', 'Aurelio', 'Aurelio', '2023-01-12 15:28:57'),
(5, '325dcd008b5b048cc61e048806678ec1', 'Aurelio', 'Aurelio', '2023-01-12 15:29:13');

-- --------------------------------------------------------

--
-- Table structure for table `nota`
--

CREATE TABLE `nota` (
  `kd` varchar(50) NOT NULL DEFAULT '',
  `book_kd` varchar(50) DEFAULT NULL,
  `book_kode` varchar(100) DEFAULT NULL,
  `pelanggan` varchar(100) DEFAULT NULL,
  `tgl` datetime DEFAULT NULL,
  `no_nota` varchar(100) DEFAULT NULL,
  `total` varchar(15) DEFAULT NULL,
  `total_bayar` varchar(15) DEFAULT NULL,
  `total_kembali` varchar(15) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL,
  `postdate_update` datetime DEFAULT NULL,
  `jml_brg` varchar(10) DEFAULT NULL,
  `jml_qty` varchar(10) DEFAULT NULL,
  `user_kd` varchar(50) DEFAULT NULL,
  `user_kode` varchar(100) DEFAULT NULL,
  `user_nama` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nota`
--

INSERT INTO `nota` (`kd`, `book_kd`, `book_kode`, `pelanggan`, `tgl`, `no_nota`, `total`, `total_bayar`, `total_kembali`, `postdate`, `postdate_update`, `jml_brg`, `jml_qty`, `user_kd`, `user_kode`, `user_nama`) VALUES
('20221031141809218', '20221031141809218', '20221031141809218', NULL, '2022-10-31 14:25:53', '20221031141809218', '', NULL, NULL, '2022-10-31 14:25:53', NULL, '0', '', NULL, NULL, NULL),
('20221031142727820', '20221031142727820', '20221031142727820', NULL, '2022-10-31 14:27:29', '20221031142727820', '', NULL, NULL, '2022-10-31 14:27:29', NULL, '0', '', NULL, NULL, NULL),
('2022103114295272', '2022103114295272', '2022103114295272', NULL, '2022-10-31 14:29:53', '2022103114295272', '', NULL, NULL, '2022-10-31 14:29:53', NULL, '1', '2', NULL, NULL, NULL),
('20221103172123691', '20221103172123691', '20221103172123691', NULL, '2022-11-03 17:21:26', '20221103172123691', '20000', NULL, NULL, '2022-11-03 17:21:26', NULL, '1', '1', NULL, NULL, NULL),
('20221031143059646', '20221031143059646', '20221031143059646', NULL, '2022-10-31 14:31:05', '20221031143059646', '', NULL, NULL, '2022-10-31 14:31:05', NULL, '0', '', NULL, NULL, NULL),
('20221031143228625', '20221031143228625', '20221031143228625', NULL, '2022-10-31 14:34:11', '20221031143228625', '', NULL, NULL, '2022-10-31 14:34:11', NULL, '1', '3', NULL, NULL, NULL),
('2022103115401432', '2022103115401432', '2022103115401432', NULL, '2022-10-31 15:40:20', '2022103115401432', '', NULL, NULL, '2022-10-31 15:40:20', NULL, '0', '', NULL, NULL, NULL),
('a87c7c19072731be84e0dd4381d9f0c4', 'a87c7c19072731be84e0dd4381d9f0c4', '22110007', 'Ibnu', '2022-11-04 00:00:00', '22110007', '160000', NULL, NULL, '2022-11-04 19:29:45', NULL, '0', '', NULL, NULL, NULL),
('b79020be40bd22e05c12e90a93fae9b6', 'b79020be40bd22e05c12e90a93fae9b6', '22110004', 'Naufal', '2022-11-04 00:00:00', '22110004', '120000', NULL, NULL, '2022-11-04 19:37:20', NULL, '0', '', NULL, NULL, NULL),
('3bd788f4e13c4fdb483228ee0565d244', '3bd788f4e13c4fdb483228ee0565d244', '22110005', 'Ran', '2022-11-04 00:00:00', '22110005', '120000', NULL, NULL, '2022-11-04 20:19:55', NULL, '0', '', NULL, NULL, NULL),
('4724068562f9b5668b77b86fe76e982f', '4724068562f9b5668b77b86fe76e982f', '22110009', 'Telkom', '2022-11-04 00:00:00', '22110009', '240000', NULL, NULL, '2022-11-05 17:45:21', NULL, '0', '', NULL, NULL, NULL),
('4b55072ae0eae841d59b5a1b17808675', '4b55072ae0eae841d59b5a1b17808675', '22110008', 'Fik unisulla', '2022-11-04 00:00:00', '22110008', '240000', NULL, NULL, '2022-11-05 17:46:06', NULL, '0', '', NULL, NULL, NULL),
('685d9015979c3a1bdba0820b903208d5', '685d9015979c3a1bdba0820b903208d5', '22110017', 'Siliwangi', '2022-11-05 00:00:00', '22110017', '140000', NULL, NULL, '2022-11-05 17:46:20', NULL, '0', '', NULL, NULL, NULL),
('5e9f94271f7c46c4b998f818a3823bab', '5e9f94271f7c46c4b998f818a3823bab', '22110016', 'Angin laut', '2022-11-05 00:00:00', '22110016', '140000', NULL, NULL, '2022-11-05 20:17:43', NULL, '0', '', NULL, NULL, NULL),
('4b77c909de45af3ad02655685127ba95', '4b77c909de45af3ad02655685127ba95', '22110013', 'Fauzan', '2022-11-05 00:00:00', '22110013', '240000', NULL, NULL, '2022-11-05 21:58:32', NULL, '0', '', NULL, NULL, NULL),
('77473e5602d541763ae947f68026d6f5', '77473e5602d541763ae947f68026d6f5', '22110019', 'Atlantik', '2022-11-05 00:00:00', '22110019', '140000', NULL, NULL, '2022-11-05 23:00:02', NULL, '0', '', NULL, NULL, NULL),
('5bde16a25d4f6da4b3bb08fc5ae7af4d', '5bde16a25d4f6da4b3bb08fc5ae7af4d', '22110018', 'Pirel fc', '2022-11-05 00:00:00', '22110018', '295000', NULL, NULL, '2022-11-05 23:01:06', NULL, '2', '3', NULL, NULL, NULL),
('bc4f560fdea10481d5418ff3b17a73df', 'bc4f560fdea10481d5418ff3b17a73df', '22110024', 'Devandra', '2022-11-07 00:00:00', '22110024', '240000', NULL, NULL, '2022-11-07 18:04:04', NULL, '0', '', NULL, NULL, NULL),
('c274d618652c862a21c188f653cbe78d', 'c274d618652c862a21c188f653cbe78d', '22110022', 'Usm', '2022-11-07 00:00:00', '22110022', '120000', NULL, NULL, '2022-11-07 18:05:58', NULL, '0', '', NULL, NULL, NULL),
('d3d239696051d3d6b09bd4b0ab5048df', 'd3d239696051d3d6b09bd4b0ab5048df', '22100040', 'Jodevan', '2022-11-01 00:00:00', '22100040', '240000', NULL, NULL, '2022-11-07 18:07:04', NULL, '0', '', NULL, NULL, NULL),
('242f888da1b55a67fb956afc1b877874', '242f888da1b55a67fb956afc1b877874', '22110025', 'Indonesia power', '2022-11-07 00:00:00', '22110025', '270000', NULL, NULL, '2022-11-07 19:51:35', NULL, '1', '10', NULL, NULL, NULL),
('20221107195908428', '20221107195908428', '20221107195908428', NULL, '2022-11-07 19:59:16', '20221107195908428', '110000', NULL, NULL, '2022-11-07 19:59:16', NULL, '1', '11', NULL, NULL, NULL),
('2022110719593196', '2022110719593196', '2022110719593196', NULL, '2022-11-07 19:59:35', '2022110719593196', '110000', '0', '0', '2022-11-07 19:59:35', '2022-11-07 20:00:01', '1', '11', NULL, NULL, NULL),
('20ab28bd41a71d1c6887f960aa0f6ecd', '20ab28bd41a71d1c6887f960aa0f6ecd', '22110030', 'Wawan', '2022-11-07 00:00:00', '22110030', '240000', NULL, NULL, '2022-11-07 21:03:03', NULL, '0', '', NULL, NULL, NULL),
('2022110808140149', '2022110808140149', '2022110808140149', NULL, '2022-11-08 08:14:03', '2022110808140149', '', NULL, NULL, '2022-11-08 08:14:03', NULL, '0', '', NULL, NULL, NULL),
('20221108081403258', '20221108081403258', '20221108081403258', NULL, '2022-11-08 08:14:15', '20221108081403258', '', NULL, NULL, '2022-11-08 08:14:15', NULL, '0', '', NULL, NULL, NULL),
('20221108081415396', '20221108081415396', '20221108081415396', NULL, '2022-11-08 08:14:33', '20221108081415396', '', '0', '0', '2022-11-08 08:14:33', '2022-11-08 08:14:48', '0', '', NULL, NULL, NULL),
('20221108095535897', '20221108095535897', '20221108095535897', NULL, '2022-11-08 09:55:37', '20221108095535897', '', NULL, NULL, '2022-11-08 09:55:37', NULL, '0', '', NULL, NULL, NULL),
('22d6a0eee7ff3c1a0cc65fd292f32149', '22d6a0eee7ff3c1a0cc65fd292f32149', '22110051', 'Iqbal', '2022-11-12 00:00:00', '22110051', '280000', NULL, NULL, '2022-11-08 10:09:34', NULL, '0', '', NULL, NULL, NULL),
('20221108141557806', '20221108141557806', '20221108141557806', NULL, '2022-11-08 14:15:59', '20221108141557806', '', NULL, NULL, '2022-11-08 14:15:59', NULL, '2', '3', NULL, NULL, NULL),
('20221108141727598', '20221108141727598', '20221108141727598', NULL, '2022-11-08 14:17:45', '20221108141727598', '', NULL, NULL, '2022-11-08 14:17:45', NULL, '0', '', NULL, NULL, NULL),
('20221108141745625', '20221108141745625', '20221108141745625', NULL, '2022-11-08 14:17:50', '20221108141745625', '', NULL, NULL, '2022-11-08 14:17:50', NULL, '0', '', NULL, NULL, NULL),
('85272cde9ff1f33c4700e5cbabae8be6', '85272cde9ff1f33c4700e5cbabae8be6', '22110028', 'Aziz', '2022-11-07 00:00:00', '22110028', '120000', NULL, NULL, '2022-11-08 16:27:30', NULL, '0', '', NULL, NULL, NULL),
('67de860d5ba97dde460cb158c683e8c5', '67de860d5ba97dde460cb158c683e8c5', '22110027', 'Aziz', '2022-11-07 00:00:00', '22110027', '120000', NULL, NULL, '2022-11-08 16:27:41', NULL, '0', '', NULL, NULL, NULL),
('7b9f042ad9f7ceb9bf7ea922c596d83f', '7b9f042ad9f7ceb9bf7ea922c596d83f', '22110033', 'Arjuna', '2022-11-08 00:00:00', '22110033', '135000', NULL, NULL, '2022-11-08 17:24:12', NULL, '1', '3', NULL, NULL, NULL),
('1d8dac03f5bdc416eaff09d16127f364', '1d8dac03f5bdc416eaff09d16127f364', '22110034', 'Tehnik sipil', '2022-11-08 00:00:00', '22110034', '120000', NULL, NULL, '2022-11-08 17:34:49', NULL, '0', '', NULL, NULL, NULL),
('0ba6a70d20f250b4bcb2220c1ed3af17', '0ba6a70d20f250b4bcb2220c1ed3af17', '22110054', 'Teknik Sipil', '2022-11-08 00:00:00', '22110054', '246000', NULL, NULL, '2022-11-08 17:53:53', NULL, '1', '2', NULL, NULL, NULL),
('5ecffe0132a16f5e24061379327f8707', '5ecffe0132a16f5e24061379327f8707', '22110062', 'Brian', '2022-11-08 00:00:00', '22110062', '140000', NULL, NULL, '2022-11-08 18:56:12', NULL, '2', '4', NULL, NULL, NULL),
('0ff0cc592e1be3d3c7791504633842a1', '0ff0cc592e1be3d3c7791504633842a1', '22110061', 'Adella', '2022-11-08 00:00:00', '22110061', '120000', NULL, NULL, '2022-11-08 18:57:22', NULL, '0', '', NULL, NULL, NULL),
('7724c68e69142aad1b1751802dc731cb', '7724c68e69142aad1b1751802dc731cb', '22110043', 'Multindo', '2022-11-10 00:00:00', '22110043', '120000', NULL, NULL, '2022-11-08 19:06:09', NULL, '0', '', NULL, NULL, NULL),
('d223fa2001dffde19212c33008422ce8', 'd223fa2001dffde19212c33008422ce8', '22110036', 'Ikrom', '2022-11-08 00:00:00', '22110036', '255000', NULL, NULL, '2022-11-08 20:51:41', NULL, '1', '3', NULL, NULL, NULL),
('fcf29da862812e941d6d572d36bedba8', 'fcf29da862812e941d6d572d36bedba8', '22110037', 'Haryono bj', '2022-11-08 00:00:00', '22110037', '240000', NULL, NULL, '2022-11-08 20:52:18', NULL, '0', '', NULL, NULL, NULL),
('20221108205842455', '20221108205842455', '20221108205842455', NULL, '2022-11-08 20:58:44', '20221108205842455', '5000', '0', '0', '2022-11-08 20:58:44', '2022-11-08 20:59:00', '1', '1', NULL, NULL, NULL),
('c5b69e058845b942bf7675e52d88898d', 'c5b69e058845b942bf7675e52d88898d', '22110055', 'Yansyah', '2022-11-08 00:00:00', '22110055', '140000', NULL, NULL, '2022-11-08 22:00:59', NULL, '1', '4', NULL, NULL, NULL),
('7235ae1f86e4ab06bba0cad87d78d4f2', '7235ae1f86e4ab06bba0cad87d78d4f2', '22110038', 'Ready fc', '2022-11-08 00:00:00', '22110038', '240000', NULL, NULL, '2022-11-08 23:59:39', NULL, '0', '', NULL, NULL, NULL),
('20221109114211893', '20221109114211893', '20221109114211893', NULL, '2022-11-09 11:42:12', '20221109114211893', '', NULL, NULL, '2022-11-09 11:42:12', NULL, '0', '', NULL, NULL, NULL),
('20221109114212346', '20221109114212346', '20221109114212346', NULL, '2022-11-09 11:42:18', '20221109114212346', '', NULL, NULL, '2022-11-09 11:42:18', NULL, '0', '', NULL, NULL, NULL),
('20221109115401849', '20221109115401849', '20221109115401849', NULL, '2022-11-09 11:54:03', '20221109115401849', '', '0', '0', '2022-11-09 11:54:03', '2022-11-09 12:01:52', '15', '40', NULL, NULL, NULL),
('20221109120152616', '20221109120152616', '20221109120152616', NULL, '2022-11-09 12:01:59', '20221109120152616', '', '0', '0', '2022-11-09 12:01:59', '2022-11-09 12:03:30', '2', '12', NULL, NULL, NULL),
('20221109120445180', '20221109120445180', '20221109120445180', NULL, '2022-11-09 12:04:46', '20221109120445180', '', NULL, NULL, '2022-11-09 12:04:46', NULL, '0', '', NULL, NULL, NULL),
('20221109155400200', '20221109155400200', '20221109155400200', NULL, '2022-11-09 15:54:01', '20221109155400200', '', NULL, NULL, '2022-11-09 15:54:01', NULL, '0', '', NULL, NULL, NULL),
('20221109155401992', '20221109155401992', '20221109155401992', NULL, '2022-11-09 15:54:11', '20221109155401992', '144000', '144000', '0', '2022-11-09 15:54:11', '2022-11-09 15:55:40', '4', '9', NULL, NULL, NULL),
('43d0fd8e5b83cebad3ca32ffce3ca2af', '43d0fd8e5b83cebad3ca32ffce3ca2af', '22110064', 'Pjkr', '2022-11-09 00:00:00', '22110064', '260000', NULL, NULL, '2022-11-09 16:45:31', NULL, '2', '5', NULL, NULL, NULL),
('bc19ec917c4a3feae2d358f0eeda0dfe', 'bc19ec917c4a3feae2d358f0eeda0dfe', '22110065', 'Gavin', '2022-11-09 00:00:00', '22110065', '240000', NULL, NULL, '2022-11-09 17:43:48', NULL, '0', '', NULL, NULL, NULL),
('45f6b1fecfb782e2518b3d970003178a', '45f6b1fecfb782e2518b3d970003178a', '22110056', 'Febbi', '2022-11-09 00:00:00', '22110056', '245000', NULL, NULL, '2022-11-09 18:56:02', NULL, '1', '1', NULL, NULL, NULL),
('6241fc7ec4009507880f47c1ca0f2e7c', '6241fc7ec4009507880f47c1ca0f2e7c', '22110040', 'Gyr', '2022-11-09 00:00:00', '22110040', '250000', NULL, NULL, '2022-11-09 20:55:35', NULL, '1', '2', NULL, NULL, NULL),
('57e0546f9cb369c85de374e2e4ca37ae', '57e0546f9cb369c85de374e2e4ca37ae', '22110039', 'Kofu', '2022-11-09 00:00:00', '22110039', '250000', NULL, NULL, '2022-11-09 20:55:56', NULL, '1', '2', NULL, NULL, NULL),
('20221110155503646', '20221110155503646', '20221110155503646', NULL, '2022-11-10 15:55:06', '20221110155503646', '30500', '35000', '4500', '2022-11-10 15:55:06', '2022-11-10 15:57:18', '5', '7', NULL, NULL, NULL),
('20221110155719281', '20221110155719281', '20221110155719281', NULL, '2022-11-10 16:00:01', '20221110155719281', '', NULL, NULL, '2022-11-10 16:00:01', NULL, '0', '', NULL, NULL, NULL),
('20221110200305464', '20221110200305464', '20221110200305464', NULL, '2022-11-10 20:03:08', '20221110200305464', '8000', NULL, NULL, '2022-11-10 20:03:08', NULL, '2', '2', NULL, NULL, NULL),
('20221110200317792', '20221110200317792', '20221110200317792', NULL, '2022-11-10 20:03:29', '20221110200317792', '', NULL, NULL, '2022-11-10 20:03:29', NULL, '0', '', NULL, NULL, NULL),
('20221110200347811', '20221110200347811', '20221110200347811', NULL, '2022-11-10 20:03:49', '20221110200347811', '8000', '0', '0', '2022-11-10 20:03:49', '2022-11-10 20:04:02', '2', '2', NULL, NULL, NULL),
('ebf5b076dd826e895d919d44205bd3c9', 'ebf5b076dd826e895d919d44205bd3c9', '22110042', 'Upgris', '2022-11-09 00:00:00', '22110042', '240000', NULL, NULL, '2022-11-10 20:05:31', NULL, '0', '', NULL, NULL, NULL),
('d877c316c97932287d18c2a3f536bf99', 'd877c316c97932287d18c2a3f536bf99', '22110041', 'Gondrong', '2022-11-09 00:00:00', '22110041', '120000', NULL, NULL, '2022-11-10 20:05:46', NULL, '0', '', NULL, NULL, NULL),
('f3199f1bf8ad9ec44d45ee8ed78d018d', 'f3199f1bf8ad9ec44d45ee8ed78d018d', '22110067', 'Barito fc', '2022-11-10 00:00:00', '22110067', '120000', NULL, NULL, '2022-11-10 22:02:32', NULL, '0', '', NULL, NULL, NULL),
('bb77bfbceb4428762cae3d159dfd975a', 'bb77bfbceb4428762cae3d159dfd975a', '22110044', 'Barito fc', '2022-11-10 00:00:00', '22110044', '253000', NULL, NULL, '2022-11-10 22:02:48', NULL, '3', '4', NULL, NULL, NULL),
('b5098b12959e731109214f6ac3f63788', 'b5098b12959e731109214f6ac3f63788', '22110046', 'Wahyu', '2022-11-10 00:00:00', '22110046', '240000', NULL, NULL, '2022-11-10 22:15:58', NULL, '0', '', NULL, NULL, NULL),
('51ad36f0edad3d29d503673ef3a27eb5', '51ad36f0edad3d29d503673ef3a27eb5', '22110047', 'Hjma', '2022-11-11 00:00:00', '22110047', '200000', NULL, NULL, '2022-11-11 17:01:52', NULL, '0', '', NULL, NULL, NULL),
('20221111170207449', '20221111170207449', '20221111170207449', NULL, '2022-11-11 17:02:09', '20221111170207449', '240000', '0', '0', '2022-11-11 17:02:09', '2022-11-11 17:03:10', '2', '2', NULL, NULL, NULL),
('20221111192137943', '20221111192137943', '20221111192137943', NULL, '2022-11-11 19:21:39', '20221111192137943', '280000', '0', '0', '2022-11-11 19:21:39', '2022-11-11 19:22:23', '1', '2', NULL, NULL, NULL),
('20221111192210606', '20221111192210606', '20221111192210606', NULL, '2022-11-11 19:22:12', '20221111192210606', '', NULL, NULL, '2022-11-11 19:22:12', NULL, '0', '', NULL, NULL, NULL),
('20221113212142151', '20221113212142151', '20221113212142151', NULL, '2022-11-13 21:21:45', '20221113212142151', '280000', '0', '0', '2022-11-13 21:21:45', '2022-11-13 21:22:12', '1', '2', NULL, NULL, NULL),
('20221116000935202', '20221116000935202', '20221116000935202', NULL, '2022-11-16 00:09:37', '20221116000935202', '120000', '0', '0', '2022-11-16 00:09:37', '2022-11-16 00:10:14', '1', '1', NULL, NULL, NULL),
('20221117222247117', '20221117222247117', '20221117222247117', NULL, '2022-11-17 22:22:53', '20221117222247117', '255000', '0', '0', '2022-11-17 22:22:53', '2022-11-17 22:23:42', '2', '5', NULL, NULL, NULL),
('20221118134959658', '20221118134959658', '20221118134959658', NULL, '2022-11-18 13:50:34', '20221118134959658', '1725000', '0', '0', '2022-11-18 13:50:34', '2022-11-18 13:51:04', '1', '15', NULL, NULL, NULL),
('20221118140736347', '20221118140736347', '20221118140736347', NULL, '2022-11-18 14:07:38', '20221118140736347', '', NULL, NULL, '2022-11-18 14:07:38', NULL, '0', '', NULL, NULL, NULL),
('20221118140833812', '20221118140833812', '20221118140833812', NULL, '2022-11-18 14:08:35', '20221118140833812', '1725000', '0', '0', '2022-11-18 14:08:35', '2022-11-18 14:08:48', '1', '15', NULL, NULL, NULL),
('20221118151428817', '20221118151428817', '20221118151428817', NULL, '2022-11-18 15:14:30', '20221118151428817', '5000', '0', '0', '2022-11-18 15:14:30', '2022-11-18 15:16:15', '1', '1', NULL, NULL, NULL),
('20221118201218115', '20221118201218115', '20221118201218115', NULL, '2022-11-18 20:12:20', '20221118201218115', '420000', '0', '0', '2022-11-18 20:12:20', '2022-11-18 20:13:18', '1', '3', NULL, NULL, NULL),
('20221118222756377', '20221118222756377', '20221118222756377', NULL, '2022-11-18 22:27:57', '20221118222756377', '60500', '0', '0', '2022-11-18 22:27:57', '2022-11-18 22:53:48', '6', '9', NULL, NULL, NULL),
('20221118230712953', '20221118230712953', '20221118230712953', NULL, '2022-11-18 23:07:13', '20221118230712953', '5000', '0', '0', '2022-11-18 23:07:13', '2022-11-18 23:07:26', '1', '1', NULL, NULL, NULL),
('20221119112123484', '20221119112123484', '20221119112123484', NULL, '2022-11-19 11:21:24', '20221119112123484', '15000', '0', '0', '2022-11-19 11:21:24', '2022-11-19 11:21:53', '1', '3', NULL, NULL, NULL),
('2022111911241818', '2022111911241818', '2022111911241818', NULL, '2022-11-19 11:24:20', '2022111911241818', '15000', '0', '0', '2022-11-19 11:24:20', '2022-11-19 11:24:42', '1', '3', NULL, NULL, NULL),
('2022111911485410', '2022111911485410', '2022111911485410', NULL, '2022-11-19 11:48:55', '2022111911485410', '12000', '20000', '8000', '2022-11-19 11:48:55', '2022-11-19 11:49:20', '1', '2', NULL, NULL, NULL),
('20221120212319272', '20221120212319272', '20221120212319272', NULL, '2022-11-20 21:23:21', '20221120212319272', '', NULL, NULL, '2022-11-20 21:23:21', NULL, '0', '', NULL, NULL, NULL),
('20221120212321596', '20221120212321596', '20221120212321596', NULL, '2022-11-20 21:23:22', '20221120212321596', '280000', '0', '0', '2022-11-20 21:23:22', '2022-11-20 21:27:00', '1', '2', NULL, NULL, NULL),
('20221120212835664', '20221120212835664', '20221120212835664', NULL, '2022-11-20 21:28:37', '20221120212835664', '280000', '0', '0', '2022-11-20 21:28:37', '2022-11-20 21:29:01', '1', '2', NULL, NULL, NULL),
('2022112210383229', '2022112210383229', '2022112210383229', NULL, '2022-11-22 10:38:33', '2022112210383229', '', NULL, NULL, '2022-11-22 10:38:33', NULL, '0', '', NULL, NULL, NULL),
('20221123001110312', '20221123001110312', '20221123001110312', NULL, '2022-11-23 00:11:12', '20221123001110312', '120000', '0', '0', '2022-11-23 00:11:12', '2022-11-23 00:11:33', '1', '1', NULL, NULL, NULL),
('20221123151556166', '20221123151556166', '20221123151556166', NULL, '2022-11-23 15:15:58', '20221123151556166', '', NULL, NULL, '2022-11-23 15:15:58', NULL, '0', '', NULL, NULL, NULL),
('2022112315183366', '2022112315183366', '2022112315183366', NULL, '2022-11-23 15:18:35', '2022112315183366', '', NULL, NULL, '2022-11-23 15:18:35', NULL, '0', '', NULL, NULL, NULL),
('2022112315214184', '2022112315214184', '2022112315214184', NULL, '2022-11-23 15:21:43', '2022112315214184', '36000', '36000', '0', '2022-11-23 15:21:43', '2022-11-23 15:24:26', '3', '4', NULL, NULL, NULL),
('20221123223222213', '20221123223222213', '20221123223222213', NULL, '2022-11-23 22:32:24', '20221123223222213', '296000', '0', '0', '2022-11-23 22:32:24', '2022-11-23 22:36:29', '5', '14', NULL, NULL, NULL),
('20221124000137762', '20221124000137762', '20221124000137762', NULL, '2022-11-24 00:01:39', '20221124000137762', '145000', '0', '0', '2022-11-24 00:01:39', '2022-11-24 00:02:19', '2', '6', NULL, NULL, NULL),
('20221124220213540', '20221124220213540', '20221124220213540', NULL, '2022-11-24 22:02:15', '20221124220213540', '256000', '0', '0', '2022-11-24 22:02:15', '2022-11-24 22:09:34', '3', '6', NULL, NULL, NULL),
('20221125124055744', '20221125124055744', '20221125124055744', NULL, '2022-11-25 12:40:56', '20221125124055744', '62000', '58000', 'xstrix4000', '2022-11-25 12:40:56', '2022-11-25 12:43:23', '6', '8', NULL, NULL, NULL),
('2022112512464479', '2022112512464479', '2022112512464479', NULL, '2022-11-25 12:46:45', '2022112512464479', '23000', '0', '0', '2022-11-25 12:46:45', '2022-11-25 12:47:06', '1', '1', NULL, NULL, NULL),
('20221125125344520', '20221125125344520', '20221125125344520', NULL, '2022-11-25 12:53:56', '20221125125344520', '85000', '58000', 'xstrix27000', '2022-11-25 12:53:56', '2022-11-25 12:57:15', '6', '9', NULL, NULL, NULL),
('20221125220703388', '20221125220703388', '20221125220703388', NULL, '2022-11-25 22:07:05', '20221125220703388', '29500', NULL, NULL, '2022-11-25 22:07:05', NULL, '4', '5', NULL, NULL, NULL),
('20221125231149896', '20221125231149896', '20221125231149896', NULL, '2022-11-25 23:11:51', '20221125231149896', '47500', '0', '0', '2022-11-25 23:11:51', '2022-11-25 23:13:40', '6', '8', NULL, NULL, NULL),
('20221125231914372', '20221125231914372', '20221125231914372', NULL, '2022-11-25 23:19:16', '20221125231914372', '150000', '0', '0', '2022-11-25 23:19:16', '2022-11-25 23:19:53', '2', '3', NULL, NULL, NULL),
('20221127152021531', '20221127152021531', '20221127152021531', NULL, '2022-11-27 15:20:23', '20221127152021531', '57000', '157000', '100000', '2022-11-27 15:20:23', '2022-11-27 15:22:16', '8', '10', NULL, NULL, NULL),
('20221127210100969', '20221127210100969', '20221127210100969', NULL, '2022-11-27 21:01:01', '20221127210100969', '280000', '0', '0', '2022-11-27 21:01:01', '2022-11-27 21:01:19', '1', '2', NULL, NULL, NULL),
('2022112815415017', '2022112815415017', '2022112815415017', NULL, '2022-11-28 15:41:51', '2022112815415017', '97000', '147000', '50000', '2022-11-28 15:41:51', '2022-11-28 15:44:44', '3', '4', NULL, NULL, NULL),
('20221129122817842', '20221129122817842', '20221129122817842', NULL, '2022-11-29 12:28:19', '20221129122817842', '25000', '0', '0', '2022-11-29 12:28:19', '2022-11-29 12:29:46', '5', '6', NULL, NULL, NULL),
('20221130160029499', '20221130160029499', '20221130160029499', NULL, '2022-11-30 16:00:30', '20221130160029499', '131000', '0', '0', '2022-11-30 16:00:30', '2022-11-30 16:03:36', '8', '20', NULL, NULL, NULL),
('20221130230858904', '20221130230858904', '20221130230858904', NULL, '2022-11-30 23:08:59', '20221130230858904', '134000', '0', '0', '2022-11-30 23:08:59', '2022-11-30 23:09:43', '2', '3', NULL, NULL, NULL),
('20221201160009387', '20221201160009387', '20221201160009387', NULL, '2022-12-01 16:00:10', '20221201160009387', '138000', '0', '0', '2022-12-01 16:00:10', '2022-12-01 16:03:56', '12', '23', NULL, NULL, NULL),
('20221201204149738', '20221201204149738', '20221201204149738', NULL, '2022-12-01 20:41:50', '20221201204149738', '', NULL, NULL, '2022-12-01 20:41:50', NULL, '0', '', NULL, NULL, NULL),
('20221201220232595', '20221201220232595', '20221201220232595', NULL, '2022-12-01 22:02:34', '20221201220232595', '338000', '0', '0', '2022-12-01 22:02:34', '2022-12-01 22:13:25', '4', '9', NULL, NULL, NULL),
('20221202201539253', '20221202201539253', '20221202201539253', NULL, '2022-12-02 20:15:41', '20221202201539253', '355000', '0', '0', '2022-12-02 20:15:41', '2022-12-02 20:16:38', '3', '4', NULL, NULL, NULL),
('20221202215521949', '20221202215521949', '20221202215521949', NULL, '2022-12-02 21:55:22', '20221202215521949', '150000', '0', '0', '2022-12-02 21:55:22', '2022-12-02 21:56:02', '2', '3', NULL, NULL, NULL),
('20221206154226820', '20221206154226820', '20221206154226820', NULL, '2022-12-06 15:42:29', '20221206154226820', '3000', '0', '0', '2022-12-06 15:42:29', '2022-12-06 15:45:04', '1', '1', NULL, NULL, NULL),
('20221206155947342', '20221206155947342', '20221206155947342', NULL, '2022-12-06 15:59:50', '20221206155947342', '420000', '0', '0', '2022-12-06 15:59:50', '2022-12-06 16:02:54', '10', '19', NULL, NULL, NULL),
('20221208152504358', '20221208152504358', '20221208152504358', NULL, '2022-12-08 15:25:05', '20221208152504358', '294000', '0', '0', '2022-12-08 15:25:05', '2022-12-08 15:26:44', '6', '13', NULL, NULL, NULL),
('20221209160335149', '20221209160335149', '20221209160335149', NULL, '2022-12-09 16:03:36', '20221209160335149', '275000', '0', '0', '2022-12-09 16:03:36', '2022-12-09 16:07:13', '10', '14', NULL, NULL, NULL),
('20221210152339383', '20221210152339383', '20221210152339383', NULL, '2022-12-10 15:24:49', '20221210152339383', '583500', '0', '0', '2022-12-10 15:24:49', '2022-12-10 15:34:12', '18', '55', NULL, NULL, NULL),
('20221211114300556', '20221211114300556', '20221211114300556', NULL, '2022-12-11 11:43:01', '20221211114300556', '148000', '0', '0', '2022-12-11 11:43:01', '2022-12-11 11:43:23', '3', '11', NULL, NULL, NULL),
('2022121117561877', '2022121117561877', '2022121117561877', NULL, '2022-12-11 17:56:20', '2022121117561877', '320000', '0', '0', '2022-12-11 17:56:20', '2022-12-11 17:58:06', '2', '10', NULL, NULL, NULL),
('20221214155032164', '20221214155032164', '20221214155032164', NULL, '2022-12-14 15:50:34', '20221214155032164', '138000', '0', '0', '2022-12-14 15:50:34', '2022-12-14 15:52:34', '8', '13', NULL, NULL, NULL),
('20221214231336866', '20221214231336866', '20221214231336866', NULL, '2022-12-14 23:14:40', '20221214231336866', '120000', '0', '0', '2022-12-14 23:14:40', '2022-12-14 23:15:07', '1', '1', NULL, NULL, NULL),
('20221215160323123', '20221215160323123', '20221215160323123', NULL, '2022-12-15 16:03:24', '20221215160323123', '180000', '0', '0', '2022-12-15 16:03:24', '2022-12-15 16:03:48', '2', '6', NULL, NULL, NULL),
('2022121622373258', '2022121622373258', '2022121622373258', NULL, '2022-12-16 22:37:36', '2022121622373258', '32000', '0', '0', '2022-12-16 22:37:36', '2022-12-16 22:39:33', '5', '6', NULL, NULL, NULL),
('20221217162608410', '20221217162608410', '20221217162608410', NULL, '2022-12-17 16:26:09', '20221217162608410', '269500', '0', '0', '2022-12-17 16:26:09', '2022-12-17 16:28:49', '10', '15', NULL, NULL, NULL),
('20221218110159145', '20221218110159145', '20221218110159145', NULL, '2022-12-18 11:02:05', '20221218110159145', '130000', '0', '0', '2022-12-18 11:02:05', '2022-12-18 11:02:31', '2', '7', NULL, NULL, NULL),
('20221218154431227', '20221218154431227', '20221218154431227', NULL, '2022-12-18 15:44:33', '20221218154431227', '394500', NULL, NULL, '2022-12-18 15:44:33', NULL, '9', '19', NULL, NULL, NULL),
('20221218155616977', '20221218155616977', '20221218155616977', NULL, '2022-12-18 15:56:17', '20221218155616977', '399000', '0', '0', '2022-12-18 15:56:17', '2022-12-18 15:58:01', '9', '21', NULL, NULL, NULL),
('20221221213110475', '20221221213110475', '20221221213110475', NULL, '2022-12-21 21:31:12', '20221221213110475', '294000', '0', '0', '2022-12-21 21:31:12', '2022-12-21 21:33:03', '5', '14', NULL, NULL, NULL),
('20221223222943310', '20221223222943310', '20221223222943310', NULL, '2022-12-23 22:29:46', '20221223222943310', '39000', '0', '0', '2022-12-23 22:29:46', '2022-12-23 22:30:58', '4', '6', NULL, NULL, NULL),
('20221226155513206', '20221226155513206', '20221226155513206', NULL, '2022-12-26 15:55:15', '20221226155513206', '398000', '0', '0', '2022-12-26 15:55:15', '2022-12-26 16:01:10', '13', '34', NULL, NULL, NULL),
('20221227145859387', '20221227145859387', '20221227145859387', NULL, '2022-12-27 14:59:01', '20221227145859387', '119000', '0', '0', '2022-12-27 14:59:01', '2022-12-27 14:59:39', '4', '8', NULL, NULL, NULL),
('20221227210334120', '20221227210334120', '20221227210334120', NULL, '2022-12-27 21:03:37', '20221227210334120', '240000', '0', '0', '2022-12-27 21:03:37', '2022-12-27 21:03:56', '1', '2', NULL, NULL, NULL),
('2023010615560234', '2023010615560234', '2023010615560234', NULL, '2023-01-06 15:56:04', '2023010615560234', '', NULL, NULL, '2023-01-06 15:56:04', NULL, '0', '', NULL, NULL, NULL),
('20230106155825737', '20230106155825737', '20230106155825737', NULL, '2023-01-06 15:58:26', '20230106155825737', '', NULL, NULL, '2023-01-06 15:58:26', NULL, '0', '', NULL, NULL, NULL),
('20230106155933760', '20230106155933760', '20230106155933760', NULL, '2023-01-06 15:59:35', '20230106155933760', '220000', '0', '0', '2023-01-06 15:59:35', '2023-01-06 16:00:04', '1', '2', NULL, NULL, NULL),
('20230106160047213', '20230106160047213', '20230106160047213', NULL, '2023-01-06 16:00:49', '20230106160047213', '220000', '0', '0', '2023-01-06 16:00:49', '2023-01-06 16:01:20', '1', '2', NULL, NULL, NULL),
('2023010616083430', '2023010616083430', '2023010616083430', NULL, '2023-01-06 16:08:35', '2023010616083430', '15500', NULL, NULL, '2023-01-06 16:08:35', NULL, '3', '5', NULL, NULL, NULL),
('20230106161032108', '20230106161032108', '20230106161032108', NULL, '2023-01-06 16:10:33', '20230106161032108', '240500', '140500', 'xstrix100000', '2023-01-06 16:10:33', '2023-01-06 16:13:10', '5', '8', NULL, NULL, NULL),
('20230106223108428', '20230106223108428', '20230106223108428', NULL, '2023-01-06 22:31:42', '20230106223108428', '199500', '0', '0', '2023-01-06 22:31:42', '2023-01-06 22:36:24', '7', '8', NULL, NULL, NULL),
('20230109221404993', '20230109221404993', '20230109221404993', NULL, '2023-01-09 22:14:07', '20230109221404993', '330000', '0', '0', '2023-01-09 22:14:07', '2023-01-09 22:14:39', '2', '4', NULL, NULL, NULL),
('20230110222452523', '20230110222452523', '20230110222452523', NULL, '2023-01-10 22:24:54', '20230110222452523', '307000', '0', '0', '2023-01-10 22:24:54', '2023-01-10 22:26:36', '3', '9', NULL, NULL, NULL),
('20230112120631383', '20230112120631383', '20230112120631383', NULL, '2023-01-12 12:06:32', '20230112120631383', '135000', '0', '0', '2023-01-12 12:06:32', '2023-01-12 12:07:26', '3', '6', NULL, NULL, NULL),
('20230112144434592', '20230112144434592', '20230112144434592', NULL, '2023-01-12 14:44:37', '20230112144434592', '', NULL, NULL, '2023-01-12 14:44:37', NULL, '0', '', NULL, NULL, NULL),
('20230112154948572', '20230112154948572', '20230112154948572', NULL, '2023-01-12 15:49:49', '20230112154948572', '35000', '0', '0', '2023-01-12 15:49:49', '2023-01-12 15:51:06', '5', '7', NULL, NULL, NULL),
('20230114121137438', '20230114121137438', '20230114121137438', NULL, '2023-01-14 12:11:38', '20230114121137438', '136500', '0', '0', '2023-01-14 12:11:38', '2023-01-14 12:12:24', '4', '6', NULL, NULL, NULL),
('20230114154347715', '20230114154347715', '20230114154347715', NULL, '2023-01-14 15:43:48', '20230114154347715', '517000', '0', '0', '2023-01-14 15:43:48', '2023-01-14 15:49:32', '16', '39', NULL, NULL, NULL),
('20230114160321473', '20230114160321473', '20230114160321473', NULL, '2023-01-14 16:03:22', '20230114160321473', '6000', '0', '0', '2023-01-14 16:03:22', '2023-01-14 16:03:31', '1', '1', NULL, NULL, NULL),
('20230115121538424', '20230115121538424', '20230115121538424', NULL, '2023-01-15 12:15:39', '20230115121538424', '235000', '0', '0', '2023-01-15 12:15:39', '2023-01-15 12:16:15', '2', '5', NULL, NULL, NULL),
('2023011722264630', '2023011722264630', '2023011722264630', NULL, '2023-01-17 22:26:48', '2023011722264630', '305000', '0', '0', '2023-01-17 22:26:48', '2023-01-17 22:27:28', '2', '7', NULL, NULL, NULL),
('20230119204209528', '20230119204209528', '20230119204209528', NULL, '2023-01-19 20:42:11', '20230119204209528', '280000', '0', '0', '2023-01-19 20:42:11', '2023-01-19 20:42:45', '1', '2', NULL, NULL, NULL),
('20230120225434545', '20230120225434545', '20230120225434545', NULL, '2023-01-20 22:54:36', '20230120225434545', '208500', '0', '0', '2023-01-20 22:54:36', '2023-01-20 23:07:06', '6', '11', NULL, NULL, NULL),
('20230126212218359', '20230126212218359', '20230126212218359', NULL, '2023-01-26 21:22:19', '20230126212218359', '5000', '0', '0', '2023-01-26 21:22:19', '2023-01-26 21:22:27', '1', '1', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi'),
('2023020116065618', '2023020116065618', '2023020116065618', NULL, '2023-02-01 16:06:58', '2023020116065618', '203000', '0', '0', '2023-02-01 16:06:58', '2023-02-01 16:08:22', '6', '14', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi'),
('20230204091432617', '20230204091432617', '20230204091432617', NULL, '2023-02-04 09:14:34', '20230204091432617', '', NULL, NULL, '2023-02-04 09:14:34', NULL, '0', '', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN'),
('20230204091447222', '20230204091447222', '20230204091447222', NULL, '2023-02-04 09:14:48', '20230204091447222', '120000', '0', '0', '2023-02-04 09:14:48', '2023-02-04 09:15:10', '2', '3', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN'),
('20230205090730945', '20230205090730945', '20230205090730945', NULL, '2023-02-05 09:07:32', '20230205090730945', '11000', '0', '0', '2023-02-05 09:07:32', '2023-02-05 09:08:09', '2', '2', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi'),
('20230205101947130', '20230205101947130', '20230205101947130', NULL, '2023-02-05 10:19:50', '20230205101947130', '', NULL, NULL, '2023-02-05 10:19:50', NULL, '0', '', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi'),
('20230205112547266', '20230205112547266', '20230205112547266', NULL, '2023-02-05 11:25:48', '20230205112547266', '11000', '0', '0', '2023-02-05 11:25:48', '2023-02-05 11:26:06', '2', '2', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi'),
('20230205123921133', '20230205123921133', '20230205123921133', NULL, '2023-02-05 12:39:22', '20230205123921133', '10000', '0', '0', '2023-02-05 12:39:22', '2023-02-05 12:39:34', '1', '1', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi');

-- --------------------------------------------------------

--
-- Table structure for table `nota_detail`
--

CREATE TABLE `nota_detail` (
  `kd` varchar(50) NOT NULL DEFAULT '',
  `nota_kd` varchar(50) DEFAULT NULL,
  `nota_kode` varchar(100) DEFAULT NULL,
  `brg_kd` varchar(50) DEFAULT NULL,
  `brg_kode` varchar(100) DEFAULT NULL,
  `brg_barkode` varchar(100) DEFAULT NULL,
  `brg_nama` varchar(100) DEFAULT NULL,
  `brg_kategori` varchar(100) DEFAULT NULL,
  `brg_satuan` varchar(100) DEFAULT NULL,
  `brg_harga` varchar(15) DEFAULT NULL,
  `qty` varchar(10) DEFAULT NULL,
  `subtotal` varchar(15) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL,
  `user_kd` varchar(50) DEFAULT NULL,
  `user_kode` varchar(100) DEFAULT NULL,
  `user_nama` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `nota_detail`
--

INSERT INTO `nota_detail` (`kd`, `nota_kd`, `nota_kode`, `brg_kd`, `brg_kode`, `brg_barkode`, `brg_nama`, `brg_kategori`, `brg_satuan`, `brg_harga`, `qty`, `subtotal`, `postdate`, `user_kd`, `user_kode`, `user_nama`) VALUES
('3d33f16556cd176efb2a5a85156a488b', '2022103114295272', '2022103114295272', '99797ea5ce8cc9b4ed11fdaa69476dfa20', '19', '19', 'SEWA LAPANGAN MERAH MALAM WEEK END', 'SEWA', 'JAM', '140000', '2', '280000', '2022-10-31 14:30:59', NULL, NULL, NULL),
('97143c329b9dba0756b464a58d795f0b', '20221031143228625', '20221031143228625', '99797ea5ce8cc9b4ed11fdaa69476dfa27', '26', '26', 'AIR MINERAL PRIMA 600 ML', 'MNM', 'PCS', '3000', '3', '9000', '2022-10-31 14:34:18', NULL, NULL, NULL),
('0ed5e37657ac0967af18809ea7c1c028', '20221103172123691', '20221103172123691', '99797ea5ce8cc9b4ed11fdaa69476dfa2', '1', '123456789012', 'AIR GELAS', 'MNM', 'DUS', '20000', '1', '20000', '2022-11-03 17:21:39', NULL, NULL, NULL),
('ab72ed7692a604b6f832d802d73f826e', '5bde16a25d4f6da4b3bb08fc5ae7af4d', '22110018', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '2', '10000', '2022-11-05 23:03:35', NULL, NULL, NULL),
('e5b5a0731097b2c22939320193802e3f', '5bde16a25d4f6da4b3bb08fc5ae7af4d', '22110018', '99797ea5ce8cc9b4ed11fdaa69476dfa41', '40', '40', 'TEH JAVANA 350 ML', 'MNM', 'PCS', '5000', '1', '5000', '2022-11-05 23:01:33', NULL, NULL, NULL),
('f3994cef84c09f52b2608d9b70af0153', '242f888da1b55a67fb956afc1b877874', '22110025', '99797ea5ce8cc9b4ed11fdaa69476dfa27', '26', '26', 'AIR MINERAL PRIMA 600 ML', 'MNM', 'PCS', '3000', '10', '30000', '2022-11-07 19:51:44', NULL, NULL, NULL),
('879196db829f956a6472501c6bf159c7', '20221107195908428', '20221107195908428', '99797ea5ce8cc9b4ed11fdaa69476dfa14', '13', '13', 'HYDRO COCO 330 ML', 'MNM', 'PCS', '10000', '11', '110000', '2022-11-07 19:59:31', NULL, NULL, NULL),
('a43a54fa1d02cb55caba4385b561a494', '2022110719593196', '2022110719593196', '99797ea5ce8cc9b4ed11fdaa69476dfa14', '13', '13', 'HYDRO COCO 330 ML', 'MNM', 'PCS', '10000', '11', '110000', '2022-11-07 19:59:52', NULL, NULL, NULL),
('74c24c29aebf9187cd0e3c094883ab0e', '20221108141557806', '20221108141557806', '99797ea5ce8cc9b4ed11fdaa69476dfa29', '28', '28', 'ROKOK ECERAN', 'ROKOK', 'PCS', '2500', '2', '5000', '2022-11-08 14:16:35', NULL, NULL, NULL),
('54ad478bca5c82082350d963d35b77e4', '20221108141557806', '20221108141557806', '99797ea5ce8cc9b4ed11fdaa69476dfa36', '35', '35', 'SUSU ULTRA MILK', 'MNM', 'PCS', '7000', '1', '7000', '2022-11-08 14:17:27', NULL, NULL, NULL),
('1f19a926a1b7768bc16af714a11dd0e3', '7b9f042ad9f7ceb9bf7ea922c596d83f', '22110033', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '3', '15000', '2022-11-08 17:32:18', NULL, NULL, NULL),
('2ad0174a755f0148e3038d4cb403d454', '0ba6a70d20f250b4bcb2220c1ed3af17', '22110054', '99797ea5ce8cc9b4ed11fdaa69476dfa27', '26', '26', 'AIR MINERAL PRIMA 600 ML', 'MNM', 'PCS', '3000', '2', '6000', '2022-11-08 17:54:22', NULL, NULL, NULL),
('213bfbcdc497e087fecf66ab693a2a56', '5ecffe0132a16f5e24061379327f8707', '22110062', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '2', '10000', '2022-11-08 18:56:30', NULL, NULL, NULL),
('c3e14c0e79479081e37d8b7f89a2749e', '5ecffe0132a16f5e24061379327f8707', '22110062', '99797ea5ce8cc9b4ed11fdaa69476dfa42', '41', '41', 'TEH PUCUK HARUM', 'MNM', 'PCS', '5000', '2', '10000', '2022-11-08 18:57:02', NULL, NULL, NULL),
('a41bd7eaf202b558081bffbce91d9d81', 'd223fa2001dffde19212c33008422ce8', '22110036', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '3', '15000', '2022-11-08 20:51:49', NULL, NULL, NULL),
('9c905b34351cf2a114b2bb86ee214a0b', '20221108205842455', '20221108205842455', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '1', '5000', '2022-11-08 20:58:48', NULL, NULL, NULL),
('2e3bdb6c1b34f3990258000249da260b', 'c5b69e058845b942bf7675e52d88898d', '22110055', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '4', '20000', '2022-11-08 22:01:18', NULL, NULL, NULL),
('9d8ccd0efe317206442613460f758f20', '20221109115401849', '20221109115401849', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '8', '40000', '2022-11-09 11:55:24', NULL, NULL, NULL),
('60630a766c49f6d673d1a3f64124cfd9', '20221109115401849', '20221109115401849', '99797ea5ce8cc9b4ed11fdaa69476dfa27', '26', '26', 'AIR MINERAL PRIMA 600 ML', 'MNM', 'PCS', '3000', '5', '15000', '2022-11-09 11:55:44', NULL, NULL, NULL),
('b9cb6b4df4a1fe33a11fbe92179e0a0b', '20221109115401849', '20221109115401849', '99797ea5ce8cc9b4ed11fdaa69476dfa38', '37', '37', 'TEBS BOTOL PLASTIK 500 ML', 'MNM', 'PCS', '7000', '4', '28000', '2022-11-09 11:56:19', NULL, NULL, NULL),
('90346c25e56ff5d1a7a785e54c944043', '20221109115401849', '20221109115401849', '99797ea5ce8cc9b4ed11fdaa69476dfa33', '32', '32', 'SARI KACANG HIJAU', 'MNM', 'PCS', '5000', '1', '5000', '2022-11-09 11:56:34', NULL, NULL, NULL),
('3e853a7dad4848f538fadb04f5270d50', '20221109115401849', '20221109115401849', '99797ea5ce8cc9b4ed11fdaa69476dfa13', '12', '12', 'HYDRO COCO 250 ML', 'MNM', 'PCS', '7500', '3', '22500', '2022-11-09 11:57:15', NULL, NULL, NULL),
('1446d61e10d91f155339da2bd2442adb', '20221109115401849', '20221109115401849', '99797ea5ce8cc9b4ed11fdaa69476dfa14', '13', '13', 'HYDRO COCO 330 ML', 'MNM', 'PCS', '10000', '1', '10000', '2022-11-09 11:57:23', NULL, NULL, NULL),
('8be1b20aabc02230ec25a75f281b7179', '20221109115401849', '20221109115401849', '99797ea5ce8cc9b4ed11fdaa69476dfa42', '41', '41', 'TEH PUCUK HARUM', 'MNM', 'PCS', '5000', '3', '15000', '2022-11-09 12:01:44', NULL, NULL, NULL),
('1ed17feec9240a1e960aeefa0cda10c0', '20221109115401849', '20221109115401849', '99797ea5ce8cc9b4ed11fdaa69476dfa23', '22', '22', 'MIZONE', 'MNM', 'PCS', '6000', '1', '6000', '2022-11-09 11:58:00', NULL, NULL, NULL),
('17819fe9f2c6c16ffab6a851cca1904b', '20221109115401849', '20221109115401849', '99797ea5ce8cc9b4ed11fdaa69476dfa25', '24', '24', 'POCARI SWEAT', 'MNM', 'PCS', '8000', '3', '24000', '2022-11-09 11:58:11', NULL, NULL, NULL),
('60a4057d4f67f49cb9a00e77f07e185e', '20221109115401849', '20221109115401849', '99797ea5ce8cc9b4ed11fdaa69476dfa28', '27', '27', 'PULPY ORANGE', 'MNM', 'PCS', '5000', '2', '10000', '2022-11-09 11:58:22', NULL, NULL, NULL),
('2c2dde83b11decbc66dfdd5374c5ff50', '20221109115401849', '20221109115401849', '99797ea5ce8cc9b4ed11fdaa69476dfa24', '23', '23', 'ORANGE WATER', 'MNM', 'PCS', '10000', '3', '30000', '2022-11-09 11:58:34', NULL, NULL, NULL),
('b8ebc3b941b5cbe88075b70197542895', '20221109120152616', '20221109120152616', '99797ea5ce8cc9b4ed11fdaa69476dfa9', '8', '8', 'SEWA LAPANGAN HIJAU MALAM WEEK DAY', 'SEWA', 'JAM', '120000', '7', '840000', '2022-11-09 12:02:51', NULL, NULL, NULL),
('092d2c1144ef0bf79a3e29b3f20d64b1', '20221109115401849', '20221109115401849', '99797ea5ce8cc9b4ed11fdaa69476dfa6', '5', '5', 'FLORIDINA', 'MNM', 'PCS', '5000', '3', '15000', '2022-11-09 11:58:53', NULL, NULL, NULL),
('6e8ef97e7da23a4ba340b3476de30c60', '20221109115401849', '20221109115401849', '99797ea5ce8cc9b4ed11fdaa69476dfa41', '40', '40', 'TEH JAVANA 350 ML', 'MNM', 'PCS', '5000', '1', '5000', '2022-11-09 11:59:05', NULL, NULL, NULL),
('3ac9c59348b1a873c10131e74fcb4ec4', '20221109115401849', '20221109115401849', '99797ea5ce8cc9b4ed11fdaa69476dfa31', '30', '30', 'ROKOK GUDANG GARAM SURYA 12', 'MNM', 'PCS', '22000', '1', '22000', '2022-11-09 11:59:43', NULL, NULL, NULL),
('42aac59337f313d914dbeee08aa30db3', '20221109115401849', '20221109115401849', '99797ea5ce8cc9b4ed11fdaa69476dfa30', '29', '29', 'ROKOK GUDANG GARAM SIGNATURE', 'MNM', 'PCS', '20000', '1', '20000', '2022-11-09 11:59:52', NULL, NULL, NULL),
('90b3111b8a9c606713b13b719ff2d324', '20221109120152616', '20221109120152616', '99797ea5ce8cc9b4ed11fdaa69476dfa19', '18', '18', 'SEWA LAPANGAN MERAH MALAM WEEK DAY', 'SEWA', 'JAM', '120000', '5', '600000', '2022-11-09 12:03:19', NULL, NULL, NULL),
('48632c5098bb4ed430fa881b356bca2e', '20221109155401992', '20221109155401992', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '2', '10000', '2022-11-09 15:54:30', NULL, NULL, NULL),
('f8c21eec7e0c9ef5c810def1b60be422', '20221109155401992', '20221109155401992', '99797ea5ce8cc9b4ed11fdaa69476dfa31', '30', '30', 'ROKOK GUDANG GARAM SURYA 12', 'MNM', 'PCS', '22000', '2', '44000', '2022-11-09 15:54:42', NULL, NULL, NULL),
('3caee2180c24582fc4868f79884c2c1f', '20221109155401992', '20221109155401992', '99797ea5ce8cc9b4ed11fdaa69476dfa29', '28', '28', 'ROKOK ECERAN', 'ROKOK', 'PCS', '2500', '4', '10000', '2022-11-09 15:54:56', NULL, NULL, NULL),
('4424c6d5c139f82d5843f2a55a8ecd75', '20221109155401992', '20221109155401992', '99797ea5ce8cc9b4ed11fdaa69476dfa11', '10', '10', 'SEWA LAPANGAN HIJAU PAGI WEEK DAY', 'SEWA', 'JAM', '80000', '1', '80000', '2022-11-09 15:55:11', NULL, NULL, NULL),
('92e38da16f1d3cb93cc828f487d76512', '43d0fd8e5b83cebad3ca32ffce3ca2af', '22110064', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '4', '20000', '2022-11-09 17:01:13', NULL, NULL, NULL),
('2f19fa9ddf19bf3d5d738ac6ae88960c', '43d0fd8e5b83cebad3ca32ffce3ca2af', '22110064', '99797ea5ce8cc9b4ed11fdaa69476dfa19', '18', '18', 'SEWA LAPANGAN MERAH MALAM WEEK DAY', 'SEWA', 'JAM', '120000', '1', '120000', '2022-11-09 17:26:15', NULL, NULL, NULL),
('f279cd150f3500b0f65c6bd37f736c65', '45f6b1fecfb782e2518b3d970003178a', '22110056', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '1', '5000', '2022-11-09 18:59:16', NULL, NULL, NULL),
('4c20c3dc0d56b6976f4ca8088c760885', '6241fc7ec4009507880f47c1ca0f2e7c', '22110040', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '2', '10000', '2022-11-09 20:55:44', NULL, NULL, NULL),
('84efc72cf3be42dffc63ae54a02bdff2', '57e0546f9cb369c85de374e2e4ca37ae', '22110039', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '2', '10000', '2022-11-09 21:03:29', NULL, NULL, NULL),
('31db54fbaaad379f4d8f83e9749affb0', '20221110155503646', '20221110155503646', '99797ea5ce8cc9b4ed11fdaa69476dfa27', '26', '26', 'AIR MINERAL PRIMA 600 ML', 'MNM', 'PCS', '3000', '1', '3000', '2022-11-10 15:55:25', NULL, NULL, NULL),
('52bf3f788e95ccd3d2fc2172b166fdb4', '20221110155503646', '20221110155503646', '99797ea5ce8cc9b4ed11fdaa69476dfa40', '39', '39', 'TEH BOTOL SOSRO PLASTIK 350 ML', 'MNM', 'PCS', '5000', '1', '5000', '2022-11-10 15:55:49', NULL, NULL, NULL),
('5b51286e0674ebf90573517f589268d9', '20221110155503646', '20221110155503646', '99797ea5ce8cc9b4ed11fdaa69476dfa13', '12', '12', 'HYDRO COCO 250 ML', 'MNM', 'PCS', '7500', '1', '7500', '2022-11-10 15:55:56', NULL, NULL, NULL),
('a76ef88f244ecb9364e1facafdfa4b43', '20221110155503646', '20221110155503646', '99797ea5ce8cc9b4ed11fdaa69476dfa16', '15', '15', 'ISO PLUS', 'MNM', 'PCS', '5000', '2', '10000', '2022-11-10 15:56:24', NULL, NULL, NULL),
('96032a4173809efb801b7df87bf7e5b6', '20221110155503646', '20221110155503646', '99797ea5ce8cc9b4ed11fdaa69476dfa29', '28', '28', 'ROKOK ECERAN', 'ROKOK', 'PCS', '2500', '2', '5000', '2022-11-10 15:56:38', NULL, NULL, NULL),
('50cf83f4c23ffe8216549919112b80c0', '20221110200305464', '20221110200305464', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '1', '5000', '2022-11-10 20:03:12', NULL, NULL, NULL),
('c90b216239f81b5ef97644647059b2ac', '20221110200305464', '20221110200305464', '99797ea5ce8cc9b4ed11fdaa69476dfa27', '26', '26', 'AIR MINERAL PRIMA 600 ML', 'MNM', 'PCS', '3000', '1', '3000', '2022-11-10 20:03:17', NULL, NULL, NULL),
('794163020169cb28c1bbb24fc0572503', '20221110200347811', '20221110200347811', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '1', '5000', '2022-11-10 20:03:53', NULL, NULL, NULL),
('c2bdb382142d03cff85dbfc38d1b9704', '20221110200347811', '20221110200347811', '99797ea5ce8cc9b4ed11fdaa69476dfa27', '26', '26', 'AIR MINERAL PRIMA 600 ML', 'MNM', 'PCS', '3000', '1', '3000', '2022-11-10 20:03:57', NULL, NULL, NULL),
('d5386f4a4f277c9664d21cc607069908', 'bb77bfbceb4428762cae3d159dfd975a', '22110044', '99797ea5ce8cc9b4ed11fdaa69476dfa9', '8', '8', 'SEWA LAPANGAN HIJAU MALAM WEEK DAY', 'SEWA', 'JAM', '120000', '1', '120000', '2022-11-10 22:03:04', NULL, NULL, NULL),
('91261a47ed8b4c43cce5829635ced190', 'bb77bfbceb4428762cae3d159dfd975a', '22110044', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '2', '10000', '2022-11-10 22:15:06', NULL, NULL, NULL),
('fdf77434c74bc1f6950407d264571a9e', 'bb77bfbceb4428762cae3d159dfd975a', '22110044', '99797ea5ce8cc9b4ed11fdaa69476dfa27', '26', '26', 'AIR MINERAL PRIMA 600 ML', 'MNM', 'PCS', '3000', '1', '3000', '2022-11-10 22:05:32', NULL, NULL, NULL),
('2b739006dde724e2ce2f1906f0cad297', '20221111170207449', '20221111170207449', '99797ea5ce8cc9b4ed11fdaa69476dfa12', '11', '11', 'SEWA LAPANGAN HIJAU PAGI WEEK END', 'SEWA', 'JAM', '100000', '1', '100000', '2022-11-11 17:02:23', NULL, NULL, NULL),
('e1496af6639a3b3e98c75389cb078c06', '20221111170207449', '20221111170207449', '99797ea5ce8cc9b4ed11fdaa69476dfa10', '9', '9', 'SEWA LAPANGAN HIJAU MALAM WEEK END', 'SEWA', 'JAM', '140000', '1', '140000', '2022-11-11 17:02:38', NULL, NULL, NULL),
('0b1175872f3bf40a1bc27ddfabaa07c7', '20221111192137943', '20221111192137943', '99797ea5ce8cc9b4ed11fdaa69476dfa10', '9', '9', 'SEWA LAPANGAN HIJAU MALAM WEEK END', 'SEWA', 'JAM', '140000', '2', '280000', '2022-11-11 19:22:10', NULL, NULL, NULL),
('e31a666414d272a445a55705262588a0', '20221113212142151', '20221113212142151', '99797ea5ce8cc9b4ed11fdaa69476dfa20', '19', '19', 'SEWA LAPANGAN MERAH MALAM WEEK END', 'SEWA', 'JAM', '140000', '2', '280000', '2022-11-13 21:22:07', NULL, NULL, NULL),
('b6454628bd0a0e403c7dc9a461339bd2', '20221116000935202', '20221116000935202', '99797ea5ce8cc9b4ed11fdaa69476dfa19', '18', '18', 'SEWA LAPANGAN MERAH MALAM WEEK DAY', 'SEWA', 'JAM', '120000', '1', '120000', '2022-11-16 00:10:09', NULL, NULL, NULL),
('8bc2b864b43030716833fe4b1e4f1b2d', '20221117222247117', '20221117222247117', '99797ea5ce8cc9b4ed11fdaa69476dfa9', '8', '8', 'SEWA LAPANGAN HIJAU MALAM WEEK DAY', 'SEWA', 'JAM', '120000', '2', '240000', '2022-11-17 22:23:27', NULL, NULL, NULL),
('cfd9ff1b94618cebaab2f9102cfcb1c7', '20221117222247117', '20221117222247117', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '3', '15000', '2022-11-17 22:23:34', NULL, NULL, NULL),
('4fe688ddaa44c5516b4d28f49a85d616', '20221118134959658', '20221118134959658', '33fb5bd883d55f362546a452975ad140', '2203', '', 'BOLA', 'Bola', 'PCS', '115000', '15', '1725000', '2022-11-18 13:50:49', NULL, NULL, NULL),
('a724623b5fd79d2bb48b10aa38890100', '20221118140833812', '20221118140833812', '33fb5bd883d55f362546a452975ad140', '2203', '', 'BOLA', 'Bola', 'PCS', '115000', '15', '1725000', '2022-11-18 14:08:43', NULL, NULL, NULL),
('947be19b43ae214f53b3d325b9ef1b8b', '20221118151428817', '20221118151428817', '99797ea5ce8cc9b4ed11fdaa69476dfa8', '7', '7', 'FRUIT TEA SOSRO PLATIK 350 ML', 'MNM', 'PCS', '5000', '1', '5000', '2022-11-18 15:14:37', NULL, NULL, NULL),
('b82544280f7d994db7552d17eb59ee08', '20221118201218115', '20221118201218115', '99797ea5ce8cc9b4ed11fdaa69476dfa10', '9', '9', 'SEWA LAPANGAN HIJAU MALAM WEEK END', 'SEWA', 'JAM', '140000', '3', '420000', '2022-11-18 20:12:48', NULL, NULL, NULL),
('ca3a31e4231c1e2f2895ba34ff53cd6e', '20221118222756377', '20221118222756377', '99797ea5ce8cc9b4ed11fdaa69476dfa36', '35', '35', 'SUSU ULTRA MILK', 'MNM', 'PCS', '7000', '2', '14000', '2022-11-18 22:52:11', NULL, NULL, NULL),
('2668d601b9f80acbec8a92b95a3e884e', '20221118222756377', '20221118222756377', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '3', '15000', '2022-11-18 22:52:19', NULL, NULL, NULL),
('7ccc85d77817e31f19a89e062d28a7f9', '20221118222756377', '20221118222756377', '99797ea5ce8cc9b4ed11fdaa69476dfa13', '12', '12', 'HYDRO COCO 250 ML', 'MNM', 'PCS', '7500', '1', '7500', '2022-11-18 22:52:36', NULL, NULL, NULL),
('de7eb575ffa703e53ef8f329fabf768d', '20221118222756377', '20221118222756377', '99797ea5ce8cc9b4ed11fdaa69476dfa14', '13', '13', 'HYDRO COCO 330 ML', 'MNM', 'PCS', '10000', '1', '10000', '2022-11-18 22:52:43', NULL, NULL, NULL),
('1f851c86dd2b7fe7e4779c98ceb60e5d', '20221118222756377', '20221118222756377', '99797ea5ce8cc9b4ed11fdaa69476dfa23', '22', '22', 'MIZONE', 'MNM', 'PCS', '6000', '1', '6000', '2022-11-18 22:52:57', NULL, NULL, NULL),
('309f79f6bf28412f36865f5ad6b06971', '20221118222756377', '20221118222756377', '99797ea5ce8cc9b4ed11fdaa69476dfa25', '24', '24', 'POCARI SWEAT', 'MNM', 'PCS', '8000', '1', '8000', '2022-11-18 22:53:10', NULL, NULL, NULL),
('c866bc24089cfb9a9883c8d4ac6d92ee', '20221118230712953', '20221118230712953', '99797ea5ce8cc9b4ed11fdaa69476dfa8', '7', '7', 'FRUIT TEA SOSRO PLATIK 350 ML', 'MNM', 'PCS', '5000', '1', '5000', '2022-11-18 23:07:21', NULL, NULL, NULL),
('5fe1b3fed6883bc6b0be690d84482bf6', '20221119112123484', '20221119112123484', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '3', '15000', '2022-11-19 11:21:49', NULL, NULL, NULL),
('e66a035afff6cdab14e334086c8eae0d', '2022111911241818', '2022111911241818', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '3', '15000', '2022-11-19 11:24:32', NULL, NULL, NULL),
('aaca9dd7341fc6c2bcc5081e82e95927', '2022111911485410', '2022111911485410', '99797ea5ce8cc9b4ed11fdaa69476dfa23', '22', '22', 'MIZONE', 'MNM', 'PCS', '6000', '2', '12000', '2022-11-19 11:49:09', NULL, NULL, NULL),
('09d4bcb6ab893cfe1b64aea801686f4f', '20221120212321596', '20221120212321596', '99797ea5ce8cc9b4ed11fdaa69476dfa20', '19', '19', 'SEWA LAPANGAN MERAH MALAM WEEK END', 'SEWA', 'JAM', '140000', '2', '280000', '2022-11-20 21:23:41', NULL, NULL, NULL),
('3dc134981a2a460a139955a976d22e74', '20221120212835664', '20221120212835664', '99797ea5ce8cc9b4ed11fdaa69476dfa20', '19', '19', 'SEWA LAPANGAN MERAH MALAM WEEK END', 'SEWA', 'JAM', '140000', '2', '280000', '2022-11-20 21:28:56', NULL, NULL, NULL),
('c63d1876229b0054fb3bcc7bca164305', '20221123001110312', '20221123001110312', '99797ea5ce8cc9b4ed11fdaa69476dfa9', '8', '8', 'SEWA LAPANGAN HIJAU MALAM WEEK DAY', 'SEWA', 'JAM', '120000', '1', '120000', '2022-11-23 00:11:28', NULL, NULL, NULL),
('6b37822166c00ab28fd924ebdf6d5003', '2022112315214184', '2022112315214184', '99797ea5ce8cc9b4ed11fdaa69476dfa30', '29', '29', 'ROKOK GUDANG GARAM SIGNATURE', 'MNM', 'PCS', '21000', '1', '21000', '2022-11-23 15:22:15', NULL, NULL, NULL),
('7d0a05bcad2db1bbe87f958a394eba9c', '2022112315214184', '2022112315214184', '99797ea5ce8cc9b4ed11fdaa69476dfa14', '13', '13', 'HYDRO COCO 330 ML', 'MNM', 'PCS', '10000', '1', '10000', '2022-11-23 15:21:50', NULL, NULL, NULL),
('1604d551d309630c939a5e3347a078da', '2022112315214184', '2022112315214184', '99797ea5ce8cc9b4ed11fdaa69476dfa29', '28', '28', 'ROKOK ECERAN', 'ROKOK', 'PCS', '2500', '2', '5000', '2022-11-23 15:22:02', NULL, NULL, NULL),
('828a0c5c50a99f23e80a7aea61fb2a46', '20221123223222213', '20221123223222213', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '8', '40000', '2022-11-23 22:34:21', NULL, NULL, NULL),
('b700ecdb0cd1f9f4390a14a0d0321a85', '20221123223222213', '20221123223222213', '99797ea5ce8cc9b4ed11fdaa69476dfa29', '28', '28', 'ROKOK ECERAN', 'ROKOK', 'PCS', '2500', '2', '5000', '2022-11-23 22:34:46', NULL, NULL, NULL),
('aec3f4ea3ea32e486866544914b02d7f', '20221123223222213', '20221123223222213', '99797ea5ce8cc9b4ed11fdaa69476dfa17', '16', '16', 'KOPI GOLDA', 'MNM', 'PCS', '5000', '1', '5000', '2022-11-23 22:35:07', NULL, NULL, NULL),
('d6347485e31573218a43bbebf21b4952', '20221123223222213', '20221123223222213', '99797ea5ce8cc9b4ed11fdaa69476dfa23', '22', '22', 'MIZONE', 'MNM', 'PCS', '6000', '1', '6000', '2022-11-23 22:35:39', NULL, NULL, NULL),
('d17d53d023b2c032d2a5db0b9e46907b', '20221123223222213', '20221123223222213', '99797ea5ce8cc9b4ed11fdaa69476dfa19', '18', '18', 'SEWA LAPANGAN MERAH MALAM WEEK DAY', 'SEWA', 'JAM', '120000', '2', '240000', '2022-11-23 22:36:17', NULL, NULL, NULL),
('1683df9fe4ae6c67d96f4f45450a60af', '20221124000137762', '20221124000137762', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '5', '25000', '2022-11-24 00:01:53', NULL, NULL, NULL),
('75e1300d27404cffb8bc2ffa32ecdbdc', '20221124000137762', '20221124000137762', '99797ea5ce8cc9b4ed11fdaa69476dfa9', '8', '8', 'SEWA LAPANGAN HIJAU MALAM WEEK DAY', 'SEWA', 'JAM', '120000', '1', '120000', '2022-11-24 00:02:10', NULL, NULL, NULL),
('2705ff785724bfe7f0c2fe8defed230a', '20221124220213540', '20221124220213540', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '2', '10000', '2022-11-24 22:02:30', NULL, NULL, NULL),
('1cf08851a9fd2956ef00786002c0581b', '20221124220213540', '20221124220213540', '99797ea5ce8cc9b4ed11fdaa69476dfa27', '26', '26', 'AIR MINERAL PRIMA 600 ML', 'MNM', 'PCS', '3000', '2', '6000', '2022-11-24 22:02:41', NULL, NULL, NULL),
('c7ce9876f4986e1249dcaa352f2332a8', '20221124220213540', '20221124220213540', '99797ea5ce8cc9b4ed11fdaa69476dfa9', '8', '8', 'SEWA LAPANGAN HIJAU MALAM WEEK DAY', 'SEWA', 'JAM', '120000', '2', '240000', '2022-11-24 22:03:03', NULL, NULL, NULL),
('3f18072b30048f339f3992775295eecb', '20221125124055744', '20221125124055744', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '2', '10000', '2022-11-25 12:41:11', NULL, NULL, NULL),
('e7b824dfedf2d6295fdeba17f14b4e7d', '20221125124055744', '20221125124055744', '99797ea5ce8cc9b4ed11fdaa69476dfa27', '26', '26', 'AIR MINERAL PRIMA 600 ML', 'MNM', 'PCS', '3000', '2', '6000', '2022-11-25 12:41:18', NULL, NULL, NULL),
('8b91d781858acdfce65426faef0e7811', '20221125124055744', '20221125124055744', '99797ea5ce8cc9b4ed11fdaa69476dfa14', '13', '13', 'HYDRO COCO 330 ML', 'MNM', 'PCS', '10000', '1', '10000', '2022-11-25 12:41:28', NULL, NULL, NULL),
('6611435a3ee4b5e9e1af21ab39f48ac4', '20221125124055744', '20221125124055744', '99797ea5ce8cc9b4ed11fdaa69476dfa42', '41', '41', 'TEH PUCUK HARUM', 'MNM', 'PCS', '5000', '1', '5000', '2022-11-25 12:41:41', NULL, NULL, NULL),
('fd201e90d4d1d593253c09648d0e7f77', '20221125124055744', '20221125124055744', '99797ea5ce8cc9b4ed11fdaa69476dfa25', '24', '24', 'POCARI SWEAT', 'MNM', 'PCS', '8000', '1', '8000', '2022-11-25 12:41:59', NULL, NULL, NULL),
('79b398905ccd6502e9b53d8b211b4644', '20221125124055744', '20221125124055744', '99797ea5ce8cc9b4ed11fdaa69476dfa31', '30', '30', 'ROKOK GUDANG GARAM SURYA 12', 'Rokok', 'PCS', '23000', '1', '23000', '2022-11-25 12:42:14', NULL, NULL, NULL),
('d36392d6e8228dc847008cf4428382df', '2022112512464479', '2022112512464479', '99797ea5ce8cc9b4ed11fdaa69476dfa31', '30', '30', 'ROKOK GUDANG GARAM SURYA 12', 'Rokok', 'PCS', '23000', '1', '23000', '2022-11-25 12:46:54', NULL, NULL, NULL),
('50d69d662f172a1ff1be8ca85a8e884d', '20221125125344520', '20221125125344520', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '2', '10000', '2022-11-25 12:54:08', NULL, NULL, NULL),
('a7e46c13e2950d610e9cc554529f66f2', '20221125125344520', '20221125125344520', '99797ea5ce8cc9b4ed11fdaa69476dfa27', '26', '26', 'AIR MINERAL PRIMA 600 ML', 'MNM', 'PCS', '3000', '2', '6000', '2022-11-25 12:54:14', NULL, NULL, NULL),
('b9175b00c9cb085bb717625ebb3db572', '20221125125344520', '20221125125344520', '99797ea5ce8cc9b4ed11fdaa69476dfa14', '13', '13', 'HYDRO COCO 330 ML', 'MNM', 'PCS', '10000', '1', '10000', '2022-11-25 12:54:21', NULL, NULL, NULL),
('417de3961811afba32e6f715c86875b5', '20221125125344520', '20221125125344520', '99797ea5ce8cc9b4ed11fdaa69476dfa42', '41', '41', 'TEH PUCUK HARUM', 'MNM', 'PCS', '5000', '1', '5000', '2022-11-25 12:54:30', NULL, NULL, NULL),
('23809b195da58bdae8298fc8eb52c2c6', '20221125125344520', '20221125125344520', '99797ea5ce8cc9b4ed11fdaa69476dfa25', '24', '24', 'POCARI SWEAT', 'MNM', 'PCS', '8000', '1', '8000', '2022-11-25 12:54:44', NULL, NULL, NULL),
('eb5f850f3ed7c2131c059d423457080c', '20221125125344520', '20221125125344520', '99797ea5ce8cc9b4ed11fdaa69476dfa31', '30', '30', 'ROKOK GUDANG GARAM SURYA 12', 'Rokok', 'PCS', '23000', '2', '46000', '2022-11-25 12:54:57', NULL, NULL, NULL),
('6e99835e0d6da8aa2ecde765c31486bc', '20221125220703388', '20221125220703388', '99797ea5ce8cc9b4ed11fdaa69476dfa13', '12', '12', 'HYDRO COCO 250 ML', 'MNM', 'PCS', '7500', '1', '7500', '2022-11-25 22:07:20', NULL, NULL, NULL),
('924cb6459391b0e72f24c2baff173618', '20221125220703388', '20221125220703388', '99797ea5ce8cc9b4ed11fdaa69476dfa36', '35', '35', 'SUSU ULTRA MILK', 'MNM', 'PCS', '7000', '1', '7000', '2022-11-25 22:07:29', NULL, NULL, NULL),
('77f7b20cee075f51d6734f309d807e39', '20221125220703388', '20221125220703388', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '2', '10000', '2022-11-25 22:07:37', NULL, NULL, NULL),
('7ccbf385be0d5060db6e630482282374', '20221125220703388', '20221125220703388', '99797ea5ce8cc9b4ed11fdaa69476dfa40', '39', '39', 'TEH BOTOL SOSRO PLASTIK 350 ML', 'MNM', 'PCS', '5000', '1', '5000', '2022-11-25 22:07:45', NULL, NULL, NULL),
('46f1399f553da77c3b75155c8655d673', '20221125231149896', '20221125231149896', '99797ea5ce8cc9b4ed11fdaa69476dfa23', '22', '22', 'MIZONE', 'MNM', 'PCS', '6000', '1', '6000', '2022-11-25 23:11:58', NULL, NULL, NULL),
('18af332e5e15578fa1173447cb5ef9d3', '20221125231149896', '20221125231149896', '99797ea5ce8cc9b4ed11fdaa69476dfa33', '32', '32', 'SARI KACANG HIJAU', 'MNM', 'PCS', '5000', '1', '5000', '2022-11-25 23:12:09', NULL, NULL, NULL),
('93e76b469e46852fbd3706b83d6859b0', '20221125231149896', '20221125231149896', '99797ea5ce8cc9b4ed11fdaa69476dfa13', '12', '12', 'HYDRO COCO 250 ML', 'MNM', 'PCS', '7500', '1', '7500', '2022-11-25 23:12:21', NULL, NULL, NULL),
('75d3e2c86673b1b3093d74d1cdd824d0', '20221125231149896', '20221125231149896', '99797ea5ce8cc9b4ed11fdaa69476dfa36', '35', '35', 'SUSU ULTRA MILK', 'MNM', 'PCS', '7000', '2', '14000', '2022-11-25 23:13:07', NULL, NULL, NULL),
('eba2d1635ce3ca3800fe682f5a86ff76', '20221125231149896', '20221125231149896', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '2', '10000', '2022-11-25 23:12:41', NULL, NULL, NULL),
('ed732febd142bfc18b6d76471f9f12cf', '20221125231149896', '20221125231149896', '99797ea5ce8cc9b4ed11fdaa69476dfa40', '39', '39', 'TEH BOTOL SOSRO PLASTIK 350 ML', 'MNM', 'PCS', '5000', '1', '5000', '2022-11-25 23:12:48', NULL, NULL, NULL),
('8d696a77705d0ca57e789fa787cc945c', '20221125231914372', '20221125231914372', '99797ea5ce8cc9b4ed11fdaa69476dfa10', '9', '9', 'SEWA LAPANGAN HIJAU MALAM WEEK END', 'SEWA', 'JAM', '140000', '1', '140000', '2022-11-25 23:19:31', NULL, NULL, NULL),
('43a1920853ca93b7cee34e4e9185b7b0', '20221125231914372', '20221125231914372', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '2', '10000', '2022-11-25 23:19:50', NULL, NULL, NULL),
('049f43004f11ffe004abdbd458c4f8a3', '20221127152021531', '20221127152021531', '99797ea5ce8cc9b4ed11fdaa69476dfa27', '26', '26', 'AIR MINERAL PRIMA 600 ML', 'MNM', 'PCS', '3000', '1', '3000', '2022-11-27 15:20:27', NULL, NULL, NULL),
('a72d8d11e4d66a91ecf00f177d12b5c4', '20221127152021531', '20221127152021531', '99797ea5ce8cc9b4ed11fdaa69476dfa40', '39', '39', 'TEH BOTOL SOSRO PLASTIK 350 ML', 'MNM', 'PCS', '5000', '1', '5000', '2022-11-27 15:20:43', NULL, NULL, NULL),
('9eee91b9475dab51983ae1a794703ac3', '20221127152021531', '20221127152021531', '99797ea5ce8cc9b4ed11fdaa69476dfa42', '41', '41', 'TEH PUCUK HARUM', 'MNM', 'PCS', '5000', '1', '5000', '2022-11-27 15:20:56', NULL, NULL, NULL),
('1fde5a876ec7380841a78f51f3a292e4', '20221127152021531', '20221127152021531', '99797ea5ce8cc9b4ed11fdaa69476dfa23', '22', '22', 'MIZONE', 'MNM', 'PCS', '6000', '1', '6000', '2022-11-27 15:21:05', NULL, NULL, NULL),
('2287e7467da081c0338162235eb2605f', '20221127152021531', '20221127152021531', '99797ea5ce8cc9b4ed11fdaa69476dfa8', '7', '7', 'FRUIT TEA SOSRO PLATIK 350 ML', 'MNM', 'PCS', '5000', '1', '5000', '2022-11-27 15:21:11', NULL, NULL, NULL),
('547c4155510cafb401fee67715d5210e', '20221127152021531', '20221127152021531', '99797ea5ce8cc9b4ed11fdaa69476dfa25', '24', '24', 'POCARI SWEAT', 'MNM', 'PCS', '8000', '2', '16000', '2022-11-27 15:21:20', NULL, NULL, NULL),
('3f8a63e9495c2598a3a93a4108b973d0', '20221127152021531', '20221127152021531', '99797ea5ce8cc9b4ed11fdaa69476dfa36', '35', '35', 'SUSU ULTRA MILK', 'MNM', 'PCS', '7000', '1', '7000', '2022-11-27 15:21:31', NULL, NULL, NULL),
('ae5e429466e12a038b65dca52e8a1299', '20221127152021531', '20221127152021531', '99797ea5ce8cc9b4ed11fdaa69476dfa6', '5', '5', 'FLORIDINA', 'MNM', 'PCS', '5000', '2', '10000', '2022-11-27 15:21:37', NULL, NULL, NULL),
('182e49a4a51f0ea47bca5ad0ee79a8da', '20221127210100969', '20221127210100969', '99797ea5ce8cc9b4ed11fdaa69476dfa20', '19', '19', 'SEWA LAPANGAN MERAH MALAM WEEK END', 'SEWA', 'JAM', '140000', '2', '280000', '2022-11-27 21:01:13', NULL, NULL, NULL),
('1d1940cb3bd187c2e13211349dc18a23', '2022112815415017', '2022112815415017', '99797ea5ce8cc9b4ed11fdaa69476dfa11', '10', '10', 'SEWA LAPANGAN HIJAU PAGI WEEK DAY', 'SEWA', 'JAM', '80000', '1', '80000', '2022-11-28 15:42:12', NULL, NULL, NULL),
('d2f2223751c98f383b95f5427974c8af', '2022112815415017', '2022112815415017', '99797ea5ce8cc9b4ed11fdaa69476dfa23', '22', '22', 'MIZONE', 'MNM', 'PCS', '6000', '2', '12000', '2022-11-28 15:42:43', NULL, NULL, NULL),
('aa8379232648e2191e5a23935e70fa4b', '2022112815415017', '2022112815415017', '99797ea5ce8cc9b4ed11fdaa69476dfa16', '15', '15', 'ISO PLUS', 'MNM', 'PCS', '5000', '1', '5000', '2022-11-28 15:42:58', NULL, NULL, NULL),
('615b8060c02dadd5dce00329de0cb5c0', '20221129122817842', '20221129122817842', '99797ea5ce8cc9b4ed11fdaa69476dfa37', '36', '36', 'TEBS BOTOL KACA', 'MNM', 'PCS', '4000', '1', '4000', '2022-11-29 12:28:32', NULL, NULL, NULL),
('d54ce3015874be8234527a7ea2b38a57', '20221129122817842', '20221129122817842', '99797ea5ce8cc9b4ed11fdaa69476dfa42', '41', '41', 'TEH PUCUK HARUM', 'MNM', 'PCS', '5000', '1', '5000', '2022-11-29 12:28:44', NULL, NULL, NULL),
('27259a79500b3f74d26c6fb38e80b2dc', '20221129122817842', '20221129122817842', '99797ea5ce8cc9b4ed11fdaa69476dfa7', '6', '6', 'FRUIT TEA SOSRO KACA 235 ML', 'MNM', 'PCS', '3000', '1', '3000', '2022-11-29 12:28:58', NULL, NULL, NULL),
('360f3eb448174c8996fe7400da490d6d', '20221129122817842', '20221129122817842', '99797ea5ce8cc9b4ed11fdaa69476dfa25', '24', '24', 'POCARI SWEAT', 'MNM', 'PCS', '8000', '1', '8000', '2022-11-29 12:29:15', NULL, NULL, NULL),
('6313f309765e00d419ba08ecffdf451b', '20221129122817842', '20221129122817842', '99797ea5ce8cc9b4ed11fdaa69476dfa29', '28', '28', 'ROKOK ECERAN', 'ROKOK', 'PCS', '2500', '2', '5000', '2022-11-29 12:29:39', NULL, NULL, NULL),
('9835962e1708c2b0890196f0d8b55722', '20221130160029499', '20221130160029499', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '2', '10000', '2022-11-30 16:00:47', NULL, NULL, NULL),
('798677cb57713f70994566b831c3f601', '20221130160029499', '20221130160029499', '99797ea5ce8cc9b4ed11fdaa69476dfa13', '12', '12', 'HYDRO COCO 250 ML', 'MNM', 'PCS', '7500', '4', '30000', '2022-11-30 16:00:58', NULL, NULL, NULL),
('926bc4c15c862ac136f9113c3add6125', '20221130160029499', '20221130160029499', '99797ea5ce8cc9b4ed11fdaa69476dfa14', '13', '13', 'HYDRO COCO 330 ML', 'MNM', 'PCS', '10000', '2', '20000', '2022-11-30 16:01:13', NULL, NULL, NULL),
('30b7118e1b48817e32e4a0975846fb03', '20221130160029499', '20221130160029499', '99797ea5ce8cc9b4ed11fdaa69476dfa42', '41', '41', 'TEH PUCUK HARUM', 'MNM', 'PCS', '5000', '5', '25000', '2022-11-30 16:01:46', NULL, NULL, NULL),
('776d74ca8f4c2023f6f6e0fc0ecf8c03', '20221130160029499', '20221130160029499', '99797ea5ce8cc9b4ed11fdaa69476dfa25', '24', '24', 'POCARI SWEAT', 'MNM', 'PCS', '8000', '1', '8000', '2022-11-30 16:02:19', NULL, NULL, NULL),
('d217e5368aeedd4aa791f9fcaa802afb', '20221130160029499', '20221130160029499', '99797ea5ce8cc9b4ed11fdaa69476dfa16', '15', '15', 'ISO PLUS', 'MNM', 'PCS', '5000', '1', '5000', '2022-11-30 16:02:32', NULL, NULL, NULL),
('797ef0f5b69fd88d7a6ac350ac5abff9', '20221130160029499', '20221130160029499', '99797ea5ce8cc9b4ed11fdaa69476dfa36', '35', '35', 'SUSU ULTRA MILK', 'MNM', 'PCS', '7000', '4', '28000', '2022-11-30 16:03:07', NULL, NULL, NULL),
('e6365ffb1cf5eb0c6ca2b5c0d76e5177', '20221130160029499', '20221130160029499', '5a999455e84eca294d9dd7b616f04b1b', '8997212800035', '8997212800035', 'YUZU ISOTONIC 350ML', 'Minuman', 'PCS', '5000', '1', '5000', '2022-11-30 16:03:14', NULL, NULL, NULL),
('4257cd5dabc3d83691e95794c81159d1', '20221130230858904', '20221130230858904', '99797ea5ce8cc9b4ed11fdaa69476dfa9', '8', '8', 'SEWA LAPANGAN HIJAU MALAM WEEK DAY', 'SEWA', 'JAM', '120000', '1', '120000', '2022-11-30 23:09:09', NULL, NULL, NULL),
('690ff4f86d52eb59e06dda4152760af5', '20221130230858904', '20221130230858904', '99797ea5ce8cc9b4ed11fdaa69476dfa36', '35', '35', 'SUSU ULTRA MILK', 'MNM', 'PCS', '7000', '2', '14000', '2022-11-30 23:09:36', NULL, NULL, NULL),
('aaf651cc91a00e8445ab9800e96b6a3a', '20221201160009387', '20221201160009387', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '5', '25000', '2022-12-01 16:00:17', NULL, NULL, NULL),
('0fd99d972d446908910ddd3cd950b470', '20221201160009387', '20221201160009387', '99797ea5ce8cc9b4ed11fdaa69476dfa27', '26', '26', 'AIR MINERAL PRIMA 600 ML', 'MNM', 'PCS', '3000', '4', '12000', '2022-12-01 16:00:25', NULL, NULL, NULL),
('7e335c987c7ca1f8c6eef00039c18ab5', '20221201160009387', '20221201160009387', '99797ea5ce8cc9b4ed11fdaa69476dfa37', '36', '36', 'TEBS BOTOL KACA', 'MNM', 'PCS', '4000', '1', '4000', '2022-12-01 16:00:39', NULL, NULL, NULL),
('a28127e2cc2c62cbf0d26b6871f80b61', '20221201160009387', '20221201160009387', '99797ea5ce8cc9b4ed11fdaa69476dfa13', '12', '12', 'HYDRO COCO 250 ML', 'MNM', 'PCS', '7500', '1', '7500', '2022-12-01 16:00:53', NULL, NULL, NULL),
('d58adaacbc2dea57c1e52472daa174de', '20221201160009387', '20221201160009387', '99797ea5ce8cc9b4ed11fdaa69476dfa14', '13', '13', 'HYDRO COCO 330 ML', 'MNM', 'PCS', '10000', '3', '30000', '2022-12-01 16:01:07', NULL, NULL, NULL),
('0a7ef200223a5fc920936611e15b9a84', '20221201160009387', '20221201160009387', '99797ea5ce8cc9b4ed11fdaa69476dfa42', '41', '41', 'TEH PUCUK HARUM', 'MNM', 'PCS', '5000', '1', '5000', '2022-12-01 16:01:23', NULL, NULL, NULL),
('41e8fbba4a1c931a4b2c7188f238ad72', '20221201160009387', '20221201160009387', '99797ea5ce8cc9b4ed11fdaa69476dfa23', '22', '22', 'MIZONE', 'MNM', 'PCS', '6000', '1', '6000', '2022-12-01 16:01:30', NULL, NULL, NULL),
('f4e2e1ded74c0bc9117a027b13233a4c', '20221201160009387', '20221201160009387', '99797ea5ce8cc9b4ed11fdaa69476dfa8', '7', '7', 'FRUIT TEA SOSRO PLATIK 350 ML', 'MNM', 'PCS', '5000', '2', '10000', '2022-12-01 16:01:47', NULL, NULL, NULL),
('1647fb863995259660e9136e8192f7c5', '20221201160009387', '20221201160009387', '99797ea5ce8cc9b4ed11fdaa69476dfa25', '24', '24', 'POCARI SWEAT', 'MNM', 'PCS', '8000', '2', '16000', '2022-12-01 16:02:13', NULL, NULL, NULL),
('f1dcb3f38e072bfab40dcaf3dbba3297', '20221201160009387', '20221201160009387', '99797ea5ce8cc9b4ed11fdaa69476dfa16', '15', '15', 'ISO PLUS', 'MNM', 'PCS', '5000', '1', '5000', '2022-12-01 16:02:19', NULL, NULL, NULL),
('9ee9e9dd63e98992b9b9b7490d95d271', '20221201160009387', '20221201160009387', '99797ea5ce8cc9b4ed11fdaa69476dfa4', '3', '3', 'BIR BINTANG 0xpersenx', 'MNM', 'PCS', '15000', '1', '15000', '2022-12-01 16:02:41', NULL, NULL, NULL),
('644d64bc69924105e8c5d67dd125d9b4', '20221201160009387', '20221201160009387', '99797ea5ce8cc9b4ed11fdaa69476dfa29', '28', '28', 'ROKOK ECERAN', 'ROKOK', 'PCS', '2500', '1', '2500', '2022-12-01 16:02:49', NULL, NULL, NULL),
('76b7315ce86cf21dde9ea446471fc714', '20221201220232595', '20221201220232595', '99797ea5ce8cc9b4ed11fdaa69476dfa9', '8', '8', 'SEWA LAPANGAN HIJAU MALAM WEEK DAY', 'SEWA', 'JAM', '120000', '2', '240000', '2022-12-01 22:02:58', NULL, NULL, NULL),
('d010dbe3929c608c90179664a7d542ec', '20221201220232595', '20221201220232595', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '5', '25000', '2022-12-01 22:03:09', NULL, NULL, NULL),
('1b7b3e2c6c342c4848298f6805ce8edd', '20221201220232595', '20221201220232595', '99797ea5ce8cc9b4ed11fdaa69476dfa27', '26', '26', 'AIR MINERAL PRIMA 600 ML', 'MNM', 'PCS', '3000', '1', '3000', '2022-12-01 22:03:14', NULL, NULL, NULL),
('e1ec5ff5a1b349296131dab780cd96fa', '20221201220232595', '20221201220232595', '37e320310ce2485ec011d30eef1eb52e', '0001', '', 'Hastspray', 'Hastspray', 'PCS', '70000', '1', '70000', '2022-12-01 22:03:53', NULL, NULL, NULL),
('014a40ad1d8f96ac98ed282951577f3e', '20221202201539253', '20221202201539253', '99797ea5ce8cc9b4ed11fdaa69476dfa10', '9', '9', 'SEWA LAPANGAN HIJAU MALAM WEEK END', 'SEWA', 'JAM', '140000', '2', '280000', '2022-12-02 20:16:10', NULL, NULL, NULL),
('648830f3882bf7d88356a07bb86aa407', '20221202201539253', '20221202201539253', '5b3dd8aff28ed9108acf7911ad778318', '888999', '', 'SEWA LAPANGAN MERAH SETENGAH JAM MERAH WEEKEND', 'Bola', 'PCS', '70000', '1', '70000', '2022-12-02 20:16:22', NULL, NULL, NULL),
('92ec84c4e7a7b7c9d94dba136a403e4f', '20221202201539253', '20221202201539253', '99797ea5ce8cc9b4ed11fdaa69476dfa40', '39', '39', 'TEH BOTOL SOSRO PLASTIK 350 ML', 'MNM', 'PCS', '5000', '1', '5000', '2022-12-02 20:16:30', NULL, NULL, NULL),
('2204371bfcf83f9b1d7c03940b4b59a2', '20221202215521949', '20221202215521949', '99797ea5ce8cc9b4ed11fdaa69476dfa10', '9', '9', 'SEWA LAPANGAN HIJAU MALAM WEEK END', 'SEWA', 'JAM', '140000', '1', '140000', '2022-12-02 21:55:40', NULL, NULL, NULL),
('bdbe5f8c6f34918b05dd6a16b52aaada', '20221202215521949', '20221202215521949', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '2', '10000', '2022-12-02 21:56:00', NULL, NULL, NULL),
('fb641155868d7062eec522ffead54fd2', '20221206155947342', '20221206155947342', '99797ea5ce8cc9b4ed11fdaa69476dfa11', '10', '10', 'SEWA LAPANGAN HIJAU PAGI WEEK DAY', 'SEWA', 'JAM', '80000', '2', '160000', '2022-12-06 16:00:04', NULL, NULL, NULL),
('a85b748ab55b739f851167539a2322c6', '20221206155947342', '20221206155947342', '99797ea5ce8cc9b4ed11fdaa69476dfa21', '20', '20', 'SEWA LAPANGAN MERAH PAGI WEEK DAY', 'SEWA', 'JAM', '80000', '2', '160000', '2022-12-06 16:00:23', NULL, NULL, NULL),
('cbdd292fd2978918b5ac0870f5251173', '20221206154226820', '20221206154226820', '99797ea5ce8cc9b4ed11fdaa69476dfa27', '26', '26', 'AIR MINERAL PRIMA 600 ML', 'MNM', 'PCS', '3000', '1', '3000', '2022-12-06 15:44:38', NULL, NULL, NULL),
('62b94bb422c886bc6d7cd71d7c1a1ae4', '20221206155947342', '20221206155947342', '99797ea5ce8cc9b4ed11fdaa69476dfa27', '26', '26', 'AIR MINERAL PRIMA 600 ML', 'MNM', 'PCS', '3000', '1', '3000', '2022-12-06 16:00:33', NULL, NULL, NULL),
('0f2db74ccffbcb0ebe748fa62bdcc142', '20221206155947342', '20221206155947342', '99797ea5ce8cc9b4ed11fdaa69476dfa37', '36', '36', 'TEBS BOTOL KACA', 'MNM', 'PCS', '4000', '2', '8000', '2022-12-06 16:00:51', NULL, NULL, NULL),
('c0a81c51fc05693f6014ea4bb7f371f9', '20221206155947342', '20221206155947342', '99797ea5ce8cc9b4ed11fdaa69476dfa42', '41', '41', 'TEH PUCUK HARUM', 'MNM', 'PCS', '5000', '5', '25000', '2022-12-06 16:01:09', NULL, NULL, NULL),
('999f757b4d166a16c64be658e6f91ae8', '20221206155947342', '20221206155947342', '99797ea5ce8cc9b4ed11fdaa69476dfa23', '22', '22', 'MIZONE', 'MNM', 'PCS', '6000', '1', '6000', '2022-12-06 16:01:15', NULL, NULL, NULL),
('c6a8d7b210e196168bcd0b0a4fad0b0f', '20221206155947342', '20221206155947342', '99797ea5ce8cc9b4ed11fdaa69476dfa25', '24', '24', 'POCARI SWEAT', 'MNM', 'PCS', '8000', '2', '16000', '2022-12-06 16:01:32', NULL, NULL, NULL),
('ba1897839481a192e496deada0f28838', '20221206155947342', '20221206155947342', '99797ea5ce8cc9b4ed11fdaa69476dfa8', '7', '7', 'FRUIT TEA SOSRO PLATIK 350 ML', 'MNM', 'PCS', '5000', '1', '5000', '2022-12-06 16:01:52', NULL, NULL, NULL),
('331e78e755cbe8c483109549e66e5975', '20221206155947342', '20221206155947342', '99797ea5ce8cc9b4ed11fdaa69476dfa36', '35', '35', 'SUSU ULTRA MILK', 'MNM', 'PCS', '7000', '2', '14000', '2022-12-06 16:02:11', NULL, NULL, NULL),
('1f0292302709312a5d80a307e888600b', '20221206155947342', '20221206155947342', '99797ea5ce8cc9b4ed11fdaa69476dfa31', '30', '30', 'ROKOK GUDANG GARAM SURYA 12', 'Rokok', 'PCS', '23000', '1', '23000', '2022-12-06 16:02:26', NULL, NULL, NULL),
('a9182762ebd9b1fed6accfb4a068e9a8', '20221208152504358', '20221208152504358', '99797ea5ce8cc9b4ed11fdaa69476dfa9', '8', '8', 'SEWA LAPANGAN HIJAU MALAM WEEK DAY', 'SEWA', 'JAM', '120000', '2', '240000', '2022-12-08 15:25:24', NULL, NULL, NULL),
('9d66cf5900b77882d4e16fc65bcec384', '20221208152504358', '20221208152504358', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '5', '25000', '2022-12-08 15:25:33', NULL, NULL, NULL),
('87ed555a60b1e246210afe013f4335ef', '20221208152504358', '20221208152504358', '99797ea5ce8cc9b4ed11fdaa69476dfa27', '26', '26', 'AIR MINERAL PRIMA 600 ML', 'MNM', 'PCS', '3000', '1', '3000', '2022-12-08 15:25:39', NULL, NULL, NULL),
('38f9604d9683ed238674e4b5c1ad03a0', '20221208152504358', '20221208152504358', '99797ea5ce8cc9b4ed11fdaa69476dfa42', '41', '41', 'TEH PUCUK HARUM', 'MNM', 'PCS', '5000', '2', '10000', '2022-12-08 15:26:04', NULL, NULL, NULL),
('461d3ecde38c22735775534dbfff5fb9', '20221208152504358', '20221208152504358', '99797ea5ce8cc9b4ed11fdaa69476dfa23', '22', '22', 'MIZONE', 'MNM', 'PCS', '6000', '1', '6000', '2022-12-08 15:26:09', NULL, NULL, NULL),
('ffa6e969cf187511ab40d0ab3d957b5e', '20221208152504358', '20221208152504358', '99797ea5ce8cc9b4ed11fdaa69476dfa35', '34', '34', 'SUSU MILKU', 'MNM', 'PCS', '5000', '2', '10000', '2022-12-08 15:26:21', NULL, NULL, NULL),
('8deb0e69cd7e729aa700bab16f32c7f5', '20221209160335149', '20221209160335149', '99797ea5ce8cc9b4ed11fdaa69476dfa12', '11', '11', 'SEWA LAPANGAN HIJAU PAGI WEEK END', 'SEWA', 'JAM', '100000', '2', '200000', '2022-12-09 16:03:57', NULL, NULL, NULL),
('8d1ac85942dbf110cdeb947e2504ad4b', '20221209160335149', '20221209160335149', '99797ea5ce8cc9b4ed11fdaa69476dfa39', '38', '38', 'TEH BOTOL SOSRO KACA', 'MNM', 'PCS', '3000', '2', '6000', '2022-12-09 16:04:11', NULL, NULL, NULL),
('a2511fb4710665390856bab7c3d71482', '20221209160335149', '20221209160335149', '99797ea5ce8cc9b4ed11fdaa69476dfa37', '36', '36', 'TEBS BOTOL KACA', 'MNM', 'PCS', '4000', '1', '4000', '2022-12-09 16:04:53', NULL, NULL, NULL),
('a45e5257ec6f726899a3f84082777226', '20221209160335149', '20221209160335149', '99797ea5ce8cc9b4ed11fdaa69476dfa14', '13', '13', 'HYDRO COCO 330 ML', 'MNM', 'PCS', '10000', '1', '10000', '2022-12-09 16:05:04', NULL, NULL, NULL),
('65202d43548c1302c5c3eb4381a55ad6', '20221209160335149', '20221209160335149', '99797ea5ce8cc9b4ed11fdaa69476dfa42', '41', '41', 'TEH PUCUK HARUM', 'MNM', 'PCS', '5000', '1', '5000', '2022-12-09 16:05:18', NULL, NULL, NULL),
('bfc383d56bfa603989b10e9bcb8bca90', '20221209160335149', '20221209160335149', '99797ea5ce8cc9b4ed11fdaa69476dfa23', '22', '22', 'MIZONE', 'MNM', 'PCS', '6000', '2', '12000', '2022-12-09 16:05:29', NULL, NULL, NULL),
('7787264ba0e8f2d283d6a7d9c57df70a', '20221209160335149', '20221209160335149', '99797ea5ce8cc9b4ed11fdaa69476dfa16', '15', '15', 'ISO PLUS', 'MNM', 'PCS', '5000', '1', '5000', '2022-12-09 16:05:35', NULL, NULL, NULL),
('2b32a9ff3277be8a0fb441d8a94224bd', '20221209160335149', '20221209160335149', '99797ea5ce8cc9b4ed11fdaa69476dfa36', '35', '35', 'SUSU ULTRA MILK', 'MNM', 'PCS', '7000', '1', '7000', '2022-12-09 16:05:46', NULL, NULL, NULL),
('8e0b5bef8f8b4ff3b25ffc2552691b6a', '20221209160335149', '20221209160335149', '99797ea5ce8cc9b4ed11fdaa69476dfa30', '29', '29', 'ROKOK GUDANG GARAM SIGNATURE', 'MNM', 'PCS', '21000', '1', '21000', '2022-12-09 16:06:06', NULL, NULL, NULL),
('0de5f009051ab2536e9178de796c1141', '20221209160335149', '20221209160335149', '99797ea5ce8cc9b4ed11fdaa69476dfa29', '28', '28', 'ROKOK ECERAN', 'ROKOK', 'PCS', '2500', '2', '5000', '2022-12-09 16:06:22', NULL, NULL, NULL),
('d0faa8e8f96bbb70513c32e41b6fa311', '20221210152339383', '20221210152339383', '99797ea5ce8cc9b4ed11fdaa69476dfa12', '11', '11', 'SEWA LAPANGAN HIJAU PAGI WEEK END', 'SEWA', 'JAM', '100000', '3', '300000', '2022-12-10 15:25:12', NULL, NULL, NULL),
('64f38e7c3c709962ef91c8398c5f9d2b', '20221210152339383', '20221210152339383', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '7', '35000', '2022-12-10 15:25:25', NULL, NULL, NULL),
('fa78f7e5fbcb8b42c892f1c3f4cdd53b', '20221210152339383', '20221210152339383', '99797ea5ce8cc9b4ed11fdaa69476dfa27', '26', '26', 'AIR MINERAL PRIMA 600 ML', 'MNM', 'PCS', '3000', '1', '3000', '2022-12-10 15:25:30', NULL, NULL, NULL),
('b92f782f6e4346175998dcd924b0a9c5', '20221210152339383', '20221210152339383', '99797ea5ce8cc9b4ed11fdaa69476dfa39', '38', '38', 'TEH BOTOL SOSRO KACA', 'MNM', 'PCS', '3000', '9', '27000', '2022-12-10 15:25:46', NULL, NULL, NULL),
('6acdab5f6dc3c645d98a8fb3fc293941', '20221210152339383', '20221210152339383', '99797ea5ce8cc9b4ed11fdaa69476dfa37', '36', '36', 'TEBS BOTOL KACA', 'MNM', 'PCS', '4000', '1', '4000', '2022-12-10 15:28:38', NULL, NULL, NULL),
('f9b1073a753beba3d43d84c2b2b15239', '20221210152339383', '20221210152339383', '99797ea5ce8cc9b4ed11fdaa69476dfa13', '12', '12', 'HYDRO COCO 250 ML', 'MNM', 'PCS', '7500', '1', '7500', '2022-12-10 15:29:02', NULL, NULL, NULL),
('608e4026c1793156ea4183b157c5367d', '20221210152339383', '20221210152339383', '99797ea5ce8cc9b4ed11fdaa69476dfa14', '13', '13', 'HYDRO COCO 330 ML', 'MNM', 'PCS', '10000', '5', '50000', '2022-12-10 15:29:13', NULL, NULL, NULL),
('02869a7807550854d1dd8fb9e043c86b', '20221210152339383', '20221210152339383', '99797ea5ce8cc9b4ed11fdaa69476dfa42', '41', '41', 'TEH PUCUK HARUM', 'MNM', 'PCS', '5000', '3', '15000', '2022-12-10 15:29:27', NULL, NULL, NULL),
('21845d6d6356c3dadf8b047c4e28607e', '20221210152339383', '20221210152339383', '99797ea5ce8cc9b4ed11fdaa69476dfa23', '22', '22', 'MIZONE', 'MNM', 'PCS', '6000', '5', '30000', '2022-12-10 15:29:37', NULL, NULL, NULL),
('9170eddb839d4477c2fba3f9da88a87f', '20221210152339383', '20221210152339383', '99797ea5ce8cc9b4ed11fdaa69476dfa7', '6', '6', 'FRUIT TEA SOSRO KACA 235 ML', 'MNM', 'PCS', '3000', '9', '27000', '2022-12-10 15:29:53', NULL, NULL, NULL),
('4687370f8c54ce7e533eb0563a2db5db', '20221210152339383', '20221210152339383', '99797ea5ce8cc9b4ed11fdaa69476dfa25', '24', '24', 'POCARI SWEAT', 'MNM', 'PCS', '8000', '1', '8000', '2022-12-10 15:30:07', NULL, NULL, NULL),
('982c68ed0c5cadf02658d2021ebe9cd6', '20221210152339383', '20221210152339383', '99797ea5ce8cc9b4ed11fdaa69476dfa16', '15', '15', 'ISO PLUS', 'MNM', 'PCS', '5000', '1', '5000', '2022-12-10 15:30:15', NULL, NULL, NULL),
('b98a6a017fc79cb5dfcf75e171fe05ca', '20221210152339383', '20221210152339383', '99797ea5ce8cc9b4ed11fdaa69476dfa24', '23', '23', 'ORANGE WATER', 'MNM', 'PCS', '10000', '1', '10000', '2022-12-10 15:30:22', NULL, NULL, NULL),
('3aebb667fd25b3958d0249ef4d21f273', '20221210152339383', '20221210152339383', '99797ea5ce8cc9b4ed11fdaa69476dfa36', '35', '35', 'SUSU ULTRA MILK', 'MNM', 'PCS', '7000', '1', '7000', '2022-12-10 15:30:28', NULL, NULL, NULL),
('86223f0ffd4d9b7ccdb81ad7a3c6180f', '20221210152339383', '20221210152339383', '99797ea5ce8cc9b4ed11fdaa69476dfa6', '5', '5', 'FLORIDINA', 'MNM', 'PCS', '5000', '3', '15000', '2022-12-10 15:30:46', NULL, NULL, NULL),
('015be3d0d82e7ee4909660a4d334b1f9', '20221210152339383', '20221210152339383', '99797ea5ce8cc9b4ed11fdaa69476dfa4', '3', '3', 'BIR BINTANG 0xpersenx', 'MNM', 'PCS', '15000', '1', '15000', '2022-12-10 15:30:55', NULL, NULL, NULL),
('d2afd273e09ec91e95e9a1ad1604cae4', '20221210152339383', '20221210152339383', '5a999455e84eca294d9dd7b616f04b1b', '8997212800035', '8997212800035', 'YUZU ISOTONIC 350ML', 'Minuman', 'PCS', '5000', '1', '5000', '2022-12-10 15:31:04', NULL, NULL, NULL),
('7bae5a13c8dea752128f517dba507944', '20221210152339383', '20221210152339383', '99797ea5ce8cc9b4ed11fdaa69476dfa34', '33', '33', 'SNACK', 'MNM', 'PCS', '10000', '2', '20000', '2022-12-10 15:32:47', NULL, NULL, NULL),
('d2f70c9c332e55f5957198a0fc5e2582', '20221211114300556', '20221211114300556', '99797ea5ce8cc9b4ed11fdaa69476dfa12', '11', '11', 'SEWA LAPANGAN HIJAU PAGI WEEK END', 'SEWA', 'JAM', '100000', '1', '100000', '2022-12-11 11:43:09', NULL, NULL, NULL),
('e0e91267c64e062f39ff8c4498e26d30', '20221211114300556', '20221211114300556', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '9', '45000', '2022-12-11 11:43:17', NULL, NULL, NULL),
('c7bbe431ac13fe72b3c3bc212a89d770', '20221211114300556', '20221211114300556', '99797ea5ce8cc9b4ed11fdaa69476dfa27', '26', '26', 'AIR MINERAL PRIMA 600 ML', 'MNM', 'PCS', '3000', '1', '3000', '2022-12-11 11:43:20', NULL, NULL, NULL),
('0f51ce8feb78a146f5aaf9ec720f5eb4', '2022121117561877', '2022121117561877', '99797ea5ce8cc9b4ed11fdaa69476dfa10', '9', '9', 'SEWA LAPANGAN HIJAU MALAM WEEK END', 'SEWA', 'JAM', '140000', '2', '280000', '2022-12-11 17:56:34', NULL, NULL, NULL),
('e22cdcb5de8d5e4134bcded747644277', '2022121117561877', '2022121117561877', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '8', '40000', '2022-12-11 17:58:02', NULL, NULL, NULL);
INSERT INTO `nota_detail` (`kd`, `nota_kd`, `nota_kode`, `brg_kd`, `brg_kode`, `brg_barkode`, `brg_nama`, `brg_kategori`, `brg_satuan`, `brg_harga`, `qty`, `subtotal`, `postdate`, `user_kd`, `user_kode`, `user_nama`) VALUES
('db3803d4899fd3518c595b840381dc21', '20221214155032164', '20221214155032164', '99797ea5ce8cc9b4ed11fdaa69476dfa11', '10', '10', 'SEWA LAPANGAN HIJAU PAGI WEEK DAY', 'SEWA', 'JAM', '80000', '1', '80000', '2022-12-14 15:50:46', NULL, NULL, NULL),
('fb1e68bda5374d4be0e9ee76049730a7', '20221214155032164', '20221214155032164', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '3', '15000', '2022-12-14 15:51:08', NULL, NULL, NULL),
('d6a114147b65e0b4b33157b088046cf5', '20221214155032164', '20221214155032164', '99797ea5ce8cc9b4ed11fdaa69476dfa27', '26', '26', 'AIR MINERAL PRIMA 600 ML', 'MNM', 'PCS', '3000', '2', '6000', '2022-12-14 15:51:15', NULL, NULL, NULL),
('1239224f9b0d0d92cc6e1d85141d516d', '20221214155032164', '20221214155032164', '99797ea5ce8cc9b4ed11fdaa69476dfa38', '37', '37', 'TEBS BOTOL PLASTIK 500 ML', 'MNM', 'PCS', '7000', '1', '7000', '2022-12-14 15:51:28', NULL, NULL, NULL),
('448ec43e99e3f4a87023b4cbdd73a30c', '20221214155032164', '20221214155032164', '99797ea5ce8cc9b4ed11fdaa69476dfa13', '12', '12', 'HYDRO COCO 250 ML', 'MNM', 'PCS', '7500', '1', '7500', '2022-12-14 15:51:35', NULL, NULL, NULL),
('049cba71e5bc188d9f4e15df82eb7738', '20221214155032164', '20221214155032164', '99797ea5ce8cc9b4ed11fdaa69476dfa24', '23', '23', 'ORANGE WATER', 'MNM', 'PCS', '10000', '1', '10000', '2022-12-14 15:51:51', NULL, NULL, NULL),
('7521e54daab58cdc4bac4aedf0654713', '20221214155032164', '20221214155032164', '99797ea5ce8cc9b4ed11fdaa69476dfa35', '34', '34', 'SUSU MILKU', 'MNM', 'PCS', '5000', '1', '5000', '2022-12-14 15:52:12', NULL, NULL, NULL),
('b82fbc413b003852ba22507dffac1e73', '20221214155032164', '20221214155032164', '99797ea5ce8cc9b4ed11fdaa69476dfa29', '28', '28', 'ROKOK ECERAN', 'ROKOK', 'PCS', '2500', '3', '7500', '2022-12-14 15:52:27', NULL, NULL, NULL),
('83f094c0ad39ea401c6573aebbc64e02', '20221214231336866', '20221214231336866', '99797ea5ce8cc9b4ed11fdaa69476dfa9', '8', '8', 'SEWA LAPANGAN HIJAU MALAM WEEK DAY', 'SEWA', 'JAM', '120000', '1', '120000', '2022-12-14 23:15:02', NULL, NULL, NULL),
('105f69fe3c4f316027bd28b8fa913723', '20221215160323123', '20221215160323123', '99797ea5ce8cc9b4ed11fdaa69476dfa11', '10', '10', 'SEWA LAPANGAN HIJAU PAGI WEEK DAY', 'SEWA', 'JAM', '80000', '2', '160000', '2022-12-15 16:03:34', NULL, NULL, NULL),
('a2578ffa5aa815b824248fdbbc54c28f', '20221215160323123', '20221215160323123', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '4', '20000', '2022-12-15 16:03:41', NULL, NULL, NULL),
('b5448bc4a231bfdb4047decf40a378a2', '2022121622373258', '2022121622373258', '99797ea5ce8cc9b4ed11fdaa69476dfa33', '32', '32', 'SARI KACANG HIJAU', 'MNM', 'PCS', '5000', '1', '5000', '2022-12-16 22:38:02', NULL, NULL, NULL),
('a8606e491c5f9ea3d3723f18ff011002', '2022121622373258', '2022121622373258', '99797ea5ce8cc9b4ed11fdaa69476dfa36', '35', '35', 'SUSU ULTRA MILK', 'MNM', 'PCS', '7000', '1', '7000', '2022-12-16 22:38:13', NULL, NULL, NULL),
('092c98bff13e0b3744a3b466229be3ae', '2022121622373258', '2022121622373258', '99797ea5ce8cc9b4ed11fdaa69476dfa28', '27', '27', 'PULPY ORANGE', 'MNM', 'PCS', '5000', '1', '5000', '2022-12-16 22:38:30', NULL, NULL, NULL),
('01bb35cc6a20b52184b7e546c0212666', '2022121622373258', '2022121622373258', '99797ea5ce8cc9b4ed11fdaa69476dfa40', '39', '39', 'TEH BOTOL SOSRO PLASTIK 350 ML', 'MNM', 'PCS', '5000', '1', '5000', '2022-12-16 22:38:45', NULL, NULL, NULL),
('4d9b3632ab0268877d66d07ce371525b', '2022121622373258', '2022121622373258', '99797ea5ce8cc9b4ed11fdaa69476dfa35', '34', '34', 'SUSU MILKU', 'MNM', 'PCS', '5000', '2', '10000', '2022-12-16 22:38:56', NULL, NULL, NULL),
('75a2fd11c588038d778ab0c0ae6dc0d2', '20221217162608410', '20221217162608410', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '2', '10000', '2022-12-17 16:26:16', NULL, NULL, NULL),
('a61dee6bd2d6343d42043dd2616f9d94', '20221217162608410', '20221217162608410', '99797ea5ce8cc9b4ed11fdaa69476dfa27', '26', '26', 'AIR MINERAL PRIMA 600 ML', 'MNM', 'PCS', '3000', '1', '3000', '2022-12-17 16:26:29', NULL, NULL, NULL),
('b327685103e9df3a18ef76813e4499be', '20221217162608410', '20221217162608410', '99797ea5ce8cc9b4ed11fdaa69476dfa14', '13', '13', 'HYDRO COCO 330 ML', 'MNM', 'PCS', '10000', '2', '20000', '2022-12-17 16:26:39', NULL, NULL, NULL),
('41378e0bc0dc356665bd045e05750f83', '20221217162608410', '20221217162608410', '99797ea5ce8cc9b4ed11fdaa69476dfa23', '22', '22', 'MIZONE', 'MNM', 'PCS', '6000', '1', '6000', '2022-12-17 16:26:46', NULL, NULL, NULL),
('a4f964c979e5eb508e482e5f967e293c', '20221217162608410', '20221217162608410', '99797ea5ce8cc9b4ed11fdaa69476dfa7', '6', '6', 'FRUIT TEA SOSRO KACA 235 ML', 'MNM', 'PCS', '3000', '1', '3000', '2022-12-17 16:26:51', NULL, NULL, NULL),
('66c977b2f9db367adc439347c3578123', '20221217162608410', '20221217162608410', '99797ea5ce8cc9b4ed11fdaa69476dfa16', '15', '15', 'ISO PLUS', 'MNM', 'PCS', '5000', '3', '15000', '2022-12-17 16:27:00', NULL, NULL, NULL),
('f2ca736ef69d6b925cb186e3d00c56a9', '20221217162608410', '20221217162608410', '5a999455e84eca294d9dd7b616f04b1b', '8997212800035', '8997212800035', 'YUZU ISOTONIC 350ML', 'Minuman', 'PCS', '5000', '1', '5000', '2022-12-17 16:27:08', NULL, NULL, NULL),
('7ddd8bb570c401bcbef1d0c5765c2a85', '20221217162608410', '20221217162608410', '99797ea5ce8cc9b4ed11fdaa69476dfa41', '40', '40', 'TEH JAVANA 350 ML', 'MNM', 'PCS', '5000', '1', '5000', '2022-12-17 16:27:16', NULL, NULL, NULL),
('bf1d64f5083a85508f35c62db5366db2', '20221217162608410', '20221217162608410', '99797ea5ce8cc9b4ed11fdaa69476dfa29', '28', '28', 'ROKOK ECERAN', 'ROKOK', 'PCS', '2500', '1', '2500', '2022-12-17 16:27:51', NULL, NULL, NULL),
('8b5e3044d3e1294ecdb3a5413d0f2c53', '20221217162608410', '20221217162608410', '99797ea5ce8cc9b4ed11fdaa69476dfa12', '11', '11', 'SEWA LAPANGAN HIJAU PAGI WEEK END', 'SEWA', 'JAM', '100000', '2', '200000', '2022-12-17 16:28:39', NULL, NULL, NULL),
('6d129a12f295fc559c4739326472909c', '20221218110159145', '20221218110159145', '99797ea5ce8cc9b4ed11fdaa69476dfa12', '11', '11', 'SEWA LAPANGAN HIJAU PAGI WEEK END', 'SEWA', 'JAM', '100000', '1', '100000', '2022-12-18 11:02:14', NULL, NULL, NULL),
('521f68ee92cfb0293e2c6420fc1d5627', '20221218110159145', '20221218110159145', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '6', '30000', '2022-12-18 11:02:20', NULL, NULL, NULL),
('60a1b5945489bcfeba3442d3db40b6fd', '20221218154431227', '20221218154431227', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '6', '30000', '2022-12-18 15:44:38', NULL, NULL, NULL),
('263d8099f26652739cb82d9e99153663', '20221218154431227', '20221218154431227', '99797ea5ce8cc9b4ed11fdaa69476dfa27', '26', '26', 'AIR MINERAL PRIMA 600 ML', 'MNM', 'PCS', '3000', '2', '6000', '2022-12-18 15:44:47', NULL, NULL, NULL),
('6987621d0f81adf90c2281a9e5f67d84', '20221218154431227', '20221218154431227', '99797ea5ce8cc9b4ed11fdaa69476dfa39', '38', '38', 'TEH BOTOL SOSRO KACA', 'MNM', 'PCS', '3000', '1', '3000', '2022-12-18 15:45:01', NULL, NULL, NULL),
('a3ec4f64ac9d4be61f753fe0298ce0ce', '20221218154431227', '20221218154431227', '99797ea5ce8cc9b4ed11fdaa69476dfa37', '36', '36', 'TEBS BOTOL KACA', 'MNM', 'PCS', '4000', '3', '12000', '2022-12-18 15:45:15', NULL, NULL, NULL),
('ddc610fd0da2ccbae3fd43ae3f35fa68', '20221218154431227', '20221218154431227', '99797ea5ce8cc9b4ed11fdaa69476dfa38', '37', '37', 'TEBS BOTOL PLASTIK 500 ML', 'MNM', 'PCS', '7000', '1', '7000', '2022-12-18 15:45:23', NULL, NULL, NULL),
('a87582bc80c8a3b5a5e21096eec875a4', '20221218154431227', '20221218154431227', '99797ea5ce8cc9b4ed11fdaa69476dfa13', '12', '12', 'HYDRO COCO 250 ML', 'MNM', 'PCS', '7500', '1', '7500', '2022-12-18 15:45:34', NULL, NULL, NULL),
('cd7b50807e3d6e56c9276c81d82639c0', '20221218154431227', '20221218154431227', '99797ea5ce8cc9b4ed11fdaa69476dfa23', '22', '22', 'MIZONE', 'MNM', 'PCS', '6000', '1', '6000', '2022-12-18 15:45:41', NULL, NULL, NULL),
('f909d28367fb077dadd6777649e91f02', '20221218154431227', '20221218154431227', '99797ea5ce8cc9b4ed11fdaa69476dfa31', '30', '30', 'ROKOK GUDANG GARAM SURYA 12', 'Rokok', 'PCS', '23000', '1', '23000', '2022-12-18 15:45:50', NULL, NULL, NULL),
('be02ab4bb5a961d5d505b9957d368f54', '20221218154431227', '20221218154431227', '99797ea5ce8cc9b4ed11fdaa69476dfa12', '11', '11', 'SEWA LAPANGAN HIJAU PAGI WEEK END', 'SEWA', 'JAM', '100000', '3', '300000', '2022-12-18 15:47:40', NULL, NULL, NULL),
('d08cc65fae377493b89ad719a063aafb', '20221218155616977', '20221218155616977', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '6', '30000', '2022-12-18 15:56:24', NULL, NULL, NULL),
('acec5e344dbb355fc1eb423eef78056d', '20221218155616977', '20221218155616977', '99797ea5ce8cc9b4ed11fdaa69476dfa27', '26', '26', 'AIR MINERAL PRIMA 600 ML', 'MNM', 'PCS', '3000', '2', '6000', '2022-12-18 15:56:29', NULL, NULL, NULL),
('2758b03932f959568f1c3ac4b4a10176', '20221218155616977', '20221218155616977', '99797ea5ce8cc9b4ed11fdaa69476dfa37', '36', '36', 'TEBS BOTOL KACA', 'MNM', 'PCS', '4000', '3', '12000', '2022-12-18 15:56:46', NULL, NULL, NULL),
('8cc19376e0dfb018f1a07a12cf28923a', '20221218155616977', '20221218155616977', '99797ea5ce8cc9b4ed11fdaa69476dfa38', '37', '37', 'TEBS BOTOL PLASTIK 500 ML', 'MNM', 'PCS', '7000', '1', '7000', '2022-12-18 15:56:56', NULL, NULL, NULL),
('2016b71419db707875abf3fbb764d37c', '20221218155616977', '20221218155616977', '99797ea5ce8cc9b4ed11fdaa69476dfa13', '12', '12', 'HYDRO COCO 250 ML', 'MNM', 'PCS', '7500', '1', '7500', '2022-12-18 15:57:06', NULL, NULL, NULL),
('cdc73c239939db9fbbb5e2e6d28a2533', '20221218155616977', '20221218155616977', '99797ea5ce8cc9b4ed11fdaa69476dfa23', '22', '22', 'MIZONE', 'MNM', 'PCS', '6000', '1', '6000', '2022-12-18 15:57:12', NULL, NULL, NULL),
('20b79648013353cb53de1200917d86a7', '20221218155616977', '20221218155616977', '99797ea5ce8cc9b4ed11fdaa69476dfa31', '30', '30', 'ROKOK GUDANG GARAM SURYA 12', 'Rokok', 'PCS', '23000', '1', '23000', '2022-12-18 15:57:18', NULL, NULL, NULL),
('7cddc5c074a94bed99776554a67df68d', '20221218155616977', '20221218155616977', '99797ea5ce8cc9b4ed11fdaa69476dfa29', '28', '28', 'ROKOK ECERAN', 'ROKOK', 'PCS', '2500', '3', '7500', '2022-12-18 15:57:26', NULL, NULL, NULL),
('9be461f851e08cdb48168ff253421ba2', '20221218155616977', '20221218155616977', '99797ea5ce8cc9b4ed11fdaa69476dfa12', '11', '11', 'SEWA LAPANGAN HIJAU PAGI WEEK END', 'SEWA', 'JAM', '100000', '3', '300000', '2022-12-18 15:57:41', NULL, NULL, NULL),
('5c7abf694e0ba688b9366c55198e6273', '20221221213110475', '20221221213110475', '99797ea5ce8cc9b4ed11fdaa69476dfa9', '8', '8', 'SEWA LAPANGAN HIJAU MALAM WEEK DAY', 'SEWA', 'JAM', '120000', '2', '240000', '2022-12-21 21:32:47', NULL, NULL, NULL),
('d6ae5d7a8f5905969195910726b425d7', '20221221213110475', '20221221213110475', '99797ea5ce8cc9b4ed11fdaa69476dfa40', '39', '39', 'TEH BOTOL SOSRO PLASTIK 350 ML', 'MNM', 'PCS', '5000', '2', '10000', '2022-12-21 21:32:01', NULL, NULL, NULL),
('2a11bfd3b7f1e3128a91e715a472b261', '20221221213110475', '20221221213110475', '99797ea5ce8cc9b4ed11fdaa69476dfa18', '17', '17', 'KOPI KOPIKO', 'MNM', 'PCS', '7000', '1', '7000', '2022-12-21 21:32:08', NULL, NULL, NULL),
('dcea0a4a3588f2487e9191bf3067e1e7', '20221221213110475', '20221221213110475', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '5', '25000', '2022-12-21 21:32:17', NULL, NULL, NULL),
('b9a260c512adacde93037077970bc491', '20221221213110475', '20221221213110475', '99797ea5ce8cc9b4ed11fdaa69476dfa27', '26', '26', 'AIR MINERAL PRIMA 600 ML', 'MNM', 'PCS', '3000', '4', '12000', '2022-12-21 21:32:24', NULL, NULL, NULL),
('195bbe4b799649fbbf2be7518c771607', '20221223222943310', '20221223222943310', '99797ea5ce8cc9b4ed11fdaa69476dfa14', '13', '13', 'HYDRO COCO 330 ML', 'MNM', 'PCS', '10000', '1', '10000', '2022-12-23 22:29:53', NULL, NULL, NULL),
('2320df8e3383a65a2442175c940d8fa7', '20221223222943310', '20221223222943310', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '2', '10000', '2022-12-23 22:30:03', NULL, NULL, NULL),
('c33cd3743b99823c03059dd41d7a385f', '20221223222943310', '20221223222943310', '99797ea5ce8cc9b4ed11fdaa69476dfa36', '35', '35', 'SUSU ULTRA MILK', 'MNM', 'PCS', '7000', '2', '14000', '2022-12-23 22:30:39', NULL, NULL, NULL),
('b307017df6c7b8f6a095253f8392d78e', '20221223222943310', '20221223222943310', '99797ea5ce8cc9b4ed11fdaa69476dfa40', '39', '39', 'TEH BOTOL SOSRO PLASTIK 350 ML', 'MNM', 'PCS', '5000', '1', '5000', '2022-12-23 22:30:54', NULL, NULL, NULL),
('d4fd1879cb28fe72c7a415fa26436c10', '20221226155513206', '20221226155513206', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '9', '45000', '2022-12-26 15:55:25', NULL, NULL, NULL),
('2f20e462f5b51f2f5168ed8a29c9ab79', '20221226155513206', '20221226155513206', '99797ea5ce8cc9b4ed11fdaa69476dfa40', '39', '39', 'TEH BOTOL SOSRO PLASTIK 350 ML', 'MNM', 'PCS', '5000', '1', '5000', '2022-12-26 15:55:41', NULL, NULL, NULL),
('96410084265e248414d2b27e4d7c91dc', '20221226155513206', '20221226155513206', '99797ea5ce8cc9b4ed11fdaa69476dfa38', '37', '37', 'TEBS BOTOL PLASTIK 500 ML', 'MNM', 'PCS', '7000', '1', '7000', '2022-12-26 15:55:55', NULL, NULL, NULL),
('36ea0ef6e535113e68d01b75458aca6c', '20221226155513206', '20221226155513206', '99797ea5ce8cc9b4ed11fdaa69476dfa13', '12', '12', 'HYDRO COCO 250 ML', 'MNM', 'PCS', '7500', '1', '7500', '2022-12-26 15:56:08', NULL, NULL, NULL),
('c9b562418882b8557ce3fa73a19d6376', '20221226155513206', '20221226155513206', '99797ea5ce8cc9b4ed11fdaa69476dfa14', '13', '13', 'HYDRO COCO 330 ML', 'MNM', 'PCS', '10000', '1', '10000', '2022-12-26 15:56:14', NULL, NULL, NULL),
('c91fcf5b2b0c26de987a2145fe18ab34', '20221226155513206', '20221226155513206', '99797ea5ce8cc9b4ed11fdaa69476dfa42', '41', '41', 'TEH PUCUK HARUM', 'MNM', 'PCS', '5000', '5', '25000', '2022-12-26 15:56:45', NULL, NULL, NULL),
('038df17de9280c86da43c8f43ab30180', '20221226155513206', '20221226155513206', '99797ea5ce8cc9b4ed11fdaa69476dfa8', '7', '7', 'FRUIT TEA SOSRO PLATIK 350 ML', 'MNM', 'PCS', '5000', '1', '5000', '2022-12-26 15:57:10', NULL, NULL, NULL),
('181e10d8aa28d0d800577fbf02874db7', '20221226155513206', '20221226155513206', '99797ea5ce8cc9b4ed11fdaa69476dfa25', '24', '24', 'POCARI SWEAT', 'MNM', 'PCS', '8000', '2', '16000', '2022-12-26 15:57:18', NULL, NULL, NULL),
('e39baa562ba59fe9bcf81b85d1ae8d85', '20221226155513206', '20221226155513206', '99797ea5ce8cc9b4ed11fdaa69476dfa16', '15', '15', 'ISO PLUS', 'MNM', 'PCS', '5000', '1', '5000', '2022-12-26 15:57:30', NULL, NULL, NULL),
('305bb853a2391c05842295c628475f62', '20221226155513206', '20221226155513206', '99797ea5ce8cc9b4ed11fdaa69476dfa24', '23', '23', 'ORANGE WATER', 'MNM', 'PCS', '10000', '1', '10000', '2022-12-26 15:57:45', NULL, NULL, NULL),
('93ddbf24a055f1f7f815097bd94c301a', '20221226155513206', '20221226155513206', '99797ea5ce8cc9b4ed11fdaa69476dfa41', '40', '40', 'TEH JAVANA 350 ML', 'MNM', 'PCS', '5000', '1', '5000', '2022-12-26 15:57:58', NULL, NULL, NULL),
('cfd18128d1f87224b1a2a3383d15a416', '20221226155513206', '20221226155513206', '99797ea5ce8cc9b4ed11fdaa69476dfa29', '28', '28', 'ROKOK ECERAN', 'ROKOK', 'PCS', '2500', '7', '17500', '2022-12-26 16:00:27', NULL, NULL, NULL),
('bfbc6eff9567256ade316c68e6e6c1d1', '20221226155513206', '20221226155513206', '99797ea5ce8cc9b4ed11fdaa69476dfa11', '10', '10', 'SEWA LAPANGAN HIJAU PAGI WEEK DAY', 'SEWA', 'JAM', '80000', '3', '240000', '2022-12-26 16:00:56', NULL, NULL, NULL),
('1310e7a658bdc7954097897fdcf442b1', '20221227145859387', '20221227145859387', '99797ea5ce8cc9b4ed11fdaa69476dfa11', '10', '10', 'SEWA LAPANGAN HIJAU PAGI WEEK DAY', 'SEWA', 'JAM', '80000', '1', '80000', '2022-12-27 14:59:09', NULL, NULL, NULL),
('dfb69d85f1a730c8b00571a8d913528f', '20221227145859387', '20221227145859387', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '4', '20000', '2022-12-27 14:59:19', NULL, NULL, NULL),
('6f4a3126dbcb41ad12444026ba7d4535', '20221227145859387', '20221227145859387', '99797ea5ce8cc9b4ed11fdaa69476dfa25', '24', '24', 'POCARI SWEAT', 'MNM', 'PCS', '8000', '2', '16000', '2022-12-27 14:59:30', NULL, NULL, NULL),
('24baab10953a26aad0221b77ccde3023', '20221227145859387', '20221227145859387', '99797ea5ce8cc9b4ed11fdaa69476dfa27', '26', '26', 'AIR MINERAL PRIMA 600 ML', 'MNM', 'PCS', '3000', '1', '3000', '2022-12-27 14:59:35', NULL, NULL, NULL),
('652811d36fe6c8fb3250bef7cf461d6c', '20221227210334120', '20221227210334120', '99797ea5ce8cc9b4ed11fdaa69476dfa19', '18', '18', 'SEWA LAPANGAN MERAH MALAM WEEK DAY', 'SEWA', 'JAM', '120000', '2', '240000', '2022-12-27 21:03:52', NULL, NULL, NULL),
('b752ef5a7631c4990885418e9e24ee67', '20230106155933760', '20230106155933760', '99797ea5ce8cc9b4ed11fdaa69476dfa12', '11', '11', 'SEWA LAPANGAN HIJAU PAGI', 'SEWA', 'JAM', '110000', '2', '220000', '2023-01-06 15:59:53', NULL, NULL, NULL),
('cce913cfe7e2effcb0bc05f8cb8105f9', '20230106160047213', '20230106160047213', '99797ea5ce8cc9b4ed11fdaa69476dfa12', '11', '11', 'SEWA LAPANGAN HIJAU PAGI', 'SEWA', 'JAM', '110000', '2', '220000', '2023-01-06 16:01:17', NULL, NULL, NULL),
('0a648d00088280f23e7e55f71a85c962', '2023010616083430', '2023010616083430', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '1', '5000', '2023-01-06 16:08:38', NULL, NULL, NULL),
('26d5b31cb36257fba956bb4b90db6e21', '2023010616083430', '2023010616083430', '99797ea5ce8cc9b4ed11fdaa69476dfa27', '26', '26', 'AIR MINERAL PRIMA 600 ML', 'MNM', 'PCS', '3000', '1', '3000', '2023-01-06 16:08:41', NULL, NULL, NULL),
('90096c6abd084c500f8f11351818b568', '2023010616083430', '2023010616083430', '99797ea5ce8cc9b4ed11fdaa69476dfa29', '28', '28', 'ROKOK ECERAN', 'ROKOK', 'PCS', '2500', '3', '7500', '2023-01-06 16:08:50', NULL, NULL, NULL),
('a6e4032f76792cbbcd147df24a31b6c2', '20230106161032108', '20230106161032108', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '1', '5000', '2023-01-06 16:10:41', NULL, NULL, NULL),
('32ccb73ea9453316600422c2507cb9bd', '20230106161032108', '20230106161032108', '99797ea5ce8cc9b4ed11fdaa69476dfa27', '26', '26', 'AIR MINERAL PRIMA 600 ML', 'MNM', 'PCS', '3000', '1', '3000', '2023-01-06 16:10:44', NULL, NULL, NULL),
('b955cb2e0adb9a121f94822f983eeb8f', '20230106161032108', '20230106161032108', '99797ea5ce8cc9b4ed11fdaa69476dfa29', '28', '28', 'ROKOK ECERAN', 'ROKOK', 'PCS', '2500', '3', '7500', '2023-01-06 16:10:52', NULL, NULL, NULL),
('dc67850144a364afbea4f519c120be26', '20230106161032108', '20230106161032108', 'a16e94b812c206f5d2f6d97d2bca0581', '43', '43', 'Get It', 'Snack', 'PCS', '5000', '1', '5000', '2023-01-06 16:11:06', NULL, NULL, NULL),
('1a68d9943c777d205db7aaa1e4860152', '20230106161032108', '20230106161032108', '99797ea5ce8cc9b4ed11fdaa69476dfa12', '11', '11', 'SEWA LAPANGAN HIJAU PAGI', 'SEWA', 'JAM', '110000', '2', '220000', '2023-01-06 16:11:26', NULL, NULL, NULL),
('8ba710d284b1775558297715170c59cf', '20230106223108428', '20230106223108428', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '1', '5000', '2023-01-06 22:31:51', NULL, NULL, NULL),
('5928eec7c945bcf1447391130e17870d', '20230106223108428', '20230106223108428', '99797ea5ce8cc9b4ed11fdaa69476dfa42', '41', '41', 'TEH PUCUK HARUM', 'MNM', 'PCS', '5000', '1', '5000', '2023-01-06 22:32:23', NULL, NULL, NULL),
('4e2bb0a4bea9d727400c32f1cb9a9943', '20230106223108428', '20230106223108428', '99797ea5ce8cc9b4ed11fdaa69476dfa28', '27', '27', 'PULPY ORANGE', 'MNM', 'PCS', '5000', '2', '10000', '2023-01-06 22:32:58', NULL, NULL, NULL),
('42b26331d496957a641a356c37627dd2', '20230106223108428', '20230106223108428', '99797ea5ce8cc9b4ed11fdaa69476dfa40', '39', '39', 'TEH BOTOL SOSRO PLASTIK 350 ML', 'MNM', 'PCS', '5000', '1', '5000', '2023-01-06 22:33:12', NULL, NULL, NULL),
('cb27d8e4da402a1c882a628280926f02', '20230106223108428', '20230106223108428', '99797ea5ce8cc9b4ed11fdaa69476dfa36', '35', '35', 'SUSU ULTRA MILK', 'MNM', 'PCS', '7000', '1', '7000', '2023-01-06 22:35:30', NULL, NULL, NULL),
('139109871e5dfe788c80f46b69c6a454', '20230106223108428', '20230106223108428', '99797ea5ce8cc9b4ed11fdaa69476dfa13', '12', '12', 'HYDRO COCO 250 ML', 'MNM', 'PCS', '7500', '1', '7500', '2023-01-06 22:35:47', NULL, NULL, NULL),
('79474702f07097f405f19cf628960af4', '20230106223108428', '20230106223108428', '99797ea5ce8cc9b4ed11fdaa69476dfa9', '8', '8', 'SEWA LAPANGAN HIJAU MALAM', 'SEWA', 'JAM', '160000', '1', '160000', '2023-01-06 22:36:17', NULL, NULL, NULL),
('8bc8029bf79e30666d5fbe06454ebfc7', '20230109221404993', '20230109221404993', '99797ea5ce8cc9b4ed11fdaa69476dfa9', '8', '8', 'SEWA LAPANGAN HIJAU MALAM', 'SEWA', 'JAM', '160000', '2', '320000', '2023-01-09 22:14:27', NULL, NULL, NULL),
('3b248b0242e78df33f973c0e0cf98111', '20230109221404993', '20230109221404993', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '2', '10000', '2023-01-09 22:14:35', NULL, NULL, NULL),
('a57cf81103fcb38a7844d68629d2dc1e', '20230110222452523', '20230110222452523', '99797ea5ce8cc9b4ed11fdaa69476dfa10', '9', '9', 'SEWA LAPANGAN HIJAU MALAM WEEK END', 'SEWA', 'JAM', '140000', '2', '280000', '2023-01-10 22:26:28', NULL, NULL, NULL),
('ff718220f0e27b872a783f143c6b8697', '20230110222452523', '20230110222452523', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '3', '15000', '2023-01-10 22:25:28', NULL, NULL, NULL),
('dbda38a69fcf72819418ebf5d59e286b', '20230110222452523', '20230110222452523', '99797ea5ce8cc9b4ed11fdaa69476dfa27', '26', '26', 'AIR MINERAL PRIMA 600 ML', 'MNM', 'PCS', '3000', '4', '12000', '2023-01-10 22:25:36', NULL, NULL, NULL),
('0fbd35f040b798f1c747ebe95f9c0263', '20230112120631383', '20230112120631383', '99797ea5ce8cc9b4ed11fdaa69476dfa12', '11', '11', 'SEWA LAPANGAN HIJAU PAGI', 'SEWA', 'JAM', '110000', '1', '110000', '2023-01-12 12:06:51', NULL, NULL, NULL),
('11cc398c5e1f1fced65fd5cf4150a373', '20230112120631383', '20230112120631383', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '4', '20000', '2023-01-12 12:06:57', NULL, NULL, NULL),
('4e2623392d10b04f649c8f47b4b1000c', '20230112120631383', '20230112120631383', '99797ea5ce8cc9b4ed11fdaa69476dfa16', '15', '15', 'ISO PLUS', 'MNM', 'PCS', '5000', '1', '5000', '2023-01-12 12:07:17', NULL, NULL, NULL),
('7f48b8c08af9dbe65a474dff8083105c', '20230112154948572', '20230112154948572', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '2', '10000', '2023-01-12 15:50:22', NULL, NULL, NULL),
('5d6acec6ca13f813fafd78d8544b10ff', '20230112154948572', '20230112154948572', '99797ea5ce8cc9b4ed11fdaa69476dfa14', '13', '13', 'HYDRO COCO 330 ML', 'MNM', 'PCS', '10000', '1', '10000', '2023-01-12 15:50:30', NULL, NULL, NULL),
('9c316b2d85e528331eadf5c3b12c8323', '20230112154948572', '20230112154948572', '99797ea5ce8cc9b4ed11fdaa69476dfa8', '7', '7', 'FRUIT TEA SOSRO PLATIK 350 ML', 'MNM', 'PCS', '5000', '1', '5000', '2023-01-12 15:50:37', NULL, NULL, NULL),
('aa839963ea8e5d1381251722048e327c', '20230112154948572', '20230112154948572', '99797ea5ce8cc9b4ed11fdaa69476dfa6', '5', '5', 'FLORIDINA', 'MNM', 'PCS', '5000', '1', '5000', '2023-01-12 15:50:44', NULL, NULL, NULL),
('3b5c986b3a65e6b8dbbc529c68c6328a', '20230112154948572', '20230112154948572', '99797ea5ce8cc9b4ed11fdaa69476dfa29', '28', '28', 'ROKOK ECERAN', 'ROKOK', 'PCS', '2500', '2', '5000', '2023-01-12 15:50:55', NULL, NULL, NULL),
('4e2a9200ca8fbba6ee7b730363a30ef6', '20230114121137438', '20230114121137438', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '3', '15000', '2023-01-14 12:11:45', NULL, NULL, NULL),
('7edfa3eb33e5b2b695b0e3d5960b2d54', '20230114121137438', '20230114121137438', '99797ea5ce8cc9b4ed11fdaa69476dfa13', '12', '12', 'HYDRO COCO 250 ML', 'MNM', 'PCS', '7500', '1', '7500', '2023-01-14 12:11:50', NULL, NULL, NULL),
('672fa90327e4ad496af78d3643103ec2', '20230114121137438', '20230114121137438', '99797ea5ce8cc9b4ed11fdaa69476dfa37', '36', '36', 'TEBS BOTOL KACA', 'MNM', 'PCS', '4000', '1', '4000', '2023-01-14 12:12:07', NULL, NULL, NULL),
('d1e39dae14ff21ba93a362f11929a429', '20230114121137438', '20230114121137438', '2047a24fc4df502f655903e12a6d1254', '1', '', 'SEWA LAPANGAN HIJAU PAGI', 'Lapangan', 'JAM', '110000', '1', '110000', '2023-01-14 12:12:16', NULL, NULL, NULL),
('5f22c575307bae68f3bc8f49061534e5', '20230114154347715', '20230114154347715', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '2', '10000', '2023-01-14 15:44:06', NULL, NULL, NULL),
('48dfac8c8a65389e85336ed14986a823', '20230114154347715', '20230114154347715', '99797ea5ce8cc9b4ed11fdaa69476dfa27', '26', '26', 'AIR MINERAL PRIMA 600 ML', 'MNM', 'PCS', '3000', '15', '45000', '2023-01-14 15:44:14', NULL, NULL, NULL),
('0f280df47c4bf7dd9b493e73847516f0', '20230114154347715', '20230114154347715', '99797ea5ce8cc9b4ed11fdaa69476dfa40', '39', '39', 'TEH BOTOL SOSRO PLASTIK 350 ML', 'MNM', 'PCS', '5000', '1', '5000', '2023-01-14 15:44:31', NULL, NULL, NULL),
('8ceae84db2cc56a96f8460cf2caf20a4', '20230114154347715', '20230114154347715', '99797ea5ce8cc9b4ed11fdaa69476dfa37', '36', '36', 'TEBS BOTOL KACA', 'MNM', 'PCS', '4000', '1', '4000', '2023-01-14 15:44:58', NULL, NULL, NULL),
('5ac113eec3000d3ddb142e45340eaab8', '20230114154347715', '20230114154347715', '99797ea5ce8cc9b4ed11fdaa69476dfa38', '37', '37', 'TEBS BOTOL PLASTIK 500 ML', 'MNM', 'PCS', '7000', '1', '7000', '2023-01-14 15:45:25', NULL, NULL, NULL),
('b2a67e1c8ba387f1bdd59aea8b602b7c', '20230114154347715', '20230114154347715', '99797ea5ce8cc9b4ed11fdaa69476dfa14', '13', '13', 'HYDRO COCO 330 ML', 'MNM', 'PCS', '10000', '2', '20000', '2023-01-14 15:45:40', NULL, NULL, NULL),
('671c10f1d05fcfa80f7a75a1fa4f9a6d', '20230114154347715', '20230114154347715', '99797ea5ce8cc9b4ed11fdaa69476dfa42', '41', '41', 'TEH PUCUK HARUM', 'MNM', 'PCS', '5000', '3', '15000', '2023-01-14 15:46:10', NULL, NULL, NULL),
('96360db301c7e38edb15f6a5e9d5dd1a', '20230114154347715', '20230114154347715', '99797ea5ce8cc9b4ed11fdaa69476dfa8', '7', '7', 'FRUIT TEA SOSRO PLATIK 350 ML', 'MNM', 'PCS', '5000', '2', '10000', '2023-01-14 15:47:21', NULL, NULL, NULL),
('961e58b8e4841b6a5ea70f601736c5da', '20230114154347715', '20230114154347715', '99797ea5ce8cc9b4ed11fdaa69476dfa16', '15', '15', 'ISO PLUS', 'MNM', 'PCS', '5000', '1', '5000', '2023-01-14 15:47:30', NULL, NULL, NULL),
('562a2c37890a016da54d0e22820192a0', '20230114154347715', '20230114154347715', 'c7a28783c9e8b19a4e48347be7c473ef', '8996006858245', '8996006858245', 'TEBS BOTOL PLASTIK 300ML', 'Minuman', 'PCS', '5000', '1', '5000', '2023-01-14 15:47:43', NULL, NULL, NULL),
('d43e7dd7737aae6246f0f7509c6cd3ba', '20230114154347715', '20230114154347715', '99797ea5ce8cc9b4ed11fdaa69476dfa36', '35', '35', 'SUSU ULTRA MILK', 'MNM', 'PCS', '7000', '3', '21000', '2023-01-14 15:47:56', NULL, NULL, NULL),
('331c39c67ee74c26e965e638c7cea9c4', '20230114154347715', '20230114154347715', '5a999455e84eca294d9dd7b616f04b1b', '8997212800035', '8997212800035', 'YUZU ISOTONIC 350ML', 'Minuman', 'PCS', '5000', '2', '10000', '2023-01-14 15:48:08', NULL, NULL, NULL),
('25a4da9cf6c9a555260c201497f35b55', '20230114154347715', '20230114154347715', '99797ea5ce8cc9b4ed11fdaa69476dfa41', '40', '40', 'TEH JAVANA 350 ML', 'MNM', 'PCS', '5000', '1', '5000', '2023-01-14 15:48:13', NULL, NULL, NULL),
('0320ef82907c31de1fb130850785a2bb', '20230114154347715', '20230114154347715', '99797ea5ce8cc9b4ed11fdaa69476dfa31', '30', '30', 'ROKOK GUDANG GARAM SURYA 12', 'Rokok', 'PCS', '25000', '1', '25000', '2023-01-14 15:48:36', NULL, NULL, NULL),
('b5def4d93ddfe97c63345e53ae2836e4', '20230114154347715', '20230114154347715', '2047a24fc4df502f655903e12a6d1254', '1', '', 'SEWA LAPANGAN HIJAU PAGI', 'Lapangan', 'JAM', '110000', '1', '110000', '2023-01-14 15:48:58', NULL, NULL, NULL),
('7ba2a79c78b8fe7627bc00e4717fa096', '20230114154347715', '20230114154347715', '5d15012627bcc870c1c2d120d23d84eb', '11', '', 'SEWA LAPANGAN MERAH PAGI', 'Lapangan', 'JAM', '110000', '2', '220000', '2023-01-14 15:49:14', NULL, NULL, NULL),
('2dc4903a545f7f00b48ac7c5102aa7b5', '20230114160321473', '20230114160321473', '99797ea5ce8cc9b4ed11fdaa69476dfa23', '22', '22', 'MIZONE', 'MNM', 'PCS', '6000', '1', '6000', '2023-01-14 16:03:28', NULL, NULL, NULL),
('80e0ae4b454d504c2c6dc0896f0e5c4c', '20230115121538424', '20230115121538424', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '3', '15000', '2023-01-15 12:15:46', NULL, NULL, NULL),
('7bf1ca0062e654f5a12b553e945d0f1a', '20230115121538424', '20230115121538424', '2047a24fc4df502f655903e12a6d1254', '1', '', 'SEWA LAPANGAN HIJAU PAGI', 'Lapangan', 'JAM', '110000', '2', '220000', '2023-01-15 12:16:06', NULL, NULL, NULL),
('c3c7d9fc71a5cd762e9bc8b9f6318588', '2023011722264630', '2023011722264630', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '5', '25000', '2023-01-17 22:27:01', NULL, NULL, NULL),
('925fd3c06604d90529f0211a359e981e', '2023011722264630', '2023011722264630', '2d3b97e3595a05cea007f54469961af7', '11111', '', 'SEWA LAPANGAN HIJAU MALAM xgmringx NON FREE MINUM', 'Lapangan', 'JAM', '140000', '2', '280000', '2023-01-17 22:27:21', NULL, NULL, NULL),
('561c1df8a004ed5bed319ce390199ebd', '20230119204209528', '20230119204209528', '79137a3bd7bb34d9c8cb5003f94cb41a', '111111', '', 'SEWA LAPANGAN MERAH MALAM xgmringx NON FREE MINUM', 'Lapangan', 'JAM', '140000', '2', '280000', '2023-01-19 20:42:27', NULL, NULL, NULL),
('50c45637d5757babd8bfe81f537b99f1', '20230120225434545', '20230120225434545', '2d3b97e3595a05cea007f54469961af7', '11111', '', 'SEWA LAPANGAN HIJAU MALAM xgmringx NON FREE MINUM', 'Lapangan', 'JAM', '140000', '1', '140000', '2023-01-20 22:54:54', NULL, NULL, NULL),
('9dd565c53d37a86e95a435291fc24708', '20230120225434545', '20230120225434545', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '4', '20000', '2023-01-20 22:57:29', NULL, NULL, NULL),
('33f7ce522b4b1370d04af3272b2294e5', '20230120225434545', '20230120225434545', '99797ea5ce8cc9b4ed11fdaa69476dfa36', '35', '35', 'SUSU ULTRA MILK', 'MNM', 'PCS', '7000', '2', '14000', '2023-01-20 22:55:23', NULL, NULL, NULL),
('14c7b14c6a0cd908b14ba4291851242c', '20230120225434545', '20230120225434545', '99797ea5ce8cc9b4ed11fdaa69476dfa14', '13', '13', 'HYDRO COCO 330 ML', 'MNM', 'PCS', '10000', '2', '20000', '2023-01-20 22:55:37', NULL, NULL, NULL),
('53ef81d6871f601a12e6b3f3020c63eb', '20230120225434545', '20230120225434545', '99797ea5ce8cc9b4ed11fdaa69476dfa13', '12', '12', 'HYDRO COCO 250 ML', 'MNM', 'PCS', '7500', '1', '7500', '2023-01-20 22:55:51', NULL, NULL, NULL),
('719d22842e2198d3ba856ba05fe2e078', '20230120225434545', '20230120225434545', '99797ea5ce8cc9b4ed11fdaa69476dfa38', '37', '37', 'TEBS BOTOL PLASTIK 500 ML', 'MNM', 'PCS', '7000', '1', '7000', '2023-01-20 22:56:09', NULL, NULL, NULL),
('dbe892450b2d82cf371df3056d1880d3', '20230126212218359', '20230126212218359', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '1', '5000', '2023-01-26 21:22:23', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi'),
('8b50b1656950a67f614b6582250ee4dc', '2023020116065618', '2023020116065618', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '4', '20000', '2023-02-01 16:07:07', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi'),
('587d389d162217f3f12aa84069e0e6de', '2023020116065618', '2023020116065618', '99797ea5ce8cc9b4ed11fdaa69476dfa18', '17', '17', 'KOPI KOPIKO', 'MNM', 'PCS', '7000', '1', '7000', '2023-02-01 16:07:17', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi'),
('9e4065bb9be9feca381691c79b456d84', '2023020116065618', '2023020116065618', '5a999455e84eca294d9dd7b616f04b1b', '8997212800035', '8997212800035', 'YUZU ISOTONIC 350ML', 'Minuman', 'PCS', '5000', '2', '10000', '2023-02-01 16:07:27', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi'),
('4cdba6853d352ff322cbdcf26ed810ff', '2023020116065618', '2023020116065618', '99797ea5ce8cc9b4ed11fdaa69476dfa32', '31', '31', 'ROKOK GUDANG GARAM SURYA 16', 'Rokok', 'PCS', '31000', '1', '31000', '2023-02-01 16:07:37', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi'),
('76dd63a1143383c9a662f2884196d653', '2023020116065618', '2023020116065618', '2047a24fc4df502f655903e12a6d1254', '1', '', 'SEWA LAPANGAN HIJAU PAGI', 'Lapangan', 'JAM', '110000', '1', '110000', '2023-02-01 16:07:46', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi'),
('69aff3c78525cf76ea07497b5667af94', '2023020116065618', '2023020116065618', '99797ea5ce8cc9b4ed11fdaa69476dfa40', '39', '39', 'TEH BOTOL SOSRO PLASTIK 350 ML', 'MNM', 'PCS', '5000', '5', '25000', '2023-02-01 16:08:12', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi'),
('d68d57aea6ef300d305effffc0c095ee', '20230204091447222', '20230204091447222', '99797ea5ce8cc9b4ed11fdaa69476dfa26', '25', '25', 'AIR MINERAL PRIMA 1500 ML', 'MNM', 'PCS', '5000', '2', '10000', '2023-02-04 09:14:54', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN'),
('af1220cff9dc27fbafbcc7fcfb408a6d', '20230204091447222', '20230204091447222', '2047a24fc4df502f655903e12a6d1254', '1', '', 'SEWA LAPANGAN HIJAU PAGI', 'Lapangan', 'JAM', '110000', '1', '110000', '2023-02-04 09:15:03', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN'),
('625100a115f44592f065f44a287d4609', '20230205090730945', '20230205090730945', '99797ea5ce8cc9b4ed11fdaa69476dfa27', '26', '26', 'AIR MINERAL PRIMA 600 ML', 'MNM', 'PCS', '3000', '1', '3000', '2023-02-05 09:07:57', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi'),
('6e9865eae4a638980242cd1e38b4b057', '20230205090730945', '20230205090730945', '99797ea5ce8cc9b4ed11fdaa69476dfa25', '24', '24', 'POCARI SWEAT', 'MNM', 'PCS', '8000', '1', '8000', '2023-02-05 09:08:04', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi'),
('f0e053da56a1f3670cf2aac4d3e75f3f', '20230205112547266', '20230205112547266', '99797ea5ce8cc9b4ed11fdaa69476dfa27', '26', '26', 'AIR MINERAL PRIMA 600 ML', 'MNM', 'PCS', '3000', '1', '3000', '2023-02-05 11:25:52', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi'),
('101f8aa2d904798207a79abe2cd163d7', '20230205112547266', '20230205112547266', '99797ea5ce8cc9b4ed11fdaa69476dfa25', '24', '24', 'POCARI SWEAT', 'MNM', 'PCS', '8000', '1', '8000', '2023-02-05 11:25:58', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi'),
('c3a277fc0e94b8ea8a323d9006d4dc26', '20230205123921133', '20230205123921133', '99797ea5ce8cc9b4ed11fdaa69476dfa24', '23', '23', 'ORANGE WATER', 'MNM', 'PCS', '10000', '1', '10000', '2023-02-05 12:39:27', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi');

-- --------------------------------------------------------

--
-- Table structure for table `orderan`
--

CREATE TABLE `orderan` (
  `kd` varchar(50) NOT NULL,
  `kodebooking` varchar(100) DEFAULT NULL,
  `booking_postdate` datetime DEFAULT NULL,
  `booking_expire` datetime DEFAULT NULL,
  `hari_pakai` varchar(100) DEFAULT NULL,
  `tgl_pakai` date DEFAULT NULL,
  `o_nama` varchar(100) DEFAULT NULL,
  `o_nama_team` varchar(100) DEFAULT NULL,
  `o_alamat` varchar(100) DEFAULT NULL,
  `o_telp` varchar(100) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL,
  `tempat_biaya` varchar(15) DEFAULT NULL,
  `tempat_jam1` varchar(2) DEFAULT NULL,
  `tempat_jam2` varchar(2) DEFAULT NULL,
  `warna_posisi` varchar(100) DEFAULT NULL,
  `item_jml` varchar(5) DEFAULT NULL,
  `item_biaya` varchar(15) DEFAULT NULL,
  `bayar` enum('true','false') NOT NULL DEFAULT 'false',
  `bayar_postdate` datetime DEFAULT NULL,
  `total_biaya` varchar(15) DEFAULT NULL,
  `mulai` enum('true','false') NOT NULL DEFAULT 'false',
  `mulai_postdate` datetime DEFAULT NULL,
  `selesai` enum('true','false') NOT NULL DEFAULT 'false',
  `selesai_postdate` datetime DEFAULT NULL,
  `jml_brg` varchar(10) DEFAULT NULL,
  `jml_qty` varchar(10) DEFAULT NULL,
  `dp_no_nota` varchar(100) DEFAULT NULL,
  `dp_nominal` varchar(15) DEFAULT NULL,
  `dp_postdate` datetime DEFAULT NULL,
  `cara_bayar` varchar(100) DEFAULT NULL,
  `tempat_durasi` varchar(1) DEFAULT NULL,
  `ipnya` varchar(100) DEFAULT NULL,
  `filexnya` longtext DEFAULT NULL,
  `tempat_bayar` varchar(100) DEFAULT NULL,
  `user_kd` varchar(50) DEFAULT NULL,
  `user_kode` varchar(100) DEFAULT NULL,
  `user_nama` varchar(100) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orderan`
--

INSERT INTO `orderan` (`kd`, `kodebooking`, `booking_postdate`, `booking_expire`, `hari_pakai`, `tgl_pakai`, `o_nama`, `o_nama_team`, `o_alamat`, `o_telp`, `postdate`, `tempat_biaya`, `tempat_jam1`, `tempat_jam2`, `warna_posisi`, `item_jml`, `item_biaya`, `bayar`, `bayar_postdate`, `total_biaya`, `mulai`, `mulai_postdate`, `selesai`, `selesai_postdate`, `jml_brg`, `jml_qty`, `dp_no_nota`, `dp_nominal`, `dp_postdate`, `cara_bayar`, `tempat_durasi`, `ipnya`, `filexnya`, `tempat_bayar`, `user_kd`, `user_kode`, `user_nama`) VALUES
('d3d239696051d3d6b09bd4b0ab5048df', '22100040', '2022-10-31 17:10:46', '2022-10-31 18:10:00', 'Selasa', '2022-11-01', 'Jodevan', 'Jodevan', 'Semarang', '089530933761', '2022-10-31 17:10:46', '120000', '18', '20', 'Hijau', NULL, NULL, 'false', NULL, '240000', 'true', '2022-11-07 18:06:59', 'true', '2022-11-07 18:08:24', NULL, NULL, NULL, '100000', '2022-10-31 17:17:50', 'Transfer', '2', '103.213.128.189', 'd3d239696051d3d6b09bd4b0ab5048df-1.jpg', '240000', NULL, NULL, NULL),
('168e3899d5c1f5bdd74c19abf3698f05', '22100041', '2022-10-31 18:27:37', '2022-10-31 19:27:00', 'Senin', '2022-10-31', 'Silvi', 'Wkwk Fc', 'Semarang', '085785122887', '2022-10-31 18:27:37', '120000', '23', '24', 'Hijau', NULL, NULL, 'false', NULL, NULL, 'false', NULL, 'false', NULL, NULL, NULL, NULL, NULL, NULL, 'Tunai', '1', '140.213.173.184', NULL, NULL, NULL, NULL, NULL),
('387d6df87d7ac1afac347264bd7dd4d8', '22100042', '2022-10-31 18:28:52', '2022-10-31 19:28:00', 'Senin', '2022-10-31', 'Benjot', 'Palebon Fc', 'Semarang', '085785122887', '2022-10-31 18:28:52', '120000', '23', '24', 'Hijau', NULL, NULL, 'false', NULL, NULL, 'false', NULL, 'false', NULL, NULL, NULL, NULL, '1', '2022-10-31 18:28:52', 'Transfer', '1', '140.213.173.184', NULL, NULL, NULL, NULL, NULL),
('3b83c7d45717ff3edc9fe62a64e4568f', '22110001', '2022-11-01 13:55:53', '2022-11-01 14:55:00', 'Selasa', '2022-11-01', 'Nanang imam Fadjri', 'Jfc', 'Semarang', '082138912563', '2022-11-01 13:55:53', '80000', '11', '13', 'Hijau', NULL, NULL, 'false', NULL, NULL, 'false', NULL, 'false', NULL, NULL, NULL, NULL, NULL, NULL, 'Tunai', '2', '182.2.36.37', NULL, NULL, NULL, NULL, NULL),
('5cccf932ca402acb0a2e940a7fb7f04b', '22110002', '2022-11-01 21:29:04', '2022-11-01 22:29:00', 'Selasa', '2022-11-01', 'Pjkr', 'Pjkr', 'Semarang', '085700556047', '2022-11-01 21:29:04', '120000', '16', '17', 'Merah', NULL, NULL, 'false', NULL, NULL, 'false', NULL, 'false', NULL, NULL, NULL, NULL, NULL, NULL, 'Tunai', '1', '182.2.73.212', NULL, NULL, NULL, NULL, NULL),
('b37a9f8f6f7e7688acff13b4373f7e43', '22110003', '2022-11-02 17:01:23', '2022-11-02 18:01:00', 'Jumat', '2022-11-04', 'Join fc', 'Join fv', 'Semarang', '088', '2022-11-02 17:01:23', '120000', '18', '20', 'Hijau', NULL, NULL, 'false', NULL, NULL, 'false', NULL, 'false', NULL, NULL, NULL, NULL, '50000', '2022-11-02 17:04:32', 'Tunai', '2', '182.2.73.124', 'b37a9f8f6f7e7688acff13b4373f7e43-1.jpg', NULL, NULL, NULL, NULL),
('b79020be40bd22e05c12e90a93fae9b6', '22110004', '2022-11-02 20:51:15', '2022-11-02 21:51:00', 'Jumat', '2022-11-04', 'Naufal', 'Naufal', 'Semarang', '088', '2022-11-02 20:51:15', '120000', '19', '20', 'Merah', NULL, NULL, 'false', NULL, '120000', 'true', '2022-11-04 19:25:46', 'true', '2022-11-04 20:20:47', NULL, NULL, NULL, '50000', '2022-11-02 20:56:15', 'Transfer', '1', '103.213.129.109', 'b79020be40bd22e05c12e90a93fae9b6-1.jpg', '120000', NULL, NULL, NULL),
('3bd788f4e13c4fdb483228ee0565d244', '22110005', '2022-11-03 17:24:49', '2022-11-03 18:24:00', 'Jumat', '2022-11-04', 'Ran', 'Ran', 'Semarang', '088', '2022-11-03 17:24:49', '120000', '17', '18', 'Merah', NULL, NULL, 'false', NULL, '120000', 'true', '2022-11-04 19:25:11', 'true', '2022-11-04 20:19:59', NULL, NULL, NULL, '50000', '2022-11-03 17:26:31', 'Tunai', '1', '103.213.129.109', '3bd788f4e13c4fdb483228ee0565d244-1.jpg', '120000', NULL, NULL, NULL),
('fc48cf7d7025d2a0050617ebcae05a3d', '22110006', '2022-11-04 14:33:37', '2022-11-04 15:33:00', 'Jumat', '2022-11-04', 'Smansa', 'Smansa', 'Semarang', '082138912563', '2022-11-04 14:33:37', '120000', '16', '18', 'Hijau', NULL, NULL, 'false', NULL, NULL, 'false', NULL, 'false', NULL, NULL, NULL, NULL, '50000', '2022-11-04 14:35:53', 'Transfer', '2', '103.213.129.109', NULL, NULL, NULL, NULL, NULL),
('a87c7c19072731be84e0dd4381d9f0c4', '22110007', '2022-11-04 14:37:02', '2022-11-04 15:37:00', 'Jumat', '2022-11-04', 'Ibnu', 'Ibnu', 'Semarang', '089531086820', '2022-11-04 14:37:02', '80000', '15', '17', 'Merah', NULL, NULL, 'false', NULL, '160000', 'true', '2022-11-04 19:26:40', 'true', '2022-11-04 19:31:25', NULL, NULL, NULL, '50000', '2022-11-04 14:37:52', 'Transfer', '2', '103.213.129.109', NULL, '160000', NULL, NULL, NULL),
('4b55072ae0eae841d59b5a1b17808675', '22110008', '2022-11-04 14:40:01', '2022-11-04 15:40:00', 'Jumat', '2022-11-04', 'Fik unisulla', 'Fik unisulla', 'Semarang', '081953772771', '2022-11-04 14:40:01', '120000', '20', '22', 'Merah', NULL, NULL, 'false', NULL, '240000', 'true', '2022-11-04 20:21:22', 'true', '2022-11-05 17:46:09', NULL, NULL, NULL, '50000', '2022-11-04 14:42:08', 'Transfer', '2', '103.213.129.109', NULL, '240000', NULL, NULL, NULL),
('4724068562f9b5668b77b86fe76e982f', '22110009', '2022-11-04 14:43:37', '2022-11-04 15:43:00', 'Jumat', '2022-11-04', 'Telkom', 'Telkom', 'Semarang', '082138912563', '2022-11-04 14:43:37', '120000', '20', '22', 'Hijau', NULL, NULL, 'false', NULL, '240000', 'true', '2022-11-04 20:19:48', 'true', '2022-11-05 17:45:51', NULL, NULL, NULL, '50000', '2022-11-04 14:44:07', 'Transfer', '2', '103.213.129.109', NULL, '240000', NULL, NULL, NULL),
('033c462340c93e7cc10236c915179279', '22110010', '2022-11-04 14:45:29', '2022-11-04 15:45:00', 'Jumat', '2022-11-04', 'Armatim', 'Armatim', 'Semarang', '082138912563', '2022-11-04 14:45:29', '120000', '22', '24', 'Hijau', NULL, NULL, 'false', NULL, NULL, 'false', NULL, 'false', NULL, NULL, NULL, NULL, '50000', '2022-11-04 14:45:58', 'Tunai', '2', '103.213.129.109', NULL, NULL, NULL, NULL, NULL),
('a3499c98f670010f2f2398c564318698', '22110011', '2022-11-04 14:47:36', '2022-11-04 15:47:00', 'Minggu', '2022-11-06', 'Moreno', 'Moreno', 'Semarang', '082138912563', '2022-11-04 14:47:36', '80000', '13', '15', 'Hijau', NULL, NULL, 'false', NULL, NULL, 'false', NULL, 'false', NULL, NULL, NULL, NULL, '100000', '2022-11-04 14:49:30', 'Tunai', '2', '103.213.129.109', NULL, NULL, NULL, NULL, NULL),
('24691fba2026e256ed83b6112cbf24a2', '22110012', '2022-11-04 14:51:29', '2022-11-04 15:51:00', 'Sabtu', '2022-11-05', 'Moreno', 'Moreno', 'Semarang', '082138912563', '2022-11-04 14:51:29', '80000', '13', '15', 'Hijau', NULL, NULL, 'false', NULL, NULL, 'false', NULL, 'false', NULL, NULL, NULL, NULL, '100000', '2022-11-04 14:52:00', 'Tunai', '2', '103.213.129.109', NULL, NULL, NULL, NULL, NULL),
('4b77c909de45af3ad02655685127ba95', '22110013', '2022-11-04 14:53:10', '2022-11-04 15:53:00', 'Sabtu', '2022-11-05', 'Fauzan', 'Fauzan', 'Semarang', '082138912563', '2022-11-04 14:53:10', '120000', '20', '22', 'Hijau', NULL, NULL, 'false', NULL, '240000', 'true', '2022-11-05 20:17:36', 'true', '2022-11-05 21:58:37', NULL, NULL, NULL, '50000', '2022-11-04 14:53:44', 'Transfer', '2', '103.213.129.109', NULL, '240000', NULL, NULL, NULL),
('3c0386e48976af9507a8bfc23f31e57d', '22110015', '2022-11-05 17:40:27', '2022-11-05 18:40:00', 'Sabtu', '2022-11-05', 'Angin laut', 'Angin laut', 'Semarang', '082138912563', '2022-11-05 17:40:27', '140000', '17', '18', 'Hijau', NULL, NULL, 'false', NULL, NULL, 'false', NULL, 'false', NULL, NULL, NULL, NULL, '', '2022-11-05 17:40:49', 'Tunai', '1', '103.213.129.109', NULL, NULL, NULL, NULL, NULL),
('5e9f94271f7c46c4b998f818a3823bab', '22110016', '2022-11-05 17:41:41', '2022-11-05 18:41:00', 'Sabtu', '2022-11-05', 'Angin laut', 'Angin laut', 'Semarang', '082138912563', '2022-11-05 17:41:41', '140000', '17', '18', 'Hijau', NULL, NULL, 'false', NULL, '140000', 'true', '2022-11-05 17:45:11', 'true', '2022-11-05 20:17:47', NULL, NULL, NULL, '50000', '2022-11-05 17:41:54', 'Tunai', '1', '103.213.129.109', NULL, '140000', NULL, NULL, NULL),
('685d9015979c3a1bdba0820b903208d5', '22110017', '2022-11-05 17:43:26', '2022-11-05 18:43:00', 'Sabtu', '2022-11-05', 'Siliwangi', 'Siliwangi', 'Semarang', '082138912563', '2022-11-05 17:43:26', '140000', '17', '18', 'Merah', NULL, NULL, 'false', NULL, '140000', 'true', '2022-11-05 17:44:54', 'true', '2022-11-05 17:50:59', NULL, NULL, NULL, '50000', '2022-11-05 17:43:30', 'Tunai', '1', '103.213.129.109', NULL, '140000', NULL, NULL, NULL),
('5bde16a25d4f6da4b3bb08fc5ae7af4d', '22110018', '2022-11-05 17:57:34', '2022-11-05 18:57:00', 'Sabtu', '2022-11-05', 'Pirel fc', 'Pirel fc', 'Semarang', '082138912563', '2022-11-05 17:57:34', '140000', '21', '23', 'Merah', '3', '15000', 'false', NULL, '295000', 'true', '2022-11-05 21:15:19', 'true', '2022-11-05 23:08:31', NULL, NULL, NULL, '50000', '2022-11-05 17:59:28', 'Transfer', '2', '103.213.129.109', NULL, '280000', NULL, NULL, NULL),
('77473e5602d541763ae947f68026d6f5', '22110019', '2022-11-05 22:02:18', '2022-11-05 23:02:00', 'Sabtu', '2022-11-05', 'Atlantik', 'Atlantik', 'Semarang', '082138912563', '2022-11-05 22:02:18', '140000', '22', '23', 'Hijau', NULL, NULL, 'false', NULL, '140000', 'true', '2022-11-05 22:03:43', 'true', '2022-11-05 23:00:16', NULL, NULL, NULL, '50000', '2022-11-05 22:03:04', 'Tunai', '1', '103.213.129.109', NULL, '140000', NULL, NULL, NULL),
('bc4f560fdea10481d5418ff3b17a73df', '22110024', '2022-11-07 17:20:53', '2022-11-07 18:20:00', 'Senin', '2022-11-07', 'Devandra', 'Devandra', 'Semarang', '082138912563', '2022-11-07 17:20:53', '120000', '16', '18', 'Hijau', NULL, NULL, 'false', NULL, '240000', 'true', '2022-11-07 17:21:43', 'true', '2022-11-07 18:04:21', NULL, NULL, NULL, '50000', '2022-11-07 17:21:38', 'Tunai', '2', '103.213.129.109', NULL, '240000', NULL, NULL, NULL),
('242f888da1b55a67fb956afc1b877874', '22110025', '2022-11-07 17:23:38', '2022-11-07 18:23:00', 'Senin', '2022-11-07', 'Indonesia power', 'Indonesia power', 'Semarang', '082138912563', '2022-11-07 17:23:38', '120000', '18', '20', 'Hijau', '10', '30000', 'false', NULL, '270000', 'true', '2022-11-07 18:08:17', 'true', '2022-11-07 19:52:18', NULL, NULL, NULL, '50000', '2022-11-07 17:25:00', 'Tunai', '2', '103.213.129.109', NULL, '240000', NULL, NULL, NULL),
('c274d618652c862a21c188f653cbe78d', '22110022', '2022-11-07 17:18:51', '2022-11-07 18:18:00', 'Senin', '2022-11-07', 'Usm', 'Usm', 'Semarang', '082138912563', '2022-11-07 17:18:51', '120000', '17', '18', 'Merah', NULL, NULL, 'false', NULL, '120000', 'true', '2022-11-07 17:22:34', 'true', '2022-11-07 18:06:03', NULL, NULL, NULL, '50000', '2022-11-07 17:22:27', 'Tunai', '1', '103.213.129.109', NULL, '120000', NULL, NULL, NULL),
('67de860d5ba97dde460cb158c683e8c5', '22110027', '2022-11-07 17:26:47', '2022-11-07 18:26:00', 'Senin', '2022-11-07', 'Aziz', 'Aziz', 'Semarang', '082138912563', '2022-11-07 17:26:47', '120000', '20', '21', 'Hijau', NULL, NULL, 'false', NULL, '120000', 'true', '2022-11-07 19:51:26', 'true', '2022-11-08 16:27:43', NULL, NULL, NULL, '50000', '2022-11-07 17:27:33', 'Tunai', '1', '103.213.129.109', NULL, '120000', NULL, NULL, NULL),
('85272cde9ff1f33c4700e5cbabae8be6', '22110028', '2022-11-07 17:29:18', '2022-11-07 18:29:00', 'Senin', '2022-11-07', 'Aziz', 'Aziz', 'Semarang', '082138912563', '2022-11-07 17:29:18', '120000', '21', '22', 'Hijau', NULL, NULL, 'false', NULL, '120000', 'true', '2022-11-07 19:54:57', 'true', '2022-11-08 16:27:33', NULL, NULL, NULL, '50000', '2022-11-07 19:54:50', 'Tunai', '1', '103.213.129.109', NULL, '120000', NULL, NULL, NULL),
('766cf33e003884d4d23bfbf64887d33f', '22110029', '2022-11-07 17:30:25', '2022-11-07 18:30:00', 'Senin', '2022-11-07', 'Pambudi', 'Pambudi', 'Semarang', '082138912563', '2022-11-07 17:30:25', '120000', '22', '24', 'Hijau', NULL, NULL, 'false', NULL, NULL, 'false', NULL, 'false', NULL, NULL, NULL, NULL, '50000', '2022-11-07 17:32:40', 'Tunai', '2', '103.213.129.109', NULL, NULL, NULL, NULL, NULL),
('20ab28bd41a71d1c6887f960aa0f6ecd', '22110030', '2022-11-07 17:33:17', '2022-11-07 18:33:00', 'Senin', '2022-11-07', 'Wawan', 'Wawan', 'Semarang', '082138912563', '2022-11-07 17:33:17', '120000', '19', '21', 'Merah', NULL, NULL, 'false', NULL, '240000', 'true', '2022-11-07 18:51:52', 'true', '2022-11-07 21:03:12', NULL, NULL, NULL, '50000', '2022-11-07 17:34:14', 'Tunai', '2', '103.213.129.109', NULL, '240000', NULL, NULL, NULL),
('2a51dd70c8458cf7b665c5e7f8a32359', '22110052', '2022-11-07 21:08:28', '2022-11-07 22:08:00', 'Senin', '2022-11-07', 'Amri', 'Amri', 'Semarang', '082138912563', '2022-11-07 21:08:28', '120000', '21', '22', 'Merah', NULL, NULL, 'false', NULL, NULL, 'false', NULL, 'false', NULL, NULL, NULL, NULL, '', '2022-11-07 21:08:32', 'Tunai', '1', '103.213.129.109', NULL, NULL, NULL, NULL, NULL),
('0ba6a70d20f250b4bcb2220c1ed3af17', '22110054', '2022-11-08 09:32:19', '2022-11-08 10:32:00', 'Selasa', '2022-11-08', 'Teknik Sipil', 'Teknik Sipil', 'Semarang', '081325608361', '2022-11-08 09:32:19', '120000', '16', '18', 'Merah', '2', '6000', 'false', NULL, '246000', 'true', '2022-11-08 17:23:58', 'true', '2022-11-08 18:04:15', NULL, NULL, NULL, '50000', '2022-11-08 17:23:51', 'Tunai', '2', '103.213.129.109', NULL, '240000', NULL, NULL, NULL),
('7b9f042ad9f7ceb9bf7ea922c596d83f', '22110033', '2022-11-07 17:40:27', '2022-11-07 18:40:00', 'Selasa', '2022-11-08', 'Arjuna', 'Arjuna', 'Semarang', '082138912563', '2022-11-07 17:40:27', '120000', '16', '17', 'Hijau', '3', '15000', 'false', NULL, '135000', 'true', '2022-11-08 16:28:25', 'true', '2022-11-08 17:32:28', NULL, NULL, NULL, '50000', '2022-11-07 17:44:11', 'Transfer', '1', '103.213.129.109', NULL, '120000', NULL, NULL, NULL),
('1d8dac03f5bdc416eaff09d16127f364', '22110034', '2022-11-07 17:44:59', '2022-11-07 18:44:00', 'Selasa', '2022-11-08', 'Tehnik sipil', 'Tehnik sipil', 'Semarang', '082138912563', '2022-11-07 17:44:59', '120000', '16', '17', 'Merah', NULL, NULL, 'false', NULL, '120000', 'true', '2022-11-08 16:28:38', 'true', '2022-11-08 17:35:19', NULL, NULL, NULL, '50000', '2022-11-08 11:41:47', 'Tunai', '1', '103.213.129.109', NULL, '120000', NULL, NULL, NULL),
('66410548ea22255c62fd54293bba578d', '22110035', '2022-11-07 17:45:24', '2022-11-07 18:45:00', 'Selasa', '2022-11-08', 'Tehnik sipil', 'Tehnik sipil', 'Semarang', '082138912563', '2022-11-07 17:45:24', '120000', '16', '18', 'Merah', NULL, NULL, 'false', NULL, NULL, 'false', NULL, 'false', NULL, NULL, NULL, NULL, '50000', '2022-11-07 17:46:14', 'Tunai', '2', '103.213.129.109', NULL, NULL, NULL, NULL, NULL),
('d223fa2001dffde19212c33008422ce8', '22110036', '2022-11-07 17:47:38', '2022-11-07 18:47:00', 'Selasa', '2022-11-08', 'Ikrom', 'Ikrom', 'Semarang', '082138912563', '2022-11-07 17:47:38', '120000', '19', '21', 'Hijau', '3', '15000', 'false', NULL, '255000', 'true', '2022-11-08 18:58:21', 'true', '2022-11-08 20:57:34', NULL, NULL, NULL, '50000', '2022-11-07 17:48:30', 'Transfer', '2', '103.213.129.109', NULL, '240000', NULL, NULL, NULL),
('fcf29da862812e941d6d572d36bedba8', '22110037', '2022-11-07 17:49:20', '2022-11-07 18:49:00', 'Selasa', '2022-11-08', 'Haryono bj', 'Bank Jateng', 'Semarang', '082138912563', '2022-11-07 17:49:20', '120000', '19', '21', 'Merah', NULL, NULL, 'false', NULL, '240000', 'true', '2022-11-08 19:08:06', 'true', '2022-11-08 20:55:30', NULL, NULL, NULL, '1', '2022-11-07 17:49:35', 'Transfer', '2', '103.213.129.109', NULL, '240000', NULL, NULL, NULL),
('7235ae1f86e4ab06bba0cad87d78d4f2', '22110038', '2022-11-07 17:51:39', '2022-11-07 18:51:00', 'Selasa', '2022-11-08', 'Ready fc', 'Ready fc', 'Semarang', '082138912563', '2022-11-07 17:51:39', '120000', '22', '24', 'Hijau', NULL, NULL, 'false', NULL, '240000', 'true', '2022-11-08 22:10:39', 'true', '2022-11-08 23:59:43', NULL, NULL, NULL, '50000', '2022-11-07 17:52:23', 'Transfer', '2', '103.213.129.109', NULL, '240000', NULL, NULL, NULL),
('57e0546f9cb369c85de374e2e4ca37ae', '22110039', '2022-11-07 17:53:55', '2022-11-07 18:53:00', 'Rabu', '2022-11-09', 'Kofu', 'Kofu', 'Semarang', '082138912563', '2022-11-07 17:53:55', '120000', '19', '21', 'Hijau', '2', '10000', 'false', NULL, '250000', 'true', '2022-11-09 18:57:20', 'true', '2022-11-09 21:36:00', NULL, NULL, NULL, '100000', '2022-11-07 17:54:29', 'Tunai', '2', '103.213.129.109', NULL, '240000', NULL, NULL, NULL),
('6241fc7ec4009507880f47c1ca0f2e7c', '22110040', '2022-11-07 17:55:17', '2022-11-07 18:55:00', 'Rabu', '2022-11-09', 'Gyr', 'Gyr', 'Semarang', '082138912563', '2022-11-07 17:55:17', '120000', '19', '21', 'Merah', '2', '10000', 'false', NULL, '250000', 'true', '2022-11-09 18:57:00', 'true', '2022-11-09 21:12:49', NULL, NULL, NULL, '50000', '2022-11-07 17:55:49', 'Tunai', '2', '103.213.129.109', NULL, '240000', NULL, NULL, NULL),
('d877c316c97932287d18c2a3f536bf99', '22110041', '2022-11-07 17:58:20', '2022-11-07 18:58:00', 'Rabu', '2022-11-09', 'Gondrong', 'Gondrong', 'Semarang', '082138912563', '2022-11-07 17:58:20', '120000', '21', '22', 'Hijau', NULL, NULL, 'false', NULL, '120000', 'true', '2022-11-09 21:10:13', 'true', '2022-11-10 20:06:49', NULL, NULL, NULL, '50000', '2022-11-07 17:58:47', 'Tunai', '1', '103.213.129.109', NULL, '120000', NULL, NULL, NULL),
('ebf5b076dd826e895d919d44205bd3c9', '22110042', '2022-11-07 18:00:05', '2022-11-07 19:00:00', 'Rabu', '2022-11-09', 'Upgris', 'Upgris', 'Semarang', '082138912563', '2022-11-07 18:00:05', '120000', '21', '23', 'Merah', NULL, NULL, 'false', NULL, '240000', 'true', '2022-11-09 21:04:14', 'true', '2022-11-10 20:05:34', NULL, NULL, NULL, '50000', '2022-11-07 18:00:49', 'Tunai', '2', '103.213.129.109', NULL, '240000', NULL, NULL, NULL),
('7724c68e69142aad1b1751802dc731cb', '22110043', '2022-11-07 18:15:41', '2022-11-07 19:15:00', 'Kamis', '2022-11-10', 'Multindo', 'Multindo', 'Semarang', '082138912563', '2022-11-07 18:15:41', '120000', '19', '20', 'Hijau', NULL, NULL, 'false', NULL, '120000', 'true', '2022-11-08 19:06:04', 'true', '2022-11-08 19:08:22', NULL, NULL, NULL, '50000', '2022-11-07 18:17:12', 'Transfer', '1', '103.213.129.109', NULL, '120000', NULL, NULL, NULL),
('bb77bfbceb4428762cae3d159dfd975a', '22110044', '2022-11-07 18:18:13', '2022-11-07 19:18:00', 'Kamis', '2022-11-10', 'Barito fc', 'Barito fc', 'Semarang', '082138912563', '2022-11-07 18:18:13', '120000', '20', '21', 'Hijau', '4', '133000', 'false', NULL, '253000', 'true', '2022-11-10 20:08:55', 'true', '2022-11-10 22:28:50', NULL, NULL, NULL, '50000', '2022-11-07 18:19:39', 'Tunai', '1', '103.213.129.109', NULL, '120000', NULL, NULL, NULL),
('f3199f1bf8ad9ec44d45ee8ed78d018d', '22110067', '2022-11-09 16:38:20', '2022-11-09 17:38:00', 'Kamis', '2022-11-10', 'Barito fc', 'Barito fc', 'Semarang', '082138912563', '2022-11-09 16:38:20', '120000', '21', '22', 'Hijau', NULL, NULL, 'false', NULL, '120000', 'true', '2022-11-10 20:10:41', 'true', '2022-11-10 22:02:35', NULL, NULL, NULL, '1', '2022-11-09 16:38:24', 'Transfer', '1', '103.213.129.109', NULL, '120000', NULL, NULL, NULL),
('b5098b12959e731109214f6ac3f63788', '22110046', '2022-11-07 18:22:55', '2022-11-07 19:22:00', 'Kamis', '2022-11-10', 'Wahyu', 'Wahyu', 'Semarang', '082138912563', '2022-11-07 18:22:55', '120000', '20', '22', 'Merah', NULL, NULL, 'false', NULL, '240000', 'true', '2022-11-10 20:11:01', 'true', '2022-11-10 22:16:01', NULL, NULL, NULL, '50000', '2022-11-07 18:23:58', 'Transfer', '2', '103.213.129.109', NULL, '240000', NULL, NULL, NULL),
('51ad36f0edad3d29d503673ef3a27eb5', '22110047', '2022-11-07 18:25:42', '2022-11-07 19:25:00', 'Jumat', '2022-11-11', 'Hjma', 'Hjma', 'Semarang', '082138912563', '2022-11-07 18:25:42', '100000', '15', '17', 'Hijau', NULL, NULL, 'false', NULL, '200000', 'true', '2022-11-11 17:01:48', 'true', '2022-11-11 17:04:41', NULL, NULL, NULL, '50000', '2022-11-07 18:27:32', 'Transfer', '2', '103.213.129.109', NULL, '200000', NULL, NULL, NULL),
('6580d47b9b8d9fc17dbb8d4009d54e94', '22110048', '2022-11-07 18:28:59', '2022-11-07 19:28:00', 'Sabtu', '2022-11-12', 'Dwi Andi', 'Dwi andi', 'Semarang', '082138912563', '2022-11-07 18:28:59', '140000', '19', '20', 'Hijau', NULL, NULL, 'false', NULL, NULL, 'false', NULL, 'false', NULL, NULL, NULL, NULL, '50000', '2022-11-07 18:29:30', 'Tunai', '1', '103.213.129.109', NULL, NULL, NULL, NULL, NULL),
('4675e0751f6506e87370c944ca126a08', '22110068', '2022-11-09 16:40:25', '2022-11-09 17:40:00', 'Sabtu', '2022-11-12', 'Dwi Andi', 'Dwi andi', 'Semarang', '082138912563', '2022-11-09 16:40:25', '140000', '20', '21', 'Hijau', NULL, NULL, 'false', NULL, NULL, 'false', NULL, 'false', NULL, NULL, NULL, NULL, '1', '2022-11-09 16:40:34', 'Transfer', '1', '103.213.129.109', NULL, NULL, NULL, NULL, NULL),
('2ee8f98c876e4286785766e2579e2223', '22110050', '2022-11-07 18:35:08', '2022-11-07 19:35:00', 'Sabtu', '2022-11-12', 'Pirel fc', 'Pirel fc', 'Semarang', '082138912563', '2022-11-07 18:35:08', '140000', '21', '23', 'Hijau', NULL, NULL, 'false', NULL, NULL, 'false', NULL, 'false', NULL, NULL, NULL, NULL, '50000', '2022-11-07 18:36:07', 'Tunai', '2', '103.213.129.109', NULL, NULL, NULL, NULL, NULL),
('22d6a0eee7ff3c1a0cc65fd292f32149', '22110051', '2022-11-07 18:37:26', '2022-11-07 19:37:00', 'Sabtu', '2022-11-12', 'Iqbal', 'Iqbaal', 'Semarang', '082138912563', '2022-11-07 18:37:26', '140000', '20', '22', 'Merah', NULL, NULL, 'false', NULL, '280000', 'true', '2022-11-08 10:09:30', 'true', '2022-11-08 10:09:40', NULL, NULL, NULL, '50000', '2022-11-07 18:38:16', 'Tunai', '2', '103.213.129.109', NULL, '280000', NULL, NULL, NULL),
('aa3ad3733f306a56f63e0e2e7d199ce3', '22110053', '2022-11-07 21:09:22', '2022-11-07 22:09:00', 'Senin', '2022-11-07', 'Amri', 'Amri', 'Semarang', '082138912563', '2022-11-07 21:09:22', '120000', '21', '22', 'Merah', NULL, NULL, 'false', NULL, NULL, 'false', NULL, 'false', NULL, NULL, NULL, NULL, '', '2022-11-07 21:09:42', 'Tunai', '1', '103.213.129.109', NULL, NULL, NULL, NULL, NULL),
('c5b69e058845b942bf7675e52d88898d', '22110055', '2022-11-08 16:46:36', '2022-11-08 17:46:00', 'Selasa', '2022-11-08', 'Yansyah', 'Yansyah', 'Semarang', '082138912563', '2022-11-08 16:46:36', '120000', '21', '22', 'Hijau', '4', '20000', 'false', NULL, '140000', 'true', '2022-11-08 20:58:36', 'true', '2022-11-08 22:09:56', NULL, NULL, NULL, '50000', '2022-11-08 16:47:22', 'Transfer', '1', '103.213.129.109', NULL, '120000', NULL, NULL, NULL),
('45f6b1fecfb782e2518b3d970003178a', '22110056', '2022-11-08 16:48:11', '2022-11-08 17:48:00', 'Rabu', '2022-11-09', 'Febbi', 'Febbi', 'Semarang', '082138912563', '2022-11-08 16:48:11', '120000', '17', '19', 'Hijau', '1', '5000', 'false', NULL, '245000', 'true', '2022-11-09 17:11:34', 'true', '2022-11-09 19:24:21', NULL, NULL, NULL, '50000', '2022-11-08 16:48:51', 'Tunai', '2', '103.213.129.109', NULL, '240000', NULL, NULL, NULL),
('5ecffe0132a16f5e24061379327f8707', '22110062', '2022-11-08 17:42:10', '2022-11-08 18:42:00', 'Selasa', '2022-11-08', 'Brian', 'Brian', 'Semarang', '082138912563', '2022-11-08 17:42:10', '120000', '18', '19', 'Merah', '4', '20000', 'false', NULL, '140000', 'true', '2022-11-08 17:55:38', 'true', '2022-11-08 19:02:48', NULL, NULL, NULL, '1', '2022-11-08 17:42:16', 'Transfer', '1', '103.213.129.109', NULL, '120000', NULL, NULL, NULL),
('0ff0cc592e1be3d3c7791504633842a1', '22110061', '2022-11-08 17:41:33', '2022-11-08 18:41:00', 'Selasa', '2022-11-08', 'Adella', 'Adella', 'Semarang', '082138912563', '2022-11-08 17:41:33', '120000', '18', '19', 'Hijau', NULL, NULL, 'false', NULL, '120000', 'true', '2022-11-08 18:08:31', 'true', '2022-11-08 19:04:32', NULL, NULL, NULL, '1', '2022-11-08 17:41:38', 'Transfer', '1', '103.213.129.109', NULL, '120000', NULL, NULL, NULL),
('4dad06c1e523fc387129514cea1ac2e9', '22110059', '2022-11-08 17:39:36', '2022-11-08 18:39:00', 'Jumat', '2022-11-11', 'Pasukan rob', 'Pasukan rob', 'Semarang', '082138912563', '2022-11-08 17:39:36', '140000', '19', '21', 'Merah', NULL, NULL, 'false', NULL, NULL, 'false', NULL, 'false', NULL, NULL, NULL, NULL, '1', '2022-11-08 17:39:41', 'Transfer', '2', '103.213.129.109', NULL, NULL, NULL, NULL, NULL),
('80a566071e57a5848035caa8f7c6b7a7', '22110060', '2022-11-08 17:40:23', '2022-11-08 18:40:00', 'Jumat', '2022-11-11', 'Telkom', 'Telkom', 'Semarang', '082138912563', '2022-11-08 17:40:23', '140000', '20', '22', 'Hijau', NULL, NULL, 'false', NULL, NULL, 'false', NULL, 'false', NULL, NULL, NULL, NULL, '1', '2022-11-08 17:40:26', 'Transfer', '2', '103.213.129.109', NULL, NULL, NULL, NULL, NULL),
('43d0fd8e5b83cebad3ca32ffce3ca2af', '22110064', '2022-11-09 16:28:03', '2022-11-09 17:28:00', 'Rabu', '2022-11-09', 'Pjkr', 'Pjkr', 'Semarang', '082138912563', '2022-11-09 16:28:03', '120000', '16', '17', 'Hijau', '5', '140000', 'false', NULL, '260000', 'true', '2022-11-09 16:29:01', 'true', '2022-11-09 17:26:44', NULL, NULL, NULL, '50000', '2022-11-09 16:28:55', 'Transfer', '1', '103.213.129.109', NULL, '120000', NULL, NULL, NULL),
('bc19ec917c4a3feae2d358f0eeda0dfe', '22110065', '2022-11-09 16:30:05', '2022-11-09 17:30:00', 'Rabu', '2022-11-09', 'Gavin', 'Gavin', 'Semarang', '082138912563', '2022-11-09 16:30:05', '120000', '17', '19', 'Merah', NULL, NULL, 'false', NULL, '240000', 'true', '2022-11-09 17:23:26', 'true', '2022-11-09 18:55:50', NULL, NULL, NULL, '50000', '2022-11-09 16:31:29', 'Transfer', '2', '103.213.129.109', NULL, '240000', NULL, NULL, NULL),
('4ba36e534fc713b1347c2a621c4f6764', '22110066', '2022-11-09 16:32:54', '2022-11-09 17:32:00', 'Jumat', '2022-11-11', 'Irfandi', 'Irfandi', 'Semarang', '082138912563', '2022-11-09 16:32:54', '140000', '19', '20', 'Hijau', NULL, NULL, 'false', NULL, NULL, 'false', NULL, 'false', NULL, NULL, NULL, NULL, '50000', '2022-11-09 16:33:27', 'Transfer', '1', '103.213.129.109', NULL, NULL, NULL, NULL, NULL),
('944b5d113cac9b564c4fa900b68131b6', '23010001', '2023-01-07 19:38:35', '2023-01-07 20:38:00', 'Minggu', '2023-01-08', 'Ilham agum', 'Pt pel', 'Pt pel', '+62 895xstrix3604xstrix81709', '2023-01-07 19:38:35', '110.000', '10', '12', 'Hijau', NULL, NULL, 'false', NULL, NULL, 'false', NULL, 'false', NULL, NULL, NULL, NULL, '50000', '2023-01-07 19:39:28', 'Transfer', '2', '202.80.213.32', NULL, NULL, NULL, NULL, NULL),
('d07e7f373db01fb6eb875da031b80eca', '23010002', '2023-01-07 19:38:35', '2023-01-07 20:38:00', 'Minggu', '2023-01-08', 'Ilham agum', 'Pt pel', 'Pt pel', '+62 895xstrix3604xstrix81709', '2023-01-07 19:38:35', '110.000', '10', '12', 'Hijau', NULL, NULL, 'false', NULL, NULL, 'false', NULL, 'false', NULL, NULL, NULL, NULL, '1', '2023-01-07 19:38:35', 'Transfer', '2', '202.80.213.32', NULL, NULL, NULL, NULL, NULL),
('ed43d75dced775f0370a00934f1b077c', '23010003', '2023-01-12 15:28:25', '2023-01-12 16:28:00', 'Kamis', '2023-01-12', 'Aurelio', 'Aurelio', 'Semarang', '089676760663', '2023-01-12 15:28:25', '160.000', '19', '21', 'Merah', NULL, NULL, 'false', NULL, NULL, 'false', NULL, 'false', NULL, NULL, NULL, NULL, '', '2023-01-12 15:28:41', 'Tunai', '2', '202.80.218.52', NULL, NULL, NULL, NULL, NULL),
('211c09ae09cbe9926a7bfd929cb18915', '23010004', '2023-01-12 15:28:57', '2023-01-12 16:28:00', 'Kamis', '2023-01-12', 'Aurelio', 'Aurelio', 'Semarang', '089676760663', '2023-01-12 15:28:57', '160.000', '19', '21', 'Merah', NULL, NULL, 'false', NULL, NULL, 'false', NULL, 'false', NULL, NULL, NULL, NULL, '50000', '2023-01-12 15:32:33', 'Tunai', '2', '202.80.218.52', NULL, NULL, NULL, NULL, NULL),
('325dcd008b5b048cc61e048806678ec1', '23010005', '2023-01-12 15:29:13', '2023-01-12 16:29:00', 'Kamis', '2023-01-12', 'Aurelio', 'Aurelio', 'Semarang', '089676760663', '2023-01-12 15:29:13', '160.000', '19', '21', 'Merah', NULL, NULL, 'false', NULL, NULL, 'false', NULL, 'false', NULL, NULL, NULL, NULL, NULL, NULL, 'Tunai', '2', '202.80.218.52', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `orderan_jam`
--

CREATE TABLE `orderan_jam` (
  `kd` varchar(50) NOT NULL,
  `book_kd` varchar(50) DEFAULT NULL,
  `book_kode` varchar(100) DEFAULT NULL,
  `warna_posisi` varchar(100) DEFAULT NULL,
  `hari_pakai` varchar(100) DEFAULT NULL,
  `tgl_pakai` date DEFAULT NULL,
  `postdate` datetime DEFAULT NULL,
  `tempat_jam` varchar(2) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `orderan_jam`
--

INSERT INTO `orderan_jam` (`kd`, `book_kd`, `book_kode`, `warna_posisi`, `hari_pakai`, `tgl_pakai`, `postdate`, `tempat_jam`) VALUES
('d3d239696051d3d6b09bd4b0ab5048df17461', 'd3d239696051d3d6b09bd4b0ab5048df', '22100040', 'Hijau', 'Selasa', '2022-11-01', '2022-10-31 17:10:46', '18'),
('d3d239696051d3d6b09bd4b0ab5048df17462', 'd3d239696051d3d6b09bd4b0ab5048df', '22100040', 'Hijau', 'Selasa', '2022-11-01', '2022-10-31 17:10:46', '19'),
('168e3899d5c1f5bdd74c19abf3698f0518371', '168e3899d5c1f5bdd74c19abf3698f05', '22100041', 'Hijau', 'Senin', '2022-10-31', '2022-10-31 18:27:37', '23'),
('387d6df87d7ac1afac347264bd7dd4d818521', '387d6df87d7ac1afac347264bd7dd4d8', '22100042', 'Hijau', 'Senin', '2022-10-31', '2022-10-31 18:28:52', '23'),
('3b83c7d45717ff3edc9fe62a64e4568f13531', '3b83c7d45717ff3edc9fe62a64e4568f', '22110001', 'Hijau', 'Selasa', '2022-11-01', '2022-11-01 13:55:53', '11'),
('3b83c7d45717ff3edc9fe62a64e4568f13532', '3b83c7d45717ff3edc9fe62a64e4568f', '22110001', 'Hijau', 'Selasa', '2022-11-01', '2022-11-01 13:55:53', '12'),
('5cccf932ca402acb0a2e940a7fb7f04b21041', '5cccf932ca402acb0a2e940a7fb7f04b', '22110002', 'Merah', 'Selasa', '2022-11-01', '2022-11-01 21:29:04', '16'),
('b37a9f8f6f7e7688acff13b4373f7e4317231', 'b37a9f8f6f7e7688acff13b4373f7e43', '22110003', 'Hijau', 'Jumat', '2022-11-04', '2022-11-02 17:01:23', '18'),
('b37a9f8f6f7e7688acff13b4373f7e4317232', 'b37a9f8f6f7e7688acff13b4373f7e43', '22110003', 'Hijau', 'Jumat', '2022-11-04', '2022-11-02 17:01:23', '19'),
('b79020be40bd22e05c12e90a93fae9b620151', 'b79020be40bd22e05c12e90a93fae9b6', '22110004', 'Merah', 'Jumat', '2022-11-04', '2022-11-02 20:51:15', '19'),
('3bd788f4e13c4fdb483228ee0565d24417491', '3bd788f4e13c4fdb483228ee0565d244', '22110005', 'Merah', 'Jumat', '2022-11-04', '2022-11-03 17:24:49', '17'),
('fc48cf7d7025d2a0050617ebcae05a3d14371', 'fc48cf7d7025d2a0050617ebcae05a3d', '22110006', 'Hijau', 'Jumat', '2022-11-04', '2022-11-04 14:33:37', '16'),
('fc48cf7d7025d2a0050617ebcae05a3d14372', 'fc48cf7d7025d2a0050617ebcae05a3d', '22110006', 'Hijau', 'Jumat', '2022-11-04', '2022-11-04 14:33:37', '17'),
('a87c7c19072731be84e0dd4381d9f0c414021', 'a87c7c19072731be84e0dd4381d9f0c4', '22110007', 'Merah', 'Jumat', '2022-11-04', '2022-11-04 14:37:02', '15'),
('a87c7c19072731be84e0dd4381d9f0c414022', 'a87c7c19072731be84e0dd4381d9f0c4', '22110007', 'Merah', 'Jumat', '2022-11-04', '2022-11-04 14:37:02', '16'),
('4b55072ae0eae841d59b5a1b1780867514011', '4b55072ae0eae841d59b5a1b17808675', '22110008', 'Merah', 'Jumat', '2022-11-04', '2022-11-04 14:40:01', '20'),
('4b55072ae0eae841d59b5a1b1780867514012', '4b55072ae0eae841d59b5a1b17808675', '22110008', 'Merah', 'Jumat', '2022-11-04', '2022-11-04 14:40:01', '21'),
('4724068562f9b5668b77b86fe76e982f14371', '4724068562f9b5668b77b86fe76e982f', '22110009', 'Hijau', 'Jumat', '2022-11-04', '2022-11-04 14:43:37', '20'),
('4724068562f9b5668b77b86fe76e982f14372', '4724068562f9b5668b77b86fe76e982f', '22110009', 'Hijau', 'Jumat', '2022-11-04', '2022-11-04 14:43:37', '21'),
('033c462340c93e7cc10236c91517927914291', '033c462340c93e7cc10236c915179279', '22110010', 'Hijau', 'Jumat', '2022-11-04', '2022-11-04 14:45:29', '22'),
('033c462340c93e7cc10236c91517927914292', '033c462340c93e7cc10236c915179279', '22110010', 'Hijau', 'Jumat', '2022-11-04', '2022-11-04 14:45:29', '23'),
('a3499c98f670010f2f2398c56431869814361', 'a3499c98f670010f2f2398c564318698', '22110011', 'Hijau', 'Minggu', '2022-11-06', '2022-11-04 14:47:36', '13'),
('a3499c98f670010f2f2398c56431869814362', 'a3499c98f670010f2f2398c564318698', '22110011', 'Hijau', 'Minggu', '2022-11-06', '2022-11-04 14:47:36', '14'),
('24691fba2026e256ed83b6112cbf24a214291', '24691fba2026e256ed83b6112cbf24a2', '22110012', 'Hijau', 'Sabtu', '2022-11-05', '2022-11-04 14:51:29', '13'),
('24691fba2026e256ed83b6112cbf24a214292', '24691fba2026e256ed83b6112cbf24a2', '22110012', 'Hijau', 'Sabtu', '2022-11-05', '2022-11-04 14:51:29', '14'),
('4b77c909de45af3ad02655685127ba9514101', '4b77c909de45af3ad02655685127ba95', '22110013', 'Hijau', 'Sabtu', '2022-11-05', '2022-11-04 14:53:10', '20'),
('4b77c909de45af3ad02655685127ba9514102', '4b77c909de45af3ad02655685127ba95', '22110013', 'Hijau', 'Sabtu', '2022-11-05', '2022-11-04 14:53:10', '21'),
('8492dc67e010b9a75dfa3b39a4a4759017551', '8492dc67e010b9a75dfa3b39a4a47590', '22110014', 'Hijau', 'Sabtu', '2022-11-05', '2022-11-05 17:10:55', '7'),
('3c0386e48976af9507a8bfc23f31e57d17271', '3c0386e48976af9507a8bfc23f31e57d', '22110015', 'Hijau', 'Sabtu', '2022-11-05', '2022-11-05 17:40:27', '17'),
('5e9f94271f7c46c4b998f818a3823bab17411', '5e9f94271f7c46c4b998f818a3823bab', '22110016', 'Hijau', 'Sabtu', '2022-11-05', '2022-11-05 17:41:41', '17'),
('685d9015979c3a1bdba0820b903208d517261', '685d9015979c3a1bdba0820b903208d5', '22110017', 'Merah', 'Sabtu', '2022-11-05', '2022-11-05 17:43:26', '17'),
('5bde16a25d4f6da4b3bb08fc5ae7af4d17341', '5bde16a25d4f6da4b3bb08fc5ae7af4d', '22110018', 'Merah', 'Sabtu', '2022-11-05', '2022-11-05 17:57:34', '21'),
('5bde16a25d4f6da4b3bb08fc5ae7af4d17342', '5bde16a25d4f6da4b3bb08fc5ae7af4d', '22110018', 'Merah', 'Sabtu', '2022-11-05', '2022-11-05 17:57:34', '22'),
('77473e5602d541763ae947f68026d6f522181', '77473e5602d541763ae947f68026d6f5', '22110019', 'Hijau', 'Sabtu', '2022-11-05', '2022-11-05 22:02:18', '22'),
('7c726696c617fc27d10baeb170a0660717171', '7c726696c617fc27d10baeb170a06607', '22110020', 'Hijau', 'Senin', '2022-11-07', '2022-11-07 17:07:17', '15'),
('7c726696c617fc27d10baeb170a0660717172', '7c726696c617fc27d10baeb170a06607', '22110020', 'Hijau', 'Senin', '2022-11-07', '2022-11-07 17:07:17', '16'),
('49468ad1e459b024ec01242a6b54d2d717401', '49468ad1e459b024ec01242a6b54d2d7', '22110021', 'Merah', 'Senin', '2022-11-07', '2022-11-07 17:17:40', '16'),
('c274d618652c862a21c188f653cbe78d17511', 'c274d618652c862a21c188f653cbe78d', '22110022', 'Merah', 'Senin', '2022-11-07', '2022-11-07 17:18:51', '17'),
('20dad599d84f285f3927bec10279d8d217491', '20dad599d84f285f3927bec10279d8d2', '22110023', 'Hijau', 'Senin', '2022-11-07', '2022-11-07 17:19:49', '16'),
('20dad599d84f285f3927bec10279d8d217492', '20dad599d84f285f3927bec10279d8d2', '22110023', 'Hijau', 'Senin', '2022-11-07', '2022-11-07 17:19:49', '17'),
('bc4f560fdea10481d5418ff3b17a73df17531', 'bc4f560fdea10481d5418ff3b17a73df', '22110024', 'Hijau', 'Senin', '2022-11-07', '2022-11-07 17:20:53', '16'),
('bc4f560fdea10481d5418ff3b17a73df17532', 'bc4f560fdea10481d5418ff3b17a73df', '22110024', 'Hijau', 'Senin', '2022-11-07', '2022-11-07 17:20:53', '17'),
('242f888da1b55a67fb956afc1b87787417381', '242f888da1b55a67fb956afc1b877874', '22110025', 'Hijau', 'Senin', '2022-11-07', '2022-11-07 17:23:38', '18'),
('242f888da1b55a67fb956afc1b87787417382', '242f888da1b55a67fb956afc1b877874', '22110025', 'Hijau', 'Senin', '2022-11-07', '2022-11-07 17:23:38', '19'),
('8550b226f1671fa0b918176d9b5d083a17401', '8550b226f1671fa0b918176d9b5d083a', '22110026', 'Merah', 'Senin', '2022-11-07', '2022-11-07 17:25:40', '18'),
('67de860d5ba97dde460cb158c683e8c517471', '67de860d5ba97dde460cb158c683e8c5', '22110027', 'Hijau', 'Senin', '2022-11-07', '2022-11-07 17:26:47', '20'),
('85272cde9ff1f33c4700e5cbabae8be617181', '85272cde9ff1f33c4700e5cbabae8be6', '22110028', 'Hijau', 'Senin', '2022-11-07', '2022-11-07 17:29:18', '21'),
('766cf33e003884d4d23bfbf64887d33f17251', '766cf33e003884d4d23bfbf64887d33f', '22110029', 'Hijau', 'Senin', '2022-11-07', '2022-11-07 17:30:25', '22'),
('766cf33e003884d4d23bfbf64887d33f17252', '766cf33e003884d4d23bfbf64887d33f', '22110029', 'Hijau', 'Senin', '2022-11-07', '2022-11-07 17:30:25', '23'),
('20ab28bd41a71d1c6887f960aa0f6ecd17171', '20ab28bd41a71d1c6887f960aa0f6ecd', '22110030', 'Merah', 'Senin', '2022-11-07', '2022-11-07 17:33:17', '19'),
('20ab28bd41a71d1c6887f960aa0f6ecd17172', '20ab28bd41a71d1c6887f960aa0f6ecd', '22110030', 'Merah', 'Senin', '2022-11-07', '2022-11-07 17:33:17', '20'),
('baf0266e12caabceacceb15439f3ae7c17291', 'baf0266e12caabceacceb15439f3ae7c', '22110031', 'Merah', 'Senin', '2022-11-07', '2022-11-07 17:38:29', '21'),
('f756894c228fe0c1e4986404da9d5bf917141', 'f756894c228fe0c1e4986404da9d5bf9', '22110032', 'Merah', 'Senin', '2022-11-07', '2022-11-07 17:39:14', '22'),
('7b9f042ad9f7ceb9bf7ea922c596d83f17271', '7b9f042ad9f7ceb9bf7ea922c596d83f', '22110033', 'Hijau', 'Selasa', '2022-11-08', '2022-11-07 17:40:27', '16'),
('1d8dac03f5bdc416eaff09d16127f36417591', '1d8dac03f5bdc416eaff09d16127f364', '22110034', 'Merah', 'Selasa', '2022-11-08', '2022-11-07 17:44:59', '16'),
('66410548ea22255c62fd54293bba578d17241', '66410548ea22255c62fd54293bba578d', '22110035', 'Merah', 'Selasa', '2022-11-08', '2022-11-07 17:45:24', '16'),
('66410548ea22255c62fd54293bba578d17242', '66410548ea22255c62fd54293bba578d', '22110035', 'Merah', 'Selasa', '2022-11-08', '2022-11-07 17:45:24', '17'),
('d223fa2001dffde19212c33008422ce817381', 'd223fa2001dffde19212c33008422ce8', '22110036', 'Hijau', 'Selasa', '2022-11-08', '2022-11-07 17:47:38', '19'),
('d223fa2001dffde19212c33008422ce817382', 'd223fa2001dffde19212c33008422ce8', '22110036', 'Hijau', 'Selasa', '2022-11-08', '2022-11-07 17:47:38', '20'),
('fcf29da862812e941d6d572d36bedba817201', 'fcf29da862812e941d6d572d36bedba8', '22110037', 'Merah', 'Selasa', '2022-11-08', '2022-11-07 17:49:20', '19'),
('fcf29da862812e941d6d572d36bedba817202', 'fcf29da862812e941d6d572d36bedba8', '22110037', 'Merah', 'Selasa', '2022-11-08', '2022-11-07 17:49:20', '20'),
('7235ae1f86e4ab06bba0cad87d78d4f217391', '7235ae1f86e4ab06bba0cad87d78d4f2', '22110038', 'Hijau', 'Selasa', '2022-11-08', '2022-11-07 17:51:39', '22'),
('7235ae1f86e4ab06bba0cad87d78d4f217392', '7235ae1f86e4ab06bba0cad87d78d4f2', '22110038', 'Hijau', 'Selasa', '2022-11-08', '2022-11-07 17:51:39', '23'),
('57e0546f9cb369c85de374e2e4ca37ae17551', '57e0546f9cb369c85de374e2e4ca37ae', '22110039', 'Hijau', 'Rabu', '2022-11-09', '2022-11-07 17:53:55', '19'),
('57e0546f9cb369c85de374e2e4ca37ae17552', '57e0546f9cb369c85de374e2e4ca37ae', '22110039', 'Hijau', 'Rabu', '2022-11-09', '2022-11-07 17:53:55', '20'),
('6241fc7ec4009507880f47c1ca0f2e7c17171', '6241fc7ec4009507880f47c1ca0f2e7c', '22110040', 'Merah', 'Rabu', '2022-11-09', '2022-11-07 17:55:17', '19'),
('6241fc7ec4009507880f47c1ca0f2e7c17172', '6241fc7ec4009507880f47c1ca0f2e7c', '22110040', 'Merah', 'Rabu', '2022-11-09', '2022-11-07 17:55:17', '20'),
('d877c316c97932287d18c2a3f536bf9917201', 'd877c316c97932287d18c2a3f536bf99', '22110041', 'Hijau', 'Rabu', '2022-11-09', '2022-11-07 17:58:20', '21'),
('ebf5b076dd826e895d919d44205bd3c918051', 'ebf5b076dd826e895d919d44205bd3c9', '22110042', 'Merah', 'Rabu', '2022-11-09', '2022-11-07 18:00:05', '21'),
('ebf5b076dd826e895d919d44205bd3c918052', 'ebf5b076dd826e895d919d44205bd3c9', '22110042', 'Merah', 'Rabu', '2022-11-09', '2022-11-07 18:00:05', '22'),
('7724c68e69142aad1b1751802dc731cb18411', '7724c68e69142aad1b1751802dc731cb', '22110043', 'Hijau', 'Kamis', '2022-11-10', '2022-11-07 18:15:41', '19'),
('bb77bfbceb4428762cae3d159dfd975a18131', 'bb77bfbceb4428762cae3d159dfd975a', '22110044', 'Hijau', 'Kamis', '2022-11-10', '2022-11-07 18:18:13', '20'),
('a915c17627587b15c3e6d8f2ca9fafb918241', 'a915c17627587b15c3e6d8f2ca9fafb9', '22110045', 'Hijau', 'Kamis', '2022-11-10', '2022-11-07 18:20:24', '21'),
('b5098b12959e731109214f6ac3f6378818551', 'b5098b12959e731109214f6ac3f63788', '22110046', 'Merah', 'Kamis', '2022-11-10', '2022-11-07 18:22:55', '20'),
('b5098b12959e731109214f6ac3f6378818552', 'b5098b12959e731109214f6ac3f63788', '22110046', 'Merah', 'Kamis', '2022-11-10', '2022-11-07 18:22:55', '21'),
('51ad36f0edad3d29d503673ef3a27eb518421', '51ad36f0edad3d29d503673ef3a27eb5', '22110047', 'Hijau', 'Jumat', '2022-11-11', '2022-11-07 18:25:42', '15'),
('51ad36f0edad3d29d503673ef3a27eb518422', '51ad36f0edad3d29d503673ef3a27eb5', '22110047', 'Hijau', 'Jumat', '2022-11-11', '2022-11-07 18:25:42', '16'),
('6580d47b9b8d9fc17dbb8d4009d54e9418591', '6580d47b9b8d9fc17dbb8d4009d54e94', '22110048', 'Hijau', 'Sabtu', '2022-11-12', '2022-11-07 18:28:59', '19'),
('ddd6fbe450211e2343e620649e5fbda718421', 'ddd6fbe450211e2343e620649e5fbda7', '22110049', 'Hijau', 'Sabtu', '2022-11-12', '2022-11-07 18:30:42', '20'),
('2ee8f98c876e4286785766e2579e222318081', '2ee8f98c876e4286785766e2579e2223', '22110050', 'Hijau', 'Sabtu', '2022-11-12', '2022-11-07 18:35:08', '21'),
('2ee8f98c876e4286785766e2579e222318082', '2ee8f98c876e4286785766e2579e2223', '22110050', 'Hijau', 'Sabtu', '2022-11-12', '2022-11-07 18:35:08', '22'),
('22d6a0eee7ff3c1a0cc65fd292f3214918261', '22d6a0eee7ff3c1a0cc65fd292f32149', '22110051', 'Merah', 'Sabtu', '2022-11-12', '2022-11-07 18:37:26', '20'),
('22d6a0eee7ff3c1a0cc65fd292f3214918262', '22d6a0eee7ff3c1a0cc65fd292f32149', '22110051', 'Merah', 'Sabtu', '2022-11-12', '2022-11-07 18:37:26', '21'),
('2a51dd70c8458cf7b665c5e7f8a3235921281', '2a51dd70c8458cf7b665c5e7f8a32359', '22110052', 'Merah', 'Senin', '2022-11-07', '2022-11-07 21:08:28', '21'),
('aa3ad3733f306a56f63e0e2e7d199ce321221', 'aa3ad3733f306a56f63e0e2e7d199ce3', '22110053', 'Merah', 'Senin', '2022-11-07', '2022-11-07 21:09:22', '21'),
('0ba6a70d20f250b4bcb2220c1ed3af1709191', '0ba6a70d20f250b4bcb2220c1ed3af17', '22110054', 'Merah', 'Selasa', '2022-11-08', '2022-11-08 09:32:19', '16'),
('0ba6a70d20f250b4bcb2220c1ed3af1709192', '0ba6a70d20f250b4bcb2220c1ed3af17', '22110054', 'Merah', 'Selasa', '2022-11-08', '2022-11-08 09:32:19', '17'),
('c5b69e058845b942bf7675e52d88898d16361', 'c5b69e058845b942bf7675e52d88898d', '22110055', 'Hijau', 'Selasa', '2022-11-08', '2022-11-08 16:46:36', '21'),
('45f6b1fecfb782e2518b3d970003178a16111', '45f6b1fecfb782e2518b3d970003178a', '22110056', 'Hijau', 'Rabu', '2022-11-09', '2022-11-08 16:48:11', '17'),
('45f6b1fecfb782e2518b3d970003178a16112', '45f6b1fecfb782e2518b3d970003178a', '22110056', 'Hijau', 'Rabu', '2022-11-09', '2022-11-08 16:48:11', '18'),
('ff116ada7979d6612621375af7a2bb2717091', 'ff116ada7979d6612621375af7a2bb27', '22110057', 'Hijau', 'Selasa', '2022-11-08', '2022-11-08 17:02:09', '18'),
('7c292ac6704844737b4639b95291bc4717551', '7c292ac6704844737b4639b95291bc47', '22110058', 'Merah', 'Selasa', '2022-11-08', '2022-11-08 17:37:55', '18'),
('4dad06c1e523fc387129514cea1ac2e917361', '4dad06c1e523fc387129514cea1ac2e9', '22110059', 'Merah', 'Jumat', '2022-11-11', '2022-11-08 17:39:36', '19'),
('4dad06c1e523fc387129514cea1ac2e917362', '4dad06c1e523fc387129514cea1ac2e9', '22110059', 'Merah', 'Jumat', '2022-11-11', '2022-11-08 17:39:36', '20'),
('80a566071e57a5848035caa8f7c6b7a717231', '80a566071e57a5848035caa8f7c6b7a7', '22110060', 'Hijau', 'Jumat', '2022-11-11', '2022-11-08 17:40:23', '20'),
('80a566071e57a5848035caa8f7c6b7a717232', '80a566071e57a5848035caa8f7c6b7a7', '22110060', 'Hijau', 'Jumat', '2022-11-11', '2022-11-08 17:40:23', '21'),
('0ff0cc592e1be3d3c7791504633842a117331', '0ff0cc592e1be3d3c7791504633842a1', '22110061', 'Hijau', 'Selasa', '2022-11-08', '2022-11-08 17:41:33', '18'),
('5ecffe0132a16f5e24061379327f870717101', '5ecffe0132a16f5e24061379327f8707', '22110062', 'Merah', 'Selasa', '2022-11-08', '2022-11-08 17:42:10', '18'),
('518b4257b0f283b5a755d59cb150a3bf16491', '518b4257b0f283b5a755d59cb150a3bf', '22110063', 'Hijau', 'Rabu', '2022-11-09', '2022-11-09 16:02:49', '11'),
('43d0fd8e5b83cebad3ca32ffce3ca2af16031', '43d0fd8e5b83cebad3ca32ffce3ca2af', '22110064', 'Hijau', 'Rabu', '2022-11-09', '2022-11-09 16:28:03', '16'),
('bc19ec917c4a3feae2d358f0eeda0dfe16051', 'bc19ec917c4a3feae2d358f0eeda0dfe', '22110065', 'Merah', 'Rabu', '2022-11-09', '2022-11-09 16:30:05', '17'),
('bc19ec917c4a3feae2d358f0eeda0dfe16052', 'bc19ec917c4a3feae2d358f0eeda0dfe', '22110065', 'Merah', 'Rabu', '2022-11-09', '2022-11-09 16:30:05', '18'),
('4ba36e534fc713b1347c2a621c4f676416541', '4ba36e534fc713b1347c2a621c4f6764', '22110066', 'Hijau', 'Jumat', '2022-11-11', '2022-11-09 16:32:54', '19'),
('f3199f1bf8ad9ec44d45ee8ed78d018d16201', 'f3199f1bf8ad9ec44d45ee8ed78d018d', '22110067', 'Hijau', 'Kamis', '2022-11-10', '2022-11-09 16:38:20', '21'),
('4675e0751f6506e87370c944ca126a0816251', '4675e0751f6506e87370c944ca126a08', '22110068', 'Hijau', 'Sabtu', '2022-11-12', '2022-11-09 16:40:25', '20'),
('1e2b13db100eae0a036c9198656ff3c917371', '1e2b13db100eae0a036c9198656ff3c9', '22110069', 'Merah', 'Kamis', '2022-11-10', '2022-11-10 17:30:37', '18'),
('1e2b13db100eae0a036c9198656ff3c917372', '1e2b13db100eae0a036c9198656ff3c9', '22110069', 'Merah', 'Kamis', '2022-11-10', '2022-11-10 17:30:37', '19'),
('944b5d113cac9b564c4fa900b68131b619351', '944b5d113cac9b564c4fa900b68131b6', '23010001', 'Hijau', 'Minggu', '2023-01-08', '2023-01-07 19:38:35', '10'),
('944b5d113cac9b564c4fa900b68131b619352', '944b5d113cac9b564c4fa900b68131b6', '23010001', 'Hijau', 'Minggu', '2023-01-08', '2023-01-07 19:38:35', '11'),
('d07e7f373db01fb6eb875da031b80eca19351', 'd07e7f373db01fb6eb875da031b80eca', '23010002', 'Hijau', 'Minggu', '2023-01-08', '2023-01-07 19:38:35', '10'),
('d07e7f373db01fb6eb875da031b80eca19352', 'd07e7f373db01fb6eb875da031b80eca', '23010002', 'Hijau', 'Minggu', '2023-01-08', '2023-01-07 19:38:35', '11'),
('ed43d75dced775f0370a00934f1b077c15251', 'ed43d75dced775f0370a00934f1b077c', '23010003', 'Merah', 'Kamis', '2023-01-12', '2023-01-12 15:28:25', '19'),
('ed43d75dced775f0370a00934f1b077c15252', 'ed43d75dced775f0370a00934f1b077c', '23010003', 'Merah', 'Kamis', '2023-01-12', '2023-01-12 15:28:25', '20'),
('211c09ae09cbe9926a7bfd929cb1891515571', '211c09ae09cbe9926a7bfd929cb18915', '23010004', 'Merah', 'Kamis', '2023-01-12', '2023-01-12 15:28:57', '19'),
('211c09ae09cbe9926a7bfd929cb1891515572', '211c09ae09cbe9926a7bfd929cb18915', '23010004', 'Merah', 'Kamis', '2023-01-12', '2023-01-12 15:28:57', '20'),
('325dcd008b5b048cc61e048806678ec115131', '325dcd008b5b048cc61e048806678ec1', '23010005', 'Merah', 'Kamis', '2023-01-12', '2023-01-12 15:29:13', '19'),
('325dcd008b5b048cc61e048806678ec115132', '325dcd008b5b048cc61e048806678ec1', '23010005', 'Merah', 'Kamis', '2023-01-12', '2023-01-12 15:29:13', '20');

-- --------------------------------------------------------

--
-- Table structure for table `saldo`
--

CREATE TABLE `saldo` (
  `kd` varchar(50) NOT NULL,
  `per_tgl` date DEFAULT NULL,
  `perolehan` varchar(15) DEFAULT NULL,
  `pengeluaran` varchar(15) DEFAULT NULL,
  `saldo` varchar(15) DEFAULT NULL,
  `user_kd` varchar(50) DEFAULT NULL,
  `user_kode` varchar(100) DEFAULT NULL,
  `user_nama` varchar(100) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL,
  `dp` varchar(15) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `saldo`
--

INSERT INTO `saldo` (`kd`, `per_tgl`, `perolehan`, `pengeluaran`, `saldo`, `user_kd`, `user_kode`, `user_nama`, `postdate`, `dp`) VALUES
('e8bb853c9121b286ae16987bcb997ff9', '2023-01-01', '', '', '0', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', '2023-01-28 08:50:38', NULL),
('cd134aa029084eb33f8ea379f30a3df0', '2023-01-02', '', '', '0', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', '2023-01-28 08:50:38', NULL),
('9bcdd5d065bfea305a17e202e870c70d', '2023-01-03', '', '', '0', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', '2023-01-28 08:50:38', NULL),
('dce96261b85453573d0273e5207893f7', '2023-01-04', '', '', '0', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', '2023-01-28 08:50:38', NULL),
('803d4125740da446aadb4477491c3e33', '2023-01-05', '', '', '0', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', '2023-01-28 08:50:38', NULL),
('19edb4b70ee1a956699b9572eeeeed8f', '2023-01-06', '895500', '', '895500', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', '2023-01-28 08:50:38', NULL),
('30fa058f61ccc37c6464f0784c5def39', '2023-01-07', '', '', '0', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', '2023-01-28 08:50:38', NULL),
('45a75669b9a69e170d69120cfb8bf8d2', '2023-01-08', '', '', '0', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', '2023-01-28 08:50:38', NULL),
('b14070a60d39d33df429261933a44f98', '2023-01-09', '330000', '', '330000', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', '2023-01-28 08:50:38', NULL),
('730eaece2035daa1eed1970f182a5ffc', '2023-01-10', '307000', '', '307000', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', '2023-01-28 08:50:38', NULL),
('1719585b673534d0a5ee2efd3d1a69de', '2023-01-11', '', '', '0', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', '2023-01-28 08:50:38', NULL),
('edca5dc9fe7b2ac77f46bc0337a073cf', '2023-01-12', '170000', '17000', '153000', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', '2023-01-28 08:50:38', NULL),
('e51f0c7d63442f0f6bb01fa04a5fb8c1', '2023-01-13', '', '', '0', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', '2023-01-28 08:50:38', NULL),
('cb2079a1605e53b264f92cb49c7fbfa9', '2023-01-14', '659500', '180000', '479500', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', '2023-01-28 08:50:38', NULL),
('8a7d8ff12dce0861bb96c6cb51e4d2a4', '2023-01-15', '235000', '', '235000', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', '2023-01-28 08:50:38', NULL),
('d6514e94323ff53579fdf84af897b992', '2023-01-16', '', '', '0', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', '2023-01-28 08:50:38', NULL),
('1c649e198676a6bfea9a579469ce62cf', '2023-01-17', '305000', '80000', '225000', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', '2023-01-28 08:50:38', NULL),
('e818cae790c7a81f0459965377c2c95c', '2023-01-18', '', '', '0', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', '2023-01-28 08:50:38', NULL),
('7519e16595e0f51559ba848659c75571', '2023-01-19', '280000', '', '280000', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', '2023-01-28 08:50:38', NULL),
('ebff0da38c39162815b4f40985742a99', '2023-02-02', '', '', '0', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', '2023-02-05 13:15:41', NULL),
('c3d6940c58f107d3c550814f524342d6', '2023-01-21', '', '', '0', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', '2023-01-28 08:50:38', NULL),
('7a783382ab7fd5c1305599e648e4838f', '2023-01-22', '', '', '0', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', '2023-01-28 08:50:38', NULL),
('a17c0278d81b858fc24fbad18da590ad', '2023-01-23', '', '', '0', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', '2023-01-28 08:50:38', NULL),
('517be4bddbc522ef586a6d4c5c03a705', '2023-01-24', '', '', '0', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', '2023-01-28 08:50:38', NULL),
('ffb89825b9dfb63954b0deb17b25a632', '2023-01-25', '', '', '0', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', '2023-01-28 08:50:38', NULL),
('32080cb0544379580e165bf464b9414c', '2023-02-01', '203000', '', '203000', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', '2023-02-05 13:15:41', NULL),
('335babe3ad9b263e6e2abf9ffcd33337', '2023-01-27', '', '', '0', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', '2023-01-28 08:50:38', NULL),
('35f2afff8bcb5b1292a3e4a9acbc0945', '2023-01-28', '', '', '0', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', '2023-01-28 08:50:38', NULL),
('dcf6f217391742caed9fa81abe91a84e', '2023-01-29', '', '', '0', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', '2023-01-28 08:50:38', NULL),
('9ff9b538594beeb3edb05395761a11fd', '2023-01-30', '', '', '0', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', '2023-01-28 08:50:38', NULL),
('5165148811a1442680b58a464798f700', '2023-01-31', '', '', '0', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', '2023-01-28 08:50:38', NULL),
('f1d2ed0eada897df8b1216c6f80228e3', '0000-00-00', '0', '0', '0', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', '2023-01-26 13:09:38', NULL),
('eef6898e3d9cc56f250b89fb826c13aa', '0000-00-00', '0', '0', '0', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', '2023-01-26 13:09:38', NULL),
('0fbd2b69be7cc029aa8a233654829ead', '0000-00-00', '0', '0', '0', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', '2023-01-26 13:09:38', NULL),
('f311bdee4f96a04dd2e5aec51c1ec985', '0000-00-00', '0', '0', '0', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', '2023-01-26 13:09:38', NULL),
('1279ba323fe05ca16a574109ea802045', '0000-00-00', '0', '0', '0', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', '2023-01-26 13:09:38', NULL),
('b6e6d2b3e1d1779f2c9f6e469185efd7', '0000-00-00', '0', '0', '0', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', '2023-01-26 13:09:38', NULL),
('b7b5d51ea88998e4a40fed18d0b3741a', '0000-00-00', '0', '0', '0', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', '2023-01-26 13:09:38', NULL),
('af31dd853da9ffdf304698530ccb574d', '0000-00-00', '0', '0', '0', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', '2023-01-26 13:09:38', NULL),
('bd94259fd5fdb487d3022bfbf5777666', '2023-01-20', '208500', '', '208500', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', '2023-01-27 22:24:54', NULL),
('6f711ae4592e7016f72994ae3be71daf', '2023-01-26', '5000', '', '5000', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', '2023-01-27 22:24:54', NULL),
('6728571e7a8fcf5cbf0d83176ea47b0b', '2023-02-03', '', '', '0', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', '2023-02-05 13:15:41', NULL),
('d82d6bf8ee032ecc341b4f7260420b7b', '2023-02-04', '120000', '', '120000', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', '2023-02-05 13:15:41', NULL),
('4ccf5a767086efe2317ffb728a6817da', '2023-02-06', '', '', '0', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', '2023-02-05 13:15:41', NULL),
('3efe7a9dacf7ab017a5585e11afd61a6', '2023-02-07', '', '', '0', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', '2023-02-05 13:15:41', NULL),
('f38a31124670287ab0913e2d6fcab649', '2023-02-08', '', '', '0', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', '2023-02-05 13:15:41', NULL),
('91eee34f41d614506536711fbcd0da8d', '2023-02-09', '', '', '0', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', '2023-02-05 13:15:41', NULL),
('77bfaa0545124800e5663f0b67bb6b03', '2023-02-10', '', '', '0', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', '2023-02-05 13:15:41', NULL),
('60ad1d5afe8903df9e14400221eb490a', '2023-02-11', '', '', '0', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', '2023-02-05 13:15:41', NULL),
('8f48c75ca30ae670540b2899e519e7be', '2023-02-12', '', '', '0', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', '2023-02-05 13:15:41', NULL),
('97f3a4d650e8cb3c25505378a7a8ad2c', '2023-02-13', '', '', '0', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', '2023-02-05 13:15:41', NULL),
('a08b792940b774455cec336efcb3d1de', '2023-02-14', '', '', '0', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', '2023-02-05 13:15:41', NULL),
('a4f3c24ec465af14979c219ceb609050', '2023-02-15', '', '', '0', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', '2023-02-05 13:15:41', NULL),
('5b2b0f484a24ab5e3b3af3d94f106643', '2023-02-16', '', '', '0', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', '2023-02-05 13:15:41', NULL),
('187de3f39bd2acab60a0707a83223130', '2023-02-17', '', '', '0', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', '2023-02-05 13:15:41', NULL),
('f56b3131f1e1a6ce2c20d5a46913b521', '2023-02-18', '', '', '0', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', '2023-02-05 13:15:41', NULL),
('d24984a33e4807b60f61034e02d53fdc', '2023-02-19', '', '', '0', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', '2023-02-05 13:15:41', NULL),
('53f4ed54419ebf57b1772f44541e7148', '2023-02-20', '', '', '0', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', '2023-02-05 13:15:41', NULL),
('0d159871306553ca4f3679f787b5f68f', '2023-02-21', '', '', '0', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', '2023-02-05 13:15:41', NULL),
('afd523d38e49c9737c4f67bfd78c3117', '2023-02-22', '', '', '0', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', '2023-02-05 13:15:41', NULL),
('052afe33e85d2ca3c6ed8432dde05172', '2023-02-23', '', '', '0', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', '2023-02-05 13:15:41', NULL),
('f65989c77c6485cef3a6a0be9fbb2ac7', '2023-02-24', '', '', '0', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', '2023-02-05 13:15:41', NULL),
('95c8f408aeba917250fef3a02c5e0aac', '2023-02-25', '', '', '0', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', '2023-02-05 13:15:41', NULL),
('3ed51aebf72811b67662f2f5b7e46456', '2023-02-26', '', '', '0', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', '2023-02-05 13:15:41', NULL),
('21d300caea281d9c9aed9746e5eb87fd', '2023-02-27', '', '', '0', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', '2023-02-05 13:15:41', NULL),
('90d001b4d8dfae9aba70fe38459a71c5', '2023-02-28', '', '', '0', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', '2023-02-05 13:15:41', NULL),
('5628781bb683c566a1358b37c986c93a', '0000-00-00', '0', '0', '0', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', '2023-02-01 16:06:51', NULL),
('8a837efe3ea3c165906ac909f9d87640', '0000-00-00', '0', '0', '0', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', '2023-02-04 09:13:44', NULL),
('d5e6e163620c5edd07881a11fd4da460', '0000-00-00', '0', '0', '0', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', '2023-02-04 09:13:44', NULL),
('afc842255f2bd93e838286500cd49eed', '0000-00-00', '0', '0', '0', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', '2023-02-04 09:13:44', NULL),
('2d753c71da04ef1997206636322a0a61', '0000-00-00', '0', '0', '0', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', '2023-02-05 09:07:22', NULL),
('52e16a88ecce1df4ea17dce21a82afc2', '2023-02-05', '32000', '', '32000', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', '2023-02-05 13:15:41', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tempat_tgl_libur`
--

CREATE TABLE `tempat_tgl_libur` (
  `kd` varchar(50) NOT NULL,
  `biaya_pagi` varchar(15) DEFAULT NULL,
  `biaya_sore` varchar(15) DEFAULT NULL,
  `hari` varchar(100) DEFAULT NULL,
  `tgl` date DEFAULT NULL,
  `ket` varchar(100) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tempat_tgl_libur`
--

INSERT INTO `tempat_tgl_libur` (`kd`, `biaya_pagi`, `biaya_sore`, `hari`, `tgl`, `ket`, `postdate`) VALUES
('87f6d1dce72b7c2cf979de404be9a8b6', '110000', '160000', 'Sabtu', '2023-01-14', 'Main 2 Jam Free Air Mineral Prima 600ml 10 Botol', '2023-01-14 11:13:31'),
('9d0f6d552cfee4cc7e9a46d6d45e5a9a', '110000', '140000', 'Sabtu', '2023-01-14', 'Special Price xgmringx Non Free Minum', '2023-01-14 11:13:45');

-- --------------------------------------------------------

--
-- Table structure for table `uang_keluar`
--

CREATE TABLE `uang_keluar` (
  `kd` varchar(50) NOT NULL,
  `per_tgl` date DEFAULT NULL,
  `nama` longtext DEFAULT NULL,
  `nilai` varchar(50) DEFAULT NULL,
  `postdate` datetime DEFAULT NULL,
  `user_kd` varchar(50) DEFAULT NULL,
  `user_kode` varchar(100) DEFAULT NULL,
  `user_nama` varchar(100) DEFAULT NULL,
  `postdate_cetak` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `uang_keluar`
--

INSERT INTO `uang_keluar` (`kd`, `per_tgl`, `nama`, `nilai`, `postdate`, `user_kd`, `user_kode`, `user_nama`, `postdate_cetak`) VALUES
('c6f26a358d1597d4426f687f11264c03', '2023-01-14', 'aas', '180000', '2023-01-25 14:53:28', 'ADMIN', 'ADMIN', 'ADMIN', NULL),
('5100743b0655e9bf9972ca94959d6e70', '2023-01-17', '888', '80000', '2023-01-25 14:24:53', 'ADMIN', 'ADMIN', 'ADMIN', NULL),
('2b99262e4b8373bb3a4495a42692c7fb', '2023-01-12', 'daddx', '17000', '2023-01-25 14:53:19', 'ADMIN', 'ADMIN', 'ADMIN', '2023-01-26 11:06:33');

-- --------------------------------------------------------

--
-- Table structure for table `user_log_entri`
--

CREATE TABLE `user_log_entri` (
  `kd` varchar(50) NOT NULL,
  `user_kd` varchar(50) DEFAULT NULL,
  `user_kode` varchar(100) DEFAULT NULL,
  `user_nama` varchar(100) DEFAULT NULL,
  `user_posisi` varchar(100) DEFAULT NULL,
  `user_jabatan` varchar(100) DEFAULT NULL,
  `ket` longtext DEFAULT NULL,
  `dibaca` enum('true','false') NOT NULL DEFAULT 'false',
  `dibaca_postdate` datetime DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_log_entri`
--

INSERT INTO `user_log_entri` (`kd`, `user_kd`, `user_kode`, `user_nama`, `user_posisi`, `user_jabatan`, `ket`, `dibaca`, `dibaca_postdate`, `postdate`) VALUES
('a6f08d43b0bd0af5b75b72b639e2c32f', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : Admin Web  [Administrator]', 'true', '2023-01-21 10:46:01', '2023-01-21 10:33:00'),
('9d3e679712457e5abc1630eda8a6b4e8', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [HISTORY]. Login', 'true', '2023-01-21 10:46:01', '2023-01-21 10:33:08'),
('d68e08b0bd7fa979a6f2acdeba428d4a', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [HISTORY]. History Entri', 'true', '2023-01-21 10:46:01', '2023-01-21 10:43:27'),
('20da34e2eb55be529f368aaacf131772', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [HISTORY]. History Login', 'true', '2023-01-26 11:45:09', '2023-01-21 10:43:32'),
('bad19b0b6efe2932cd9c3cdb9efb2aeb', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [HISTORY]. History Login', 'true', '2023-01-26 11:45:09', '2023-01-21 10:45:19'),
('d642f97f397fc9a5cb22c5af0f411896', '', '', '', '', '', 'MENU : FUTSAL', 'true', '2023-01-26 11:45:09', '2023-01-21 10:45:25'),
('406e32550fc93f6b54717578e19f2dbe', '', '', '', '', '', 'MENU : LOGIN ADMIN', 'true', '2023-01-26 11:45:09', '2023-01-21 10:45:28'),
('ef8388b115b971c2afbcc33f7a613d60', '', '', '', '', '', 'MENU : LOGIN ADMIN', 'true', '2023-01-26 11:45:09', '2023-01-21 10:45:53'),
('6cc71e7873e9a5ac65e477517014d471', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : Admin Web  [Administrator]', 'true', '2023-01-26 11:45:09', '2023-01-21 10:45:56'),
('304f31bc4b993a2307bcc5025bc240c6', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [HISTORY]. History Entri', 'true', '2023-01-26 11:45:09', '2023-01-21 10:46:01'),
('43865670d4b8b672616c0a8316ae9f17', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [HISTORY]. History Login', 'true', '2023-01-26 11:45:09', '2023-01-21 10:46:05'),
('ac43fd72671b9aaecf52ed4eda6c85b4', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : Admin Web  [Administrator]', 'true', '2023-01-26 11:45:09', '2023-01-21 10:46:09'),
('4f6480320905ea05f966280b90cdd94c', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : Admin Web  [Administrator]', 'true', '2023-01-26 11:45:09', '2023-01-21 10:50:06'),
('17eca833d3d58a47d7911ad7ee002792', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : Admin Web  [Administrator]', 'true', '2023-01-26 11:45:09', '2023-01-21 10:51:01'),
('bc06bf1ef217235ac7e8ceb86c0d798d', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : Admin Web  [Administrator]', 'true', '2023-01-26 11:45:09', '2023-01-21 10:51:11'),
('f817c3fc5e885853211508cd09c91307', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : Admin Web  [Administrator]', 'true', '2023-01-26 11:45:09', '2023-01-21 10:59:20'),
('06bb34e465a83cf1dcc7996874a88a59', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [SETTING] Akses User', 'true', '2023-01-26 11:45:09', '2023-01-21 10:59:25'),
('ed17bbaf98f34b3217da72950845e2cc', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [SETTING] Akses Petugas', 'true', '2023-01-26 11:45:09', '2023-01-21 11:05:02'),
('fe741e324b98b7aa6314c35043c8775d', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [SETTING] Akses Petugas', 'true', '2023-01-26 11:45:09', '2023-01-21 11:05:05'),
('32a4e2e523f36c8871fa2b67cc04aa43', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [SETTING] Akses Petugas', 'true', '2023-01-26 11:45:09', '2023-01-21 11:05:12'),
('440a15435d550fbe5637154c2d18bba7', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [SETTING] Akses Petugas', 'true', '2023-01-26 11:45:09', '2023-01-21 11:05:15'),
('b35767e703f6b92871bb69fe07e5bdd6', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [SETTING] Akses Petugas', 'true', '2023-01-26 11:45:09', '2023-01-21 11:05:20'),
('d78f0fbcc09b532531f757952f96466c', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [SETTING] Akses Petugas', 'true', '2023-01-26 11:45:09', '2023-01-21 11:05:26'),
('8b00b002b139d7cfb64b8b8a925c2df2', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [SETTING] Akses Petugas', 'true', '2023-01-26 11:45:09', '2023-01-21 11:05:32'),
('b6cd65b327c8b121e5bf73db01fb82ea', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [SETTING] Akses Petugas', 'true', '2023-01-26 11:45:09', '2023-01-21 11:05:40'),
('5aa06e9b1a3af019174aca6b740c82e5', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [SETTING] Akses Petugas', 'true', '2023-01-26 11:45:09', '2023-01-21 11:05:44'),
('7abfa48c5823a996074347192f8d86f9', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [SETTING] Akses Petugas', 'true', '2023-01-26 11:45:09', '2023-01-21 11:05:46'),
('907c5fc3cd065326c492a93a6d034ee9', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [SETTING] Akses Petugas', 'true', '2023-01-26 11:45:09', '2023-01-21 11:05:51'),
('5ed055b925e89fe44e8ca41533b055eb', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [SETTING] Akses Petugas', 'true', '2023-01-26 11:45:09', '2023-01-21 11:05:53'),
('31c369c6b8b7d434464f08ae7c4a1fef', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [SETTING] Akses Petugas', 'true', '2023-01-26 11:45:09', '2023-01-21 11:05:59'),
('ae0ba1a07fc63433f140d8d3336ed4b3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [SETTING] Akses Petugas', 'true', '2023-01-26 11:45:09', '2023-01-21 11:07:41'),
('d0b68ae029c24a2d39aba68b37239a24', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [SETTING] Akses Petugas', 'true', '2023-01-26 11:45:09', '2023-01-21 11:08:46'),
('6fb6493ad4360ba68e066b3a4cc26da1', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : Admin Web  [Administrator]', 'true', '2023-01-26 11:45:09', '2023-01-26 11:45:05'),
('d55a489a9e5da4d1b7555715b71c7e74', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [HISTORY]. History Entri', 'false', NULL, '2023-01-26 11:45:09'),
('8f5a87c2b80cfa6fde59563000b3a379', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [SETTING]. Ganti Password', 'false', NULL, '2023-01-26 11:45:14'),
('39923be50e510e68c494ad09baa47e23', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : Admin Web  [Administrator]', 'false', NULL, '2023-01-26 11:45:19'),
('624650c1ed5298630494acc176805f51', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : Admin Web  [Administrator]', 'false', NULL, '2023-01-26 11:50:18'),
('99e7d13fa310326b57f40c0678dd74ab', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [TUTUP BUKU]. Tutup Buku Final', 'false', NULL, '2023-01-26 11:50:40'),
('161e0ded7983da9e80b908c7a7dc62ff', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : TUTUP BUKU CETAK THERMAL : PENGELUARAN', 'false', NULL, '2023-01-26 11:50:40'),
('5741b24a23bb0fb3318d9e3dce90bda8', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : TUTUP BUKU CETAK THERMAL', 'false', NULL, '2023-01-26 11:50:40'),
('782eeebfbfad1e60f305eca0c64923ef', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : TUTUP BUKU CETAK THERMAL : DP', 'false', NULL, '2023-01-26 11:50:40'),
('7f6c0c1befe326ef4c994b1da34f63e8', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [TUTUP BUKU]. Per Petugas', 'false', NULL, '2023-01-26 11:50:45'),
('655c33da41ad287c2d366f6f2867ccf9', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [TUTUP BUKU]. Per Petugas', 'false', NULL, '2023-01-26 11:50:47'),
('d3735ccf9137def20bc7a9bb4175d2af', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [TUTUP BUKU]. Per Petugas', 'false', NULL, '2023-01-26 11:50:49'),
('ffb2d4abfe4e1bc3b35a8b15aa0bd10f', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [TUTUP BUKU]. Per Petugas', 'false', NULL, '2023-01-26 11:51:01'),
('15c887a3d6959389e0edbb7038673f31', '', '', '', '', '', 'MENU : LOGIN ADMIN', 'false', NULL, '2023-01-26 12:05:05'),
('61b97d329852c2cd5381252a3c4f69a1', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : Admin Web  [Administrator]', 'false', NULL, '2023-01-26 12:05:12'),
('367ce2dd38ac54c508a1c79609d91cbd', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : Admin Web  [Administrator]', 'false', NULL, '2023-01-26 12:05:19'),
('f78808ce2959f1a21b52210955e1225a', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [TUTUP BUKU]. Perolehan', 'false', NULL, '2023-01-26 12:05:49'),
('3cef43666ef111de519afa0631033715', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [TUTUP BUKU]. Tutup Buku Final', 'true', '2023-01-26 13:11:22', '2023-01-26 12:06:01'),
('6e08fa966a0964fbb6e33e6ae9dc27fc', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : TUTUP BUKU CETAK THERMAL', 'true', '2023-01-26 13:11:22', '2023-01-26 12:06:02'),
('bc087a00d55875178380e4cbfc058e86', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : TUTUP BUKU CETAK THERMAL : DP', 'true', '2023-01-26 13:11:22', '2023-01-26 12:06:02'),
('dd2fcd1ac4297c2b9c22822d8de2f0d8', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : TUTUP BUKU CETAK THERMAL : PENGELUARAN', 'true', '2023-01-26 13:11:22', '2023-01-26 12:06:02'),
('c1786ce95c148c8088a0911c241b9296', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [TUTUP BUKU]. Per Petugas', 'true', '2023-01-26 13:11:22', '2023-01-26 12:35:32'),
('764dd4b735b1600e845e22dcc8f294be', '', '', '', '', '', 'MENU : LOGIN ADMIN', 'true', '2023-01-26 13:11:22', '2023-01-26 13:02:00'),
('c60ba646b1efaadb8c8e56c13f663024', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : Admin Web  [Administrator]', 'true', '2023-01-26 13:11:22', '2023-01-26 13:02:05'),
('2e9013a18a604e60a1fe506740fc70f4', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : Admin Web  [Administrator]', 'true', '2023-01-26 13:11:22', '2023-01-26 13:02:09'),
('d56be99999a78b41c9657ed355ec348a', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : Admin Web  [Administrator]', 'true', '2023-01-26 13:11:22', '2023-01-26 13:02:10'),
('d44461bbd4cdad4db0e119cae4318bcb', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : Admin Web  [Administrator]', 'true', '2023-01-26 13:11:22', '2023-01-26 13:02:11'),
('f98f74322e1769c4ff53e05a73ef11ff', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [TUTUP BUKU]. Per Petugas', 'true', '2023-01-26 13:11:22', '2023-01-26 13:02:24'),
('27278284d5a42c98c0111814ae3c1ae6', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [TUTUP BUKU]. Per Petugas', 'true', '2023-01-26 13:11:22', '2023-01-26 13:02:27'),
('e1e261e86bff007f8171cd04c7196d94', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [TUTUP BUKU]. Per Petugas', 'true', '2023-01-26 13:11:22', '2023-01-26 13:02:28'),
('eed50cf9f583c215483ad7f3635ff2f6', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : Admin Web  [Administrator]', 'true', '2023-01-26 13:11:22', '2023-01-26 13:03:10'),
('23922485e1d9c0ecb09243ab9fa1ec4a', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : Admin Web  [Administrator]', 'true', '2023-01-26 13:11:22', '2023-01-26 13:03:40'),
('c442cc8b84059da478a21fcdb0fba51c', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : Admin Web  [Administrator]', 'true', '2023-01-26 13:11:22', '2023-01-26 13:05:37'),
('c23d63aa3d7fa14ebd460f156c736b0b', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : Admin Web  [Administrator]', 'true', '2023-01-26 13:11:22', '2023-01-26 13:05:39'),
('4f064c614cf0e0dbb760b49cf4d614b7', '', '', '', '', '', 'MENU : FUTSAL', 'true', '2023-01-26 13:11:22', '2023-01-26 13:05:44'),
('7a97ed83c557280034385e2998aafd92', '', '', '', '', '', 'MENU : LOGIN ADMIN', 'true', '2023-01-26 13:11:22', '2023-01-26 13:06:15'),
('3485fcc00f86bd1d757cc87439f446d6', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : Admin Web  [Administrator]', 'true', '2023-01-26 13:11:22', '2023-01-26 13:06:18'),
('7019c34419733c1c5b1e5be611674f2a', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : Admin Web  [Administrator]', 'true', '2023-01-26 13:11:22', '2023-01-26 13:07:10'),
('f316a89104b82a927885eaaf34b48c3d', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : Admin Web  [Administrator]', 'true', '2023-01-26 13:11:22', '2023-01-26 13:07:26'),
('61f6fc791061a752dbdb406253b9a3aa', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : Admin Web  [Administrator]', 'true', '2023-01-26 13:11:22', '2023-01-26 13:09:07'),
('df7cc543b9d212916b2e5d74864a772a', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : Admin Web  [Administrator]', 'true', '2023-01-26 13:11:22', '2023-01-26 13:09:38'),
('f7935f243d32c84269c85104481e04f0', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : Admin Web  [Administrator]', 'true', '2023-01-26 13:11:22', '2023-01-26 13:09:58'),
('ffd8de10e0bab509e3c25285d4da0fef', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [TUTUP BUKU]. Perolehan', 'true', '2023-01-26 13:11:22', '2023-01-26 13:10:58'),
('c728942f2a315692b8d6acda324b2a78', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [SETTING] Akses Petugas', 'true', '2023-01-26 13:11:22', '2023-01-26 13:11:03'),
('ff38f01393d6ee00794a224eaa57fea9', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [SETTING] Akses Petugas', 'true', '2023-01-26 13:11:22', '2023-01-26 13:11:07'),
('547c088cb86d78ae153ea9343e745ef7', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [SETTING] Akses Petugas', 'true', '2023-01-26 13:11:22', '2023-01-26 13:11:16'),
('f54e7d3fc4c094fe2378cbf51ff71a50', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : Admin Web  [Administrator]', 'true', '2023-01-26 13:11:22', '2023-01-26 13:11:20'),
('121ff53ba1a66c730f91accc7cc3ba74', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [HISTORY]. History Entri', 'false', NULL, '2023-01-26 13:11:22'),
('2cf5d3a28e88aa05b4d22b2a00b7de04', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [HISTORY]. History Login', 'false', NULL, '2023-01-26 13:11:25'),
('0459a6314d5f096a482af8b87f52e4d2', '', '', '', '', '', 'MENU : FUTSAL', 'false', NULL, '2023-01-26 13:11:28'),
('dce1afc95ba80aef695b8cab85185e0d', '', '', '', '', '', 'MENU : LOGIN', 'false', NULL, '2023-01-26 13:59:26'),
('bd2479ebdfe1dc4b0c33c969721a5976', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : PETUGAS  [PETUGAS]', 'true', '2023-01-26 20:25:36', '2023-01-26 14:00:00'),
('6e35bf5a10fe78efd9c2d9278e300ff8', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : PETUGAS  [PETUGAS]', 'true', '2023-01-26 20:25:36', '2023-01-26 14:00:07'),
('6738ee3774957239f597b6a580e182c4', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : [TUTUP BUKU]. Perolehan', 'true', '2023-01-26 20:25:36', '2023-01-26 14:00:16'),
('48e0905901544b64131a412935bc3393', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : [HISTORY]. History Entri', 'true', '2023-01-26 20:25:36', '2023-01-26 14:00:28'),
('1b6d5f28ec8864ad2c464331decd89bb', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : PETUGAS  [PETUGAS]', 'true', '2023-01-26 20:25:36', '2023-01-26 14:00:33'),
('46eb0bd08a6365d792c69d1245341475', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : PETUGAS  [11]', 'true', '2023-01-26 20:25:36', '2023-01-26 14:01:36'),
('a3e72d9defd7916a196474b317a003af', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : PETUGAS  [11]', 'true', '2023-01-26 20:25:36', '2023-01-26 14:01:37'),
('846ac8e7808e18ea4d11ad303729bbee', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : PETUGAS  [Silvi]', 'true', '2023-01-26 20:25:36', '2023-01-26 14:02:13'),
('c25c941c4cf3bf81787cd5004d90e8a4', '', '', '', '', '', 'MENU : FUTSAL', 'false', NULL, '2023-01-26 14:02:16'),
('66538774485fa2099604283bf509b2fc', '', '', '', '', '', 'MENU : FUTSAL', 'false', NULL, '2023-01-26 14:05:30'),
('3b39ff20226ab6f02276a5cbb69c0c68', '', '', '', '', '', 'MENU : LOGIN', 'false', NULL, '2023-01-26 14:05:39'),
('0307f7954381d506b76fac66716a196e', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : PETUGAS  [Silvi]', 'true', '2023-01-26 20:25:36', '2023-01-26 14:06:06'),
('2469276b3d2873b9da73a3e088be9853', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : [TUTUP BUKU]. Perolehan', 'true', '2023-01-26 20:25:36', '2023-01-26 14:09:48'),
('c1b8d03613721b07fe3dbd965b93eae5', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : [TUTUP BUKU]. Per Petugas', 'true', '2023-01-26 20:25:36', '2023-01-26 14:10:00'),
('d9cb4d9daa6ea21f10829233f34e084d', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : [TUTUP BUKU]. Per Petugas', 'true', '2023-01-26 20:25:36', '2023-01-26 14:10:05'),
('e75e45eabeb16b0ee7d75bac2f4d6238', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : [TUTUP BUKU]. Per Petugas', 'true', '2023-01-26 20:25:36', '2023-01-26 14:10:10'),
('5e56025f77b21828ab65d042597e10e2', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : [TUTUP BUKU]. Per Petugas', 'true', '2023-01-26 20:25:36', '2023-01-26 14:10:38'),
('53e9fef91cbea01109a5a5739ae0f050', '', '', '', '', '', 'MENU : FUTSAL', 'false', NULL, '2023-01-26 14:28:20'),
('733679bd95f07deffbdccc521ad19a8b', '', '', '', '', '', 'MENU : LOGIN', 'false', NULL, '2023-01-26 14:28:36'),
('2b1348deeacd2dbb2d88a80b5782c196', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : Admin Web  [Administrator]', 'false', NULL, '2023-01-26 14:28:49'),
('8948a401fa6db84182fdf84f16747ff3', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [TUTUP BUKU]. Perolehan', 'false', NULL, '2023-01-26 14:28:55'),
('0cb811962a23d4739947778415660708', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [TUTUP BUKU]. Perolehan', 'false', NULL, '2023-01-26 14:29:02'),
('88834ebdb66a62d12d957056741af268', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [TUTUP BUKU]. Tutup Buku Final', 'false', NULL, '2023-01-26 14:29:26'),
('3de0069f7068ce2332801376ef740fd9', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : TUTUP BUKU CETAK THERMAL : PENGELUARAN', 'false', NULL, '2023-01-26 14:29:27'),
('17fb3a27770bd719ef0361cdf106e5ed', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : TUTUP BUKU CETAK THERMAL', 'false', NULL, '2023-01-26 14:29:27'),
('eb23cdf61b789dd2ef7fa9b71bb29851', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : TUTUP BUKU CETAK THERMAL : DP', 'false', NULL, '2023-01-26 14:29:27'),
('0fb5c54a12d23fa4664ceb854784c1f0', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [TUTUP BUKU]. Entri Pengeluaran', 'false', NULL, '2023-01-26 14:29:30'),
('4f60537bd688b63e4cdb31a1e7b34c11', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [TUTUP BUKU]. Tutup Buku Final', 'false', NULL, '2023-01-26 14:29:38'),
('d30c0d995f98073f6c269dd353c17f17', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : TUTUP BUKU CETAK THERMAL : PENGELUARAN', 'false', NULL, '2023-01-26 14:29:39'),
('ed8990d75e3ca14270358a11976fc328', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : TUTUP BUKU CETAK THERMAL', 'false', NULL, '2023-01-26 14:29:39'),
('c4d19466fea97eab6ab279936847359c', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : TUTUP BUKU CETAK THERMAL : DP', 'false', NULL, '2023-01-26 14:29:39'),
('93def09cac62f6d0a2c1f40544ad0250', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [TUTUP BUKU]. Tutup Buku Final', 'false', NULL, '2023-01-26 14:29:44'),
('64a0138518a7c0910d52ffa8dbd21d7e', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : TUTUP BUKU CETAK THERMAL', 'false', NULL, '2023-01-26 14:29:44'),
('c06f828f1ce6f810662b5c63d646f888', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : TUTUP BUKU CETAK THERMAL : PENGELUARAN', 'false', NULL, '2023-01-26 14:29:44'),
('a4d7b4e94426cabf97147c192ca2ea9d', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : TUTUP BUKU CETAK THERMAL : DP', 'false', NULL, '2023-01-26 14:29:44'),
('b0076c307338e8ea9b3363ebda08246a', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [TUTUP BUKU]. Per Petugas', 'false', NULL, '2023-01-26 14:29:59'),
('f63cf765b9fd6b68519bb18e04645e63', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [LAP.KASIR]. Hari Ini', 'false', NULL, '2023-01-26 14:30:04'),
('c7ddebf5d55fd4c5908fc9497c6c3c9b', '', '', '', '', '', 'MENU : LOGIN', 'false', NULL, '2023-01-26 20:24:50'),
('b3bb4c04d85c0d81b9ae11cdeae1ac41', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : PETUGAS  [Silvi]', 'true', '2023-01-26 20:25:36', '2023-01-26 20:25:03'),
('a5695fb1b4e2062e42c117c516bd1b68', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : [HISTORY]. History Login', 'true', '2023-01-26 20:25:36', '2023-01-26 20:25:23'),
('a1795330f8f556b9b8f95368edd39c99', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : [HISTORY]. History Login', 'true', '2023-01-26 20:25:36', '2023-01-26 20:25:28'),
('51615604ac9e41f77a83de737f4c3370', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : [HISTORY]. History Entri', 'false', NULL, '2023-01-26 20:25:36'),
('663f8b51a88a4ba19ee330450b50dee1', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : [KASIR]. Jadwal Booking', 'false', NULL, '2023-01-26 20:26:12'),
('3fb3a3cb6f0e94e340136025fb0e747d', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : [KASIR] Bayar Booking', 'false', NULL, '2023-01-26 20:26:20'),
('33a859b3cc7c5854ba3d444f8586559d', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : [KASIR] Bayar Booking', 'false', NULL, '2023-01-26 20:26:28'),
('77edad9c6675d436e56c6e69f5edcd47', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : [KASIR] Bayar Snack', 'false', NULL, '2023-01-26 20:26:30'),
('3b8fe8684744c2d0a6cec42ff762b5e0', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : [KASIR] Bayar Booking', 'false', NULL, '2023-01-26 20:26:34'),
('485cd931cdc2aff6036f54cf01b4625a', '', '', '', '', '', 'MENU : LOGIN', 'false', NULL, '2023-01-26 21:22:04'),
('0dff4d72eca4cf54330c73804477f9c4', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : PETUGAS  [Silvi]', 'false', NULL, '2023-01-26 21:22:10'),
('9b789c88aa2e05b60c50d16e4fe3f1aa', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : [KASIR] Bayar Snack', 'false', NULL, '2023-01-26 21:22:18'),
('ee324d6585f65c3fa322e5e9cea408e4', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : [KASIR] Bayar Snack', 'false', NULL, '2023-01-26 21:22:19'),
('9fbf787c2dc69007506fa90e6c3027ec', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : [KASIR] Bayar Snack', 'false', NULL, '2023-01-26 21:22:23'),
('5f57ad0ff62dc1dbdddeb167bd1f7154', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : [KASIR] Bayar Snack', 'false', NULL, '2023-01-26 21:22:27'),
('dcfca372f71f1d4f0d45224a7ab83686', '', '', '', '', '', 'MENU : Nota', 'false', NULL, '2023-01-26 21:22:27'),
('8c34514046566a9b451ea933ce02fe5d', '', '', '', '', '', 'MENU : LOGIN', 'false', NULL, '2023-01-27 22:24:43'),
('8f20a1c53f1d8408e1f1f560186c17f9', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : PETUGAS  [Silvi]', 'false', NULL, '2023-01-27 22:24:54'),
('4247c88dad6e159b7a07b95e4cf5dfb1', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : [KASIR] Bayar Booking', 'false', NULL, '2023-01-27 22:24:58'),
('0760e8bb6643e004c447da2f36a76f4d', '', '', '', '', '', 'MENU : LOGIN', 'false', NULL, '2023-01-27 22:25:22'),
('6975f417023cacc6b99b752cb4171434', '', '', '', '', '', 'MENU : LOGIN', 'false', NULL, '2023-01-28 08:49:32'),
('c39570eb0acf1a714ebcc69735547c2e', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : Admin Web  [Administrator]', 'false', NULL, '2023-01-28 08:50:22'),
('90aa3b588d821810669e4cc9969e1d24', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : Admin Web  [Administrator]', 'false', NULL, '2023-01-28 08:50:22'),
('f68f88c97c3dfa7875a276e7945aba8c', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : Admin Web  [Administrator]', 'false', NULL, '2023-01-28 08:50:22'),
('4013c1d838a316c2d3c75750c7893318', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [TUTUP BUKU]. Tutup Buku Final', 'false', NULL, '2023-01-28 08:50:29'),
('a4394cac2a8f84f04fac215c4c18adc3', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : TUTUP BUKU CETAK THERMAL', 'false', NULL, '2023-01-28 08:50:30'),
('835316bdaf440ded38954205864a36f1', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : TUTUP BUKU CETAK THERMAL : PENGELUARAN', 'false', NULL, '2023-01-28 08:50:30'),
('0f389022bb40c30bf8be33a7a69ed510', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : TUTUP BUKU CETAK THERMAL : DP', 'false', NULL, '2023-01-28 08:50:30'),
('bbb95d4ebf8bad081623f5a234a0fb12', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [KASIR] Bayar Snack', 'false', NULL, '2023-01-28 08:50:36'),
('78c721c048f6e4eb71e368d80d2ab0f7', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [TUTUP BUKU]. Tutup Buku Final', 'false', NULL, '2023-01-28 08:50:37'),
('273e39b3c989edb9cffe0e9d89b826fb', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : TUTUP BUKU CETAK THERMAL : PENGELUARAN', 'false', NULL, '2023-01-28 08:50:38'),
('f9ab0e93b92dca64d677fd3adcb07b95', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : TUTUP BUKU CETAK THERMAL', 'false', NULL, '2023-01-28 08:50:38'),
('1a3b24d4803f5f3bd3766618d70760e6', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : TUTUP BUKU CETAK THERMAL : DP', 'false', NULL, '2023-01-28 08:50:38'),
('5006abeb7fac92a7beee9c823bf0083e', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : Admin Web  [Administrator]', 'false', NULL, '2023-01-28 08:50:38'),
('60eec03ad7f7eae5ff46d3d167597633', '', '', '', '', '', 'MENU : FUTSAL', 'false', NULL, '2023-01-31 19:00:31'),
('127427380462a9dfd16d5a382360c2a9', '', '', '', '', '', 'MENU : LOGIN', 'false', NULL, '2023-02-01 16:06:32'),
('71735fac58ab1456feac19aa788ccaa4', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : PETUGAS  [Silvi]', 'false', NULL, '2023-02-01 16:06:51'),
('f525443984903cb1432aed6e6170c613', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : PETUGAS  [Silvi]', 'false', NULL, '2023-02-01 16:06:51'),
('107742f979d8da9e32ff4b52eea06cae', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : [KASIR] Bayar Snack', 'false', NULL, '2023-02-01 16:06:56'),
('204cbd1fe2f620215d7c493ba50e54d3', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : [KASIR] Bayar Snack', 'false', NULL, '2023-02-01 16:06:58'),
('4dc5f900f54a71092ee08c6e73f2b05d', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : [KASIR] Bayar Snack', 'false', NULL, '2023-02-01 16:07:07'),
('ab095504304e71a4f88ed5789038392f', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : [KASIR] Bayar Snack', 'false', NULL, '2023-02-01 16:07:17'),
('1bcc11b2f40c6563f07fd006b181957a', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : [KASIR] Bayar Snack', 'false', NULL, '2023-02-01 16:07:27'),
('5cabf481c538d6d87c93ca4c522904aa', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : [KASIR] Bayar Snack', 'false', NULL, '2023-02-01 16:07:37'),
('fd1797884bd66465d27a50f4d0398d64', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : [KASIR] Bayar Snack', 'false', NULL, '2023-02-01 16:07:46'),
('05f2a3d5781d1cb2862d2b3d9a037289', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : [KASIR] Bayar Snack', 'false', NULL, '2023-02-01 16:08:12'),
('1e367e07b96c301910ada87c94a921b9', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : [KASIR] Bayar Snack', 'false', NULL, '2023-02-01 16:08:22'),
('4ad96c045149b44035f153cedba36c8d', '', '', '', '', '', 'MENU : Nota', 'false', NULL, '2023-02-01 16:08:22'),
('c2a9105ad8b3c92b03d9e7a2dbb4bf7a', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : [KASIR] Bayar Snack', 'false', NULL, '2023-02-01 16:08:29'),
('06dd7d3e61e9fa83337ef77185ccd904', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : [KASIR] Bayar Snack', 'false', NULL, '2023-02-01 16:08:33'),
('186a76e1d722b2fce67dfa152a0c88df', '', '', '', '', '', 'MENU : LOGIN', 'false', NULL, '2023-02-02 09:56:43'),
('6f993e686df9b8f1f96129d969aa30cc', '', '', '', '', '', 'MENU : LOGIN', 'false', NULL, '2023-02-02 16:14:07'),
('05200082f5a5c3dc2210dc57a8b0ecbb', '', '', '', '', '', 'MENU : LOGIN', 'false', NULL, '2023-02-03 09:59:03'),
('fb223dc135613e51e2cc7c0465450cb5', '', '', '', '', '', 'MENU : LOGIN', 'false', NULL, '2023-02-03 15:32:11'),
('54fea2961c4476c5cba8758fcae02265', '', '', '', '', '', 'MENU : LOGIN', 'false', NULL, '2023-02-04 09:12:37'),
('e019abdee37d00eb9d2e430f91b7d944', '', '', '', '', '', 'MENU : LOGIN', 'false', NULL, '2023-02-04 09:13:30'),
('3b82a2f66cff8d23370ebe1bf3463171', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : Admin Web  [Administrator]', 'false', NULL, '2023-02-04 09:13:44'),
('1dba97ed885d40a47248a0258bb77147', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [MASTER] Kategori Item', 'false', NULL, '2023-02-04 09:13:52'),
('c2914df2dd34ec1f2e7760bed7927fe5', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [MASTER]. Data Item Produk', 'false', NULL, '2023-02-04 09:13:56'),
('0eda3351d0e28df21bdfa7ceba7e2d60', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [MASTER]. Data Item Produk', 'false', NULL, '2023-02-04 09:14:00'),
('1a55e663a72675539f818ed2fe762837', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [MASTER]. Data Item Produk', 'false', NULL, '2023-02-04 09:14:26'),
('7ca88ce3a9214157d2550ce44fa50fb3', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [MASTER]. Data Item Produk', 'false', NULL, '2023-02-04 09:14:27'),
('4a25b02eb0f1014a2585e75a6be7ed9b', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [KASIR] Bayar Snack', 'false', NULL, '2023-02-04 09:14:32'),
('2c979dd40c40754851bc5b7a8314c854', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [KASIR] Bayar Snack', 'false', NULL, '2023-02-04 09:14:34'),
('143112e3a7da2ce6770e5f5183ab6e33', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [KASIR] Bayar Snack', 'false', NULL, '2023-02-04 09:14:39'),
('16a3ea7828388fe1465c31dfaf42f370', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [MASTER]. Data Item Produk', 'false', NULL, '2023-02-04 09:14:40'),
('4e9d3711614f15b9013e0edc74607362', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [KASIR] Bayar Booking', 'false', NULL, '2023-02-04 09:14:43'),
('1adb64c9e8cfee7a13e45f5c1cba6b0b', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [KASIR] Bayar Snack', 'false', NULL, '2023-02-04 09:14:47'),
('a47184a370bd1fe19a09db1762dba115', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [KASIR] Bayar Snack', 'false', NULL, '2023-02-04 09:14:48'),
('f309e38f702805083166b8ed4c69ec71', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [KASIR] Bayar Snack', 'false', NULL, '2023-02-04 09:14:54'),
('8ec09c2d2227ab9b1aa2eaadbbdcb546', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [KASIR] Bayar Snack', 'false', NULL, '2023-02-04 09:15:03'),
('3d20247bd87370e3b6ebcd8e9fed9be4', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', 'MENU : [KASIR] Bayar Snack', 'false', NULL, '2023-02-04 09:15:10'),
('20bd2a053014bc52bce0b97f0d7868fa', '', '', '', '', '', 'MENU : Nota', 'false', NULL, '2023-02-04 09:15:10'),
('81e8bb4e6e5ff17aef8fe62a83cf01a7', '', '', '', '', '', 'MENU : LOGIN', 'false', NULL, '2023-02-05 09:07:12'),
('4a2f595b29558e4ccba6558c029d1ae3', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : PETUGAS  [Silvi]', 'false', NULL, '2023-02-05 09:07:22'),
('f0834cf6be379446315065fe37d6d16c', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : [KASIR] Bayar Snack', 'false', NULL, '2023-02-05 09:07:30'),
('e68f82540be8c171312f25b8ab4667b4', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : [KASIR] Bayar Snack', 'false', NULL, '2023-02-05 09:07:32'),
('2a47f53bcb78db90b55bf2c58f2df0f3', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : [KASIR] Bayar Snack', 'false', NULL, '2023-02-05 09:07:57'),
('ccb1bcf607a457c525e8e4c32a8e7b7d', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : [KASIR] Bayar Snack', 'false', NULL, '2023-02-05 09:08:04'),
('c5a58f056922880ac2a50401c4d3207d', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : [KASIR] Bayar Snack', 'false', NULL, '2023-02-05 09:08:09'),
('51e652e432c72d1e011aa7d9c716c9a7', '', '', '', '', '', 'MENU : Nota', 'false', NULL, '2023-02-05 09:08:09'),
('74337cf06825170c2b0eec87ae6cee27', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : [KASIR] Bayar Snack', 'false', NULL, '2023-02-05 09:08:14'),
('0f4012a231ff6e14075d264b2aee6dab', '', '', '', '', '', 'MENU : LOGIN', 'false', NULL, '2023-02-05 10:19:36'),
('7172c9fbb11f5d06a96d216f27b2d43b', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : PETUGAS  [Silvi]', 'false', NULL, '2023-02-05 10:19:44'),
('d6867650ba3d51619448d14ff187d2e0', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : [KASIR] Bayar Snack', 'false', NULL, '2023-02-05 10:19:47'),
('5f7b1cd2d50d314dbb901b293aab86ce', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : [KASIR] Bayar Snack', 'false', NULL, '2023-02-05 10:19:50'),
('1eec295bfb0e531be7a6f362bd65ca5c', '', '', '', '', '', 'MENU : LOGIN', 'false', NULL, '2023-02-05 11:25:31'),
('292fb953f6f7dda1bd2cd2d216571f6d', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : PETUGAS  [Silvi]', 'false', NULL, '2023-02-05 11:25:43'),
('5a38e119c1a8f69c181e1fbbf7ae7a80', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : [KASIR] Bayar Snack', 'false', NULL, '2023-02-05 11:25:47'),
('f7814a607dc7157374998d76065101ef', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : [KASIR] Bayar Snack', 'false', NULL, '2023-02-05 11:25:48'),
('e7b8963903cdbae76b5f4d955f22aa9b', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : [KASIR] Bayar Snack', 'false', NULL, '2023-02-05 11:25:52'),
('bc500af07399863d6db32edb9adb05b3', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : [KASIR] Bayar Snack', 'false', NULL, '2023-02-05 11:25:58'),
('2b3b18ee9c5b4d5bd9205f1a1f21552f', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : [KASIR] Bayar Snack', 'false', NULL, '2023-02-05 11:26:06'),
('eb9b591ee99118400f46b7dbbc22cab7', '', '', '', '', '', 'MENU : Nota', 'false', NULL, '2023-02-05 11:26:06'),
('32f5196547b2bbb33b9ef78eaf6efb63', '', '', '', '', '', 'MENU : LOGIN', 'false', NULL, '2023-02-05 12:39:08'),
('9f5cae70fb4efce6d022345019aacd80', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : PETUGAS  [Silvi]', 'false', NULL, '2023-02-05 12:39:14'),
('b5a7bb6f248c469f74850be333c092d5', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : PETUGAS  [Silvi]', 'false', NULL, '2023-02-05 12:39:18'),
('57dca38f6bf253a37da6f18b5aaf1cda', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : [KASIR] Bayar Snack', 'false', NULL, '2023-02-05 12:39:21'),
('49b6801c8b8f3c1e017320490cf8dfb6', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : [KASIR] Bayar Snack', 'false', NULL, '2023-02-05 12:39:22'),
('5d3d7f9c671d79060074e66840ea24ae', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : [KASIR] Bayar Snack', 'false', NULL, '2023-02-05 12:39:27'),
('3ade7cef79197d4ecc1637027fe2540d', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : [KASIR] Bayar Snack', 'false', NULL, '2023-02-05 12:39:34'),
('a4b4e8f6b0e0d42f70699aed7652b73d', '', '', '', '', '', 'MENU : Nota', 'false', NULL, '2023-02-05 12:39:34'),
('248919a7a4e8f71a10a2d8b1d180d4ef', '', '', '', '', '', 'MENU : LOGIN', 'false', NULL, '2023-02-05 13:15:16'),
('c9e07a08f0ff35a5867fe8233ba3b057', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : PETUGAS  [Silvi]', 'false', NULL, '2023-02-05 13:15:22'),
('eff0d0076b9013d18fcc80b43aeafc5d', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : [HISTORY]. History Login', 'false', NULL, '2023-02-05 13:15:27'),
('557024d3dc3cd94226ad9941f2a310f4', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : PETUGAS  [Silvi]', 'false', NULL, '2023-02-05 13:15:41'),
('a3628db60ade7b4d7203836149986a47', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', 'MENU : [HISTORY]. History Login', 'false', NULL, '2023-02-05 13:15:45'),
('d66b1fa560c8661dd1c9ac3f4f4fccd1', '', '', '', '', '', 'MENU : LOGIN', 'false', NULL, '2023-02-05 13:15:47'),
('56a658235ad677a1f66e9aea3f247658', '', '', '', '', '', 'MENU : LOGIN', 'false', NULL, '2023-02-05 14:58:18');

-- --------------------------------------------------------

--
-- Table structure for table `user_log_login`
--

CREATE TABLE `user_log_login` (
  `kd` varchar(50) NOT NULL,
  `user_kd` varchar(50) DEFAULT NULL,
  `user_kode` varchar(100) DEFAULT NULL,
  `user_nama` varchar(100) DEFAULT NULL,
  `user_posisi` varchar(100) DEFAULT NULL,
  `user_jabatan` varchar(100) DEFAULT NULL,
  `ipnya` varchar(100) DEFAULT NULL,
  `dibaca` enum('true','false') NOT NULL DEFAULT 'false',
  `dibaca_postdate` datetime DEFAULT NULL,
  `postdate` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_log_login`
--

INSERT INTO `user_log_login` (`kd`, `user_kd`, `user_kode`, `user_nama`, `user_posisi`, `user_jabatan`, `ipnya`, `dibaca`, `dibaca_postdate`, `postdate`) VALUES
('29b5410436026a09d02041c0cb3e0d29', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', '127.0.0.1', 'true', '2023-01-26 13:11:25', '2023-01-21 10:45:56'),
('68b9e4f888583a59f5f81e2b9f257388', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', '103.105.28.168', 'true', '2023-01-26 13:11:25', '2023-01-26 11:40:05'),
('4bc0432fb0412add836fa1b581fc8e39', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', '103.105.28.168', 'true', '2023-01-26 13:11:25', '2023-01-26 12:05:12'),
('439e7525c30387202e3f2132d2bb204f', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', '103.105.27.87', 'true', '2023-01-26 13:11:25', '2023-01-26 13:02:05'),
('5824eb33ced2ba5d208766574d3ba347', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', '103.105.27.87', 'true', '2023-01-26 13:11:25', '2023-01-26 13:06:18'),
('6764c2d05d03f35324c29b8d8f3f17f8', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', '103.105.27.87', 'true', '2023-02-05 13:15:45', '2023-01-26 13:59:33'),
('3fe761c896cb41c7183272317a002841', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', '103.105.27.87', 'true', '2023-02-05 13:15:45', '2023-01-26 14:06:06'),
('e7e810be83176d20d89c36759830fa08', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', '103.105.27.87', 'false', NULL, '2023-01-26 14:28:49'),
('fe98fea030c0b3ac619ad04189cee89b', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', '123.253.232.141', 'true', '2023-02-05 13:15:45', '2023-01-26 20:25:02'),
('23ed18fed727a668a4b0f5170aa0cf87', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', '123.253.232.141', 'true', '2023-02-05 13:15:45', '2023-01-26 21:22:10'),
('d0dde16e46c4fe25b8f563cc6b691eec', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', '202.80.218.32', 'true', '2023-02-05 13:15:45', '2023-01-27 22:24:54'),
('9c32b326e919b86e97b3f82af9b1bc39', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', '114.10.22.59', 'false', NULL, '2023-01-28 08:50:22'),
('dc8736956bb617212e0da9196b79f4a9', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', '114.10.22.59', 'false', NULL, '2023-01-28 08:50:22'),
('2eeba0993216c12d93583c229f980b16', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', '114.10.22.59', 'false', NULL, '2023-01-28 08:50:22'),
('5b1685c72e1fa8be493267d78a263268', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', '114.10.22.59', 'false', NULL, '2023-01-28 08:50:22'),
('46db79d921e348e01df7a15a702e4ec8', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', '202.80.218.32', 'true', '2023-02-05 13:15:45', '2023-02-01 16:06:50'),
('066d466a5cf0ec0fbd4fe36bc228bde9', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', '202.80.218.32', 'true', '2023-02-05 13:15:45', '2023-02-01 16:06:51'),
('bd19b0786cca9f3dd630292cef64e9ee', '21232f297a57a5a743894a0e4a801fc3', 'ADMIN', 'ADMIN', 'ADMIN', 'ADMIN', '202.80.217.32', 'false', NULL, '2023-02-04 09:13:44'),
('c21ab3f05b2d5195bcea5a5533e32fd9', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', '202.80.217.32', 'true', '2023-02-05 13:15:45', '2023-02-05 09:07:22'),
('ca21739c3c27e25aac053244f7afaf60', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', '202.80.217.32', 'true', '2023-02-05 13:15:45', '2023-02-05 10:19:44'),
('5cf2bf45e5ec9b87e6a19865bbc7db10', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', '202.80.217.32', 'true', '2023-02-05 13:15:45', '2023-02-05 11:25:43'),
('76c53886675daee7568065e192784c66', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', '202.80.217.32', 'true', '2023-02-05 13:15:45', '2023-02-05 12:39:14'),
('0b4b174af07c04ec2fd3c4d9a2451817', '6f62d73d9ac6f0b20c3eedc56edf4015', '11', 'Silvi', 'PETUGAS', 'PETUGAS', '202.80.217.32', 'true', '2023-02-05 13:15:45', '2023-02-05 13:15:22');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `adminx`
--
ALTER TABLE `adminx`
  ADD PRIMARY KEY (`kd`);

--
-- Indexes for table `item_laris`
--
ALTER TABLE `item_laris`
  ADD PRIMARY KEY (`kd`);

--
-- Indexes for table `m_brg`
--
ALTER TABLE `m_brg`
  ADD PRIMARY KEY (`kd`);

--
-- Indexes for table `m_brg_masuk`
--
ALTER TABLE `m_brg_masuk`
  ADD PRIMARY KEY (`kd`);

--
-- Indexes for table `m_kategori`
--
ALTER TABLE `m_kategori`
  ADD PRIMARY KEY (`kd`);

--
-- Indexes for table `m_pelanggan`
--
ALTER TABLE `m_pelanggan`
  ADD PRIMARY KEY (`kd`);

--
-- Indexes for table `m_satuan`
--
ALTER TABLE `m_satuan`
  ADD PRIMARY KEY (`kd`);

--
-- Indexes for table `m_tempat`
--
ALTER TABLE `m_tempat`
  ADD PRIMARY KEY (`kd`);

--
-- Indexes for table `nomerku`
--
ALTER TABLE `nomerku`
  ADD PRIMARY KEY (`nota_kd`),
  ADD UNIQUE KEY `noid` (`noid`);

--
-- Indexes for table `nomerku2210`
--
ALTER TABLE `nomerku2210`
  ADD PRIMARY KEY (`nota_kd`),
  ADD UNIQUE KEY `noid` (`noid`);

--
-- Indexes for table `nomerku2211`
--
ALTER TABLE `nomerku2211`
  ADD PRIMARY KEY (`nota_kd`),
  ADD UNIQUE KEY `noid` (`noid`);

--
-- Indexes for table `nomerku2212`
--
ALTER TABLE `nomerku2212`
  ADD PRIMARY KEY (`nota_kd`),
  ADD UNIQUE KEY `noid` (`noid`);

--
-- Indexes for table `nomerku2301`
--
ALTER TABLE `nomerku2301`
  ADD PRIMARY KEY (`nota_kd`),
  ADD UNIQUE KEY `noid` (`noid`);

--
-- Indexes for table `nota`
--
ALTER TABLE `nota`
  ADD PRIMARY KEY (`kd`);

--
-- Indexes for table `nota_detail`
--
ALTER TABLE `nota_detail`
  ADD PRIMARY KEY (`kd`);

--
-- Indexes for table `orderan`
--
ALTER TABLE `orderan`
  ADD PRIMARY KEY (`kd`);

--
-- Indexes for table `orderan_jam`
--
ALTER TABLE `orderan_jam`
  ADD PRIMARY KEY (`kd`);

--
-- Indexes for table `saldo`
--
ALTER TABLE `saldo`
  ADD PRIMARY KEY (`kd`);

--
-- Indexes for table `tempat_tgl_libur`
--
ALTER TABLE `tempat_tgl_libur`
  ADD PRIMARY KEY (`kd`);

--
-- Indexes for table `uang_keluar`
--
ALTER TABLE `uang_keluar`
  ADD PRIMARY KEY (`kd`);

--
-- Indexes for table `user_log_entri`
--
ALTER TABLE `user_log_entri`
  ADD PRIMARY KEY (`kd`);

--
-- Indexes for table `user_log_login`
--
ALTER TABLE `user_log_login`
  ADD PRIMARY KEY (`kd`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `nomerku`
--
ALTER TABLE `nomerku`
  MODIFY `noid` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4323;

--
-- AUTO_INCREMENT for table `nomerku2210`
--
ALTER TABLE `nomerku2210`
  MODIFY `noid` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `nomerku2211`
--
ALTER TABLE `nomerku2211`
  MODIFY `noid` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT for table `nomerku2212`
--
ALTER TABLE `nomerku2212`
  MODIFY `noid` int(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `nomerku2301`
--
ALTER TABLE `nomerku2301`
  MODIFY `noid` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
